# Test that Ice works (from orca robotics)
# Defines the following variables:
# Ice_WORKS : 1 if test passed, 0 otherwise.

include(${CMAKE_ROOT}/Modules/CheckCXXSourceCompiles.cmake)

if (NOT ICE_FOUND)
    message(STATUS "Testing Ice - Failed. The Ice library was not detected")
    set(Ice_WORKS 0)
endif()

if (NOT ARMARX_OS_WIN)
    set(CMAKE_REQUIRED_INCLUDES "${Ice_INCLUDE_DIRS}")
    set(CMAKE_REQUIRED_LIBRARIES "${Ice_Ice_LIBRARY};${Ice_IceUtil_LIBRARY}")
    check_cxx_source_compiles("#include <Ice/Ice.h>\nint main() { return 0; }" Ice_WORKS)
    if (Ice_WORKS)
        set(Ice_WORKS 1)
    else()
        set(Ice_WORKS 0)
    endif()
else()
    # Windows: there is no easy way to pass separate compile and link options to the macro,
    # so assume we are told the truth
    set(Ice_WORKS 1)
endif()


# This macro needs to be applied after adding a target to link the target
# against the required libraries.
macro(target_link_ice TARGET)
    if (NOT ARMARX_OS_WIN)
        target_link_libraries(${TARGET}
            "${Ice_Ice_LIBRARY}"
            "${Ice_IceUtil_LIBRARY}"
            "${Ice_IceStorm_LIBRARY}"
            "${Ice_IceBox_LIBRARY}"
            "${Ice_IceGrid_LIBRARY}"
            "${Ice_Glacier2_LIBRARY}")
    else()
        # windows... have to link to different libs depending on build type
        target_link_libraries(${TARGET}
            optimized "${Ice_Ice_LIBRARY_RELEASE}"      debug "${Ice_Ice_LIBRARY_DEBUG}"
            optimized "${Ice_IceUtil_LIBRARY_RELEASE}"  debug "${Ice_IceUtil_LIBRARY_DEBUG}"
            optimized "${Ice_IceStorm_LIBRARY_RELEASE}" debug "${Ice_IceStorm_LIBRARY_DEBUG}"
            optimized "${Ice_IceBox_LIBRARY_RELEASE}"   debug "${Ice_IceBox_LIBRARY_DEBUG}"
            optimized "${Ice_IceGrid_LIBRARY_RELEASE}"  debug "${Ice_IceGrid_LIBRARY_DEBUG}"
            optimized "${Ice_Glacier2_LIBRARY_RELEASE}" debug "${Ice_Glacier2_LIBRARY_DEBUG}")
    endif()
endmacro()

include(CMakeParseArguments)

function(FIND_SLICE)
    set(options)
    set(oneValueArgs OUTVAR)
    set(multiValueArgs INPUTS)

    cmake_parse_arguments (FIND_SLICE "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN})

    # Also look in other source directories (MEF 2012-11-19)
    file(GLOB
        OTHER_SLICE_DIRS
        ${CMAKE_CURRENT_SOURCE_DIR}/../*/slice
        ${CMAKE_CURRENT_SOURCE_DIR}/../../*/slice
        ${CMAKE_CURRENT_SOURCE_DIR}/../../../*/slice
        ${CMAKE_CURRENT_SOURCE_DIR}/../*
        )

    # Look in the current source directory or the system-wide directories,
    # and optionally in any other directories as given
    set(_slice_paths
        ${Ice_Slice_DIR}
        ${CMAKE_CURRENT_SOURCE_DIR}/slice
        ${OTHER_SLICE_DIRS}
        ${CMAKE_INSTALL_PREFIX}/include/slice
        )
    foreach(file ${FIND_SLICE_INPUTS})
        set(${file}_PATH "${file}_PATH-NOTFOUND")
        FIND_file(${file}_PATH
            NAMES ${file}
            PATHS ${_slice_paths}
            )
        list(APPEND _paths ${${file}_PATH})
        mark_as_advanced(${file}_PATH)
        if("${file}_PATH" MATCHES "-NOTFOUND")
            MESSAGE (FATAL_ERROR "Required Slice file \"${file}\" not found!")
        endif()
    endforeach()
    set(${FIND_SLICE_OUTVAR} ${_paths} PARENT_SCOPE)
    set(${FIND_SLICE_OUTVAR} ${_paths})
endfunction(FIND_SLICE)

function(ICE_WRAP_CPP outfiles)
    # Find all of the slice files
    FIND_SLICE(OUTVAR SLICE_INPUTS INPUTS ${ARGN})

    # Figure out the include directories we need
    set(SLICE_INCLUDES -I${Ice_Slice_DIR})
    foreach(file${SLICE_INPUTS})
        # Store the directory name
        get_filename_component(_path ${FILE} PATH)
        set(SLICE_INCLUDES ${SLICE_INCLUDES} -I${_path})
    endforeach()
    list(REMOVE_DUPLICATES SLICE_INCLUDES)

    # Create the directory that we'll generate into
    set(GENERATED_SRC ${CMAKE_CURRENT_BINARY_DIR}/generated)
    file(MAKE_DIRECTORY ${GENERATED_SRC})

    # Generate the slice2cpp commands and dependencies
    foreach(infile ${SLICE_INPUTS})
        get_filename_component(name_we ${infile} NAME_WE)
        set(outfile1 ${GENERATED_SRC}/${name_we}.cpp)
        set(outfile2 ${GENERATED_SRC}/${name_we}.h)
        add_custom_command(
            OUTPUT ${outfile1} ${outfile2}
            COMMAND ${Ice_slice2cpp}
            ARGS ${SLICE_INCLUDES} --output-dir ${GENERATED_SRC} ${infile}
            MAIN_DEPENDENCY ${infile}
            )
        set(_outfiles ${_outfiles} ${outfile1} ${outfile2})
        include_directories(${GENERATED_SRC})
    endforeach(infile)
    set(${outfiles} ${_outfiles} PARENT_SCOPE)
endfunction()

function(ICE_WRAP_JAVA outfiles)
    # Find all of the slice files
    FIND_SLICE(OUTVAR SLICE_INPUTS INPUTS ${ARGN})

    # Figure out the include directories we need
    set(SLICE_INCLUDES -I${Ice_Slice_DIR})
    foreach(file${SLICE_INPUTS})
        # Store the directory name
        GET_FILENAME_COMPONENT(_path ${FILE} PATH)
        set(SLICE_INCLUDES ${SLICE_INCLUDES} -I${_path})
    endforeach(FILE)
    list(REMOVE_DUPLICATES SLICE_INCLUDES)

    # Do a dummy slice2java run to find out the set of generated files
    set(TMP_OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/slice2java.tmp)
    file(MAKE_DIRECTORY ${TMP_OUTPUT})
    execute_process(
        COMMAND ${Ice_slice2java} ${SLICE_INCLUDES} --output-dir ${TMP_OUTPUT} ${SLICE_INPUTS}
    )
    file(GLOB_RECURSE
        TMP_OUTPUTS
        RELATIVE ${TMP_OUTPUT}
        ${TMP_OUTPUT}/*.java
    )
    file(REMOVE_RECURSE ${TMP_OUTPUT})

    # Create the directory that we'll generate into
    set(GENERATED_SRC ${CMAKE_CURRENT_SOURCE_DIR}/generated)
    file(MAKE_DIRECTORY ${GENERATED_SRC})

    # Generate the slice2java command and dependencies
    foreach(file${TMP_OUTPUTS})
        list(APPEND _outfiles "${GENERATED_SRC}/${FILE}")
    endforeach(FILE)
    set(${outfiles} ${_outfiles} PARENT_SCOPE)
    add_custom_command(
        OUTPUT ${_outfiles}
        DEPENDS ${SLICE_INPUTS}
        COMMAND ${Ice_slice2java} ${SLICE_INCLUDES} --output-dir ${GENERATED_SRC} ${SLICE_INPUTS}
        COMMENT "Using slice2java to generate Java source files"
    )
endfunction()
