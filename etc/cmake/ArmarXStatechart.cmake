# Macros for ArmarX Statechart Libraries

macro(generate_statechart_headers HEADERS)

    foreach(CURRENT_HEADER_FILE ${HEADERS})
        string(REGEX MATCH "^(.+)\\.scgxml$" VOID ${CURRENT_HEADER_FILE})
        if (NOT "${CMAKE_MATCH_1}" STREQUAL "")
            set(GROUP_NAME_NO_SUFFIX ${CMAKE_MATCH_1})
            set(GROUP_FILE "${GROUP_NAME_NO_SUFFIX}.scgxml")
            FILE(STRINGS ${CURRENT_HEADER_FILE} GROUP_FILE_MATCHES REGEX "generateContext=\"true\"")
            if(GROUP_FILE_MATCHES)
                set(CONTEXT_GENERATION_ENABLED TRUE)
            endif()
        endif()
    endforeach()
    if(GROUP_FILE)
        MESSAGE(STATUS "Statechartgroup: ${GROUP_FILE} in ${CMAKE_CURRENT_BINARY_DIR}")
        if(CONTEXT_GENERATION_ENABLED)
            MESSAGE(STATUS "Context generation enabled")
            add_custom_command(OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/${GROUP_NAME_NO_SUFFIX}StatechartContext.generated.h.touch
                               COMMAND ${ArmarXCore_BINARY_DIR}/StatechartGroupGeneratorAppRun
                               ARGS    "context" "${CMAKE_CURRENT_SOURCE_DIR}/${GROUP_FILE}" "${CMAKE_BINARY_DIR}"
                               MAIN_DEPENDENCY "${CMAKE_CURRENT_SOURCE_DIR}/${GROUP_NAME_NO_SUFFIX}.scgxml"
                               COMMENT "Generating ${GROUP_NAME_NO_SUFFIX}StatechartContext.generated.h from ${CMAKE_CURRENT_SOURCE_DIR}/${GROUP_NAME_NO_SUFFIX}.scgxml")
            list(APPEND HEADERS "${CMAKE_CURRENT_BINARY_DIR}/${GROUP_NAME_NO_SUFFIX}StatechartContext.generated.h.touch")
        endif()
        SET(GENERATED_FILES "")
        SET(HEADERS_COPY "${HEADERS}")
        SET(ANY_XML_FOUND FALSE)
        foreach(CURRENT_HEADER_FILE ${HEADERS_COPY})
            string(REGEX MATCH "^(.+)\\.xml$" VOID ${CURRENT_HEADER_FILE})
            set(STATE_PATH_NO_SUFFIX ${CMAKE_MATCH_1})
            if (NOT "${STATE_PATH_NO_SUFFIX}" STREQUAL "")
                string(REGEX MATCH "([^\\/]+)\\.xml$" VOID ${CURRENT_HEADER_FILE})
                set(STATE_NAME ${CMAKE_MATCH_1})
                SET(ANY_XML_FOUND TRUE)

                if(CONTEXT_GENERATION_ENABLED)
                    set(STATE_DEPENDENCIES "${CMAKE_CURRENT_SOURCE_DIR}/${GROUP_FILE}" "${CMAKE_CURRENT_BINARY_DIR}/${GROUP_NAME_NO_SUFFIX}StatechartContext.generated.h.touch")
                else()
                    set(STATE_DEPENDENCIES "${CMAKE_CURRENT_SOURCE_DIR}/${GROUP_FILE}")
                endif()
                add_custom_command(OUTPUT "${CMAKE_CURRENT_BINARY_DIR}/${STATE_NAME}.generated.h.touch"
                                   COMMAND "${ArmarXCore_BINARY_DIR}/StatechartGroupGeneratorAppRun"
                                   ARGS    "${CMAKE_CURRENT_SOURCE_DIR}/${GROUP_FILE}" "${CMAKE_CURRENT_SOURCE_DIR}/${STATE_PATH_NO_SUFFIX}.xml" "${CMAKE_BINARY_DIR}"
                                   DEPENDS ${STATE_DEPENDENCIES}
                                   MAIN_DEPENDENCY "${CMAKE_CURRENT_SOURCE_DIR}/${STATE_PATH_NO_SUFFIX}.xml"
                                   COMMENT "Generating ${STATE_NAME}.generated.h from ${CMAKE_CURRENT_SOURCE_DIR}/${STATE_PATH_NO_SUFFIX}.xml")



                list(APPEND HEADERS "${CMAKE_CURRENT_BINARY_DIR}/${STATE_NAME}.generated.h.touch")
            endif()

        endforeach()
        if(ANY_XML_FOUND)
            include_directories("${CMAKE_CURRENT_BINARY_DIR}")
            include_directories("${CMAKE_CURRENT_SOURCE_DIR}")
        endif()
    endif()

endmacro()
