# Setup documentation target for the ArmarX framework

find_package(Doxygen)

set(DOXYGEN_TEMPLATE_DIR "${ArmarXCore_TEMPLATES_DIR}/doxygen")
set(DOXYGEN_CONFIG_FILE "${DOXYGEN_DOCUMENTATION_DIR}/${PROJECT_NAME}.Doxyfile")
set(DOXYGEN_PROPERTIES_DOC_FILE "${DOXYGEN_DOCUMENTATION_DIR}/PropertiesDocumentation.cmake")

macro(generateDocumentationFiles)
    message(STATUS "Documentation: Generating Doxygen files")

    # The ${DOXYGEN_DOCUMENTATION_INPUT_DIR_TEMP} stores the value of DOXYGEN_DOCUMENTATION_INPUT_DIR_TEMP without reference to mainpage.dox.
    # This is required since mainpage.dox must NOT be included in the DOXYGEN_DOCUMENTATION_INPUT_DIR
    # which gets written to the ProjectConfigBuildTreeSettings.cmake.
    # If it were included, the complete documentation index would be messed up and all subpages of all mainpage.dox
    # files would appear as toplevel pages.
    set(DOXYGEN_DOCUMENTATION_INPUT_DIR_TEMP "${DOXYGEN_DOCUMENTATION_INPUT_DIR}")
    set(DOXYGEN_DOCUMENTATION_INPUT_DIR "${DOXYGEN_DOCUMENTATION_INPUT_DIR} ${PROJECT_ETC_DIR}/doxygen/mainpage.dox")

    CONFIGURE_FILE(
       "${DOXYGEN_TEMPLATE_DIR}/Project.Doxyfile.in"
       "${DOXYGEN_CONFIG_FILE}"
        @ONLY)
    set(DOXYGEN_DOCUMENTATION_INPUT_DIR "${DOXYGEN_DOCUMENTATION_INPUT_DIR_TEMP}")

    CONFIGURE_FILE(
       "${DOXYGEN_TEMPLATE_DIR}/slicedocumentation.dox.in"
       "${DOXYGEN_DOCUMENTATION_DIR}/slicedocumentation.dox"
        @ONLY)

    set(DOXYGEN_TEMPLATE_FILES
        ${DOXYGEN_TEMPLATE_DIR}/Doxygen.css
        ${DOXYGEN_TEMPLATE_DIR}/Header.html
        ${DOXYGEN_TEMPLATE_DIR}/ArmarX-Logo-Ax-16x16.png
        ${DOXYGEN_TEMPLATE_DIR}/ArmarX-Logo-Ax-32x32.ico
        ${DOXYGEN_TEMPLATE_DIR}/DoxygenLayout.xml
        ${DOXYGEN_TEMPLATE_DIR}/doc-background.png)
    file(COPY ${DOXYGEN_TEMPLATE_FILES} DESTINATION ${DOXYGEN_DOCUMENTATION_LAYOUT_DIR})


    CONFIGURE_FILE(
       "${ArmarXCore_TEMPLATES_DIR}/cmake/PropertiesDocumentation.cmake.in"
       "${DOXYGEN_PROPERTIES_DOC_FILE}"
        @ONLY)
    # create directory if it does not exist, otherwise make install fails on the documentation
    file(MAKE_DIRECTORY "${DOXYGEN_DOCUMENTATION_DIR}/${DOXYGEN_DOCUMENTATION_HTML_OUTPUT_DIR_REL}")
endmacro()

macro(generateSliceDocumentation SLICE_FILES SLICE2HTML_INCLUDE_FLAGS)
    set(SLICE2HTML_OUTPUT_DIR "${DOXYGEN_DOCUMENTATION_DIR}/${DOXYGEN_DOCUMENTATION_HTML_OUTPUT_DIR_REL}/slice/")
    set(SLICE2HTML_TEMPLATE_HEADER "${ArmarXCore_TEMPLATES_DIR}/slicedoc/slice_documentation_header.html")
    file(MAKE_DIRECTORY "${SLICE2HTML_OUTPUT_DIR}")
    set(SLICE2HTML_FLAGS  --output-dir "${SLICE2HTML_OUTPUT_DIR}"
                          --hdr "${SLICE2HTML_TEMPLATE_HEADER}"
                          -I"${PROJECT_SOURCECODE_DIR}"
                          -I"${Ice_Slice_DIR}"
                          --underscore)
    list(APPEND SLICE2HTML_FLAGS ${SLICE2HTML_INCLUDE_FLAGS})

    list(LENGTH SLICE_FILES BUILD_INTERFACE_LIBRARY)

    if (${BUILD_INTERFACE_LIBRARY})
        # add target for building the slice documentation
        add_custom_target(slicedoc
                          COMMAND ${Ice_slice2html} ${SLICE2HTML_FLAGS} ${SLICE_FILES}
                          WORKING_DIRECTORY "${PROJECT_SOURCECODE_DIRECTORY}"
                          COMMENT "Generating Slice documentation for ${PROJECT_NAME}")
        # build the slice documentation before the doxygen documentation
    else()
        add_custom_target(slicedoc
                          COMMENT "Slice documentation not generated: no input files found")
    endif()
    if(DOXYGEN_FOUND)
        # only add dependency if doxygen docu is built
        add_dependencies(doc slicedoc)
    endif()
endmacro()

if (DOXYGEN_FOUND)
    find_file(DOXYINDEXER_EXECUTABLE doxyindexer PATHS /usr/bin/ /usr/local/bin/ NO_DEFAULT_PATH)
    find_file(DOXYSEARCH_EXECUTABLE doxysearch.cgi PATHS /usr/bin/ /usr/local/bin/ NO_DEFAULT_PATH)

    if (DOXYINDEXER_EXECUTABLE)
        message(STATUS "doxyindexer found: ${DOXYINDEXER_EXECUTABLE}")
    else ()
        message(STATUS "doxyindexer not found! See howto_doxyindexer_for_ubuntu.txt for instructions if you want to build a documentation with working server-side search.")
        set(DOXYINDEXER_EXECUTABLE "/bin/true")
    endif ()

    if (DOXYSEARCH_EXECUTABLE)
        message(STATUS "doxysearch.cgi found: ${DOXYSEARCH_EXECUTABLE}")
    else ()
        message(STATUS "doxysearch.cgi not found! See howto_doxyindexer_for_ubuntu.txt for instructions if you want to build a documentation with working server-side search.")
        set(DOXYINDEXER_EXECUTABLE "/bin/true")
    endif ()

    generateDocumentationFiles()

    add_custom_target(doc
        COMMAND ${CMAKE_COMMAND} -P "${DOXYGEN_PROPERTIES_DOC_FILE}"
        # chmod or true , so that it never fails
        COMMAND chmod "+x" "${ArmarXCore_BINARY_DIR}/StatechartGroupDocGeneratorAppRun" "||" "true"
        COMMAND ${ArmarXCore_BINARY_DIR}/StatechartGroupDocGeneratorAppRun "${PROJECT_SOURCECODE_DIR}"
        COMMAND ${DOXYGEN_EXECUTABLE} "${DOXYGEN_CONFIG_FILE}"
        COMMAND ${DOXYINDEXER_EXECUTABLE} -o html searchdata.xml
        # DEPENDS ${DOXYGEN_CONFIG_FILE}
        WORKING_DIRECTORY "${DOXYGEN_DOCUMENTATION_DIR}"
        COMMENT "Generating documentation for ${PROJECT_NAME}")

    # install the documentation
    install(DIRECTORY "${DOXYGEN_DOCUMENTATION_DIR}/${DOXYGEN_DOCUMENTATION_HTML_OUTPUT_DIR_REL}"
        DESTINATION share/doc/${PROJECT_NAME}
        COMPONENT documentation)
else ()
    add_custom_target(doc
        COMMENT "Doxygen not found: the documentation can not be generated for ${PROJECT_NAME}")
endif ()
