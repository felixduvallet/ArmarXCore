# Various compiler settings for common targets

macro(executable_settings_ext EXECUTABLE_NAME CFLAGS LDFLAGS)
    # Like executable_settings, but allows additional compiler-flags and linker-flags
    ARMARX_MESSAGE(STATUS "        Configuring executable ${EXECUTABLE_NAME}.")
    compilation_settings()

    ARMARX_MESSAGE(STATUS "        Using compiler flags (CXX_FLAGS): ${CXX_FLAGS} ${CFLAGS}")
    ARMARX_MESSAGE(STATUS "        Using linker flags: ${LDFLAGS}")
    set_target_properties(${EXECUTABLE_NAME} PROPERTIES
        COMPILE_FLAGS "${CXX_FLAGS} ${CFLAGS}"
        LINK_FLAGS "${LDFLAGS}"
        LIBRARY_OUTPUT_DIRECTORY "${ARMARX_LIB_DIR}"
        ARCHIVE_OUTPUT_DIRECTORY "${ARMARX_ARCHIVE_DIR}"
        RUNTIME_OUTPUT_DIRECTORY "${ARMARX_BIN_DIR}"
    )

    ARMARX_MESSAGE(STATUS "        RUNTIME_OUTPUT_DIRECTORY: " ${ARMARX_BIN_DIR})

    if (ARMARX_OS_LINUX)
        get_target_property(EXECUTABLE_FILE ${EXECUTABLE_NAME} LOCATION)
        if (${CMAKE_BUILD_TYPE} MATCHES "Release")
            MESSAGE(STATUS "        RELEASE_MODE: stripping binaries")
            add_custom_command(TARGET ${EXECUTABLE_NAME} POST_BUILD
                               COMMAND strip ARGS --strip-unneeded ${EXECUTABLE_FILE})
        endif()
    endif()
endmacro()

macro(executable_settings EXECUTABLE_NAME)
    executable_settings_ext(${EXECUTABLE_NAME} "" "")
endmacro()

macro(library_base_settings_end LIB_NAME HEADERS)
    set_target_properties(${LIB_NAME} PROPERTIES
        VERSION   ${ARMARX_PACKAGE_LIBRARY_VERSION}
        SOVERSION ${ARMARX_PACKAGE_LIBRARY_SOVERSION})
    if(NOT LIB_ALLOW_UNDEFINED_SYMBOLS)
        if (ARMARX_OS_LINUX)
            set(LDFLAGS "${LDFLAGS} -Wl,-z,defs")
        elseif (ARMARX_OS_MAC)
            set(LDFLAGS "${LDFLAGS} -Wl,-undefined,error")
        endif()
    endif()

    ARMARX_MESSAGE(STATUS "        Using compiler flags (CXX_FLAGS): " ${CXX_FLAGS})
    ARMARX_MESSAGE(STATUS "        Using linker flags: ${LDFLAGS}")

    set_target_properties(${LIB_NAME} PROPERTIES
        COMPILE_FLAGS "${CXX_FLAGS}"
        LINK_FLAGS "${LDFLAGS}"
        LIBRARY_OUTPUT_DIRECTORY "${ARMARX_LIB_DIR}"
        ARCHIVE_OUTPUT_DIRECTORY "${ARMARX_ARCHIVE_DIR}"
        RUNTIME_OUTPUT_DIRECTORY "${ARMARX_BIN_DIR}"
    )

   ARMARX_MESSAGE(STATUS "        LIBRARY_OUTPUT_DIRECTORY: " ${ARMARX_LIB_DIR})
   ARMARX_MESSAGE(STATUS "        ARCHIVE_OUTPUT_DIRECTORY: " ${ARMARX_ARCHIVE_DIR})
   ARMARX_MESSAGE(STATUS "        RUNTIME_OUTPUT_DIRECTORY: " ${ARMARX_BIN_DIR})

    if (ARMARX_OS_LINUX)
        get_target_property(LIB_FILE ${LIB_NAME} LOCATION)
        if (${CMAKE_BUILD_TYPE} MATCHES "Release")
            MESSAGE(STATUS "        RELEASE_MODE: stripping binaries")
            add_custom_command(TARGET ${LIB_NAME} POST_BUILD
                               COMMAND strip ARGS --strip-unneeded ${LIB_FILE})
        endif()
    endif()

    set(SLICE_DEPENDS "")
    foreach(SLICE_DEPEND ${SLICE_DEPENDS_DIRTY})
        string(LENGTH ${SLICE_DEPEND} SLICE_DEPEND_LENGTH)
        math(EXPR SLICE_DEPEND_LENGTH "${SLICE_DEPEND_LENGTH}-1")
        string(SUBSTRING ${SLICE_DEPEND} 1 ${SLICE_DEPEND_LENGTH} SLICE_DEPEND)
        string(STRIP ${SLICE_DEPEND} SLICE_DEPEND)
        list(APPEND SLICE_DEPENDS ${SLICE_DEPEND})
    endforeach()

    string(REPLACE "${PROJECT_SOURCECODE_DIR}/" "" HEADER_DIR "${CMAKE_CURRENT_SOURCE_DIR}")
    library_install("${LIB_NAME}" "${HEADERS}" "${HEADER_DIR}")
endmacro()

macro(library_settings LIB_NAME HEADERS)
    ARMARX_MESSAGE(STATUS "        Configuring shared library ${LIB_NAME} version ${ARMARX_PACKAGE_LIBRARY_VERSION}.")
    compilation_settings()

    if(NOT ARMARX_OS_WIN)
        set(CXX_FLAGS "${CXX_FLAGS} -fPIC -D_REENTRANT")
    else()
        add_definitions(-D_USE_MATH_DEFINES)
        add_definitions(-D${ARMARX_PROJECT_NAME}_EXPORTS)
    endif()

    library_base_settings_end("${LIB_NAME}" "${HEADERS}")
endmacro()


macro(compilation_settings)
    # Ice uses long long in C++ which -pedantic turns into an error instead of
    # a warning - we turn it off entirely
    # would really like to use -pedantic here, but Ice breaks it
    set(GENERAL_FLAGS "-Wall -Wextra -Wno-long-long -Wno-unused-parameter -std=c++0x")

    if (NOT ARMARX_OS_WIN)
        if (CMAKE_BUILD_TYPE STREQUAL Release)
            set(CXX_FLAGS "-O2 ${GENERAL_FLAGS}")
        else()
            set(CXX_FLAGS "-g ${GENERAL_FLAGS}")
        endif()
    endif()

    # enable multipthreaded compilation on visual studio
    if (ARMARX_OS_WIN AND MSVC)
        add_definitions(/MP)
    endif()
endmacro()
