/**


\page DistributedProcessing Distributed Processing
Distributed Processing can easily be achieved in ArmarX with the \ref armarx::ArmarXManager "ArmarXManager" and \ref armarx::ManagedIceObject "ManagedIceObject".
@image html Lifecycle-UML.svg Lifecyle of a ManagedIceObject managed by the ArmarXObjectScheduler
The ArmarXManager is an administrative class that manages the complete lifecylcle of one or more \ref armarx::ManagedIceObject "ManagedIceObjects".
\ref armarx::ManagedIceObject "ManagedIceObjects" are objects that should be reachable via network and offer an implementation of an interface (e.g. armarx::KinematicUnitInterface).
An new component that should be reachable via Ice just needs to inherit from  \ref armarx::ManagedIceObject "ManagedIceObject" and the Slice Interface it whishes to offer.
Afterwards it needs to be added to an \ref armarx::ArmarXManager "ArmarXManager", which checks all the dependencies of the object and schedules it afterwards for starting.
The \ref armarx::ArmarXManager "ArmarXManager" can easily instantiated by specifying the IceGridRegistry parameters (host and port). This is done automatically if the
armarx::Application is used.
In case configurable objects are needed the armarx::Component class can be used, which is just an extension of \ref armarx::ManagedIceObject "ManagedIceObject" with \ref properties.

\defgroup DistributedProcessingGrp Distributed Processing
\ingroup ArmarXCore
\copydetails DistributedProcessing

\defgroup DistributedProcessingSub Subcomponents
\ingroup DistributedProcessingGrp
This group contains classes that are used mainly for internal processes.
IceManager and IceGridAdmin are of interest for components that wish to use Ice
functionality directly.
*/
