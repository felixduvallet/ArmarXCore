/**
\page conventions Coding Conventions


\tableofcontents

First of all: the documentation as well as class and variable names are written in English.

The rule set for the coding conventions is based on the BSD/Allman style.
Here is an example:

\code
 /**
  * A sample source file for the code formatter preview
  */

 #include <boost/shared_ptr.hpp>
 #include <cmath>

 namespace Foo
 {
     class Point
     {
     public:
         Point(double xc, double yc) :
             x(xc),
             y(yc),
             MATH_PI(3.14)
         {
         }

         double distance(const Point& other) const;
         bool isZero() const;

         static double Distance(const Point& a, const Point& b);

     private:
         double x;
         double y;
     };

     typedef boost::shared_ptr<Point> PointPtr;

     enum EnumType
     {
         eFirstEntry,
         eSecondEntry,
         eThirdEntry
     };

     const float MATH_PI;
 }

 using namespace Foo;

 double Point::distance(const Point& other) const
 {
     double dx = x - other.x;
     double dy = y - other.y;
     return sqrt(dx * dx + dy * dy);
 }

 bool Point::isZero() const
 {
     bool isZero = false;

     if ((0 == x) && (0 == y))
     {
         isZero = true;
     }

     return isZero;
 }

 double Point::Distance(const Point& a, const Point& b)
 {
     return a.distance(b);
 }
\endcode

\section conventions-classes Classes

\li Classnames all start with an uppercase letter.
\li Classnames do not start with a C prefix(applies primarily to new classes).
\li Classnames are written in CamelCase.
\li Classes are stored in files named exactly like the class.

\section conventions-classmembers Classmembers

\li Static member methods start with an Uppercase letter.
\li Non-Static member methods start with a lowercase letter.
\li All member methods are written in camelCase.
\li Constants are written all UPPER_CASE and single words are separated by underscores.
\li Members are NEVER aligned in the sourcecode. There is only one Space between the classname and the variablename. (Maintaining this order is too much work if members get added or removed).

\section conventions-enums Enums

\li Try to avoid Enums if possible
\li If you really think you have to use Enums use the following style:
Name the Enum type like a classname in CamelCase and don't put a preceeding 'E' at the beginning;
start the entries in the Enum type with a lowercase 'e' and proceed with CamelCase naming.

\section conventions-pointer C++ Pointer

\li Always use smart-/shared pointer whenever possible.
\li For classes inheriting from Ice use IceUtil::Handle.
\li For classes inheriting from Qt classes use QPointer if the object has a parent and QSharedPointer if no parent is set.
\li For every other kind of classes use boost::shared_ptr<ClassName> (requires #include <boost/shared_ptr.hpp>).
\li Always provide a "ClassNamePtr" typedef for the appropriate smartpointer type in the header file (note the Ptr suffix).

\section conventions-names Naming Conventions

\li include guards (those after #ifdef ... #define ... in headers): _ARMARX_<ComponentType>_<ArmarXPackageName>_<ComponentName>_h (you can also take a look at the skeleton files in 'ArmarXCore/etc/scripts/armarx-package/skeleton/package/source')
\li Topic names
\li Library/Component names

\section conventions-indentation Indentation

\li NO TABS
\li Standard Indentation value is 4 Spaces
\li No trailing whitespaces at the end of line (most editors can handle that automatically)
\li All opening/closing brackets start on a seperate line aligned with the enclosing code block they belong to.
\li No space between method name and parenthesis.
\li One space between all control structures and the beginning parenthesis of their corresponding condition (if, while, for, else if, switch).
\li One space after each comma or semicolon in the middle of a statement.
\li One space before and after a comparison or assignment operator (=, ==, !=, >, <, >=, <=, ...).
\li public:/protected:/private: align with the class definition.
\li The : of the initialisation list is placed after the closing bracket of the constructor.
\li The inialisation list itself is placed on a new line and indented by the standard indentation value.
\li Everything inside namespaces is indented by the standard indentation value.
\li If a 'normal' pointer is needed it is written the following way:  <Classname>* <variableName> (one space before the variable name, no space after the class-/typename)
\li one space between leading 'const' and a class-/typename
\li class-/typename followed by a space followed by 'const' followed by another space followed by the variablename

\section convention-files Files

\li All files must be named like the classes they contain with exactly the same upper-/lowercase spelling.
\li No comments AFTER #endif directives in header files.
\li C++ files end with .cpp (no .cxx, .cc or anything else).
\li Source files are located in source/PackageName/
\li Slice files end with .ice
\li Slice files are located in source/PackageName/interface/

\section conventions-general General

\li Never place a 'using namspace XYZ;' directive in header files (Reason: this results in namespace pollution and might override method definitions in namespace XYZ).
\li Remove all #include directives in header files which are not required for error free compilation (see next item).
\li Use forward class declarations in header files for each class (Reason: this results in a cleaner and faster build).
\li NEVER put forward class declarations inside #ifdefs (Reason: these declarations tells the compiler to expect a pointer to a class so that it does not need the class declaration. Therefore it doesn't make any sense to put those declarations inside #ifdefs because no other information is associated with it than 'use each pointer declaration as a pointer to a class').
\li Use cmath instead of math.h and cstdio instead of stdio.h in C++ code (applies to most of the standard C header files). This is the official C++ way of including C headers (Reason: using the .h versions can result in namespace issues. The method in the 'c' prefixed files are all defined in the std namespace). See here for a list of available headers: http://www.cplusplus.com/reference/clibrary/. Only use the C versions if method definitions should be missing in the C++ headers.


\section conventions-slice Slice Definitions

\li Interface-, Class-, Struct-, and Exception-names are written CamelCase.
\li Interface definitions _always_ end with "Interface".
\li Implementations of an interface definition omit the "Interface" suffix.
\li Class names in Slice files end with "IceBase".
\li Subclasses of classes defined in Slice files omit the "IceBase" suffix.
\li No comments after #endif.
\li For Enums apply the same rules as mentioned in \ref conventions-enums "Enums"

\li Naming of report functions?
\li Slice Documentation?
\li #ifdef convention?

\section conventions-cmake CMake

\li functions and macros are spelled lowercase
\li all header and source files are listed explicitly (not by using GLOB or GLOB_RECURSE)
\li NEVER use find_package(Xyz REQUIRED); instead use a combination of find_package(Xyz QUIET) + armarx_build_if(Xyz_FOUND "Xyz Library is missing")
\li every generated file must be generated inside CMAKE_BINARY_DIR

\section conventions-units Units

\li Angle: radian
\li Angular velocity: radian / second
\li Length/Position: millimeters
\li Velocity: millimeter / second?
\li Time: seconds / milliseconds?
\li Coordinate Systems: right/left handed?
\li Current: ?
\li Force: ?
\li Torque: ?

\section convetions-xenomai Xenomai

\li All Xenomai binaries must have a ".xeno" suffix.

\section convetions-scenarios Scenarios

\li Scenarios are located in the "scenarios" directory of each ArmarX Package
\li Each scenario is placed inside its own direktory under "scenarios"
\li Configuration files must follow the format: "config/${COMPONENT_NAME}[.optionalString].cfg"

\section conventions-documentation Documentation

\li Documentation is done using Doxygen.
\li Slice files are documented using slice2html.
\li All classes, methods, members, etc. are documented in the header files NOT in the cpp files (Rational: Template methods can only be implemented and thus documented in header files).
\li Section identifiers on pages are prefixed with the pagename to prevent duplicate identfiers
which lead to inconsistent cross-linking.
\li It does not matter if you use \\param or \@param but be consistent within your project.
Also keep the current documentation style of other projects you are working on.
\li ${ArmarXPackage_DIR}/etc/doxygen/images contains images which are included in the documentation with the following doxygen command:
\code
\image html Image.svg "Text Description of image" width=300px
\endcode
        \li ${ArmarXPackage_DIR}/etc/doxygen/snippets contains sourcecode stored in .dox files. These are later added with the following command:
\code
\include MyCodeExample.dox
\endcode
\li Additional documentation resides in ${ArmarXPackage_DIR}/etc/doxygen/pages and is put into .dox files.
\li API documentation is divided into groups defined by \@defgroup which is usually defined in the header file of the baseclass
\li Any class belonging to that group must use the following doc header
\code
@class ClassName
@brief Description of the class in one line
@ingroup GroupOfClass (defined by a @defgroup command)

Here comes the rest of the documentation
\endcode
\li Tables:


\section conventions-how-to-include How To #include
Include files in the following order:

\li "": Include files belonging to the same project and going into the same library with "".
\li <>: Include all system headers and all headers from other libraries with <>.

\section conventions-astyle Automatic code formatting with AStyle

Astyle is a tool for automatic code formatting and can be found at http://astyle.sourceforge.net/.

ArmarX Packages have convenience make target to perform the formatting automatically:

\code
make astyle
\endcode

The rules applied by astyle can be found in ${ArmarXCore_DIR}/etc/templates/armarx.astylerc.

*/

