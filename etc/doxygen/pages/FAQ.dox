/**

\page ArmarXCore-FAQ  ArmarXCore FAQ
\tableofcontents
\section ArmarXCore-FAQ-update-armarx How do I update all ArmarX related repositories easily?

Use the following commands if you want to do a "git pull" in every repository (please be aware of how git pull works if you are not on the master branch)

\code
git submodule update --remote
\endcode

or

\code
git submodule foreach "git pull"
\endcode

As an alternative to performing "git pull --rebase" it is also possible to use the following repo command.
Please make sure you understand what REBASING means and what it will do if you are working on a branch.
\warn Do not use this command if your branch has already been published!

\code
git submodule update --remote --rebase
\endcode

\subsection Updating-and-compiling Updating in combination with compiling
With the \ref armarx-dev-cli "armarx-dev" script you can compile and pull all repositories needed for the compilation target with one command.
Just pass --pull to the command. If you have changes in one or more of the repositories you can use --stashnpull.
For example for RobotSkillTemplates use:
\code
armarx-dev build RobotSkillTemplates --pull
\endcode

\section ArmarXCore-FAQ-ice-start-noendpoint  I get the exception 'no suitable endpoint available for proxy' or NoObjectFactoryException (wrong encoding)

Currently ArmarX only works with the Ice encoding version 1.0.
If you use Ice 3.5+ you will encounter the following error on start-up.
\verbatim
no suitable endpoint available for proxy `IceStorm/TopicManager -t -e 1.1'
\endverbatim
To fix this, uncomment the following line in the ~/.armarx/default.cfg
\verbatim
#Ice.Default.EncodingVersion=1.0
\endverbatim
This tells the Ice Runtime to use the old encoding version 1.0 instead of the new default (1.1).

While the exception *NoObjectFactoryException* is usually caused by missing objectfactories, this exception can also be caused by the wrong encoding.

\section ArmarxCore-FAQ-ice-start-port I get the exception *icegridnode: failure occurred in daemon*

If your ice nodes failed to start you will see an error like this
\verbatim
icegridnode: failure occurred in daemon
\endverbatim


To inspect the startup procedure, you can check the log file
\verbatim
~/.armarx/icegridnode_data/NodeMain/log/stderr.log
\endverbatim

An errror similar to the following line, indicates that another user is running ice deamons on the same port (or you  may have already started ice):
\verbatim
NodeMain: error: an IceGrid registry is already running and listening on the client endpoints `tcp -p 4061'
\endverbatim
You can change the port in the main armarx config file, which is located at
\verbatim ${HOME}/.armarx/default.cfg
\endverbatim
More details can be found here: \ref execution-multi-user "Multi-User Setup".


\section ArmarXCore-FAQ-demarshalling-error I get an unmarshalling exception when using Variant subclasses in my program

If you get an exception about mismatched types during the unmarshalling of any armarx::Variant subclass
you probably forgot to initialize the required ObjectFactories.
ObjectFactories are required by Ice in order to know how serialized objects can be deserialized after
they have been transfered over the network.

For using LinkedPose instances you need to do the following:

- include ObjectFactory header somewhere in your library
\verbatim
#include <ArmarXCore/robotstate/RobotStateObjectFactories.h>
\endverbatim
- link the library that contains the class LinkedPose: ArmarXCoreRemoteRobot

For any other armarx::Variant subclasses you need to locate the relevant factory and add this instead.

This error message can also occur when the Ice protocol version 1.1 is used (the default in Ice 3.5, but not in 3.4) in combination with statecharts.
The problem then arises when a process not linked against the necessary library tries to deserialize a variant which is used, e.g. as a default value for a statechart parameter.

As a temporary fix, you can resort to Ice protocol version 1.0 which handles unknown types during deserialization more gracefully.
To do this, add
\verbatim
Ice.Default.EncodingVersion=1.0
\endverbatim
to your global ArmarX Ice config (default path: ~/.armarx/default.cfg).




\section ArmarXCore-FAQ-concurrent-proxie-requests Can a Proxy function be called simultaneously from multiple Ice interface methods without locking?

YES.<br/>
For the long answer have a look at http://www.zeroc.com/forums/help-center/2435-multiple-incoming-connections-same-proxy.html




\section ArmarXCore-FAQ-weird-segfaults I get segmentation faults in weird places

If you get segmentation faults in places were it is almost impossible (e.g. on accessing an object which has just been created)
it might be a problem with the incremental building of cmake.

In this case it is usually best to recompile all ArmarX Packages starting with ArmarXCore.
To do this delete the contents of the build directory and run cmake and make from scratch.

\section ArmarXCore-FAQ-undefined-reference How can I find the library to undefined references/symbols?

Whenever you are trying to use a new class you also need to link it to your library. Otherwise you will get 'undefined reference' errors.
If you forget to link the library to a class you might get an error like this:
\verbatim
source/MemoryX/components/ProfilerStorage/CMakeFiles/ProfilerStorage.dir/ProfilerStorage.cpp.o: In function `memoryx::ObjectInstance::ObjectInstance()':
/home/waechter/armarx/MemoryX/build/../source/MemoryX/libraries/memorytypes/entity/ObjectInstance.h:214: undefined reference to `VTT for memoryx::ObjectInstance'
\endverbatim

Since ArmarX consists of many many libraries, we offer a convenient way to find where symbols in ArmarX are defined:
\verbatim
armarx findSymbol -p VisionX -s "memoryx::ObjectInstance::ObjectInstance("
\endverbatim
This will search in the VisionX project and all its ArmarX dependencies for the definition of constructors of the ObjectInstance class.
The output will look like this:
\verbatim
Ice Config: /home/waechter/.armarx/default.cfg
Profile: default
Searching for memoryx::ObjectInstance::ObjectInstance(
Searching in the following projects: ['ArmarXCore', 'ArmarXGui', 'RobotAPI', 'MemoryX', 'VisionX']
Found 4 matching symbol(s) in package MemoryX in file /home/waechter/armarx/MemoryX/build/lib/libMemoryXMemoryTypes.so.0.6.0
#1: memoryx::ObjectInstance::ObjectInstance(memoryx::ObjectInstance const&)
#2: memoryx::ObjectInstance::ObjectInstance(std::string const&, std::string const&)
#3: memoryx::ObjectInstance::ObjectInstance(memoryx::ObjectInstance const&)
#4: memoryx::ObjectInstance::ObjectInstance(std::string const&, std::string const&)
\endverbatim

This means you need to link the library \a MemoryXMemoryTypes, if you want to use the class ObjectInstance in your library.

\section ArmarXCore-FAQ-wrong-package I get xyz file not found although it is definitely in the ArmarX Package

If you get cmake or compiler warnings about missing files or dependencies it is usually a good idea to
check against which ArmarX Packages you are compiling.
This information can either be extracted from the CMake cache or by inspecting the output of a cmake run.
The relevant lines will look like this:

\verbatim
-- Using ArmarXCore Source SDK from: /home/user/armarx/ArmarXCore/build
-- Using ArmarXGui Source SDK from: /home/user/armarx/Gui/build
-- Using MemoryX Source SDK from: /home/user/armarx/MemoryX/build
-- Running on Mac OS X
\endverbatim

If you use multiple checkouts of the same Package or multiple build directories for different configurations
it can happen that CMake gets mixed up and uses the wrong directory.
To fix this problem you can specify the dependent ArmarX Package directly upon cmake invocation with the
following parameter:

\verbatim
cmake -D<ArmarXPackageName>_DIR=/path/to/package/build ..
\endverbatim

This can be done for each ArmarXPackage.

\section ArmarXCore-FAQ-Topic-not-called My Topic subscriber is not called

If you have a subscriber to a topic but the subscriber is not called altough the provider calls
the interface functions of the topic, check first if you use the correct name for the topic.
You can search in the log for "topic" to find the log entries of offering und using a topic.

Additionally, if a subscriber throws an exception at least once the subscription will be canceled by Ice.
Since this is Ice Internal it will not be shown in the ArmarX-Log.
With the current Ice version (<=3.5) it is not possible to change this behaviour.
You can inspect the log of IceStorm with the icegrid-gui of ZeroC Ice.


\section ArmarXCore-FAQ-Multiple-Inheritance I have compilation problems due to multiple inheritance

This problem shows itself in the form of a compiler error that says that you class does not unambiguously
overwrite some functions like ice_is_a(...). This happens when you indirectly inherit from more than one
ICE interface. To solve this, you have to find all ICE interfaces that are implemented by the classes that
you inherit from (or that you inherit from yourself). Create an ICE interface that inherits from all those
interfaces and inherit from that one.



*/