/**
\page ArmarXCore-About About

ArmarX is developed and maintained at the <a href="http://h2t.anthropomatik.kit.edu/english/index.php" target="_blank">High Performance Humanoid Technologies (H2T) Lab</a>, Institute for Anthropomatics and Robotics, Karlsruhe Institute of Technology (KIT).

Contact: Tamim Asfour, asfour@kit.edu

\section armarx-contributors Contributors

ArmarX is the result of the collaboration of many people:

Nikolaus Vahrenkamp (Concept, Simulation, ArmarXGui, MemoryX, RobotAPI, Armar3, Armar4) <br>
Mirko Wächter (Concept, \ref ArmarXCore "ArmarXCore", ArmarXGui, RobotAPI, MemoryX, RobotSkills, \ref Statechart "Statecharts", Armar3, Armar4)<br>
Manfred Kröhnert (Concept, Setup, Installation, Deployment, MemoryX, RobotSkills, Armar3)<br>
Kai Welke (Concept, \ref ArmarXCore "ArmarXCore", VisionX, MemoryX) <br>
David Schiebener (VisionX, RobotSkills, Armar3)<br>
Martin Do (RobotSkills, ArmarXGui, YouBot)<br>
Peter Kaiser (ArmarXRT, RobotSkills, Armar4)<br>
Stefan Ulbrich (RobotAPI)<br>
Christian Mandery (\ref Statechart "Statecharts", Documentation)<br>
Simon Ottenhaus (\ref Statechart "Statecharts", Armar3)<br>
David Gonzalez (VisionX)<br>
Markus Grotz (VisionX, Documentation, Installation)<br>
Lukas Kaul (Documentation)<br>
Markus Przybylski (RobotSkills)<br>

Alexey Kozlov (MemoryX)<br>
David Ruscheweyh (ArmarXGui)<br>
Nils Adernmann (\ref ArmarXCore "ArmarXCore")<br>
Patrick Niklaus (Simulation)<br>
Valerij Wittenbeck (RobotAPI, RobotSkills)<br>
Jan Issac (\ref ArmarXCore "ArmarXCore", ArmarXGui , VisionX)<br>
Thomas von der Heyde (VisionX, RobotAPI)<br>
Raphael Grimm (Gui, MemoryX, Tutorials)

We would also like to thank all students of the H2T lab who contributed to ArmarX.

*/
