##
# This file is part of ArmarX.
#
# ArmarX is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# ArmarX is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# @package    ArmarXCore::scripts
# @author     Mirko Waechter (waechter at kit dot edu)
# @date       2015
# @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
#             GNU General Public License


__author__ = 'waechter'

import argparse
import os.path, os
from subprocess import call

from armarx import ArmarXBuilder

from Command import Command


class Gui(Command):

    commandName = "gui"

    requiredArgumentCount = 0

    requiresIce = True

    parser = argparse.ArgumentParser(description='ArmarX Graphical User Interface')

    def __init__(self, configuration, profile):
        super(Gui, self).__init__(configuration, profile)
    @classmethod
    def addtoSubArgParser(cls, subParser):
        subParser.add_argument('guiconfig', nargs='?', help="Config file for the GUI (*.armarxgui)", type=argparse.FileType('r'))

    def execute(self, args):
        self.addtoSubArgParser(self.parser)
        args = self.parser.parse_args(args)


        generator = None

        builder = ArmarXBuilder.ArmarXBuilder(generator, False, False, False, False)
        packageData = builder.getArmarXPackageData("ArmarXGui")
        if not len(packageData):
            print "Could not find ArmarXGui package!"
        else:
            build_dir_data = builder.getArmarXPackageDataValue(packageData, "BINARY_DIR")
            if build_dir_data is None:
                print "Could not read build dir from cmake data!"
            else:
                gui_exec = os.path.join(build_dir_data[0], "ArmarXGuiRun")
                if args.guiconfig:
                    gui_config_path = args.guiconfig.name
                else:
                    gui_config_path = ""
                os.system(gui_exec + " " + gui_config_path)
    @classmethod
    def getHelpString(cls):
        return "start the ArmarX GUI"
