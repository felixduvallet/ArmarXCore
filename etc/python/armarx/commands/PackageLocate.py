##
# This file is part of ArmarX.
#
# ArmarX is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# ArmarX is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# @package    ArmarXCore::scripts
# @author     Manfred Kroehnert (Manfred dot Kroehnert at kit dot edu)
# @date       2015
# @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
#             GNU General Public License

__author__ = 'kroehner'

from Command import Command, CommandType
import armarx.ArmarXBuilder as ArmarXBuilder
import argparse
class PackageLocate(Command):

    commandName = "locatePackage"
    commandType = CommandType.Developer
    requiredArgumentCount = 1

    requiresIce = False

    parser = argparse.ArgumentParser(description='Locate an ArmarX package and prints the available cmake variables')


    def __init__(self, configuration, profile):
        super(PackageLocate, self).__init__(configuration, profile)

    @classmethod
    def addtoSubArgParser(cls, subParser):
        subParser.add_argument('package', help='ArmarX package to search for').completer = ArmarXBuilder.getArmarXDefaultPackages



    def execute(self, args):
        self.addtoSubArgParser(self.parser)
        args = self.parser.parse_args(args)

        packagename = args.package

        generator = ArmarXBuilder.ArmarXMakeGenerator(0)
        builder = ArmarXBuilder.ArmarXBuilder(generator, True, True, True, False)
    
        package_data = builder.getArmarXPackageData(packagename)
        if not package_data:
            print("ArmarX package <" + packagename + "> not found")
            return
    
        base_directories = builder.getArmarXPackageDataValue(package_data, "PACKAGE_CONFIG_DIR")

        if not base_directories:
            print("ArmarX package <" + packagename + "> not found")
            return

        print("\nArmarX Package <" + packagename + ">\n")
        for variable in ["PACKAGE_CONFIG_DIR", "PROJECT_BASE_DIR", "BUILD_DIR", "INCLUDE_DIRS", "LIBRARY_DIRS", "LIBRARIES", "CMAKE_DIR", "BINARY_DIR", "SCENARIOS_DIR", "DATA_DIR", "EXECUTABLE", "INTERFACE_DIRS",  "DEPENDENCIES", "SOURCE_PACKAGE_DEPENDENCIES", "PACKAGE_DEPENDENCY_PATHS"]:
            print("\t" + variable + " : " + str(builder.getArmarXPackageDataValue(package_data, variable)) + "\n")
        

    @classmethod
    def getHelpString(cls):
        return "Locate and ArmarX package and print meta information to the screen (location, data directories, ...)"
