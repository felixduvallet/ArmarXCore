##
# This file is part of ArmarX.
#
# ArmarX is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# ArmarX is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# @package    ArmarXCore::scripts
# @author     Manfred Kroehnert (Manfred dot Kroehnert at kit dot edu)
# @date       2015
# @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
#             GNU General Public License

__author__ = 'kroehner'

from Command import Command
from DeployApplication import DeployApplication

import armarx.Util as Util

import os
import sys
import subprocess


class IceStart(Command):

    commandName = "start"

    requiredArgumentCount = 0

    requiresIce = False

    def __init__(self, configuration, profile):
        super(IceStart, self).__init__(configuration, profile)


    def execute(self, args):
        """
        check if icegridnodes are running and start them if required
        and deploy IceStorm after all nodes are running
        :param args:
        :return:
        """
        for nodeName in self.configuration.iceGridNodeNames:
            # use the isIceGridNodeRunning() method defined here
            # it uses the "icegridadmin" console command to check the connection
            # otherwise it would be necessary to reinitialize self.iceHelper() after the registry node has been started
            if self.isIceGridNodeRunning(nodeName):
                print "icegridnode already running: " + nodeName
                continue

            print "starting icegridnode: " + nodeName + "\n"
            self.startIceGridNode(nodeName)
            if not self.isIceGridNodeRunning(nodeName):
                print "\nIce could not be started"
                print "\nDo you need to switch profiles? (armarx switch <profilename>)"
                return

        iceStormDeploymentXml = os.path.join(self.configuration.armarxCoreConfigurationDirectory, "IceStorm.icegrid.xml")
        if os.path.exists(iceStormDeploymentXml):
            if not "IceStorm" in self.iceHelper().getApplicationList():
                DeployApplication(self.configuration, self.profile).execute([iceStormDeploymentXml])
        else:
            print "IceStorm deployment file not found: " + iceStormDeploymentXml


    @classmethod
    def getHelpString(cls):
        return "start IceGrid and do required setup of default configuration files if required"


    def isIceGridNodeRunning(self, nodeName):
        """
        try to contact the icegridnode by name and return True if it is already up and running
        :param nodeName: name of the icegridnode to contact
        :return: True if node is running, false otherwise
        """
        command = self.getIceGridAdminCommand("node ping " + nodeName)
        process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        process.wait()
        return 0 == process.returncode


    def getIceGridAdminCommand(self, commandString):
        """
        create the string for executing icegridadmin with the script given as parameter
        :param commandString: command to execute with icegridadmin
        :return:
        """
        iceGridAdminCommand = "icegridadmin --Ice.Config=\"" + self.configuration.iceGridDefaultConfigurationFile
        iceGridAdminCommand += "\" -u " + self.configuration.iceGridUsername
        iceGridAdminCommand += " -p " + self.configuration.iceGridPassword
        iceGridAdminCommand += " -e \"" + commandString + "\""
        return iceGridAdminCommand


    def startIceGridNode(self, nodeName):
        """
        create data directories required for running the icegridnode
        check if configuration files exist
        create the shell command for running the icegridnode and execute it in a subshell
        :param nodeName:
        :return:
        """
        # TODO: abort if icegridnode is to be started on a remote host
        iceGridNodeDirectory = os.path.join(self.configuration.iceGridNodeDirectory, nodeName)
        self.createIceGridNodeDataDirectories(iceGridNodeDirectory)

        if not os.path.exists(self.configuration.armarxCoreConfigurationDirectory):
            print "ArmarXCore is not located at: " + self.configuration.armarxCoreConfigurationDirectory
            return
        if not os.path.exists(self.configuration.defaultsFile):
            print "ArmarX default config does no exist at: " + self.configuration.defaultsFile
            return

        gridNodeCommand = self.createIceGridNodeStartCommand(nodeName)

        process = subprocess.Popen(gridNodeCommand, cwd=iceGridNodeDirectory, stdout=sys.stdout, stderr=sys.stderr, shell=True)
        process.communicate()


    def createIceGridNodeStartCommand(self, nodeName):
        """
        create string containing the command to start an icegridnode
        appends the IceGridRegistry configuration file if the nodeName is equal to the name of the registry node
        :param nodeName:
        :return:
        """
        gridNodeCommand = "icegridnode --IceGrid.Registry.DefaultTemplates=" + self.configuration.armarxCoreConfigurationDirectory + "/icegrid_default_templates.xml --Ice.Config=\"" + self.configuration.defaultsFile
        if nodeName == self.configuration.iceGridRegistryNodeName:
            gridNodeCommand += "," + os.path.join(self.configuration.armarxCoreConfigurationDirectory, "icegrid_registry.cfg")
        gridNodeCommand += "," + os.path.join(self.configuration.armarxCoreConfigurationDirectory, "armarx-icegridnode.cfg") + "\""
        gridNodeCommand += " --daemon --nochdir --Ice.ProgramName=" + nodeName + " --IceGrid.Node.Name=" + nodeName
        return gridNodeCommand


    def createIceGridNodeDataDirectories(self, iceGridNodeDirectory):
        """
        create the directories required to run an icegridnode
        :param iceGridNodeDirectory:
        :return:
        """
        for subDirectory in ["log", "data", "registry"]:
            Util.mkdir_p(os.path.join(iceGridNodeDirectory, subDirectory), 0o755)
