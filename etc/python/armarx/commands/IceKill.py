##
# This file is part of ArmarX.
#
# ArmarX is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# ArmarX is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# @package    ArmarXCore::scripts
# @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
# @date       2015
# @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
#             GNU General Public License


__author__ = 'waechter'

from Command import Command


class IceKill(Command):

    commandName = "killIce"
    requiresIce = False

    requiredArgumentCount = 0

    def __init__(self, configuration, profile):
        super(IceKill, self).__init__(configuration, profile)


    def execute(self, args):
        self.killIce()

    @classmethod
    def killIce(cls):
        try:
            import psutil
            iceProcessNames = ["icegridnode", "icebox", "icegridadmin"]
            for procName in iceProcessNames:
                for proc in psutil.process_iter():
                    if proc.name == procName:
                        proc.kill()
        except:
            print "killIce command not available  since psutil module is missing"

    @classmethod
    def getHelpString(cls):
        return "kills the Ice processes hard - resistance is futile. This command stop Ice only if it runs on the " \
               "localhost. Also it kills all ice instances to which it has the permission to terminate."