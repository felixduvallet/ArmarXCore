##
# This file is part of ArmarX.
#
# ArmarX is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# ArmarX is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# @package    ArmarXCore::scripts
# @author     Manfred Kroehnert (Manfred dot Kroehnert at kit dot edu)
# @date       2015
# @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
#             GNU General Public License

__author__ = 'kroehner'

from Command import Command

import os
import argparse

class DeployApplication(Command):

    commandName = "deploy"

    requiredArgumentCount = 1
    parser = argparse.ArgumentParser(description='Deploys an ArmarX scenario as an Ice application')

    def __init__(self, configuration, profile):
        super(DeployApplication, self).__init__(configuration, profile)

    @classmethod
    def addtoSubArgParser(cls, subParser):

        subParser.add_argument('app', nargs='+', help='Ice Application xml file')

    def execute(self, args):
        self.addtoSubArgParser(self.parser)
        args = self.parser.parse_args(args)

        for file in args.app:
            if not os.path.exists(file):
                print "IceGrid deployment file does not exist: " + file
                continue
            self.deploy(file)

    @classmethod
    def getHelpString(cls):
        return "deploy the applications specified in the files given as command parameters"

    def deploy(self, filename):
        print "Deploying application: " + filename
        iceGridCommand = "icegridadmin --Ice.Config=\"" + self.configuration.defaultsFile
        iceGridCommand += "\" -u " + self.configuration.iceGridUsername
        iceGridCommand += " -p " + self.configuration.iceGridPassword
        deployCommand = " -e \"application add " + filename + "\""
        updateCommand = " -e \"application update " + filename + "\""

        ret = os.system(iceGridCommand + deployCommand)
        if 0 != ret:
            print "IceGrid application exists, updating description"
            os.system(iceGridCommand + updateCommand)