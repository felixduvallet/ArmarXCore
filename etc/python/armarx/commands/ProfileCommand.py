##
# This file is part of ArmarX.
#
# ArmarX is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# ArmarX is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# @package    ArmarXCore::scripts
# @author     Manfred Kroehnert (Manfred dot Kroehnert at kit dot edu)
# @date       2015
# @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
#             GNU General Public License

__author__ = 'kroehner'

from Command import Command
from armarx.Profile import Profile

import os

class ProfileCommand(Command):

    commandName = "profile"

    requiredArgumentCount = 0

    requiresIce = False


    def execute(self, args):
        self.printConfigurationFiles()

        self.listProfiles()


    def __init__(self, configuration, profile):
        super(ProfileCommand, self).__init__(configuration, profile)


    def listProfiles(self):
        profiles = Profile.getAvailableProfiles()
        print "\nAvailable profiles (* = active): "
        for profile in profiles:
            marker = "  "
            if self.configuration.profilename == profile:
                marker = "* "
            print "  " + marker + profile


    def printConfigurationFiles(self):
        print "\nConfiguration files for " + self.configuration.profilename
        profile_directory = Profile(self.configuration.profilename).getProfileDirectory()
        configuration_files = os.listdir(profile_directory)
        for filename in configuration_files:
            print "\n" + filename + ":\n"
            with open(os.path.join(profile_directory, filename), "r") as file:
                for line in file:
                    # comma required to avoid duplicate newlines
                    print ">  " + line,


    @classmethod
    def getHelpString(cls):
        return "Switch configuration profiles (default.cfg, IceGrid synchronisation)"