##
# This file is part of ArmarX.
#
# ArmarX is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# ArmarX is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# @package    ArmarXCore::tools
# @author     Jan Issac (jan dot issac at gmail dot com)
# @date       2012
# @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
#             GNU General Public License

from os import path
from xml.etree import ElementTree as ET
from sets import Set
import re

from TemplateDefinition import TemplateDefinition
import ComponentDefinition

class DirectoryDefinition(object):

    ##
    # Constructor
    #
    def __init__(self, directoryNode, parentDefinition=None, templateReplacementStrategy=None):
        self._directoryNode = directoryNode
        self._parentDefinition = parentDefinition
        self._name = self.getDirectoryNode().get("name")
        self._required = (self.getDirectoryNode().get("required") == "yes")
        self._templateDefinitions = []
        self._subDirectoryDefinitions = []
        self._componentDefinitions = {}

        self.templateReplacementStrategy = templateReplacementStrategy

        #=======================================================================
        # Define sub-directories within this directory
        #=======================================================================
        for subDirNode in self.getDirectoryNode().findall("directory"):
            self.addSubDirectoryDefinition(DirectoryDefinition(subDirNode, self, templateReplacementStrategy=self.templateReplacementStrategy))

        #=======================================================================
        # Define templates within this directory
        #=======================================================================
        for templateNode in self._directoryNode.findall("file"):
            self.addTemplateDefinition(TemplateDefinition(templateNode))

        #=======================================================================
        # Define components within this directory level
        #=======================================================================
        for componentNode in self._directoryNode.findall("component"):
            self.addComponentDefinition(ComponentDefinition.ComponentDefinition(componentNode, self.templateReplacementStrategy))


    ##
    # Returns the directory XML Element
    #
    def getDirectoryNode(self):
        return self._directoryNode


    ##
    # Returns the parent directory definition
    #
    def getParentDefinition(self):
        return self._parentDefinition


    ##
    # Sets the parent directory definition
    #
    def setParentDefinition(self, parentDefinition):
        self._parentDefinition = parentDefinition


    ##
    # Returns the directory name
    #
    def getName(self):
        return self._name


    ##
    # Sets the directory name
    #
    def setName(self, name):
        self._name = name


    ##
    # Returns whether this directory is required by the structure.
    #
    def isRequired(self):
        return self._required

    ##
    # Sets requirement status of the directory
    #
    def setRequired(self, required):
        self._required = required


    ##
    # Adds a sub-directory
    #
    def addSubDirectoryDefinition(self, subdirectory):
        self._subDirectoryDefinitions.append(subdirectory)


    ##
    # Returns the sub-directory definition list
    #
    def getSubDirectoryDefinitions(self):
        return self._subDirectoryDefinitions


    ##
    # Adds a template definition
    #
    def addTemplateDefinition(self, templateDefinition):
        self._templateDefinitions.append(templateDefinition)


    ##
    # Returns the template definitions of this directory
    #
    def getTemplateDefinitions(self):
        return self._templateDefinitions


    ##
    # Adds a component definition
    #
    def addComponentDefinition(self, componentDef):
        self._componentDefinitions.update({componentDef.getType(): componentDef})


    ##
    # Returns the component definition if exists, otherwise None
    # The search is performed recursively.
    #
    def getComponentDefinition(self, name):
        if self._componentDefinitions.has_key(name):
            return self._componentDefinitions[name]
        else:
            for subDirectory in self._subDirectoryDefinitions:
                componentDefinition = subDirectory.getComponentDefinition(name)
                if componentDefinition:
                    return componentDefinition

        return None


    ##
    # Returns a list of component definitions
    #
    # @param recursive    If set true, all component definitions within
    #                     this directory level and all levels below will be
    #                     collected and returned as a dictionary.
    #
    def getComponentDefinitions(self, recursive = False):
        componentDefinitions = self._componentDefinitions.copy()

        if recursive:
            for subDirectory in self._subDirectoryDefinitions:
                componentDefinitions.update(subDirectory.getComponentDefinitions(recursive))

        return componentDefinitions


    ##
    # Return the path to this directory
    #
    def getPath(self):
        if self._parentDefinition:
            return path.join(self._parentDefinition.getPath(), self._name)
        else:
            return self._name


    ##
    # Determines the regular expression search patterns of all component
    # definitions
    #
    def getComponentSearchPatterns(self, recursive = False):
        componentNameRegexSet = Set([])

        # determine component names
        for compDefinition in self._componentDefinitions.values():
            # Top-level component directories and files all include the
            # @COMPONENT_NAME@ variable which is used to determine the component
            # name by generating a regular expression out of the first component
            # element. The first element is either a file or a directory
            dirDefinitions = compDefinition.getDirectoryDefinitions()

            if dirDefinitions:
                namePattern = dirDefinitions[0].getName()
                namePattern = namePattern.replace("@COMPONENT_NAME@", "VARIABLECOMPONENTNAME")
                namePattern = re.escape(namePattern)
                namePattern = namePattern.replace("VARIABLECOMPONENTNAME", "(.+)")
                if namePattern:
                    componentNameRegexSet.add(namePattern)
            else:
                # Error: a component mustn't be empty!
                raise Exception("Error: a component mustn't be empty!")

        if recursive:
            for subDirectoryDefinition in self._subDirectoryDefinitions:
                componentNameRegexSet.update(subDirectoryDefinition.getComponentSearchPatterns())

        return componentNameRegexSet


    ##
    # Creates a directory definition from a specified directory path
    #
    @staticmethod
    def CreateFromPath(dirLocation, parent = None):
        dirNode = ET.Element("directory")
        dirNode.set("name", dirLocation)
        dirNode.set("required", "yes")

        return DirectoryDefinition(dirNode, parent)


