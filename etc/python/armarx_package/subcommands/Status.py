##
# This file is part of ArmarX.
#
# ArmarX is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# ArmarX is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# @package    ArmarXCore::tools
# @author     Jan Issac (jan dot issac at gmail dot com)
# @date       2013
# @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
#             GNU General Public License

import os
from os import path
from argparse import RawTextHelpFormatter
from armarx_package.SubCommand import SubCommand
from armarx_package.Structure import Structure
from armarx_package.TemplateReplacementStrategy import TemplateReplacementStrategy

from armarx_package.Manpage import Manpage

class StatusSubCmd(SubCommand):

    ##
    # status sub-command constructor
    #
    def __init__(self, parserContainer):


        self.name = 'status'

        self.manpage = Manpage()

        self.manpage.setProgName('armarx-package')
        self.manpage.setName(self.name)
        self.manpage.setDescription('''
Displays package information such as the elements and the their categories
with a package.
''')
        self.manpage.setBriefDescription('Displays package information')
        self.manpage.setExamples('''
.B 1.
Display brief package content

    $ cd /path/to/workspace/HelloWorld
    $ armarx-package status
    $ Current package: HelloWorld
    $
    $ Content:
    $
    $   application elements:
    $     - MainApp


.B 2.
Display all package information

    $ cd /path/to/workspace/HelloWorld
    $ armarx-package status --all --print-location
    $ Current package: HelloWorld
    $
    $ Content:
    $
    $   application elements:
    $     - MainApp
    $         /path/to/workspace/HelloWorld/source/HelloWorld/applications/MainApp
    $   armarx-objects elements:
    $     - MotionPlaner [removed]
    $         /path/to/workspace/HelloWorld/source/HelloWorld/armarx-objects/MotionPlaner

''')

        self.parser = parserContainer.add_parser(
                               self.name,
                               help="Lists package information",
                               formatter_class=RawTextHelpFormatter,
                               description=self.manpage.getBriefDescription(),
                               epilog='Checkout \'armarx-package help status\''\
                                      ' for more details and examples!')

        self.parser.add_argument("-a",
                                  "--all",
                                  action='store_true',
                                  help="List all element's including "\
                                       + "removed ones")

        group = self.parser.add_mutually_exclusive_group()

        group.add_argument("-p",
                           "--print-location",
                           action='store_true',
                           help="Prints the element's relative location")

        group.add_argument("-P",
                           "--print-abs-location",
                           action='store_true',
                           help="Prints the element's absolute location")

        self.addTemplateReplacementStrategyArgument()

        self.parser.set_defaults(func=self.execute)

        self.addGlobalArguments()


    def execute(self, args):
        structureDefinitionFilePath = path.join(self.getSkeletonPath(), "package-structure.xml")

        templateReplacementStrategy = TemplateReplacementStrategy.getStrategyForComponentType(self.getName(), args.strategy)

        # create a package structure object for an existing package
        structure = Structure.CreateStructureFromPackage(structureDefinitionFilePath, args.dir, templateReplacementStrategy)
        try:
            structure.validate()

            print self.outputPrefix, "Current package:", structure.getName()
            print "\n", self.outputPrefix, "Content:"

        except Exception, e:
            self.handleException(args, e)
            return

        componentTypes = structure.getComponentDefinitions(True).keys()
        components = structure.getComponents()

        # display sorted by component type
        resultsListed = False
        intermediateResultsListed = False

        printLocation = args.print_location or args.print_abs_location
        printAbsolutePaths = args.print_abs_location

        if components:
            # TODO: reimplement this more efficiently and less ugly

            # print the components for each component type
            for typeName in componentTypes:
                printTypeHeader = True
                intermediateResultsListed = False
                outputBuffer = ""
                for component in components:
                    # print the component if it is of the current component type
                    if component.getType() == typeName:
                        if printTypeHeader:
                            outputBuffer = "    " + typeName + " elements:"
                            printTypeHeader = False

                        output = ""
                        removed = component.removed()
                        # mark the component as [removed] if it has been removed
                        # and all components should be listed (args.all set)
                        if args.all and removed:
                                output = "      - " + component.getName() + " [removed]"
                                intermediateResultsListed = True
                        else:
                            if not removed:
                                output = "      - " + component.getName()
                                intermediateResultsListed = True

                        # add the component entry under the current type
                        if output:
                            outputBuffer += "\n" + output

                            # print the location/s of the component if requested
                            if printLocation:
                                outputBuffer += " ("
                                dirs = component.getDirectories()
                                # components top level directories paths
                                if dirs:
                                    for _dir in dirs:
                                        if printAbsolutePaths:
                                            ePath = _dir.getPath()
                                        else:
                                            paths = [os.getcwd(),
                                                     _dir.getPath()]
                                            prefix = path.commonprefix(paths)
                                            ePath = path.relpath(_dir.getPath(),
                                                                 prefix)

                                        outputBuffer += ePath
                                        outputBuffer += ";"
                                    outputBuffer = outputBuffer[:-1]
                                else:
                                    outputBuffer += "("
                                    # if component consists only of files, then
                                    # print the component root directory path
                                    outputBuffer += component.getRootDirectory().getPath()
                                outputBuffer += ")"

                if intermediateResultsListed:
                    print outputBuffer
                    resultsListed = True

        if not resultsListed:
            print self.outputPrefix, "No elements included"
