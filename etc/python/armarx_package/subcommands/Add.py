##
# This file is part of ArmarX.
#
# ArmarX is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# ArmarX is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# @package    ArmarXCore::tools
# @author     Jan Issac (jan dot issac at gmail dot com)
# @date       2013
# @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
#             GNU General Public License

from os import path
from argparse import RawTextHelpFormatter
from armarx_package.SubCommand import SubCommand
from armarx_package.Structure import Structure
from armarx_package.StructureDefinition import StructureDefinition

from armarx_package.TemplateReplacementStrategy import TemplateReplacementStrategy
from armarx_package.Exceptions import InvalidPackageException
from armarx_package.Exceptions import UndefinedComponentException
from armarx_package.Exceptions import ComponentExistsException

from armarx_package.Completers import PositionalCompleter
from argcomplete.completers import ChoicesCompleter

from armarx_package.Manpage import Manpage

class AddSubCmd(SubCommand):

    ##
    # add sub-command constructor
    #
    def __init__(self, parserContainer):

        self.manpage = Manpage()

        self.name = 'add'

        self.manpage.setProgName('armarx-package')
        self.manpage.setName(self.name)
        self.manpage.setDescription('''
Use this sub-command to add new elements of a specific category to the package.

The available categories are:

%s
''' % self.getCategoriesSummary())

        self.manpage.setBriefDescription('''
  Add new package element

  The available categories are:

%s
''' % self.getCategoriesSummary())

        self.manpage.setExamples('''
.B 1.
Add an ArmarX Application element to the package

    $ cd /path/to/workspace/HelloWorld
    $ armarx-package add application MyHellowWorldApp
    $ %s MyHellowWorldApp application element created.

.B 2.
Add an ArmarX Gui-Plugin element to the package from an arbitrary location

    $ armarx-package add gui-plugin HelloWorldGui --dir=/path/to/workspace/HelloWorldPackage
    $ %s HelloWorldGui gui-plugin element created.

.B 3.
A more detailed example starting from scratch including compiling steps

    $ cd /path/to/workspace
    $ armarx-package init PickAndPlace
    $ %s PickAndPlace package created
    $ cd PickAndPlace
    $ armarx-package add application MainApp
    $ %s MainApp application element created.
    $ armarx-package add armarx-object MotionPlaner
    $ %s MotionPlaner armarx-object element created.
    $ cd build
    $ cmake ..
    $ ...
    $ make
    $ ...

.B 4.
Re-integrate a removed element back into the package
    $ cd /path/to/workspace/PickAndPlace
    $ armarx-package remove
    $ %s MotionPlaner element has been removed from the package.
    $ %s Note: The component is no longer a part of the package yet the files still remain in:
    $ %s /path/to/workspace/PickAndPlace/source/PickAndPlace/armarx-objects/MotionPlaner
    $ armarx-package add --join armarx-object MotionPlaner
    $ %s MotionPlaner element has been re-joined to the package
''' % (u'>',
       u'>',
       u'>',
       u'>',
       u'>',
       u'>',
       u'>',
       u'>',
       u'>'))

        self.parser = parserContainer.add_parser(
                               self.name,
                               help='Creates a package element',
                               formatter_class=RawTextHelpFormatter,
                               description=self.manpage.getBriefDescription(),
                               epilog='Checkout \'armarx-package help add\' for more details and examples!')

        self.parser.add_argument('elementCategory',
                                 help='ArmarX Package element').completer = ChoicesCompleter(self.getCategoryList())

        self.parser.add_argument('elementName',
                                 help='Name of the new element with an optional '
                                      "path within the component folder (e.g., 'skills/grasp' creates "
                                      "the component 'grasp' in the sub folder 'skills/grasp').").completer = \
                                                PositionalCompleter('elementName')
        self.parser.add_argument('-j', '--join', action='store_true',
                                 help='Joins an existing element into the package')

        self.parser.add_argument('-l', '--lib', action='store', default=None, nargs=1,
                                 help='Specifies in which library the component should be included. '
                                      'If none given (default), the component is included as an individual library.')

        self.addTemplateReplacementStrategyArgument()

        self.parser.set_defaults(func=self.execute)

        self.addGlobalArguments()

    def execute(self, args):
        structureDefinitionFilePath = path.join(self.getSkeletonPath(), 'package-structure.xml')

        templateReplacementStrategy = TemplateReplacementStrategy.getStrategyForComponentType(args.elementCategory, args.strategy)

        # create a package structure object for an existing package
        structure = Structure.CreateStructureFromPackage(structureDefinitionFilePath, args.dir, templateReplacementStrategy=templateReplacementStrategy)
        try:
            # validate package structure before creating the component
            structure.validate()

            # split the element name to get local paths
            path_list = args.elementName.split('/')
            elementName = path_list[-1]

            if args.lib is None:
                libPath = '/'.join(path_list)
                elementPath = '.'
                libName = elementName

            else:
                libPath = args.lib[0]
                libName = libPath.split('/')[-1]
                for i, j in zip(args.lib[0].split('/'), path_list):
                    assert(i == j)
                    path_list.remove(j)
                if len(path_list) == 1:
                    elementPath = '.'
                else:
                    elementPath = '/'.join(path_list[0:-1])

            # create component of the required type
            component = structure.createComponent(args.elementCategory, elementName, elementPath, libPath, libName)

            try:
                component.write()
                print self.outputPrefix, args.elementName, args.elementCategory, 'element created.'
            except ComponentExistsException, excExists:
                if args.join:
                    self.joinToPackage(args)
                    return
                else:
                    self.handleException(args, excExists)
                    print 'Note: use the --join option to re-integrate an existing element into the package.'

            except Exception, exc:                
                self.handleException(args, exc)

                print self.outputPrefix, 'Creating', args.elementName, args.elementCategory, 'element failed.'

        except UndefinedComponentException as unknownCompExc:
            self.handleException(args, unknownCompExc)

            self.printAvailableCategories(structure.getComponentDefinitions(True))

        except InvalidPackageException as invalidPackageExc:
                self.handleException(args, invalidPackageExc)

    ##
    # Joins a specific component to a package
    #
    def joinToPackage(self, args):
        structureDefinitionFilePath = path.join(self.getSkeletonPath(), 'package-structure.xml')

        # create a package structure object for an existing package
        structure = Structure.CreateStructureFromPackage(structureDefinitionFilePath, args.dir)
        try:
            # validate package structure before creating the component
            structure.validate()

            component = structure.getComponent(args.elementCategory, args.elementName)

            component.join()

            print self.outputPrefix, args.elementName, 'element has been re-joined to the package.'

        except UndefinedComponentException as unknownCompExc:
            self.handleException(args, unknownCompExc)

            self.printAvailableCategories(structure.getComponentDefinitions(True))

        except Exception as exc:
            self.handleException(args, exc)


    def getCategoriesSummary(self):
        structureDefinitionFilePath = path.join(self.getSkeletonPath(), 'package-structure.xml')

        definition = StructureDefinition(structureDefinitionFilePath)
        definition.getComponentDefinitions(True)

        componentDefinitions = definition.getComponentDefinitions(True)

        listContent = ''

        for definition in componentDefinitions.values():
            listContent += '           %-25s %s\n' % ("'%s'" % definition.getType(), definition.getDescription())

        return listContent

