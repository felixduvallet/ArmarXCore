##
# This file is part of ArmarX.
#
# ArmarX is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# ArmarX is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# @package    ArmarXCore::tools
# @author     Jan Issac (jan dot issac at gmail dot com)
# @date       2012
# @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
#             GNU General Public License

from os import path
import re

##
# Adds CMake directory inclusion entries along the directory paths
# starting from the top level directory. If a leafs of the directory
# hierarchy has been already included in the CMake structure, an exception
# is thrown to indicate an existing entry
#
def addToCMake(directory):
    #if isInCMake(directory):
    #    raise Exception(directory.getPath() + " component is already a " \
    #                    + "part of CMake structure.")

    while directory.getParentDirectory():
        # add cmake entry in component root directory if exists
        cmakelists = path.join(directory.getParentDirectory().getPath(), "CMakeLists.txt")

        if not isInCMake(directory) and isCMakeManaged(directory):
            with open(cmakelists, "r") as cmakelistsFile:
                content = cmakelistsFile.read()
            entry = "add_subdirectory({0})".format(directory.getName())
            if not entry in content:
                with open(cmakelists, "a+") as cmakelistsFile:

                    print '%s %s %s ...' % (u'>',
                                        'Updating cmake '.ljust(25, '.'),
                                        cmakelists)
                    cmakelistsFile.write('\n'+entry)
            else:
                print '%s %s %s ...' % (u'>',
                                        'Not updating cmake '.ljust(25, '.'),
                                        cmakelists)


        directory = directory.getParentDirectory()


def isCMakeManaged(directory):
    tmpDefs = directory.getParentDirectory().getTemplateDefinitions()
    for tmpDef in tmpDefs:
        if tmpDef.getName() == "CMakeLists.txt":
            return not tmpDef.isStatic()

    return True


##
# Checks whether the directory is included in the CMake structure
#
def isInCMake(directory):
    if directory.getParentDirectory():
        cmakelists = path.join(directory.getParentDirectory().getPath(), "CMakeLists.txt")

        if path.exists(cmakelists):
            with open(cmakelists, "a+") as cmakelistsFile:
                content = cmakelistsFile.read()
                pattern = re.compile(generateCMakeEntryRegex()\
                                     .format(directory.getName()))
                return pattern.search(content)

    return False

##
# Adds component dependecies to the top-level CMake file
#
def addDependencies(component):
    if not component.getDependencies():
        return

    # get top-level  directory
    directory = component.getRootDirectory()
    while directory.getParentDirectory():
        directory = directory.getParentDirectory()

    # add cmake entries
    cmakelists = path.join(directory.getPath(), "CMakeLists.txt")

    with open(cmakelists, "a+") as cmakelistsFile:
        oldContentLines = cmakelistsFile.readlines()

    newContent = []
    dependenciesAdded = False

    for line in oldContentLines:
        if not line.lstrip().startswith("armarx_project"):
            # every line which is not the "armarx_project()" definition
            newContent.append(line)
        else:
            # append
            newContent.append(line)
            if not newContent[-1].strip():
                del newContent[-1]

            # add all new dependencies one by one
            for dependency in component.getDependencies():
                # check if already stated
                if hasDependency(directory, dependency):
                    continue

                newContent.append("depends_on_armarx_package({0})\n".format(dependency))
                dependenciesAdded = True

    if dependenciesAdded:
        with open(cmakelists, "w") as cmakelistsFile:
            cmakelistsFile.write(''.join(newContent))


##
# Checks whether a dependency is already stated in the top-level cmake file
#
def hasDependency(topLevelDir, dependency):
    cmakelists = path.join(topLevelDir.getPath(), "CMakeLists.txt")

    with open(cmakelists, "a+") as cmakelistsFile:
        content = cmakelistsFile.read()
        # match any 'depends_on_armarx_package()' line that does not start with a comment ('#')
        # use MULTILINE so each line of the file gets checked
        pattern = re.compile(r"^\s*depends_on_armarx_package\(\s*{0}\s*\)\s*".format(dependency), re.MULTILINE)
        return pattern.search(content)




##
# Removes the specified directory from the CMake structure
#
def removeFromCMake(directory):
    if isInCMake(directory):
        cmakelists = path.join(directory.getParentDirectory().getPath(), "CMakeLists.txt")

        with open(cmakelists, "r+") as cmakelistsFile:
                content = cmakelistsFile.read()

        newContent = re.sub(generateCMakeEntryRegex().format(directory.getName()), '\n', content)

        with open(cmakelists, "w+") as cmakelistsFile:
            print '%s %s %s ...' % (u'>', 'Removing from cmake '.ljust(25, '.'), cmakelists)
            cmakelistsFile.write(newContent)


def generateCMakeEntryRegex():
    return r"\s*add_subdirectory\(\s*{0}\s*\)\s*"
