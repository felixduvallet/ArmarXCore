/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    @PACKAGE_NAME@::statechart::@COMPONENT_NAME@
 * @author     @AUTHOR_NAME@ ( @AUTHOR_EMAIL@ )
 * @date       @YEAR@
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "@COMPONENT_NAME@.h"

namespace armarx
{
    /* == @COMPONENT_NAME@ ==*/

    void @COMPONENT_NAME@::onInitRemoteStateOfferer()
    {
        addState < Statechart@COMPONENT_NAME@ > ("@COMPONENT_NAME@");
    }


    void @COMPONENT_NAME@::onConnectRemoteStateOfferer()
    {
    }


    /* == Statechart@COMPONENT_NAME@ ==*/

    void Statechart@COMPONENT_NAME@::defineState()
    {

    }


    void Statechart@COMPONENT_NAME@::defineParameters()
    {

    }


    void Statechart@COMPONENT_NAME@::defineSubstates()
    {
        //add sub-states
        setInitState(addState<State>("InitialState"));

        StatePtr stateSuccess = addState<SuccessState>("Success");
        StatePtr stateFailure = addState<FailureState>("Failure");

        // add transitions
        addTransition<Success>(getInitState(), stateSuccess);
        addTransition<Failure>(getInitState(), stateFailure);
    }


    void Statechart@COMPONENT_NAME@::onEnter()
    {
        // install conditions that fire events upon fulfillment

    }


    void Statechart@COMPONENT_NAME@::onExit()
    {
        // remove conditions
    }
}
