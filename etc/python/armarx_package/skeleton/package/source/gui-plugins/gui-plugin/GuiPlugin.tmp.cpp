/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * \package    @PACKAGE_NAME@::gui-plugins::@COMPONENT_NAME@GuiPlugin
 * \author     @AUTHOR_NAME@ ( @AUTHOR_EMAIL@ )
 * \date       @YEAR@
 * \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "@COMPONENT_NAME@GuiPlugin.h"

#include "@COMPONENT_NAME@WidgetController.h"

using namespace armarx;

@COMPONENT_NAME@GuiPlugin::@COMPONENT_NAME@GuiPlugin()
{
    addWidget < @COMPONENT_NAME@WidgetController > ();
}

Q_EXPORT_PLUGIN2(armarx_gui_@COMPONENT_NAME@GuiPlugin, @COMPONENT_NAME@GuiPlugin)
