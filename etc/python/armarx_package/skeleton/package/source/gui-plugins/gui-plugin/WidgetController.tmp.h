/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    @PACKAGE_NAME@::gui-plugins::@COMPONENT_NAME@WidgetController
 * @author     @AUTHOR_NAME@ ( @AUTHOR_EMAIL@ )
 * @date       @YEAR@
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_@PACKAGE_NAME@_@COMPONENT_NAME@_WidgetController_H
#define _ARMARX_@PACKAGE_NAME@_@COMPONENT_NAME@_WidgetController_H

#include "ui_@COMPONENT_NAME@Widget.h"

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>

#include <ArmarXCore/core/system/ImportExportComponent.h>

namespace armarx
{
    /**
    \page ArmarXGui-GuiPlugins-@COMPONENT_NAME@ @COMPONENT_NAME@
    \brief The @COMPONENT_NAME@ allows visualizing ...

    \image html @COMPONENT_NAME@.png
    The user can

    API Documentation \ref @COMPONENT_NAME@WidgetController

    \see @COMPONENT_NAME@GuiPlugin
    */

    /**
     * \class @COMPONENT_NAME@WidgetController
     * \brief @COMPONENT_NAME@WidgetController brief one line description
     *
     * Detailed description
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT
        @COMPONENT_NAME@WidgetController:
    public armarx::ArmarXComponentWidgetController
    {
        Q_OBJECT

    public:
        /**
         * Controller Constructor
         */
        explicit @COMPONENT_NAME@WidgetController();

        /**
         * Controller destructor
         */
        virtual ~@COMPONENT_NAME@WidgetController();

        /**
         * @see ArmarXWidgetController::loadSettings()
         */
        virtual void loadSettings(QSettings* settings);

        /**
         * @see ArmarXWidgetController::saveSettings()
         */
        virtual void saveSettings(QSettings* settings);

        /**
         * Returns the Widget name displayed in the ArmarXGui to create an
         * instance of this class.
         */
        virtual QString getWidgetName() const
        {
            return "@COMPONENT_NAME@";
        }

        /**
         * \see armarx::Component::onInitComponent()
         */
        virtual void onInitComponent();

        /**
         * \see armarx::Component::onConnectComponent()
         */
        virtual void onConnectComponent();

    public slots:
        /* QT slot declarations */

    signals:
        /* QT signal declarations */

    private:
        /**
         * Widget Form
         */
        Ui::@COMPONENT_NAME@Widget widget;
    };
}

#endif
