##
# This file is part of ArmarX.
#
# ArmarX is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# ArmarX is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# @package    ArmarXCore::tools
# @author     Jan Issac (jan dot issac at gmail dot com)
# @date       2012
# @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
#             GNU General Public License

from argparse import ArgumentParser
from Exceptions import ArgumentParserException

##
# Alters the behavour of ArgumentParser.error method. The overridden error
# method throws an exception instead of printing the error and exiting.
#
class CommandParser(ArgumentParser):

    def error(self, message):
        raise ArgumentParserException(message, self.prog, self.format_usage())

    def getSubParserContainer(self):
        if not self.subParsers:
            self.subParsers = self.add_subparsers(title="sub-commands")

        return self.subParsers

    def registerSubCommand(self, subCommand):
        self.subCommandDict.update({subCommand.getName() : subCommand})

    def getSubParser(self, name):
        if not self.hasSubParser(name):
            raise Exception("Undefined sub-parser %s." % name)

        return self.subCommandDict[name].getParser()

    def getSubCommand(self, name):
        if not self.hasSubParser(name):
            raise Exception("Undefined sub-command %s." % name)

        return self.subCommandDict[name]

    def getSubCommands(self):
        return self.subCommandDict


    def hasSubParser(self, name):
        return self.subCommandDict.has_key(name)

    subParsers = None
    subCommandDict = {}

