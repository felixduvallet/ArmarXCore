
#ifndef _ARMARX_CORE_VARIANT_OBJECT_FACTORIES_H
#define _ARMARX_CORE_VARIANT_OBJECT_FACTORIES_H

#include <ArmarXCore/core/system/FactoryCollectionBase.h>
#include <ArmarXCore/observers/variant/Variant.h>
#include "MatrixVariant.h"
#include <Ice/ObjectFactory.h>

namespace armarx
{


    namespace ObjectFactories
    {

        /**
        * @class ObserverObjectFactories
        * @brief
        */
        class Eigen3VariantObjectFactories : public FactoryCollectionBase
        {
        public:
            ObjectFactoryMap getFactories()
            {
                ObjectFactoryMap map;

                add<MatrixFloatBase, MatrixFloat>(map);
                add<MatrixDoubleBase, MatrixDouble>(map);

                return map;
            }
            static const FactoryCollectionBaseCleanUp VariantObjectFactoriesVar;
        };

    }

}

#endif
