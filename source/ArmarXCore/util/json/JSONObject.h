/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     ALexey Kozlov ( kozlov at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef ARMARX_CORE_JSONOBJECT_H_
#define ARMARX_CORE_JSONOBJECT_H_

#include <ArmarXCore/interface/observers/VariantBase.h>
#include <ArmarXCore/observers/AbstractObjectSerializer.h>
#include <ArmarXCore/interface/serialization/JSONSerialization.h>

#include <jsoncpp/json/json.h>

namespace armarx
{
    class JSONObject;
    typedef IceInternal::Handle<JSONObject> JSONObjectPtr;

    /**
     * @brief The JSONObject class is used to represent and (de)serialize JSON objects.
     *
     * Accessing the object is not thread safe.
     * (De)Serializing multiple JSON objects should be done with a new instance for
     * each object.
     */
    class JSONObject :
        public armarx::AbstractObjectSerializer
    {
    public:
        JSONObject(armarx::ElementType nodeType = armarx::ElementTypes::eObject, const Ice::CommunicatorPtr ic = Ice::CommunicatorPtr());
        JSONObject(const Ice::CommunicatorPtr ic);
        JSONObject(const JSONObject& toCopy);
        virtual ~JSONObject();

    public:  // ObjectSerializerBase methods implementation
        virtual std::string toString(const ::Ice::Current& = ::Ice::Current()) const;
        virtual void fromString(const ::std::string& jsonString, const ::Ice::Current& = ::Ice::Current());

    public:  //
        virtual armarx::ElementType getElementType(const ::Ice::Current& = ::Ice::Current()) const;
        virtual void setElementType(armarx::ElementType nodeType, const ::Ice::Current& = ::Ice::Current());

        virtual unsigned int size() const;
        virtual bool hasElement(const ::std::string& key) const;

        virtual float getFloat(const ::std::string& key) const;
        virtual double getDouble(const ::std::string& key) const;
        virtual int getInt(const ::std::string& key) const;
        virtual bool getBool(const ::std::string& key) const;
        virtual std::string getString(const ::std::string& key) const;
        virtual armarx::AbstractObjectSerializerPtr getElement(unsigned int index) const;
        virtual armarx::AbstractObjectSerializerPtr getElement(const ::std::string& key) const;
        virtual std::vector<std::string> getElementNames() const;

        template <typename T>
        void getArray(const std::string& key, std::vector<T>& result)
        {
            const Json::Value& arrValue = getValue(key);

            if (!arrValue.isArray())
            {
                throw JSONInvalidDataTypeException();
            }

            result.clear();
            result.resize(arrValue.size());

            for (size_t i = 0; i < arrValue.size(); ++i)
            {
                get(arrValue[int(i)], result[int(i)]);
            }
        }

        virtual void getIntArray(const ::std::string& key, std::vector<int>& result)
        {
            getArray(key, result);
        }
        virtual void getFloatArray(const ::std::string& key, std::vector<float>& result)
        {
            getArray(key, result);
        }
        virtual void getDoubleArray(const ::std::string& key, std::vector<double>& result)
        {
            getArray(key, result);
        }
        virtual void getStringArray(const ::std::string& key, std::vector<std::string>& result)
        {
            getArray(key, result);
        }
        virtual void getVariantArray(const ::std::string& key, std::vector<armarx::VariantPtr>& result);
        virtual void getVariantMap(const ::std::string& key, armarx::StringVariantBaseMap& result);

        template <typename T>
        void set(const ::std::string& key, T val)
        {
            jsonValue[key] = Json::Value(val);
        }

        template <typename T>
        void set(int index, T val)
        {
            jsonValue[index] = Json::Value(val);
        }

        template <typename T>
        void setArray(const ::std::string& key, const std::vector<T>& val)
        {
            Json::Value arr(Json::arrayValue);

            if (val.size() > 0)
            {
                arr.resize(val.size());

                for (size_t i = 0; i < val.size(); ++i)
                {
                    arr[int(i)] = Json::Value(val[int(i)]);
                }
            }

            jsonValue[key] = arr;
        }

        virtual void setBool(const ::std::string& key, bool val)
        {
            set(key, val);
        }
        virtual void setInt(const ::std::string& key, int val)
        {
            set(key, val);
        }
        virtual void setFloat(const ::std::string& key, float val)
        {
            set(key, val);
        }
        virtual void setDouble(const ::std::string& key, double val)
        {
            set(key, val);
        }
        virtual void setString(const ::std::string& key, const std::string& val)
        {
            set(key, val);
        }
        virtual void setElement(const ::std::string& key, const armarx::AbstractObjectSerializerPtr& obj);

        virtual void setIntArray(const ::std::string& key, const std::vector<int>& val)
        {
            setArray(key, val);
        }
        virtual void setFloatArray(const ::std::string& key, const std::vector<float>& val)
        {
            setArray(key, val);
        }
        virtual void setDoubleArray(const ::std::string& key, const std::vector<double>& val)
        {
            setArray(key, val);
        }
        virtual void setStringArray(const ::std::string& key, const std::vector<std::string>& val)
        {
            setArray(key, val);
        }
        virtual void setVariantArray(const ::std::string& key, const std::vector<armarx::VariantBasePtr>& val);
        virtual void setVariantArray(const ::std::string& key, const std::vector<armarx::VariantPtr>& val);
        virtual void setVariantMap(const ::std::string& key, const armarx::StringVariantBaseMap& val);

        virtual void setBool(unsigned int index, bool val)
        {
            set(index, val);
        }
        virtual void setInt(unsigned int index, int val)
        {
            set(index, val);
        }
        virtual void setFloat(unsigned int index, float val)
        {
            set(index, val);
        }
        virtual void setDouble(unsigned int index, double val)
        {
            set(index, val);
        }
        virtual void setString(unsigned int index, const std::string& val)
        {
            set(index, val);
        }
        virtual void setElement(unsigned int index, const armarx::AbstractObjectSerializerPtr& obj);

        // add all elements of val to json as siblings
        virtual void merge(const armarx::AbstractObjectSerializerPtr& val);

        virtual void append(const armarx::AbstractObjectSerializerPtr& val);

        virtual void reset();

        static StringVariantBaseMap ConvertToBasicVariantMap(const JSONObjectPtr& serializer, const VariantBasePtr& variant);

    public:
        virtual armarx::AbstractObjectSerializerPtr createElement() const;

        std::string asString(bool pretty = false) const;

    private:
        Json::Value jsonValue;

        const Json::Value& getValue(const std::string& key) const;

        void get(const Json::Value& val, bool& result) const;
        void get(const Json::Value& val, int& result) const;
        void get(const Json::Value& val, float& result) const;
        void get(const Json::Value& val, double& result) const;
        void get(const Json::Value& val, std::string& result) const;

    };

}

#endif
