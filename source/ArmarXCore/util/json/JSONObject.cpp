/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     ALexey Kozlov ( kozlov at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include <boost/algorithm/string.hpp>

#include "JSONObject.h"

using namespace armarx;

JSONObject::JSONObject(ElementType elemType, const Ice::CommunicatorPtr ic) :
    AbstractObjectSerializer::AbstractObjectSerializer(ic)
{
    setElementType(elemType);
}

JSONObject::JSONObject(const Ice::CommunicatorPtr ic) :
    AbstractObjectSerializer::AbstractObjectSerializer(ic)
{
    setElementType(armarx::ElementTypes::eObject);
}

JSONObject::JSONObject(const JSONObject& toCopy) :
    IceUtil::Shared(toCopy),
    AbstractObjectSerializer::AbstractObjectSerializer(toCopy.ic)
{
    setElementType(toCopy.getElementType());
    setIdField(toCopy.getIdField());
}

JSONObject::~JSONObject()
{
}

std::string JSONObject::toString(const ::Ice::Current&) const
{
    return asString(false);
}

std::string JSONObject::asString(bool pretty /* = false */) const
{
    if (pretty)
    {
        return jsonValue.toStyledString();
    }
    else
    {
        Json::FastWriter writer;
        //        writer.enableYAMLCompatibility();
        std::string jsonString =  writer.write(jsonValue);

        // remove newline which is appended by jsoncpp
        // MongoDB seems to have issues with it
        while (jsonString.at(jsonString.size() - 1) == '\n')
        {
            jsonString.erase(jsonString.end() - 1);
        }

        return jsonString;
    }
}

void JSONObject::fromString(const ::std::string& jsonString, const ::Ice::Current&)
{
    setElementType(ElementTypes::eObject);
    Json::Reader reader;
    reader.parse(jsonString, jsonValue);
}

armarx::AbstractObjectSerializerPtr JSONObject::createElement() const
{
    return armarx::AbstractObjectSerializerPtr(new JSONObject(armarx::ElementTypes::eObject, ic));
}


void JSONObject::setElement(const ::std::string& key, const armarx::AbstractObjectSerializerPtr& obj)
{
    const JSONObjectPtr jsonObj = JSONObjectPtr::dynamicCast(obj);
    jsonValue[key] = jsonObj->jsonValue;
}

void JSONObject::setVariantArray(const std::string& key, const std::vector<VariantBasePtr>& val)
{
    Json::Value arr(Json::arrayValue);

    if (val.size() > 0)
    {
        arr.resize(val.size());

        for (size_t i = 0; i < val.size(); ++i)
        {
            VariantPtr var = VariantPtr::dynamicCast(val.at(i));
            AbstractObjectSerializerPtr serial = serializeVariant(var);
            const JSONObjectPtr jsonObj = JSONObjectPtr::dynamicCast(serial);
            arr[int(i)] = jsonObj->jsonValue;
        }
    }

    jsonValue[key] = arr;
}

void JSONObject::setVariantArray(const std::string& key, const std::vector<VariantPtr>& val)
{
    std::vector<VariantBasePtr> baseList;
    baseList.resize(val.size());

    for (unsigned int i = 0; i < val.size(); i++)
    {
        baseList.at(i) = val.at(i);
    }

    setVariantArray(key, baseList);
}

void JSONObject::setVariantMap(const std::string& key, const StringVariantBaseMap& val)
{
    Json::Value arr(Json::arrayValue);

    if (val.size() > 0)
    {
        arr.resize(val.size());
        size_t i = 0;

        for (StringVariantBaseMap::const_iterator it = val.begin(); it != val.end(); ++it, i++)
        {
            VariantPtr var = VariantPtr::dynamicCast(it->second);
            JSONObject json(ic);
            json.setString("key", it->first);
            json.setVariant("variant", var);
            arr[int(i)] = json.jsonValue;
        }
    }

    jsonValue[key] = arr;
}

void JSONObject::setElement(unsigned int index, const armarx::AbstractObjectSerializerPtr& obj)
{
    const JSONObjectPtr jsonObj = JSONObjectPtr::dynamicCast(obj);
    jsonValue[index] = jsonObj->jsonValue;
}

void JSONObject::append(const armarx::AbstractObjectSerializerPtr& obj)
{
    const JSONObjectPtr jsonObj = JSONObjectPtr::dynamicCast(obj);

    if (jsonValue.isArray())
    {
        jsonValue.append(jsonObj->jsonValue);
    }
    else
    {
        throw JSONInvalidDataTypeException();
    }
}

void JSONObject::merge(const armarx::AbstractObjectSerializerPtr& obj)
{
    if (getElementType() == armarx::ElementTypes::eObject && obj->getElementType() == armarx::ElementTypes::eObject)
    {
        const std::vector<std::string> elemNames = obj->getElementNames();

        for (std::vector<std::string>::const_iterator it = elemNames.begin(); it != elemNames.end(); ++it)
        {
            set(*it, obj->getElement(*it));
        }
    }
    else
    {
        throw JSONInvalidDataTypeException();
    }
}

unsigned int JSONObject::size() const
{
    return jsonValue.size();
}

bool JSONObject::hasElement(const ::std::string& key) const
{
    return jsonValue.isMember(key);
}

void JSONObject::get(const Json::Value& val, bool& result) const
{
    if (!val.isBool())
    {
        throw JSONInvalidDataTypeException();
    }
    else
    {
        result = val.asBool();
    }
}

void JSONObject::get(const Json::Value& val, int& result) const
{
    if (!val.isIntegral())
    {
        throw JSONInvalidDataTypeException();
    }
    else
    {
        result = val.asInt();
    }
}

void JSONObject::get(const Json::Value& val, float& result) const
{
    if (!val.isNumeric())
    {
        throw JSONInvalidDataTypeException();
    }
    else
    {
        result = val.asFloat();
    }
}

void JSONObject::get(const Json::Value& val, double& result) const
{
    if (!val.isNumeric())
    {
        throw JSONInvalidDataTypeException();
    }
    else
    {
        result = val.asDouble();
    }
}

void JSONObject::get(const Json::Value& val, std::string& result) const
{
    if (!val.isString())
    {
        throw JSONInvalidDataTypeException();
    }
    else
    {
        result = val.asString();
    }
}

const Json::Value& JSONObject::getValue(const std::string& key) const
{
    if (key.find_first_of('.') > 0)
    {
        // the following code allows to get child fields using dot notation,
        // e.g "position.mean.x"
        std::vector<std::string> keys;
        boost::split(keys, key,  boost::is_any_of("."));
        Json::Value const* result = &jsonValue[keys[0]];

        for (size_t i = 1; i < keys.size(); ++i)
            if (result->isObject())
            {
                result = &((*result)[keys[i]]);
            }
            else
            {
                throw JSONInvalidFieldException(key);
            }

        return *result;
    }
    else
    {
        return jsonValue[key];
    }
}

float JSONObject::getFloat(const ::std::string& key) const
{
    const Json::Value& val = getValue(key);

    if (!val)
    {
        throw JSONInvalidFieldException(key);
    }
    else if (!val.isNumeric())
    {
        throw JSONInvalidDataTypeException();
    }
    else
    {
        return val.asFloat();
    }
}

double JSONObject::getDouble(const ::std::string& key) const
{
    const Json::Value& val = getValue(key);

    if (!val)
    {
        throw JSONInvalidFieldException(key);
    }
    else if (!val.isNumeric())
    {
        throw JSONInvalidDataTypeException();
    }
    else
    {
        return val.asDouble();
    }
}

int JSONObject::getInt(const ::std::string& key) const
{
    const Json::Value& val = getValue(key);

    if (!val)
    {
        throw JSONInvalidFieldException(key);
    }
    else if (!val.isInt())
    {
        throw JSONInvalidDataTypeException();
    }
    else
    {
        return val.asInt();
    }
}

bool JSONObject::getBool(const ::std::string& key) const
{
    const Json::Value& val = getValue(key);

    if (!val)
    {
        throw JSONInvalidFieldException(key);
    }
    else if (!val.isBool())
    {
        throw JSONInvalidDataTypeException();
    }
    else
    {
        return val.asBool();
    }
}

std::string JSONObject::getString(const ::std::string& key) const
{
    const Json::Value& val = getValue(key);

    if (!val)
    {
        ARMARX_IMPORTANT_S << "Invalid Field Exception for: " << key << " of object: \n" << this->asString(true);
        throw JSONInvalidFieldException(key);
    }
    else if (!val.isString())
    {
        throw JSONInvalidDataTypeException();
    }
    else
    {
        return val.asString();
    }
}

armarx::AbstractObjectSerializerPtr JSONObject::getElement(unsigned int index) const
{
    if (!jsonValue.isArray())
    {
        throw JSONInvalidDataTypeException();
    }
    else if (index >= jsonValue.size())
    {
        throw JSONInvalidArrayIndexException();
    }
    else
    {
        JSONObjectPtr elem = new JSONObject(armarx::ElementTypes::eObject, ic);
        elem->jsonValue = jsonValue[index];
        return elem;
    }
}

armarx::AbstractObjectSerializerPtr JSONObject::getElement(const std::string& key) const
{
    if (!jsonValue.isObject())
    {
        throw JSONInvalidDataTypeException();
    }
    else if (!jsonValue.isMember(key))
    {
        throw JSONInvalidFieldException(key);
    }
    else
    {
        JSONObjectPtr elem = new JSONObject(armarx::ElementTypes::eObject, ic);
        elem->jsonValue = jsonValue[key];
        return elem;
    }
}

std::vector<std::string> JSONObject::getElementNames() const
{
    if (!jsonValue.isObject())
    {
        throw JSONInvalidDataTypeException();
    }
    else
    {
        return jsonValue.getMemberNames();
    }
}

void JSONObject::getVariantArray(const std::string& key, std::vector<VariantPtr>& result)
{
    const Json::Value& arrValue = getValue(key);

    if (!arrValue.isArray())
    {
        throw JSONInvalidDataTypeException();
    }

    result.clear();
    result.resize(arrValue.size());

    for (size_t i = 0; i < arrValue.size(); ++i)
    {
        JSONObjectPtr json = JSONObjectPtr::dynamicCast(createElement());
        json->jsonValue =  arrValue[int(i)];
        VariantPtr var = json->deserializeVariant();
        result.at(i) = var;
    }
}

void JSONObject::getVariantMap(const std::string& key, StringVariantBaseMap& result)
{
    const Json::Value& arrValue = getValue(key);

    if (!arrValue.isArray())
    {
        throw JSONInvalidDataTypeException();
    }

    result.clear();

    for (size_t i = 0; i < arrValue.size(); ++i)
    {
        JSONObjectPtr json = JSONObjectPtr::dynamicCast(createElement());
        json->jsonValue =  arrValue[int(i)];
        VariantPtr var = json->getVariant("variant");
        result[json->getString("key")] = var;
    }
}

armarx::ElementType JSONObject::getElementType(const ::Ice::Current&) const
{
    if (jsonValue.isArray())
    {
        return armarx::ElementTypes::eArray;
    }
    else if (jsonValue.isObject())
    {
        return armarx::ElementTypes::eObject;
    }
    else
    {
        return armarx::ElementTypes::eScalar;
    }
}

void JSONObject::setElementType(armarx::ElementType elementType, const ::Ice::Current&)
{
    switch (elementType)
    {
        case armarx::ElementTypes::eScalar:
            jsonValue = Json::Value();
            break;

        case armarx::ElementTypes::eArray:
            jsonValue = Json::Value(Json::arrayValue);
            break;

        case armarx::ElementTypes::eObject:
        default:
            jsonValue = Json::Value(Json::objectValue);
            break;
    }
}

void JSONObject::reset()
{
    jsonValue.clear();
}

StringVariantBaseMap armarx::JSONObject::ConvertToBasicVariantMap(const JSONObjectPtr& serializer, const VariantBasePtr& variant)
{
    JSONObjectPtr var ;

    if (variant)
    {
        var = JSONObjectPtr::dynamicCast(serializer->serializeVariant(VariantPtr::dynamicCast(variant)));
    }
    else
    {
        var = serializer;
    }

    StringVariantBaseMap result;

    for (size_t i = 0; i < var->size(); i++)
    {
        //        JSONObjectPtr elem = JSONObjectPtr::dynamicCast(var->getElement(i));
        auto names = var->getElementNames();

        for (auto name : names)
        {
            JSONObjectPtr elem = JSONObjectPtr::dynamicCast(var->getElement(name));

            if (elem->getElementType() == armarx::ElementTypes::eScalar)
            {
                if (elem->jsonValue.isBool())
                {
                    result[name] = new Variant(var->getBool(name));
                }
                else if (elem->jsonValue.isString())
                {
                    result[name] = new Variant(var->getString(name));
                }
                else if (elem->jsonValue.isDouble())
                {
                    result[name] = new Variant(var->getDouble(name));
                }
                else if (elem->jsonValue.isInt())
                {
                    result[name] = new Variant(var->getInt(name));
                }
                else
                {
                    throw LocalException() << "Unhandled type in variant to Dict conversion: " << elem->jsonValue.type();
                }
            }
            else
            {
                auto subResult = ConvertToBasicVariantMap(elem, NULL);

                for (auto & entry : subResult)
                {
                    result[name + "." + entry.first] = entry.second;
                }
            }
        }
    }

    return result;
}
