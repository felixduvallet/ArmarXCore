/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Kai Welke (welke@kit.edu)
* @date       2011 Kai Welke
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef _ARMARX_CORE_CHANNEL_REF_H
#define _ARMARX_CORE_CHANNEL_REF_H

#include <boost/utility/enable_if.hpp>
#include <boost/type_traits/is_base_of.hpp>

#include <ArmarXCore/core/system/ImportExport.h>
#include <ArmarXCore/interface/observers/ObserverInterface.h>
#include <ArmarXCore/interface/observers/ChannelRefBase.h>
#include <ArmarXCore/observers/variant/DataFieldIdentifier.h>
#include <ArmarXCore/observers/variant/Variant.h>
#include <ArmarXCore/observers/Observer.h>
#include <ArmarXCore/observers/exceptions/local/InvalidDataFieldException.h>

namespace armarx
{
    class ChannelRef;
    typedef IceInternal::Handle<ChannelRef> ChannelRefPtr;

    /**
     * @class ChannelRef
     * @ingroup VariantsGrp
     * @brief The ChannelRef class is a reference to a channel on an \ref Observer.
     * It is used to access data directly from a channel or to be passed to function
     * as an identifier for a channel.
     *
     */
    class ARMARXCORE_IMPORT_EXPORT ChannelRef :
        virtual public ChannelRefBase
    {
        template <class BaseClass, class VariantClass>
        friend class GenericFactory;
    protected:
        ChannelRef() :
            validationTimeout(4000),
            waitIntervallMs(10)
        {
            initialized = false;
        }


    public:
        ChannelRef(Observer* observer, std::string channelName);
        ChannelRef(ObserverInterfacePrx observerPrx, std::string channelName);
        virtual ~ChannelRef() {}

        // inherited from VariantDataClass
        Ice::ObjectPtr ice_clone() const
        {
            return this->clone();
        }

        VariantDataClassPtr clone(const Ice::Current& c = ::Ice::Current()) const;
        std::string output(const Ice::Current& c = ::Ice::Current()) const;
        VariantTypeId getType(const Ice::Current& c = ::Ice::Current()) const;
        bool validate(const Ice::Current& c = ::Ice::Current());

        /**
         * @brief Implementation of virtual function to read a ChannelRef from an XML-file.
         *
         * Example xml-layout:
         * @code
         *      <observerName>HeadKinematicUnit</observerName>
         *      <channelName>jointAngles</channelName>
         * @endcode

         * @param xmlData String with xml-data. NOT a file path!
         * @param c <b>Important: </b>You have to set the c.adapter pointer so that this function
         * can create the observer proxy. If not set this function will throw a
         * LocalException! See InputParameterLoader::load() for an example how to set it.
         *
         * @code
         * Ice::CommunicatorPtr ic = getIceManager()->ic();
         * Ice::Current c;
         * c.adapter = ic->createObjectAdapterWithEndpoints("Dummy"+IceUtil::generateUUID(), "tcp");
         * @endcode
         *
         * @throw LocalException
         * @return ErrorCode, 1 on Success
         */
        int readFromXML(const std::string& xmlData, const Ice::Current& c = ::Ice::Current());
        std::string writeAsXML(const Ice::Current&);

        // datafield access
        DataFieldIdentifierPtr getDataFieldIdentifier(std::string datafieldName);
        VariantPtr getDataField(std::string datafieldName);
        StringSequence getDataFieldNames() const;
        bool hasDatafield(const std::string& datafieldName) const;
        bool getInitialized();
        void refetchChannel();
        // helper methods for variant content access
        template<typename T>
        typename boost::enable_if<boost::is_base_of<VariantDataClass, T>, IceInternal::Handle<T> >::type
        get(const std::string& key)
        {
            VariantPtr var = getDataField(key);

            if (!var)
            {
                throw armarx::exceptions::local::InvalidDataFieldException(getChannelName(), key);
            }

            return var->get<T>();
        }

        template<typename T>
        typename boost::disable_if<boost::is_base_of<VariantDataClass, T>, T >::type
        get(const std::string& key)
        {
            VariantPtr var = getDataField(key);

            if (!var)
            {
                throw armarx::exceptions::local::InvalidDataFieldException(getChannelName(), key);
            }

            return var->get<T>();
        }

        // channel access
        std::string getChannelName() const;

        // observer access
        std::string getObserverName() const;
        ObserverInterfacePrx getObserverProxy();

        /**
        * stream operator for DataFieldIdentifier
        */
        friend std::ostream& operator<<(std::ostream& stream, const ChannelRef& rhs)
        {
            stream << rhs.output();
            return stream;
        }

        /**
        * stream operator for Ice shared pointer of DataFieldIdentifier
        */
        friend std::ostream& operator<<(std::ostream& stream, const ChannelRefPtr& rhs)
        {
            stream << rhs->output();
            return stream;
        }

    public: // serialization
        virtual void serialize(const ObjectSerializerBasePtr& serializer, const ::Ice::Current& = ::Ice::Current()) const;
        virtual void deserialize(const ObjectSerializerBasePtr& serializer, const ::Ice::Current& = ::Ice::Current());

    private:
        bool assureProxy();
        void initializeDataFields();

        Ice::CommunicatorPtr communicator;
        const IceUtil::Int64 validationTimeout;
        const int waitIntervallMs;
    };

    namespace VariantType
    {
        const VariantTypeId ChannelRef = Variant::addTypeName("::armarx::ChannelRefBase");
    }
}


#endif
