/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Kai Welke (welke@kit.edu)
* @date       2012 Kai Welke
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include <ArmarXCore/observers/variant/ChannelRef.h>

// boost includes
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/lexical_cast.hpp>
#include <algorithm>
#include <ArmarXCore/observers/exceptions/local/InvalidDataFieldException.h>
#include <ArmarXCore/core/exceptions/local/ProxyNotInitializedException.h>
#include <ArmarXCore/observers/AbstractObjectSerializer.h>
#include <ArmarXCore/statechart/Exception.h>

#include <Ice/ObjectAdapter.h>
#include <Ice/ObjectFactory.h>

using namespace armarx;
using namespace armarx::exceptions::local;

// *******************************************************
// Construction / destruction
// *******************************************************
ChannelRef::ChannelRef(Observer* observer, std::string channelName) :
    validationTimeout(4000),
    waitIntervallMs(10)
{
    if (!observer)
    {
        ARMARX_FATAL_S << "observer must not be NULL";
        throw exceptions::local::eNullPointerException("observer must not be NULL");
    }

    this->observerProxy = ObserverInterfacePrx::uncheckedCast(observer->getProxy());
    this->observerName = observer->getName();
    this->channelName = channelName;

    // initialize data fields and initialized state
    initializeDataFields();
}

ChannelRef::ChannelRef(ObserverInterfacePrx observerPrx, std::string channelName) :
    validationTimeout(4000),
    waitIntervallMs(10)
{
    if (!observerPrx)
    {
        throw exceptions::local::eNullPointerException("observerPrx must not be NULL");
    }

    this->observerProxy = observerPrx;
    this->observerName = observerPrx->ice_getIdentity().name;
    this->channelName = channelName;

    // initialize data fields and initialized state
    initializeDataFields();
}

// *******************************************************
// Datafield access
// *******************************************************
DataFieldIdentifierPtr ChannelRef::getDataFieldIdentifier(std::string datafieldName)
{
    if (!assureProxy())
    {
        throw ProxyNotInitializedException(observerName);
    }

    if (std::find(datafieldNames.begin(), datafieldNames.end(), datafieldName) == datafieldNames.end())
    {
        refetchChannel();

        if (std::find(datafieldNames.begin(), datafieldNames.end(), datafieldName) == datafieldNames.end())
        {
            throw InvalidDataFieldException(channelName, datafieldName);
        }
    }

    return new DataFieldIdentifier(observerName, channelName, datafieldName);
}

VariantPtr ChannelRef::getDataField(std::string datafieldName)
{
    if (!assureProxy())
    {
        throw ProxyNotInitializedException(observerName);
    }

    return VariantPtr::dynamicCast(observerProxy->getDataField(getDataFieldIdentifier(datafieldName)));
}

// *******************************************************
// Properties
// *******************************************************
ObserverInterfacePrx ChannelRef::getObserverProxy()
{
    if (!assureProxy())
    {
        throw ProxyNotInitializedException(observerName);
    }

    return observerProxy;
}

std::string ChannelRef::getObserverName() const
{
    return observerName;
}

std::string ChannelRef::getChannelName() const
{
    return channelName;
}

StringSequence ChannelRef::getDataFieldNames() const
{
    return datafieldNames;
}

bool ChannelRef::hasDatafield(const std::string& datafieldName) const
{
    for (unsigned int i = 0; i < datafieldNames.size(); ++i)
    {
        if (datafieldName == datafieldNames.at(i))
        {
            return true;
        }
    }

    return false;
}

bool ChannelRef::getInitialized()
{
    // once all datafields are initialized -> channel initialized
    if (initialized)
    {
        return true;
    }

    // if proxy not there -> not initialized
    if (!assureProxy())
    {
        return false;
    }

    ARMARX_INFO_S << "ChannelRef:" << getObserverName() << "." << getChannelName() << ": Initialized as retrieved from proxy : " << initialized;

    return initialized;
}

void ChannelRef::refetchChannel()
{
    initializeDataFields();
}


// *******************************************************
// Inherited from VariantDataClass
// *******************************************************
VariantDataClassPtr ChannelRef::clone(const Ice::Current& c) const
{
    ChannelRefPtr cln = new ChannelRef(*this);

    cln->initialized = this->initialized;

    cln->observerName = this->observerName;
    cln->channelName = this->channelName;
    cln->datafieldNames = this->datafieldNames;

    cln->observerProxy = this->observerProxy;

    return cln;
}

std::string ChannelRef::output(const Ice::Current& c) const
{
    std::stringstream s;
    s << "Reference to channel " << channelName << " on observer " << observerName;

    return s.str();
}

VariantTypeId ChannelRef::getType(const Ice::Current& c) const
{
    return VariantType::ChannelRef;
}

bool ChannelRef::validate(const Ice::Current& c)
{
    int validateWaitIntervallMs = waitIntervallMs;
    IceUtil::Time waitStartTime = IceUtil::Time::now();

    IceUtil::Time timeout = IceUtil::Time::milliSeconds(validationTimeout);

    IceUtil::Time lastOutput = IceUtil::Time::milliSeconds(0);

    while (IceUtil::Time(waitStartTime + timeout - IceUtil::Time::now()).toMilliSeconds() > 0)
    {
        if (getInitialized())
        {
            return true;
        }

        if (IceUtil::Time(IceUtil::Time::now() - lastOutput).toSeconds() >= 1) // output only every second
        {
            ARMARX_VERBOSE_S << "Waiting for ChannelRef:  " << getObserverName() << "." << getChannelName() << flush;
            lastOutput = IceUtil::Time::now();
        }

        usleep(validateWaitIntervallMs * 1000);

        if (validateWaitIntervallMs < validationTimeout * 0.5f) // increase wait time for lower cpu usage
        {
            validateWaitIntervallMs *= 2;
        }
    }

    ARMARX_WARNING_S << "Could not validate ChannelRef:  " << getObserverName() << "." << getChannelName() << flush;
    return false;

}

// private
bool ChannelRef::assureProxy()
{
    if (!observerProxy)
    {
        try
        {
            observerProxy = ObserverInterfacePrx::checkedCast(communicator->stringToProxy(observerName));
        }
        catch (...)
        {
        }
    }

    if (observerProxy && !initialized)
    {
        initializeDataFields();
    }

    return (observerProxy != 0);
}

void ChannelRef::initializeDataFields()
{
    ChannelRegistry channels = observerProxy->getAvailableChannels(true);

    if (channels.find(channelName) == channels.end())
    {
        ARMARX_ERROR_S << "ChannelRef:" << getObserverName() << "." << getChannelName() << " not registered?!";
        std::stringstream ss;
        ss << "No channel with name " <<  channelName << " registered in observerProxy";
        throw LocalException(ss.str());
    }

    ChannelRegistryEntry channel = channels[channelName];

    DataFieldRegistry::const_iterator iterData = channel.dataFields.begin();
    datafieldNames.clear();

    while (iterData != channel.dataFields.end())
    {
        datafieldNames.push_back(iterData->second.identifier->datafieldName);
        iterData++;
    }

    initialized = channel.initialized;
}



int ChannelRef::readFromXML(const std::string& xmlData, const Ice::Current& c)
{
    using namespace boost::property_tree;
    ptree pt;
    std::stringstream stream;
    stream << xmlData;
    xml_parser::read_xml(stream, pt);


    observerName = (pt.get<std::string>("observerName"));
    channelName = (pt.get<std::string>("channelName"));

    if (!c.adapter)
    {
        throw LocalException("The adapter in the Ice::current object must be set to load a ChannelRef from XML");
    }

    communicator = c.adapter->getCommunicator();

    return 1;
}

std::string ChannelRef::writeAsXML(const Ice::Current&)
{
    using namespace boost::property_tree;
    ptree pt;
    pt.add("observerName", observerName);
    pt.add("channelName", channelName);

#if BOOST_VERSION >= 105600
    boost::property_tree::xml_parser::xml_writer_settings<std::string> settings('\t', 1);
#else
    boost::property_tree::xml_parser::xml_writer_settings<char> settings('\t', 1);
#endif

    std::stringstream stream;
    xml_parser::write_xml(stream, pt, settings);
    return stream.str();
}

void ChannelRef::serialize(const ObjectSerializerBasePtr& serializer, const ::Ice::Current&) const
{
    AbstractObjectSerializerPtr obj = AbstractObjectSerializerPtr::dynamicCast(serializer);

    obj->setString("observerName", observerName);
    obj->setString("channelName", channelName);
}

void ChannelRef::deserialize(const ObjectSerializerBasePtr& serializer, const ::Ice::Current& c)
{
    AbstractObjectSerializerPtr obj = AbstractObjectSerializerPtr::dynamicCast(serializer);

    observerName = obj->getString("observerName");
    channelName = obj->getString("channelName");

    if (!c.adapter)
    {
        throw LocalException("The adapter in the Ice::current object must be set to load a ChannelRef from XML");
    }

    communicator = c.adapter->getCommunicator();
}

