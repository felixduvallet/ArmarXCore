/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core
* @author     Kai Welke (welke@kit.edu)
* @date       2011
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef _ARMARX_CORE_VARIANTCONTAINER_H
#define _ARMARX_CORE_VARIANTCONTAINER_H

// ArmarX
#include <ArmarXCore/core/system/ImportExport.h>
#include <ArmarXCore/interface/observers/VariantContainers.h>
#include <ArmarXCore/interface/observers/VariantBase.h>
#include <ArmarXCore/core/system/SafeShared.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/observers/variant/Variant.h>

// Ice
#include <IceUtil/Handle.h>

// C++
#include <string>
#include <map>

// boost
#include <boost/utility/enable_if.hpp>
#include <boost/type_traits/is_base_of.hpp>




namespace armarx
{
    class SingleVariant;
    typedef IceInternal::Handle<SingleVariant> SingleVariantPtr;

    class VariantContainer : virtual public VariantContainerBase
    {
    public:
        ContainerTypePtr getContainerType(const Ice::Current& c = ::Ice::Current()) const;
        void setContainerType(const ContainerTypePtr& containerType, const Ice::Current& c = ::Ice::Current());

    };


    class SingleVariant :
        virtual public SingleVariantBase
    {
    public:
        SingleVariant();
        SingleVariant(const Variant& variant);
        SingleVariant(const SingleVariant& source);
        SingleVariant& operator=(const SingleVariant& source);
        VariantContainerBasePtr cloneContainer(const::Ice::Current& = Ice::Current()) const;
        VariantBasePtr getElementBase(const Ice::Current& = Ice::Current()) const;
        VariantPtr get() const;
        void setVariant(const VariantPtr& variant);

        int getSize(const::Ice::Current& = Ice::Current()) const
        {
            return 1;
        }
        ContainerTypePtr getContainerType(const Ice::Current& c = ::Ice::Current()) const;
        void setContainerType(const ContainerTypePtr& containerType, const Ice::Current& c = ::Ice::Current());
        static VariantTypeId getStaticType(const Ice::Current& c = ::Ice::Current());
        //        int getSubType(const::Ice::Current & = Ice::Current()) const { return subType;}
        void clear(const::Ice::Current& = Ice::Current()) { }
        bool validateElements(const::Ice::Current& = Ice::Current())
        {
            return element->validate();
        }

    public: // serialization
        virtual void serialize(const ObjectSerializerBasePtr& serializer, const ::Ice::Current& = ::Ice::Current()) const;
        virtual void deserialize(const ObjectSerializerBasePtr& serializer, const ::Ice::Current& = ::Ice::Current());
        int readFromXML(const std::string& xmlString, const Ice::Current& c = ::Ice::Current());

        template<class T>
        static T fromString(const std::string& s)
        {
            std::istringstream stream(s);
            T t;
            stream >> t;
            return t;
        }

        static std::string getTypePrefix()
        {
            return "::armarx::SingleVariantBase";
        }

        // VariantContainerBase interface
    public:
        std::string toString(const Ice::Current& c);
    };

    class ContainerDummy : virtual public VariantContainerBase
    {
    public:
        ContainerDummy();
        ContainerDummy(const ContainerDummy& source);
        VariantContainerBasePtr cloneContainer(const Ice::Current& = ::Ice::Current()) const;
        ContainerTypePtr getContainerType(const Ice::Current&   = ::Ice::Current()) const
        {
            return typeContainer;
        }
        void setContainerType(const ContainerTypePtr& containerType, const Ice::Current& = ::Ice::Current())
        {
            typeContainer = containerType;
        }
        void clear(const Ice::Current& = ::Ice::Current()) {}
        int getSize(const Ice::Current& = ::Ice::Current()) const
        {
            return 0;
        }
        bool validateElements(const Ice::Current& = ::Ice::Current())
        {
            return true;
        }
    public: // serialization
        virtual void serialize(const ObjectSerializerBasePtr& serializer, const ::Ice::Current& = ::Ice::Current()) const {}
        virtual void deserialize(const ObjectSerializerBasePtr& serializer, const ::Ice::Current& = ::Ice::Current()) {}
        int readFromXML(const std::string&, const Ice::Current& c = ::Ice::Current())
        {
            return 1;
        }

        // VariantContainerBase interface
    public:
        std::string toString(const Ice::Current& c);
    };

    class ContainerTypeI : virtual public ContainerType
    {
    public:
        ContainerTypeI() {}
        ContainerTypeI(VariantTypeId variantType);
        ContainerTypePtr clone(const Ice::Current& = Ice::Current()) const;
    };
    typedef IceInternal::Handle<ContainerTypeI> ContainerTypeIPtr;


    class VariantContainerType
    {
    public:
        VariantContainerType(std::string containerType);
        const ContainerTypeI operator()(VariantTypeId typeId) const;
        const ContainerTypeI operator()(const ContainerType& subType) const;
        static bool compare(const ContainerTypePtr& type1, const ContainerTypePtr& secondType);
        static std::string allTypesToString(const ContainerTypePtr& type);
        static ContainerTypePtr FromString(const std::string& typeStr);
    private:
        std::string containerType;
        ContainerTypePtr thisType;
    };

    namespace VariantType
    {
        const VariantContainerType SingleVariantContainer = VariantContainerType(SingleVariant::getTypePrefix());
    }

}



#endif
