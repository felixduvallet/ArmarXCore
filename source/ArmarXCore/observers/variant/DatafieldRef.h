/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Kai Welke (welke@kit.edu)
* @date       2011 Kai Welke
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef _ARMARX_CORE_DATAFIELD_REF_H
#define _ARMARX_CORE_DATAFIELD_REF_H

#include <boost/utility/enable_if.hpp>
#include <boost/type_traits/is_base_of.hpp>

#include <ArmarXCore/core/system/ImportExport.h>
#include <ArmarXCore/interface/observers/ObserverInterface.h>
#include <ArmarXCore/interface/observers/DataFieldIdentifierBase.h>
#include <ArmarXCore/observers/variant/DataFieldIdentifier.h>
#include <ArmarXCore/observers/variant/Variant.h>
#include <ArmarXCore/observers/variant/ChannelRef.h>
#include <ArmarXCore/observers/Observer.h>
#include <ArmarXCore/observers/exceptions/local/InvalidDataFieldException.h>

namespace armarx
{


    class DatafieldRef;
    typedef IceInternal::Handle<DatafieldRef> DatafieldRefPtr;

    /**
     * @class DatafieldRef
     * @ingroup VariantsGrp
     * @brief The DatafieldRef class is similar to the \ref ChannelRef, but points
     * to a specific Datafield instead of to a complete channel.
     * It can be used to query (@ref getDataField()) the data of the datafield from the \ref Observer.
     */
    class ARMARXCORE_IMPORT_EXPORT DatafieldRef :
        virtual public DatafieldRefBase

    {
        template <class BaseClass, class VariantClass>
        friend class GenericFactory;
    protected:
        DatafieldRef();
    public:
        DatafieldRef(Observer* observer, const std::string& channelName, const std::string& datafieldName, bool performValidation = true);

        DatafieldRef(ObserverInterfacePrx observerPrx, const std::string& channelName, const std::string& datafieldName, bool performValidation = true);
        DatafieldRef(ChannelRefPtr channelRef, const std::string& datafieldName, bool performValidation = true);
        virtual ~DatafieldRef() {}

        // inherited from VariantDataClass
        Ice::ObjectPtr ice_clone() const
        {
            return this->clone();
        }

        VariantDataClassPtr clone(const Ice::Current& c = ::Ice::Current()) const;
        std::string output(const Ice::Current& c = ::Ice::Current()) const;
        VariantTypeId getType(const Ice::Current& c = ::Ice::Current()) const;
        bool validate(const Ice::Current& c = ::Ice::Current());

        /**
         * @brief Implementation of virtual function to read a DatafieldRef from an XML-file.
         *
         * Example xml-layout:
         * @code
         *      <observerName>HeadKinematicUnit</observerName>
         *      <channelName>jointAngles</channelName>
         * @endcode

         * @param xmlData String with xml-data. NOT a file path!
         * @param c <b>Important: </b>You have to set the c.adapter pointer so that this function
         * can create the observer proxy. If not set this function will throw a
         * LocalException! See InputParameterLoader::load() for an example how to set it.
         *
         * @code
         * Ice::CommunicatorPtr ic = getIceManager()->ic();
         * Ice::Current c;
         * c.adapter = ic->createObjectAdapterWithEndpoints("Dummy"+IceUtil::generateUUID(), "tcp");
         * @endcode
         *
         * @throw LocalException
         * @return ErrorCode, 1 on Success
         */
        int readFromXML(const std::string& xmlData, const Ice::Current& c = ::Ice::Current());
        std::string writeAsXML(const Ice::Current&);

        // datafield access
        DataFieldIdentifierPtr getDataFieldIdentifier() const;

        /**
         * @brief Retrieves the value of the datafield from the \ref Observer.
         * @return Variant with the type and value of the datafield.
         */
        VariantPtr getDataField() const;


        // helper methods for variant content access
        template<typename T>
        typename boost::enable_if<boost::is_base_of<VariantDataClass, T>, IceInternal::Handle<T> >::type
        get()
        {
            VariantPtr var = getDataField();

            if (!var)
            {
                throw armarx::exceptions::local::InvalidDataFieldException(channelRef->channelName, datafieldName);
            }

            return var->get<T>();
        }

        template<typename T>
        typename boost::disable_if<boost::is_base_of<VariantDataClass, T>, T >::type
        get()
        {
            VariantPtr var = getDataField();

            if (!var)
            {
                throw armarx::exceptions::local::InvalidDataFieldException(channelRef->channelName, datafieldName);
            }

            return var->get<T>();
        }

        ChannelRefPtr getChannelRef() const;

        /**
        * stream operator for DataFieldIdentifier
        */
        friend std::ostream& operator<<(std::ostream& stream, const DatafieldRef& rhs)
        {
            stream << rhs.output();
            return stream;
        }

        /**
        * stream operator for Ice shared pointer of DataFieldIdentifier
        */
        friend std::ostream& operator<<(std::ostream& stream, const DatafieldRefPtr& rhs)
        {
            stream << rhs->output();
            return stream;
        }

    public: // serialization
        virtual void serialize(const ObjectSerializerBasePtr& serializer, const ::Ice::Current& = ::Ice::Current()) const;
        virtual void deserialize(const ObjectSerializerBasePtr& serializer, const ::Ice::Current& = ::Ice::Current());

    private:
        mutable DataFieldIdentifierPtr id;
    };

    namespace VariantType
    {
        const VariantTypeId DatafieldRef = Variant::addTypeName("::armarx::DatafieldRefBase");
    }
}


#endif
