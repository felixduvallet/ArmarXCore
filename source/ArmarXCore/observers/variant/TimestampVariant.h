#ifndef TIMESTAMPVARIANT_H
#define TIMESTAMPVARIANT_H

#include <ArmarXCore/core/system/ImportExport.h>
#include <ArmarXCore/observers/variant/Variant.h>

#include <ArmarXCore/interface/observers/Timestamp.h>
#include <IceUtil/Time.h>

namespace armarx
{
    namespace VariantType
    {
        // Variant types
        const VariantTypeId Timestamp = Variant::addTypeName("::armarx::TimestampBase");
    }

    class TimestampVariant;
    typedef IceInternal::Handle<TimestampVariant> TimestampVariantPtr;

    /**
     * @class TimestampVariant
     * @ingroup VariantsGrp
     * Implements a Variant type for timestamps. The timestamp value is interpreted as microseconds since the Unix Epoch.
     * Internally the class bases on the IceUtil::Time functionality of Ice.
     *
     * For information on how to use this type, refer to Variant.
     */
    class ARMARXCORE_IMPORT_EXPORT TimestampVariant : virtual public TimestampBase
    {
    public:
        TimestampVariant();

        /**
         * Construct a timestamp Variant from an initialization value.
         *
         * @param timestamp         Initialization timestamp in microseconds since the Unix Epoch
         */
        TimestampVariant(long timestamp);
        TimestampVariant(IceUtil::Time time);

        long getTimestamp();


        // inherited from VariantDataClass
        Ice::ObjectPtr ice_clone() const
        {
            return this->clone();
        }
        VariantDataClassPtr clone(const Ice::Current& c = ::Ice::Current()) const
        {
            return new TimestampVariant(*this);
        }
        std::string output(const Ice::Current& c = ::Ice::Current()) const
        {
            std::stringstream s;
            s << IceUtil::Time::microSeconds(timestamp).toDateTime();
            return s.str();
        }
        VariantTypeId getType(const Ice::Current& c = ::Ice::Current()) const
        {
            return VariantType::Timestamp;
        }
        bool validate(const Ice::Current& c = ::Ice::Current())
        {
            return true;
        }

        /**
         * @brief Implementation of virtual function to read a MatrixFloat from an XML-file.
         *        Example xml-layout:
         * @code
         *      <x>0</x>
         *      <y>0</y>
         *      <z>0</z>
         * @endcode

         * @param xmlData String with xml-data. NOT a file path!
         * @return ErrorCode, 1 on Success
         */
        int readFromXML(const std::string& xmlData, const Ice::Current& c = ::Ice::Current());
        std::string writeAsXML(const Ice::Current& c = ::Ice::Current());

        friend std::ostream& operator<<(std::ostream& stream, const TimestampVariant& rhs)
        {
            stream << "TimestampVariant: " << std::endl << rhs.output() << std::endl;
            return stream;
        }

        static TimestampVariantPtr nowPtr()
        {
            return new TimestampVariant(IceUtil::Time::now());
        }

        IceUtil::Time toTime()
        {
            return IceUtil::Time::microSeconds(timestamp);
        }

        static long nowLong()
        {
            return IceUtil::Time::now().toMicroSeconds();
        }

    public:
        // serialization
        virtual void serialize(const armarx::ObjectSerializerBasePtr& serializer, const ::Ice::Current& = ::Ice::Current()) const;
        virtual void deserialize(const armarx::ObjectSerializerBasePtr& serializer, const ::Ice::Current& = ::Ice::Current());


    };



}

#endif
