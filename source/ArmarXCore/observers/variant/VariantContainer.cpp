/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Kai Welke (welke at kit dot edu)
 * @date       2011
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include <ArmarXCore/observers/variant/VariantContainer.h>
#include <ArmarXCore/observers/variant/DataFieldIdentifier.h>
#include <ArmarXCore/observers/exceptions/user/InvalidTypeException.h>
#include <ArmarXCore/observers/exceptions/user/UnknownTypeException.h>
#include <ArmarXCore/observers/AbstractObjectSerializer.h>

// boost includs
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/regex.hpp>
#include <boost/algorithm/string/replace.hpp>

// Ice Includes
#include <IceUtil/UUID.h>
#include <Ice/ObjectAdapter.h>
#include <Ice/ObjectFactory.h>

namespace armarx
{






    SingleVariant::SingleVariant()
    {
        element = new Variant();

        typeContainer = new ContainerTypeI();
        typeContainer->typeId = Variant::typeToString(VariantType::Invalid); //Variant::addTypeName(Variant::typeToString(subType));
    }

    SingleVariant::SingleVariant(const Variant& variant)
    {
        element = new Variant(variant);

        typeContainer = new ContainerTypeI();
        typeContainer->typeId = Variant::typeToString(variant.getType()); //Variant::addTypeName(Variant::typeToString(subType));
    }

    SingleVariant::SingleVariant(const SingleVariant& source)
        :
        Shared(source),
        SingleVariantBase(source)
    {
        *this = source;
    }

    SingleVariant& SingleVariant::operator=(const SingleVariant& source)
    {
        //        ARMARX_IMPORTANT_S << VariantContainerType::allTypesToString(source.typeContainer) << "::operator ="  << LogSender::CreateBackTrace();

        element = VariantPtr::dynamicCast(source.element)->clone();
        //ARMARX_WARNING_S << "type source: " << source.element->data->ice_id() << " new element: " << element->data->ice_id();
        typeContainer = source.typeContainer->clone();
        return *this;
    }

    VariantContainerBasePtr SingleVariant::cloneContainer(const::Ice::Current&) const
    {
        return new SingleVariant(*this);
    }

    VariantBasePtr SingleVariant::getElementBase(const Ice::Current&) const
    {
        return element;
    }

    VariantPtr SingleVariant::get() const
    {
        return VariantPtr::dynamicCast(getElementBase());
    }

    void SingleVariant::setVariant(const VariantPtr& variant)
    {
        element = variant->clone();
        typeContainer = new ContainerTypeI();
        typeContainer->typeId = Variant::typeToString(element->getType()); //Variant::addTypeName(Variant::typeToString(subType));

    }

    ContainerTypePtr SingleVariant::getContainerType(const Ice::Current& c) const
    {
        return typeContainer;
    }

    void SingleVariant::setContainerType(const ContainerTypePtr& containerType, const Ice::Current& c)
    {
        this->typeContainer = containerType->clone();
    }

    VariantTypeId SingleVariant::getStaticType(const Ice::Current& c)
    {
        return Variant::addTypeName(getTypePrefix());
    }

    void SingleVariant::serialize(const ObjectSerializerBasePtr& serializer, const Ice::Current&) const
    {
        AbstractObjectSerializerPtr obj = AbstractObjectSerializerPtr::dynamicCast(serializer);
        obj->setVariant("variant", get());
    }

    void SingleVariant::deserialize(const ObjectSerializerBasePtr& serializer, const Ice::Current&)
    {
        AbstractObjectSerializerPtr obj = AbstractObjectSerializerPtr::dynamicCast(serializer);
        setVariant(obj->getVariant("variant"));
    }


    int SingleVariant::readFromXML(const std::string& xmlString, const Ice::Current& c)
    {
        std::string strippedXmlString = xmlString;
        const std::string xmlHeader = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
        size_t headerPosition = strippedXmlString.find(xmlHeader);

        if (strippedXmlString.npos != headerPosition)
        {
            strippedXmlString.replace(headerPosition, headerPosition + xmlHeader.size(), std::string(""));
        }

        VariantTypeId type = element->getType();

        if (type == VariantType::Bool)
        {
            element->setBool(fromString<bool>(strippedXmlString));
        }
        else if (type == VariantType::String)
        {
            element->setString(strippedXmlString);
        }
        else if (type == VariantType::Float)

        {
            element->setFloat(fromString<float>(strippedXmlString));
        }
        else if (type == VariantType::Double)
        {
            element->setDouble(fromString<double>(strippedXmlString));
        }
        else if (type == VariantType::Int)
        {
            element->setInt(fromString<int>(strippedXmlString));
        }
        else // must be VariantDataClass
        {
            Ice::CommunicatorPtr  ic = c.adapter->getCommunicator();

            if (!ic)
            {
                throw LocalException("Ice::Communicator was not set in Ice::Current.adapter");
            }

            std::string typeStr =  Variant::typeToString(type);
            VariantDataClassPtr data;

            if (ic)
            {
                Ice::ObjectFactoryPtr objFac = ic->findObjectFactory(typeStr);

                if (!objFac)
                {
                    throw LocalException("Could not find ObjectFactory for string '" + typeStr + "'");
                }

                data = VariantDataClassPtr::dynamicCast(objFac->create(typeStr));
            }
            else
            {
                ARMARX_WARNING_S << "could not load parameter" << flush;
                return 0;
            }

            //VariantDataClass data = element->getClass< armarx::VariantDataClass >();
            static Ice::Current c;

            if (!c.adapter)
            {
                c.adapter = ic->createObjectAdapterWithEndpoints("Dummy" + IceUtil::generateUUID(), "tcp");    // pass adapter to readFromXML so that a proxy can be retrieved in the elementiant
            }

            data->readFromXML(strippedXmlString, c);
            VariantPtr::dynamicCast(element)->setClass(data);
        }

        return 1;

    }

    std::string SingleVariant::toString(const Ice::Current& c)
    {
        VariantPtr variant = VariantPtr::dynamicCast(element);
        std::string variantString = variant->getOutputValueOnly();
        // boost::replace_all(variantString,"\n","<br>");
        return variantString;

    }

    ContainerTypeI::ContainerTypeI(VariantTypeId variantType)
    {
        typeId = Variant::typeToString(variantType);
    }

    ContainerTypePtr ContainerTypeI::clone(const Ice::Current&) const
    {
        ContainerTypePtr newType = new ContainerTypeI();
        newType->typeId = this->typeId;

        if (subType)
        {
            newType->subType = subType->clone();
        }

        return newType;
    }



    VariantContainerType::VariantContainerType(std::string containerType)
    {
        this->containerType = containerType;
        thisType = new ContainerTypeI();
        thisType->typeId = containerType;//Variant::addTypeName(containerType);
    }

    const ContainerTypeI VariantContainerType::operator()(VariantTypeId typeId) const
    {

        ContainerTypeI result = *(ContainerTypeIPtr::dynamicCast(thisType->clone()));
        result.subType = new ContainerTypeI();
        result.subType->typeId = Variant::typeToString(typeId); //SingleVariant::getTypePrefix();
        //        result->subType->subType = new ContainerTypeI();
        //        result->subType->subType->typeId = Variant::typeToString(typeId);
        return result;
    }
    const ContainerTypeI VariantContainerType::operator()(const ContainerType& subType) const
    {
        ContainerTypeI result = *(ContainerTypeIPtr::dynamicCast(thisType->clone()));
        result.subType = subType.clone();
        return result;
    }

    bool VariantContainerType::compare(const ContainerTypePtr& firstType, const ContainerTypePtr& secondType)
    {
        ContainerTypePtr type1 = firstType;
        ContainerTypePtr type2 = secondType;

        if (!type1 && !type2)
        {
            return false;
        }

        while (type1 || type2)
        {
            if ((type1 && !type2) || (!type1 && type2))
            {
                return false;
            }

            if (type1->typeId != type2->typeId)
            {
                return false;
            }

            type1 = type1->subType;
            type2 = type2->subType;
        }

        return true;
    }

    std::string VariantContainerType::allTypesToString(const ContainerTypePtr& type)
    {
        std::string result;
        ContainerTypePtr curType = type;
        std::vector<std::string> typeStrings;

        while (curType)
        {
            //            result += curType->typeId;
            typeStrings.push_back(curType->typeId);
            curType = curType->subType;
        }

        std::vector<std::string>::reverse_iterator rit = typeStrings.rbegin();

        for (; rit != typeStrings.rend(); rit++)
        {
            if (result.empty())
            {
                result = *rit;
            }
            else
            {
                result = *rit + "(" + result + ")";
            }
        }

        return result;
    }

    ContainerTypePtr VariantContainerType::FromString(const std::string& typeStr)
    {
        boost::regex exp("([a-zA-Z:0-9_\\-]+?)\\(([a-zA-Z:0-9_\\-()]+)\\)");
        boost::match_results<std::string::const_iterator> matches;
        ContainerTypePtr result = new ContainerTypeI();

        if (boost::regex_search(typeStr, matches, exp))
        {
            result->subType = FromString(matches[2]);
            result->typeId = matches[1];
        }
        else
        {
            result->typeId = typeStr;
        }

        return result;
    }

    ContainerDummy::ContainerDummy()
    {
        typeContainer = new ContainerTypeI();
        typeContainer->typeId = Variant::typeToString(VariantType::Invalid); //Variant::addTypeName(Variant::typeToString(subType));

    }

    ContainerDummy::ContainerDummy(const ContainerDummy& source)
        :
        Shared(source),
        VariantContainerBase(source)
    {
        typeContainer = source.typeContainer->clone();
    }

    VariantContainerBasePtr ContainerDummy::cloneContainer(const Ice::Current&) const
    {
        return new ContainerDummy(*this);
    }

    std::string ContainerDummy::toString(const Ice::Current& c)
    {
        return "DummyContainer - no content";
    }

    ContainerTypePtr VariantContainer::getContainerType(const Ice::Current& c) const
    {
        return typeContainer;
    }

    void VariantContainer::setContainerType(const ContainerTypePtr& containerType, const Ice::Current& c)
    {
        typeContainer = containerType;
    }



}
