/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core
* @author     Kai Welke (welke@kit.edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef _ARMARX_CORE_STRINGVALUELIST_H
#define _ARMARX_CORE_STRINGVALUELIST_H

#include <ArmarXCore/core/system/ImportExport.h>
#include <ArmarXCore/observers/variant/Variant.h>
#include <ArmarXCore/observers/variant/VariantContainer.h>
#include <ArmarXCore/interface/observers/VariantContainers.h>
#include <IceUtil/Handle.h>

#include <map>



namespace armarx
{
    class StringValueMap;
    typedef IceInternal::Handle<StringValueMap> StringValueMapPtr;

    class ARMARXCORE_IMPORT_EXPORT StringValueMap :
        virtual public VariantContainer,
        virtual public StringValueMapBase
    {
    public:
        StringValueMap();
        StringValueMap(const StringValueMap& source);
        explicit StringValueMap(const ContainerType& subType);
        explicit StringValueMap(VariantTypeId subType);
        StringValueMap& operator=(const StringValueMap& source);
        VariantContainerBasePtr cloneContainer(const Ice::Current& c = ::Ice::Current()) const;


        // element manipulation
        void addElement(const std::string& key, const VariantContainerBasePtr& variantContainer, const Ice::Current& c = ::Ice::Current());
        void addVariant(const std::string& key, const Variant& variant);
        void setElement(const std::string& key, const VariantContainerBasePtr& variantContainer, const Ice::Current& c = ::Ice::Current());
        template <typename ValueType>
        void setElements(const std::map<std::string, ValueType>& map);
        template <typename ValueType>
        void setElements(const std::vector<std::string>& keyVec, const std::vector<ValueType>& values);
        void clear(const Ice::Current& c = ::Ice::Current());

        // getters
        static VariantTypeId getStaticType(const Ice::Current& c = ::Ice::Current());
        //        VariantTypeId getSubType(const Ice::Current& c = ::Ice::Current()) const;
        int getSize(const Ice::Current& c = ::Ice::Current()) const;
        bool validateElements(const Ice::Current& c = ::Ice::Current());


        /**
         * @brief getElementBase is the slice-interface implementation for
         * getting an Element and only returns a basepointer, so a manual upcast
         * is usually necessary.
         *
         * This function exists only for completeness and compatibility. Usually
         * you should use the getElement()-function.
         * @param index is the index of the Element in the list
         * @param c Not needed, leave blank.
         * @throw IndexOutOfBoundsException
         * @return a base variant pointer
         */
        VariantContainerBasePtr getElementBase(const std::string& key, const Ice::Current& c = ::Ice::Current()) const;

        /**
         * @brief getElement is the getter-function to retrieve variants from
         * the list.
         *

         * @param index is the index of the Element in the list
         * @throw IndexOutOfBoundsException
         * @return a variant pointer
         */
        template <typename ContainerType>
        IceInternal::Handle<ContainerType> getElement(const std::string& key) const
        {
            IceInternal::Handle<ContainerType> ptr = IceInternal::Handle<ContainerType>::dynamicCast(getElementBase(key));

            if (!ptr)
            {
                throw InvalidTypeException();
            }

            return ptr;
        }


        VariantPtr getVariant(const std::string& key) const;




        // Convenience functions
        template <class ValueType>
        std::map<std::string, ValueType> toStdMap()
        {
            std::map<std::string, ValueType> map;
            StringVariantContainerBaseMap::const_iterator it = elements.begin();

            for (; it != elements.end(); it++)
            {
                const std::string& key = it->first;
                map[key] = getVariant(key)->get<ValueType>();
            }

            return map;
        }

        template <typename Type>
        std::map<std::string, Type> toContainerStdMap() const
        {
            std::map<std::string, Type> map;
            StringVariantContainerBaseMap::const_iterator it = elements.begin();

            for (; it != elements.end(); it++)
            {
                const std::string& key = it->first;
                map[key] = getElement<typename Type::element_type>(key);
            }

            return map;
        }

        template <typename T1>
        static StringValueMapPtr FromStdMap(const std::map<std::string, T1>& map)
        {
            StringValueMapPtr result = new StringValueMap();

            for (typename std::map<std::string, T1>::const_iterator it = map.begin(); it != map.end(); it++)
            {
                result->addVariant(it->first, it->second);
            }

            return result;
        }

        template <typename T1>
        static StringValueMapPtr FromContainerStdMap(const std::map<std::string, T1>& map)
        {
            StringValueMapPtr result = new StringValueMap();

            for (typename std::map<std::string, T1>::const_iterator it = map.begin(); it != map.end(); it++)
            {
                result->addElement(it->first, it->second);
            }

            return result;
        }


        static std::string getTypePrefix()
        {
            return "::armarx::StringValueMapBase";
        }
    public: // serialization
        virtual void serialize(const ObjectSerializerBasePtr& serializer, const ::Ice::Current& = ::Ice::Current()) const;
        virtual void deserialize(const ObjectSerializerBasePtr& serializer, const ::Ice::Current& = ::Ice::Current());
        int readFromXML(const std::string& xmlData, const Ice::Current& c = ::Ice::Current());

        // VariantContainerBase interface
    public:
        std::string toString(const Ice::Current& c);
    };

    namespace VariantType
    {
        const VariantContainerType Map = VariantContainerType(StringValueMap::getTypePrefix());
    }



    /////////////////////////////////////////////////////////////////////////
    // Implementations
    /////////////////////////////////////////////////////////////////////////
    template <typename ValueType>
    void StringValueMap::setElements(const std::map<std::string, ValueType>& map)
    {
        typename std::map<std::string, ValueType>::const_iterator it = map.begin();

        for (; it != map.end(); it++)
        {
            setElement(it->first, it->second);
        }
    }
    template <typename ValueType>
    void StringValueMap::setElements(const std::vector<std::string>& keyVec, const std::vector<ValueType>& values)
    {

        int size = std::min(keyVec.size(), values.size());

        if (keyVec.size() != values.size())
        {
            ARMARX_WARNING_S << "Size of keys vector does not match values vector size:" << keyVec.size() << " vs. " << values.size();
        }

        for (int i = 0; i < size; i++)
        {
            setElement(keyVec[i], values[i]);
        }
    }

}

#endif
