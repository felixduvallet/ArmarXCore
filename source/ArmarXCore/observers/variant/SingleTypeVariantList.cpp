/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Kai Welke (welke at kit dot edu)
 * @date       2011
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include <ArmarXCore/observers/variant/SingleTypeVariantList.h>
#include <ArmarXCore/observers/exceptions/user/InvalidTypeException.h>
#include <ArmarXCore/observers/AbstractObjectSerializer.h>
// boost includs
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

// Ice Includes
#include <IceUtil/UUID.h>
#include <Ice/ObjectAdapter.h>
#include <Ice/ObjectFactory.h>

using namespace armarx;

SingleTypeVariantList::SingleTypeVariantList()
{
    typeContainer = VariantType::List(VariantType::Invalid).clone();
}

SingleTypeVariantList::SingleTypeVariantList(const ContainerType& subType)

{
    this->typeContainer = VariantType::List(subType).clone();
    //    type = Variant::addTypeName(getTypePrefix() + Variant::typeToString(subType));
}

SingleTypeVariantList::SingleTypeVariantList(VariantTypeId subType)
{
    this->typeContainer = VariantType::List(subType).clone();
}

SingleTypeVariantList::SingleTypeVariantList(const SingleTypeVariantList& source) :
    IceUtil::Shared(source),
    VariantContainerBase(source),
    SingleTypeVariantListBase(source)
{
    *this = source;
}

SingleTypeVariantList& SingleTypeVariantList::operator =(const SingleTypeVariantList& source)
{
    typeContainer = ContainerTypePtr::dynamicCast(source.typeContainer->clone());
    elements.clear();

    for (unsigned int i = 0; i < source.elements.size(); i++)
    {
        elements.push_back(source.elements[i]->cloneContainer());
    }

    return *this;
}

VariantContainerBasePtr SingleTypeVariantList::cloneContainer(const Ice::Current& c) const
{
    VariantContainerBasePtr result = new SingleTypeVariantList(*this);
    return result;
}

void SingleTypeVariantList::addElement(const VariantContainerBasePtr& variantContainer, const Ice::Current& c)
{
    if (!VariantContainerType::compare(variantContainer->getContainerType(), getContainerType()->subType)
        && getContainerType()->subType->typeId != Variant::typeToString(VariantType::Invalid))
    {
        throw exceptions::user::InvalidTypeException(getContainerType()->subType->typeId, variantContainer->getContainerType()->typeId);
    }

    if (getContainerType()->subType->typeId == Variant::typeToString(VariantType::Invalid))
    {
        getContainerType()->subType = variantContainer->getContainerType()->clone();
    }

    elements.push_back(variantContainer->cloneContainer());
}

void SingleTypeVariantList::addVariant(const Variant& variant)
{
    if (getContainerType()->subType->typeId == Variant::typeToString(VariantType::Invalid))
    {
        getContainerType()->subType = new ContainerTypeI(variant.getType());
    }
    else if (!getContainerType()->subType || getContainerType()->subType->typeId != Variant::typeToString(variant.getType()))
    {
        throw exceptions::user::InvalidTypeException(getContainerType()->subType->typeId, Variant::typeToString(variant.getType()));
    }

    addElement(new SingleVariant(variant));
}

void SingleTypeVariantList::clear(const Ice::Current& c)
{
    elements.clear();
}

//ContainerTypePtr SingleTypeVariantList::getContainerType(const Ice::Current& c) const
//{
//    return typeContainer;
//}

VariantTypeId SingleTypeVariantList::getStaticType(const Ice::Current& c)
{
    return Variant::addTypeName(getTypePrefix());
}

//VariantTypeId SingleTypeVariantList::getSubType(const Ice::Current &c) const
//{
//    return subType;
//}

int SingleTypeVariantList::getSize(const Ice::Current& c) const
{
    return int(elements.size());
}

bool SingleTypeVariantList::validateElements(const Ice::Current& c)
{
    VariantContainerBaseList::iterator it = elements.begin();
    bool result = true;

    for (; it != elements.end(); it++)
    {
        result = result && (*it)->validateElements();
    }

    return result;
}


//void SingleTypeVariantList::setContainerType(const ContainerTypePtr &containerType, const Ice::Current &)
//{
//    this->typeContainer = containerType;
//}

VariantContainerBasePtr SingleTypeVariantList::getElementBase(int index, const Ice::Current& c) const
{
    if (index >= getSize(c))
    {
        throw IndexOutOfBoundsException();
    }

    return elements.at(index);
}


VariantPtr SingleTypeVariantList::getVariant(int index) const
{
    VariantPtr ptr = getElement<SingleVariant>(index)->get();

    if (!ptr)
    {
        throw InvalidTypeException();
    }

    return ptr;
}

void SingleTypeVariantList::serialize(const ObjectSerializerBasePtr& serializer, const Ice::Current&) const
{
    AbstractObjectSerializerPtr obj = AbstractObjectSerializerPtr::dynamicCast(serializer);
    AbstractObjectSerializerPtr arr = obj->createElement();
    arr->setElementType(ElementTypes::eArray);

    for (size_t i = 0; i < elements.size(); i++)
    {
        arr->setIceObject(i, elements.at(i));
    }

    obj->setString("type", ice_id());
    obj->setElement("array", arr);
}

void SingleTypeVariantList::deserialize(const ObjectSerializerBasePtr& serializer, const Ice::Current&)
{
    AbstractObjectSerializerPtr obj = AbstractObjectSerializerPtr::dynamicCast(serializer);
    AbstractObjectSerializerPtr arr = obj->getElement("array");

    for (size_t i = 0; i < arr->size(); i++)
    {
        VariantContainerBasePtr c = VariantContainerBasePtr::dynamicCast(arr->getIceObject(i));

        if (c)
        {
            addElement(c);
        }
        else
        {
            throw LocalException("Could not cast to VariantContainerBasePtr");
        }
    }
}

int SingleTypeVariantList::readFromXML(const std::string& xmlString, const Ice::Current& c)
{
    int result = 1;
    Ice::CommunicatorPtr  ic = c.adapter->getCommunicator();

    if (!ic)
    {
        throw LocalException("Ice::Communicator was not set in Ice::Current.adapter");
    }

    using namespace boost::property_tree;
    ptree tree;
    std::stringstream stream;
    stream << xmlString;
    xml_parser::read_xml(stream, tree);
    std::stringstream currentItemXmlPathStr;
    int i = 0;
    currentItemXmlPathStr << "Item" << i;

    //        while( (value=tree.get_optional<float>(currentItemXmlPathStr.str())), value.is_initialized()){
    while (tree.get_child_optional(currentItemXmlPathStr.str()).is_initialized())
    {
        std::string typeStr =  getContainerType()->subType->typeId;
        //ARMARX_INFO_S << "reading item " << currentItemXmlPathStr.str() << " type: " << typeStr;

        std::ostringstream stream;
        xml_parser::write_xml(stream, tree.get_child(currentItemXmlPathStr.str()));
        //ARMARX_ERROR << stream.str();
        Ice::Current c;
        c.adapter = ic->createObjectAdapterWithEndpoints("Dummy" + IceUtil::generateUUID(), "tcp"); // pass adapter to readFromXML so that a proxy can be retrieved in the variant

        Ice::ObjectFactoryPtr objFac = ic->findObjectFactory(typeStr);

        if (!objFac) // simple variants
        {
            Variant var;
            var.setType(Variant::hashTypeName(typeStr));
            SingleVariantPtr singleVar = new SingleVariant(var);
            singleVar->readFromXML(stream.str(), c);

            //ARMARX_IMPORTANT_S << i<<" stream: " << stream.str();
            //ARMARX_IMPORTANT_S << i<<": " << singleVar->get();
            addElement(singleVar);
        }
        else
        {
            VariantDataClassPtr variantData = VariantDataClassPtr ::dynamicCast(objFac->create(typeStr));

            if (variantData)
            {
                // complex variants
                ARMARX_INFO_S << "type fo created variant: " << Variant::typeToString(variantData->getType());
                variantData->readFromXML(stream.str(), c);
                Variant var(variantData);
                addElement(new SingleVariant(var));
            }
            else
            {
                // variant containers
                VariantContainerBasePtr container = VariantContainerBasePtr ::dynamicCast(objFac->create(typeStr));

                if (container)
                {
                    container->readFromXML(stream.str(), c);
                    addElement(container);
                }
                else
                {
                    ARMARX_WARNING_S << "Could not create container " << typeStr << " with ObjectFactory";
                    result = 0;
                }
            }
        }



        i++;
        currentItemXmlPathStr.seekp(0);
        currentItemXmlPathStr << "Item" << i;
    }

    return result;
}

std::string SingleTypeVariantList::toString(const Ice::Current& c)
{
    std::stringstream ss;

    for (auto element : elements)
    {
        if (elements[elements.size() - 1] != element)
        {
            ss << element->toString() << "\n";
        }
        else
        {
            ss << element->toString();
        }
    }

    return ss.str();
}


