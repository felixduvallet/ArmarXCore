
armarx_set_target("Core Library: ArmarXCoreObservers")
find_package(Eigen3 QUIET)

armarx_build_if(Eigen3_FOUND "Eigen3 not available")

include_directories(${Eigen3_INCLUDE_DIR})

set(LIB_NAME       ArmarXCoreObservers)



set(LIBS ArmarXCoreInterfaces ArmarXCore)

set(LIB_FILES ConditionCheck.cpp
              Observer.cpp
              Event.cpp
              ConditionHandler.cpp
              SystemObserver.cpp
              DebugObserver.cpp
              ProfilerObserver.cpp
              AbstractObjectSerializer.cpp
              condition/Term.cpp
              condition/LiteralImpl.cpp
              condition/Operations.cpp
              condition/ConditionRoot.cpp
              parameter/Parameter.cpp
              parameter/VariantParameter.cpp
              parameter/VariantListParameter.cpp
              variant/Variant.cpp
              variant/VariantContainer.cpp
              #variant/VariantContainerLoader.cpp
              variant/SingleTypeVariantList.cpp
              variant/StringValueMap.cpp
              variant/DataFieldIdentifier.cpp
              variant/ChannelRef.cpp
              variant/DatafieldRef.cpp
              variant/TimestampVariant.cpp
              filters/DatafieldFilter.cpp
              test/ExampleUnit.cpp
              test/ExampleUnitObserver.cpp
              test/LoggingExampleComponent.cpp
              )

set(LIB_HEADERS ConditionCheck.h
                Event.h
                ObserverObjectFactories.h
                Observer.h
                ConditionHandler.h
                SystemObserver.h
                DebugObserver.h
                ProfilerObserver.h
                AbstractObjectSerializer.h
                condition/Term.h
                condition/TermImpl.h
                condition/LiteralImpl.h
                condition/Operations.h
                condition/ConditionRoot.h
                parameter/Parameter.h
                parameter/VariantParameter.h
                parameter/VariantListParameter.h
                variant/Variant.h
                variant/VariantContainer.h
                #variant/VariantContainerLoader.h
                variant/SingleTypeVariantList.h
                variant/StringValueMap.h
                variant/DataFieldIdentifier.h
                variant/ChannelRef.h
                variant/DatafieldRef.h
                variant/TimestampVariant.h
                filters/DatafieldFilter.h
                filters/MedianFilter.h
                filters/AverageFilter.h
                filters/GaussianFilter.h

                exceptions/local/InvalidChannelException.h
                exceptions/local/InvalidCheckException.h
                exceptions/local/InvalidDataFieldException.h
                exceptions/user/InvalidTypeException.h
                exceptions/user/UnknownTypeException.h
                exceptions/user/NotInitializedException.h
                checks/ConditionCheckEquals.h
                checks/ConditionCheckEqualsWithTolerance.h
                checks/ConditionCheckInRange.h
                checks/ConditionCheckLarger.h
                checks/ConditionCheckSmaller.h
                checks/ConditionCheckStringContains.h
                checks/ConditionCheckUpdated.h
                checks/ConditionCheckValid.h
                test/ExampleUnit.h
                test/ExampleUnitObserver.h
                test/LoggingExampleComponent.h
                )

armarx_add_library("${LIB_NAME}"  "${LIB_FILES}" "${LIB_HEADERS}" "${LIBS}")

add_subdirectory(test)
