#include "DebugObserver.h"

namespace armarx
{

    DebugObserver::DebugObserver()
    {
    }

    void DebugObserver::onInitObserver()
    {
    }

    void DebugObserver::onConnectObserver()
    {
    }

    void DebugObserver::setDebugDatafield(const std::string& channelName, const std::string& datafieldName, const VariantBasePtr& value, const Ice::Current& c)
    {
        if (!existsChannel(channelName))
        {
            offerChannel(channelName, "");
        }

        if (!existsDataField(channelName, datafieldName))
        {
            offerDataFieldWithDefault(channelName, datafieldName, *VariantPtr::dynamicCast(value), "");
        }
        else
        {
            setDataFieldFlatCopy(channelName, datafieldName, VariantPtr::dynamicCast(value));
        }

        updateChannel(channelName);
    }

    void DebugObserver::setDebugChannel(const std::string& channelName, const StringVariantBaseMap& valueMap, const Ice::Current& c)
    {
        if (!existsChannel(channelName))
        {
            offerChannel(channelName, "");
        }

        for (const auto & value : valueMap)
        {
            const std::string& datafieldName = value.first;

            if (!existsDataField(channelName, datafieldName))
            {
                offerDataFieldWithDefault(channelName, datafieldName, *VariantPtr::dynamicCast(value.second), "");
            }
            else
            {
                setDataFieldFlatCopy(channelName, datafieldName, VariantPtr::dynamicCast(value.second));
            }
        }

        updateChannel(channelName);
    }

    void DebugObserver::removeDebugDatafield(const std::string& channelName, const std::string& datafieldName, const Ice::Current&)
    {
    }

    void DebugObserver::removeDebugChannel(const std::string& channelName, const Ice::Current&)
    {
        removeChannel(channelName);
    }

    void DebugObserver::removeAllChannels(const Ice::Current&)
    {
        ChannelRegistry channels = getAvailableChannels(false);
        ChannelRegistry::iterator it = channels.end();

        for (; it != channels.end(); it++)
        {
            ChannelRegistryEntry& entry = it->second;
            removeChannel(entry.name);
        }
    }

}
