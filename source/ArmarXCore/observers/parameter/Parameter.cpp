/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Kai Welke (welke _at_ kit _dot_ edu)
* @date       2012 Kai Welke
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include <ArmarXCore/observers/parameter/Parameter.h>

using namespace armarx;


Parameter::Parameter(const Parameter& source) :
    IceUtil::Shared(source),
    ParameterBase(source)
{
    if (type != source.type)
    {
        throw InvalidTypeException();
    }

    type = source.type;
}

Parameter& Parameter::operator =(const Parameter& source)
{
    if (type != source.type)
    {
        throw InvalidTypeException();
    }

    type = source.type;
    return *this;
}

ParameterBasePtr Parameter::clone(const Ice::Current& c) const
{
    return new Parameter(*this);
}

// setter
void Parameter::setVariant(const VariantBasePtr& variant, const Ice::Current& c)
{
    throw InvalidTypeException();
}

void Parameter::setVariantList(const SingleTypeVariantListBasePtr& variantList, const Ice::Current& c)
{
    throw InvalidTypeException();
}

void Parameter::setDataFieldIdentifier(const DataFieldIdentifierBasePtr& dataFieldIdentifier, const Ice::Current& c)
{
    throw InvalidTypeException();
}


// getter
ParameterType Parameter::getParameterType(const Ice::Current& c) const
{
    return type;
}

VariantTypeId Parameter::getVariantType(const Ice::Current& c) const
{
    throw InvalidTypeException("Accessing get getVariantTypeId() on the Parameter-Baseclass does not work.");
}

VariantBasePtr Parameter::getVariant(const Ice::Current& c) const
{
    throw InvalidTypeException("Accessing get getVariant() on the Parameter-Baseclass does not work.");
}

SingleTypeVariantListBasePtr Parameter::getVariantList(const Ice::Current& c) const
{
    throw InvalidTypeException("Accessing get getVariantList() on the Parameter-Baseclass does not work.");
}

DataFieldIdentifierBasePtr Parameter::getDataFieldIdentifier(const Ice::Current& c) const
{
    throw InvalidTypeException();
}

bool Parameter::validate(const Ice::Current& c) const
{
    return true;
}


