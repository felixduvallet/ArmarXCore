/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Kai Welke (welke _at_ kit _dot_ edu)
* @date       2011
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "ConditionHandler.h"
#include <ArmarXCore/observers/ConditionCheck.h>
#include <ArmarXCore/observers/ObserverObjectFactories.h>
#include <ArmarXCore/observers/condition/ConditionRoot.h>

#include <boost/algorithm/string/split.hpp>
#include <sstream>

using namespace armarx;

// *******************************************************
// implementation of ObserverInterface
// *******************************************************
ConditionIdentifier ConditionHandler::installCondition(const EventListenerInterfacePrx& listener, const TermImplBasePtr& expression, const EventBasePtr& e, const Ice::Current& c)
{
    return installConditionWithDescription(listener, expression, e, "", c);
}

ConditionIdentifier ConditionHandler::installConditionWithDescription(const EventListenerInterfacePrx& listener, const TermImplBasePtr& expression, const EventBasePtr& e, const std::string& desc, const Ice::Current& c)
{
    // generate root expression
    ConditionRootPtr root = new ConditionRoot(listener, e, desc);
    ARMARX_VERBOSE << "Installing condition: " << root << std::endl;
    root->addChild(expression);

    // extract checks
    std::vector<LiteralImplPtr> literals = extractLiterals(root);

    boost::mutex::scoped_lock lock(conditionRegistryMutex);

    // go through literals and install checks
    installChecks(literals, c);

    // store in registry
    std::pair<int, ConditionRootBasePtr> entry;
    entry.first = generateId();
    entry.second = root;

    conditionRegistry.insert(entry);

    ConditionIdentifier id;
    id.uniqueId = entry.first;

    ARMARX_VERBOSE << "Installed condition: " << root << std::endl;

    return id;
}


void ConditionHandler::removeCondition(const ConditionIdentifier& id, const Ice::Current& c)
{
    ARMARX_VERBOSE << "Removing condition " << id.uniqueId << std::endl;

    boost::mutex::scoped_lock lock(conditionRegistryMutex);
    ConditionRegistry::iterator iter = conditionRegistry.find(id.uniqueId);

    if (iter == conditionRegistry.end())
    {
        return;
    }

    ConditionRootPtr root = ConditionRootPtr::dynamicCast(iter->second);

    // extract checks
    std::vector<LiteralImplPtr> literals = extractLiterals(root);

    // go through literals and remove checks
    removeChecks(literals, c);

    // remove from registry
    std::pair<int, ConditionRootBasePtr> tmp = *iter;
    conditionRegistry.erase(iter);

    // insert into history
    boost::mutex::scoped_lock lockHistory(conditionHistoryMutex);
    conditionHistory.insert(tmp);

    if (int(conditionHistory.size()) > historyLength)
    {
        conditionHistory.erase(conditionHistory.begin());
    }

}

ConditionRegistry ConditionHandler::getActiveConditions(const Ice::Current& c)
{
    boost::mutex::scoped_lock lock(conditionRegistryMutex);

    return conditionRegistry;
}

ConditionRegistry ConditionHandler::getPastConditions(const Ice::Current& c)
{
    boost::mutex::scoped_lock lock(conditionHistoryMutex);

    return conditionHistory;
}

ConditionRootBasePtr ConditionHandler::getCondition(Ice::Int id, const Ice::Current& c)
{
    {
        boost::mutex::scoped_lock lock(conditionRegistryMutex);
        auto it = conditionRegistry.find(id);

        if (it != conditionRegistry.end())
        {
            return it->second;
        }
    }
    {
        boost::mutex::scoped_lock lock(conditionHistoryMutex);
        auto it = conditionHistory.find(id);

        if (it != conditionHistory.end())
        {
            return it->second;
        }
    }
    return NULL;
}

StringList ConditionHandler::getObserverNames(const Ice::Current& c)
{
    return observerNames;
}

// *******************************************************
// Component hooks
// *******************************************************
void ConditionHandler::onInitComponent()
{
    currentId = 0;

    // read observer dependencies. These observers will be precaches
    std::string observers = getProperty<std::string>("Observers").getValue();
    boost::trim(observers);

    if (!observers.empty())
    {
        boost::split(observerNames, observers, boost::is_any_of(","));
    }

    useObservers(observerNames);

    // read max length of history
    historyLength = getProperty<int>("HistoryLength").getValue();

    onInitConditionHandler();
}

void ConditionHandler::onConnectComponent()
{

    // precach
    preCacheObservers(observerNames);

    // subclass init
    onStartConditionHandler();
}

// *******************************************************
// private methods
// *******************************************************
int ConditionHandler::generateId()
{
    boost::mutex::scoped_lock lock(idMutex);
    return currentId++;
}

void ConditionHandler::useObservers(std::vector<std::string>& names)
{
    boost::mutex::scoped_lock lock(iceManagerMutex);

    std::vector<std::string>::iterator iter = names.begin();

    while (iter != names.end())
    {
        usingProxy(*iter);
        iter++;
    }
}

void ConditionHandler::preCacheObservers(std::vector<std::string>& names)
{
    boost::mutex::scoped_lock lock(iceManagerMutex);

    std::vector<std::string>::iterator iter = names.begin();

    while (iter != names.end())
    {
        getProxy<ObserverInterfacePrx>(*iter);
        iter++;
    }
}

ObserverInterfacePrx ConditionHandler::getObserver(std::string observerName)
{
    boost::mutex::scoped_lock lock(iceManagerMutex);
    ObserverInterfacePrx proxy;

    try
    {
        proxy = getProxy<ObserverInterfacePrx>(observerName);
        ObserverInterfacePrx::checkedCast(proxy);
    }
    catch (...)
    {
        throw InvalidConditionException("Observer \"" + observerName + "\" does not exist.");
    }

    return proxy;
}

std::vector<LiteralImplPtr> ConditionHandler::extractLiterals(const TermImplBasePtr& expression)
{
    std::list<TermImplPtr> terms;
    std::vector<LiteralImplPtr> literals;

    terms.push_back(TermImplPtr::dynamicCast(expression));

    while (terms.size() != 0)
    {
        // retrieve front of list
        TermImplPtr term = terms.front();
        terms.pop_front();

        // test type
        if (term->getType() == eLiteral)
        {
            literals.push_back(LiteralImplPtr::dynamicCast(term));
        }
        else
        {
            // add childs to list
            TermImplSequence childs = term->getChilds();
            TermImplSequence::iterator iterChilds = childs.begin();

            while (iterChilds != childs.end())
            {
                terms.push_back(TermImplPtr::dynamicCast(*iterChilds));
                iterChilds++;
            }
        }

    }

    return literals;
}

void ConditionHandler::installChecks(std::vector<LiteralImplPtr>& literals, const Ice::Current& c)
{
    std::vector<LiteralImplPtr>::iterator iter = literals.begin();

    while (iter != literals.end())
    {
        ObserverInterfacePrx proxy = getObserver((*iter)->checkConfig.dataFieldIdentifier->observerName);
        (*iter)->installCheck(c.adapter, proxy);

        iter++;
    }
}

void ConditionHandler::removeChecks(std::vector<LiteralImplPtr>& literals, const Ice::Current& c)
{
    std::vector<LiteralImplPtr>::iterator iter = literals.begin();

    while (iter != literals.end())
    {
        ObserverInterfacePrx proxy = getObserver((*iter)->checkConfig.dataFieldIdentifier->observerName);
        (*iter)->removeCheck(c.adapter, proxy);

        iter++;
    }
}


PropertyDefinitionsPtr ConditionHandler::createPropertyDefinitions()
{
    return PropertyDefinitionsPtr(new ConditionHandlerPropertyDefinitions(
                                      getConfigIdentifier()));
}
