
#ifndef ARMARX_FILTERS_AVERAGEFILTER_H
#define ARMARX_FILTERS_AVERAGEFILTER_H

#include "DatafieldFilter.h"
#include <ArmarXCore/interface/observers/Filters.h>
#include <ArmarXCore/observers/exceptions/user/InvalidTypeException.h>

namespace armarx
{
    namespace filters
    {


        /**
         * @class  AverageFilter
         * @ingroup ObserverFilters
         * @brief The AverageFilter class provides a simple filter by calculating the
         * average value of a window for datafields of type float, int and double.
         */
        class AverageFilter :
            public DatafieldFilter,
            public AverageFilterBase
        {
        public:
            AverageFilter()
            {
            }

            // DatafieldFilterBase interface
        public:
            VariantBasePtr calculate(const Ice::Current&) const
            {
                if (dataHistory.size() == 0)
                {
                    return NULL;
                }

                VariantTypeId type = dataHistory.begin()->second->getType();

                if (type == VariantType::Float)
                {
                    return new Variant(CalcAvg<float>(dataHistory));
                }
                else if (type == VariantType::Double)
                {
                    return new Variant(CalcAvg<double>(dataHistory));
                }
                else if (type == VariantType::Int)
                {
                    return new Variant(CalcAvg<int>(dataHistory));
                }

                throw exceptions::user::UnsupportedTypeException(type);
            }

            /**
             * @brief This filter supports: Int, Float, Double
             * @return List of VariantTypes
             */
            ParameterTypeList getSupportedTypes(const Ice::Current&) const
            {
                ParameterTypeList result;
                result.push_back(VariantType::Int);
                result.push_back(VariantType::Float);
                result.push_back(VariantType::Double);
                return result;
            }
        private:
            template <typename Type>
            static Type CalcAvg(const TimeVariantBaseMap& map)
            {
                double sum = 0;

                for (auto v : map)
                {
                    VariantPtr v2 = VariantPtr::dynamicCast(v.second);
                    sum += v2->get<Type>();
                }

                double result = sum / map.size();
                return result;
            }
        };

    } // namespace filters
} // namespace armarx

#endif // ARMARX_FILTERS_AVERAGEFILTER_H
