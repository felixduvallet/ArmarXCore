#include "DatafieldFilter.h"
#include <ArmarXCore/core/logging/Logging.h>

namespace armarx
{

    DatafieldFilter::DatafieldFilter()
    {
    }




    void DatafieldFilter::update(Ice::Long timestamp, const VariantBasePtr& value, const Ice::Current&)
    {
        dataHistory[timestamp] = value;
        int curSize = dataHistory.size();

        if (curSize > windowFilterSize)
        {
            dataHistory.erase(dataHistory.begin(), ++dataHistory.begin());
        }

        filteredValue = calculate();
    }

    VariantBasePtr DatafieldFilter::getValue(const Ice::Current&) const
    {
        if (filteredValue)
        {
            return filteredValue;
        }
        else
        {
            return calculate();
        }
    }

    bool DatafieldFilter::checkTypeSupport(VariantTypeId variantType, const Ice::Current&) const
    {
        auto types = getSupportedTypes();

        if (std::find(types.begin(), types.end(), variantType) != types.end())
        {
            return true;
        }
        else
        {
            return false;
        }
    }


} // namespace armarx



