
#ifndef ARMARX_DATAFIELDFILTER_H
#define ARMARX_DATAFIELDFILTER_H
#include <ArmarXCore/interface/observers/ObserverInterface.h>
#include <ArmarXCore/observers/variant/Variant.h>

namespace armarx
{

    /**
     * @class  DatafieldFilter
     * @ingroup ObserverFilters
     * @brief The DatafieldFilter class is the base class for all filters
     * and filter implementation should derive from this class.
     * For usage explanation see \ref Observers-Filters.
     */
    class DatafieldFilter :
        virtual public DatafieldFilterBase
    {
    public:
        DatafieldFilter();

        // DatafieldFilterBase interface

        /**
         * @brief Adds the given value to the data map, erases old values if maximum size was reached,
         * and calculates the new filtered value.
         * @param timestamp Timestamp at which the data value was taken.
         * @param value The new data value.
         */
        void update(Ice::Long timestamp, const VariantBasePtr& value, const Ice::Current&);

        /**
         * @brief Retrieves the current, filtered value.
         * Triggers calculates if if filtered value pointer is empty.
         */
        VariantBasePtr getValue(const Ice::Current&) const;

        /**
         * @brief Checks whether the given type is supported.
         * @param variantType
         * @return
         */
        bool checkTypeSupport(VariantTypeId variantType, const Ice::Current& c = Ice::Current()) const;
    };

} // namespace armarx

#endif // ARMARX_DATAFIELDFILTER_H
