/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Kai Welke (welke _at_ kit _dot_ edu)
* @date       2012 Kai Welke
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include <ArmarXCore/observers/condition/Operations.h>

using namespace armarx;

// OperationAnd
OperationAnd::OperationAnd(TermImplPtr a, TermImplPtr b)
{
    opType = eOperationAnd;
    addChild(a);
    addChild(b);
}

void OperationAnd::update(const Ice::Current& c)
{
    if (childs.size() != 2)
    {
        return;
    }

    value = childs.at(0)->getValue() && childs.at(1)->getValue();

    TermImpl::update();
}

void OperationAnd::output(std::ostream& out) const
{
    if (childs.size() != 2)
    {
        return;
    }

    out << "(" << TermImplPtr::dynamicCast(childs.at(0)) << " && " << TermImplPtr::dynamicCast(childs.at(1)) << ")";
}

// OperationOr
OperationOr::OperationOr(TermImplPtr a, TermImplPtr b)
{
    opType = eOperationOr;

    addChild(a);
    addChild(b);
}

void OperationOr::update(const Ice::Current& c)
{
    if (childs.size() != 2)
    {
        return;
    }

    value = childs.at(0)->getValue() || childs.at(1)->getValue();

    TermImpl::update();
}

void OperationOr::output(std::ostream& out) const
{
    if (childs.size() != 2)
    {
        return;
    }

    out << "(" << TermImplPtr::dynamicCast(childs.at(0)) << " || " << TermImplPtr::dynamicCast(childs.at(1)) << ")";
}

// OperationNot
OperationNot::OperationNot(TermImplPtr a)
{
    opType = eOperationNot;

    addChild(a);
}

void OperationNot::update(const Ice::Current& c)
{
    if (childs.size() != 1)
    {
        return;
    }

    value = !childs.at(0)->getValue();

    TermImpl::update();
}

void OperationNot::output(std::ostream& out) const
{
    if (childs.size() != 1)
    {
        return;
    }

    out << "!(" << TermImplPtr::dynamicCast(childs.at(0)) << ")";
}
