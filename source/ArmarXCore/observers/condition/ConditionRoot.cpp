/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Kai Welke (welke _at_ kit _dot_ edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include <ArmarXCore/observers/condition/ConditionRoot.h>
#include <cstdarg>

using namespace armarx;

void ConditionRoot::update(const Ice::Current& c)
{
    boost::mutex::scoped_lock lock(updateMutex);

    if (childs.size() != 1)
    {
        return;
    }

    // empty terms is always evaluated to true
    if (!fired && (!childs.at(0) || childs.at(0)->getValue()))
    {
        value = true;
        ARMARX_VERBOSE_S << "Condition fulfilled, sending event " << event->eventName << std::endl;
        listener->begin_reportEvent(event);

        fired = true;
    }
}
