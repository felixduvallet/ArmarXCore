/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     ALexey Kozlov ( kozlov at kit dot edu)
* @date       11.09.2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

// ArmarX Includes
#include <ArmarXCore/interface/core/Log.h>
#include <ArmarXCore/statechart/Exception.h>

// Ice Includes
#include <IceUtil/UUID.h>
#include <Ice/ObjectFactory.h>


#include "AbstractObjectSerializer.h"

using namespace armarx;

AbstractObjectSerializer::AbstractObjectSerializer() :
    idField("id")
{
}

AbstractObjectSerializer::AbstractObjectSerializer(const Ice::CommunicatorPtr ic) :
    ic(ic),
    idField("id")
{
}

AbstractObjectSerializer::~AbstractObjectSerializer()
{
}

void AbstractObjectSerializer::setIceCommunicator(Ice::CommunicatorPtr ic)
{
    this->ic = ic;
}

AbstractObjectSerializerPtr AbstractObjectSerializer::serializeVariant(const VariantPtr& val) const
{
    AbstractObjectSerializerPtr variantNode = createElement();

    const VariantTypeId type = val->getType();

    try
    {
        variantNode->setString("typeName", Variant::typeToString(type));
    }
    // catch in case typeToString fails
    catch (...)
    {
        variantNode->setInt("type", type);
    }

    if (!val->getInitialized())
    {
        variantNode->setString("value", "(not initialized)");
    }
    else if (type == VariantType::Invalid)
    {
        variantNode->setString("value", "(invalid type)");
    }
    else if (type == VariantType::Int)
    {
        variantNode->setInt("value", val->getInt());
    }
    else if (type == VariantType::Float)
    {
        variantNode->setFloat("value", val->getFloat());
    }
    else if (type == VariantType::Double)
    {
        variantNode->setDouble("value", val->getDouble());
    }
    else if (type == VariantType::String)
    {
        variantNode->setString("value", val->getString());
    }
    else if (type == VariantType::Bool)
    {
        variantNode->setBool("value", val->getBool());
    }
    else
    {
        VariantDataClassPtr ptr = val->getClass<VariantDataClass>();

        if (ptr)
        {
            const AbstractObjectSerializerPtr valueNode = createElement();
            ptr->serialize(valueNode);

            variantNode->setElement("value", valueNode);
        }
        else
        {
            variantNode->setString("value", "(NULLCAST for" + val->getTypeName() + ")");
        }
    }

    return variantNode;
}

VariantPtr AbstractObjectSerializer::deserializeVariant()
{
    // get the type
    VariantTypeId type;

    if (hasElement("typeName"))
    {
        type = Variant::hashTypeName(getString("typeName"));
    }
    else
    {
        type  = getInt("type");
    }

    // create empty variant with "invalid" type
    VariantPtr var = new Variant();

    // init variant with type and value specified in JSON/XML/... node
    // process basic types first
    if (type == VariantType::Bool)
    {
        var->setBool(getBool("value"));
    }
    else if (type == VariantType::String)
    {
        var->setString(getString("value"));
    }
    else if (type == VariantType::Float)
    {
        var->setFloat(getFloat("value"));
    }
    else if (type == VariantType::Double)
    {
        var->setDouble(getDouble("value"));
    }
    else if (type == VariantType::Int)
    {
        var->setInt(getInt("value"));
    }
    else // must be VariantDataClass
    {
        // for complex types, we need to use Ice object factory to create an instance of object by its class name
        std::string typeStr =  Variant::typeToString(type);
        VariantDataClassPtr data;

        if (ic)
        {
            Ice::ObjectFactoryPtr objFac = ic->findObjectFactory(typeStr);

            if (!objFac)
            {
                throw exceptions::local::eNullPointerException("Could not find ObjectFactory for string '" + typeStr + "'");
            }

            data = VariantDataClassPtr::dynamicCast(objFac->create(typeStr));
        }
        else
        {
            //          ARMARX_LOG << eWARNING << "could not deserialize variant parameter '" << key << "' cause IceCommunicator wasn't set in Serializer" << flush;
            return VariantPtr();
        }

        //VariantDataClass data = var->getClass< armarx::VariantDataClass >();
        static Ice::Current c;

        if (!c.adapter)
        {
            c.adapter = ic->createObjectAdapterWithEndpoints("Dummy" + IceUtil::generateUUID(), "tcp");    // pass adapter to deserialize() so that a proxy can be retrieved in the variant
        }

        data->deserialize(getElement("value"), c);
        var->setClass(data);
    }

    return var;
}

void AbstractObjectSerializer::setVariant(const ::std::string& key, const VariantPtr& val)
{
    const AbstractObjectSerializerPtr variantNode = serializeVariant(val);

    setElement(key, variantNode);
}

void AbstractObjectSerializer::setVariant(unsigned int index, const VariantPtr& val)
{
    const AbstractObjectSerializerPtr variantNode = serializeVariant(val);

    setElement(index, variantNode);
}

VariantPtr AbstractObjectSerializer::getVariant(const ::std::string& key) const
{
    // variants are stored as object nodes, e.g. in JSON myVariant: { type: <type hash>, value: 1.0 }
    // so first get the node with specified key
    AbstractObjectSerializerPtr variantNode = getElement(key);

    return variantNode->deserializeVariant();
}

VariantPtr AbstractObjectSerializer::getVariant(int index) const
{
    // variants are stored as object nodes, e.g. in JSON myVariant: { type: <type hash>, value: 1.0 }
    // so first get the node with specified key
    AbstractObjectSerializerPtr variantNode = getElement(index);

    return variantNode->deserializeVariant();
}

SerializablePtr AbstractObjectSerializer::getIceObject(const ::std::string& key) const
{
    AbstractObjectSerializerPtr iceObjectNode = getElement(key);
    return iceObjectNode ? iceObjectNode->deserializeIceObject() : SerializablePtr();
}

SerializablePtr AbstractObjectSerializer::getIceObject(int index) const
{
    AbstractObjectSerializerPtr iceObjectNode = getElement(index);
    return iceObjectNode ? iceObjectNode->deserializeIceObject() : SerializablePtr();
}

void AbstractObjectSerializer::setIceObject(const ::std::string& key, const SerializablePtr& obj)
{
    const AbstractObjectSerializerPtr iceObjectNode = createElement();
    iceObjectNode->serializeIceObject(obj);

    setElement(key, iceObjectNode);
}

void AbstractObjectSerializer::setIceObject(unsigned int index, const SerializablePtr& obj)
{
    const AbstractObjectSerializerPtr iceObjectNode = createElement();
    iceObjectNode->serializeIceObject(obj);

    setElement(index, iceObjectNode);
}

void AbstractObjectSerializer::serializeIceObject(const SerializablePtr& obj)
{
    reset();
    setString("type", obj->ice_id());
    obj->serialize(this);
}

SerializablePtr AbstractObjectSerializer::deserializeIceObject()
{
    const std::string typeStr = getString("type");

    if (ic)
    {
        Ice::ObjectFactoryPtr objFac = ic->findObjectFactory(typeStr);

        if (!objFac)
        {
            throw exceptions::local::eNullPointerException("Could not find ObjectFactory for string '" + typeStr + "'");
        }

        SerializablePtr obj = SerializablePtr::dynamicCast(objFac->create(typeStr));
        static Ice::Current c;

        if (!c.adapter)
        {
            c.adapter = ic->createObjectAdapterWithEndpoints("Dummy" + IceUtil::generateUUID(), "tcp");    // pass adapter to deserialize() so that a proxy can be retrieved in the variant
        }

        obj->deserialize(this, c);
        return obj;
    }
    else
    {
        //       ARMARX_LOG << eWARNING << "could not deserialize IceObject cause IceCommunicator wasn't set in Serializer" << flush;
        return SerializablePtr();
    }

}
