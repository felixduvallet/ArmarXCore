/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     ALexey Kozlov ( kozlov at kit dot edu)
* @date       11.09.2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef ABSTRACTOBJECTSERIALIZER_H_
#define ABSTRACTOBJECTSERIALIZER_H_

#include <ArmarXCore/interface/observers/Serialization.h>
#include <ArmarXCore/observers/variant/Variant.h>

namespace armarx
{

    namespace ElementTypes
    {
        enum ElementType
        {
            eScalar,
            eObject,
            eArray
        };
    }

    typedef ElementTypes::ElementType ElementType;

    class AbstractObjectSerializer;
    typedef IceInternal::Handle<AbstractObjectSerializer> AbstractObjectSerializerPtr;

    /**
     * @class AbstractObjectSerializer
     * @brief
     * @ingroup ObserversSub
     */
    class AbstractObjectSerializer :
        public ObjectSerializerBase
    {
    public:
        AbstractObjectSerializer();
        AbstractObjectSerializer(const Ice::CommunicatorPtr ic);
        virtual ~AbstractObjectSerializer();

    public:
        virtual ElementType getElementType(const ::Ice::Current& = ::Ice::Current()) const = 0;
        virtual void setElementType(ElementType elemType, const ::Ice::Current& = ::Ice::Current()) = 0;

        virtual unsigned int size() const = 0;
        virtual bool hasElement(const ::std::string& key) const = 0;

        virtual float getFloat(const ::std::string& key) const = 0;
        virtual double getDouble(const ::std::string& key) const = 0;
        virtual int getInt(const ::std::string& key) const = 0;
        virtual bool getBool(const ::std::string& key) const = 0;
        virtual std::string getString(const ::std::string& key) const = 0;
        virtual VariantPtr getVariant(const ::std::string& key) const;
        virtual VariantPtr getVariant(int index) const;
        virtual SerializablePtr getIceObject(const ::std::string& key) const;
        virtual SerializablePtr getIceObject(int index) const;

        virtual AbstractObjectSerializerPtr getElement(unsigned int index) const = 0;
        virtual AbstractObjectSerializerPtr getElement(const ::std::string& key) const = 0;
        virtual std::vector<std::string> getElementNames() const = 0;

        virtual void getIntArray(const ::std::string& key, std::vector<int>& result) = 0;
        virtual void getFloatArray(const ::std::string& key, std::vector<float>& result) = 0;
        virtual void getDoubleArray(const ::std::string& key, std::vector<double>& result) = 0;
        virtual void getStringArray(const ::std::string& key, std::vector<std::string>& result) = 0;
        virtual void getVariantArray(const ::std::string& key, std::vector<VariantPtr>& result) = 0;

        virtual void getVariantMap(const ::std::string& key, StringVariantBaseMap& result) = 0;

        virtual void setBool(const ::std::string& key, bool val) = 0;
        virtual void setInt(const ::std::string& key, int val) = 0;
        virtual void setFloat(const ::std::string& key, float val) = 0;
        virtual void setDouble(const ::std::string& key, double val) = 0;
        virtual void setString(const ::std::string& key, const std::string& val) = 0;
        virtual void setElement(const ::std::string& key, const AbstractObjectSerializerPtr& val) = 0;
        virtual void setVariant(const ::std::string& key, const VariantPtr& val);
        virtual void setIceObject(const ::std::string& key, const SerializablePtr& val);

        virtual void setIntArray(const ::std::string& key, const std::vector<int>& val) = 0;
        virtual void setFloatArray(const ::std::string& key, const std::vector<float>& val) = 0;
        virtual void setDoubleArray(const ::std::string& key, const std::vector<double>& val) = 0;
        virtual void setStringArray(const ::std::string& key, const std::vector<std::string>& val) = 0;
        virtual void setVariantArray(const ::std::string& key, const std::vector<VariantPtr>& val) = 0;
        virtual void setVariantArray(const ::std::string& key, const std::vector<VariantBasePtr>& val) = 0;

        virtual void setVariantMap(const ::std::string& key, const StringVariantBaseMap& val) = 0;

        virtual void setBool(unsigned int index, bool val) = 0;
        virtual void setInt(unsigned int index, int val) = 0;
        virtual void setFloat(unsigned int index, float val) = 0;
        virtual void setDouble(unsigned int index, double val) = 0;
        virtual void setString(unsigned int index, const std::string& val) = 0;
        virtual void setElement(unsigned int index, const AbstractObjectSerializerPtr& val) = 0;
        virtual void setVariant(unsigned int index, const VariantPtr& val);
        virtual void setIceObject(unsigned int index, const SerializablePtr& val);

        // add all elements of val as siblings
        virtual void merge(const AbstractObjectSerializerPtr& val) = 0;

        virtual void append(const AbstractObjectSerializerPtr& val) = 0;

        // clear state and init with empty underlying object
        virtual void reset() = 0;

    public: // convenience functions to define and use ID field (useful for CommonStorage)

        std::string getIdField() const
        {
            return idField;
        }
        void setIdField(const std::string& fieldName)
        {
            idField = fieldName;
        }

        int getIntId() const
        {
            return getInt(idField);
        }
        std::string getStringId() const
        {
            return getString(idField);
        }

        void setId(int val)
        {
            setInt(idField, val);
        }
        void setId(const std::string& val)
        {
            setString(idField, val);
        }

    public: // functions to serialize/deserialize Ice object along with its ice_id
        void serializeIceObject(const SerializablePtr& obj);
        SerializablePtr deserializeIceObject();

    public:
        virtual AbstractObjectSerializerPtr createElement() const = 0;

        void setIceCommunicator(Ice::CommunicatorPtr ic);

    protected:
        AbstractObjectSerializerPtr serializeVariant(const VariantPtr& val) const;
        VariantPtr deserializeVariant();

        Ice::CommunicatorPtr ic;
        std::string idField;

    private:

    };

}

#endif
