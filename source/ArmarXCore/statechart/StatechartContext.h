/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::Statechart
* @author     Mirko Waechter( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#ifndef _ARMARX_CORE_STATECHARTCONTEXT_H
#define _ARMARX_CORE_STATECHARTCONTEXT_H


#include "StatechartEventDistributor.h"
#include "../core/Component.h"

#include <ArmarXCore/interface/observers/SystemObserverInterface.h>
#include <ArmarXCore/interface/observers/ConditionHandlerInterface.h>


namespace armarx
{
    class StatechartEventDistributor;
    typedef IceInternal::Handle<StatechartEventDistributor> StatechartEventDistributorPtr;

    class DatafieldRef;
    typedef IceInternal::Handle<DatafieldRef> DatafieldRefPtr;

    class ChannelRef;
    typedef IceInternal::Handle<ChannelRef> ChannelRefPtr;

    class State;
    typedef IceInternal::Handle<State> StatePtr;

    class DataFieldIdentifier;

    class StatechartManager;
    typedef IceUtil::Handle<StatechartManager> StatechartManagerPtr;


    /**
     * @brief The StatechartContextPropertyDefinitions class contains properties associated with all statecharts
     */
    class StatechartContextPropertyDefinitions :
        public armarx::ComponentPropertyDefinitions
    {
    public:
        StatechartContextPropertyDefinitions(std::string prefix);
    };


    /**
      \class StatechartContext
      \ingroup StatechartGrp
      \brief This class contains a statechart and provides the interfaces to distributed components.

      This class inherits from armarx::Component and should be the entry point for applications containing a statechart.
      This class contains all the proxies to distributed components like the ConditionHandler
      or the KinematicUnit.\n
      This class should only be created once per application and the top-level state of this StatechartContext
      should be set via armarx::StatechartContext::setToplevelState().

      The StatechartContext is available from the State-implementations with
      StateBase::getContext<StatechartContextType>().

      If a statechart needs other proxies to Ice objects (\see armarx::ManagedIceObject), this class
      needs to be subclassed and armarx::StatechartContext::onInitStatechartContext()
      and armarx::StatechartContext::onConnectStatechartContext() reimplemented.
      But keep in mind to call armarx::StatechartContext::onInitStatechartContext() and
      armarx::StatechartContext::onConnectStatechartContext() (or the intermediate statechart context, from
      which you are deriving) in your reimplentation.

      \snippet
      */
    class StatechartContext :
        virtual public Component
    {
    private:
        // inherited from Component
        virtual void onInitComponent();
        virtual void onConnectComponent();
        virtual void onDisconnectComponent();
        virtual void onExitComponent();

    protected:
        /**
         * @brief onInitStatechartonInitStatechartContext can be implemented by subclasses
         *
         * Override this method to specify required proxies and topics via armarx::Component::usingProxy(),
         * armarx::Component::usingTopic() and armarx::Component::offeringTopic().
         *
         * This method is calle by armarx::StatechartContext::onInitComponent().
         */
        virtual void onInitStatechartContext() {}

        /**
         * @brief onConnectStatechartContext can be implemented by subclasses
         *
         * Override this method to retrieve the proxies and topics specified in armarx::StatechartContext::onInitStatechart()
         * via armarx::Component::getProxy() and armarx::Component::getTopic().
         *
         * This method is called by armarx::StatechartContext::onConnectComponent().
         */
        virtual void onConnectStatechartContext() {}
    public:
        StatechartContext();

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        virtual PropertyDefinitionsPtr createPropertyDefinitions();

        VariantBaseList getDataListFromObserver(std::string observerName, const DataFieldIdentifierBaseList& identifierList);
        VariantBasePtr getDataFromObserver(const DataFieldIdentifierBasePtr& identifier);
        ChannelRefPtr getChannelRef(const std::string& observerName, const std::string& channelName);
        DatafieldRefPtr getDatafieldRef(const DataFieldIdentifier& datafieldIdentifier);
        DatafieldRefPtr getDatafieldRef(ChannelRefPtr channelRef, const std::string& datafieldName);

        //! use the Component::getIceManager() method in this object class
        using Component::getIceManager;

        /**
         * @brief setToplevelState initializes \p newToplevelState with the current StatechartContext and the current StatechartContext::statechartManager
         * and sets \p newToplevelState as top-level state on the current StatechartContext::statechartManager.
         * @param newToplevelState
         * @param startParameters
         * @return
         */
        bool setToplevelState(const armarx::StatePtr& newToplevelState, StringVariantContainerBaseMap startParameters = StringVariantContainerBaseMap());

        /**
         * @brief setAutoStartStatechart controls whether the toplevelstate is
         * automatically entered, when the statechart manager is started,
         * @param autostart
         */
        void setAutoEnterToplevelState(bool autoEnter);

        /**
         * @brief startStatechart actives both, the toplevel startchart stored
         * in the variable statechart and the StatechartManager for handling
         * eventprocessing.
         */
        void startStatechart();

        /**
         * @brief onInitStatechart this method is called when the statechart is started. \see armarx::RemoteStateOfferer
         */
        virtual void onInitStatechart() = 0;

        /**
         * @brief onConnectStatechart is called before armarx::StatechartContext::startStatechart() and after armarx::StatechartContext::onConnectStatechartContext()
         */
        virtual void onConnectStatechart() = 0;
        /**
         * @brief onExitStatechart can be implemented by subclasses
         *
         * Override this method to perform cleanup tasks when the statechart is left.
         */
        virtual void onExitStatechart() {}

        boost::unordered_map<std::string, ObserverInterfacePrx> observerMap;

        /**
         * The EventListenerInterface instance, that receives events from observers and redirects them to the correct states
         */
        StatechartEventDistributorPtr eventDistributor;
        StatechartManagerPtr statechartManager;
        ConditionHandlerInterfacePrx conditionHandlerPrx;
        SystemObserverInterfacePrx systemObserverPrx;

    protected:
        bool autoEnterToplevelState;
    };
    typedef IceInternal::Handle<StatechartContext> StatechartContextPtr;

}

#endif
