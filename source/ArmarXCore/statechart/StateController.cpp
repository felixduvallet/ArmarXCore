/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::statechart
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "StateController.h"
#include "RemoteState.h"
#include "StatechartManager.h"
#include "standardstates/FinalState.h"

#include <IceUtil/Time.h>


using namespace armarx;
using namespace StateUtilFunctions;


#define MAX_USER_CODE_DURATION 100 //milliseconds



StateController::StateController() :
    __eventBufferedDueToUnbreakableState(false),
    __finished(false),
    localProfiler(new Profiler::Profiler())
{
}

StateController::StateController(const StateController& source) :
    IceUtil::Shared(source),
    Ice::Object(source),
    StateIceBase(source),
    Logging(source),
    StateBase(source),
    __eventBufferedDueToUnbreakableState(false),
    __finished(false),
    localProfiler(source.localProfiler)
{
}

StateController::~StateController()
{
    //    ARMARX_INFO << "~StateController "<< stateClassName << " " << tag.tagName << " " << (long)this /*<< "\n" << LogSender::createBackTrace() */<< std::endl;
    if (__runningTask)
    {
        __runningTask->stop();
    }

    {
        ScopedLock lock(__finishedMutex);
        __finished = true;
    }

    __finishedCondition.notify_all();
}


void StateController::waitForStateToFinish(int timeoutMs) const
{
    ARMARX_INFO << "Waiting for state to finish";
    ScopedLock lock(__finishedMutex);

    if (__getParentState())
    {
        throw LocalException("You can only wait for toplevel states to finish!");
    }

    if (__finished)
    {
        return;
    }

    if (timeoutMs == -1)
    {
        __finishedCondition.wait(lock);
    }
    else
    {
        if (!__finishedCondition.timed_wait(lock, boost::posix_time::milliseconds(timeoutMs)))
        {
            throw LocalException("Statechart did not finish in time");
        }
    }
}


void
StateController::enter(const StringVariantContainerBaseMap& tempInputParameters)
{
    __checkPhase(eDefined, __PRETTY_FUNCTION__);

    if (!initialized)
    {
        throw exceptions::local::eNotInitialized();
    }

    if (__hasSubstates() && !initState._ptr)
    {
        throw exceptions::local::eNullPointerException("initState was not set!");
    }

    ARMARX_VERBOSE << "starting state " << stateName <<  " with parameters:\n" << StateUtilFunctions::getDictionaryString(tempInputParameters) << flush;

    //fill the inputDictionary of the entry state
    if (tempInputParameters.size() > 0)
    {
        createMapping()
        ->mapFromOutput("*", "*")
        ->_addSourceDictionary(eOutput, tempInputParameters)
        ->setTargetDictToGreedy(greedyInputDictionary)
        ->_applyMapping(inputParameters);
    }

    _baseOnEnter();
}

void StateController::__propagateUpdate(bool directionUp)
{
    //do own update stuff

    //propagate further
    if (directionUp)
    {
        if (__getParentState())
        {
            __getParentState()->__propagateUpdate(directionUp);
        }
    }
    else
    {
        for (unsigned int i = 0; i < subStateList.size(); i++)
        {
            StateControllerPtr state = StateControllerPtr::dynamicCast(subStateList.at(i));
            state->__propagateUpdate(directionUp);
        }
    }
}



void
StateController::__substatesFinished(const EventPtr ev)
{

    activeSubstate = NULL; // line needed or not?! Pro: Finalstate will be broken on leaving of parentstate - Contra: baseOnExit() of finalstate wont be called.
    triggeredEndstateEvent = ev;

    if (__getParentState() && StateControllerPtr::dynamicCast(__getParentState()))
    {
        __finalize(ev);
    }
    else
    {
        clearSelfPointer();
        _baseOnExit();
        STATEINFO << "Statemachine finished with event '" <<  ev->eventName << "' in state '" << getGlobalHierarchyString() << "'\n"
                  << "The resulting output dictionary:\n" << StateUtilFunctions::getDictionaryString(getOutputParameters()) << flush ;
        {
            ScopedLock lock(__finishedMutex);
            __finished = true;
            __finishedCondition.notify_all();
        }
    }
}


// in function, so that it can be overridden in remoteStateWrapper
void StateController::__finalize(const EventPtr event)
{

    __enqueueEvent(event);
}


void StateController::__enqueueEvent(const EventPtr event)
{
    if (__getParentState())
    {
        manager->addEvent(event, __getParentState());
    }
}







bool StateController::__applyMappings(const StateControllerPtr& srcState, const TransitionIceBase& t, const EventPtr& event, TransitionError& error)
{
    // get source dictionaries and apply the mapping
    const StringVariantContainerBaseMap parentSetInputParams = __getSetInputAndLocalParameters();
    StringVariantContainerBaseMap srcSetOutputParams;

    if (srcState)
    {
        srcSetOutputParams = StateUtilFunctions::getSetValues(srcState->getOutputParameters());
    }

    auto applyMapping = [&](const ParameterMappingIceBasePtr & icepm, StateParameterMap & targetMap, bool greedy)
    {
        PMPtr pm = PMPtr::dynamicCast(icepm);

        if (pm)
        {
            pm->setTargetDictToGreedy(greedy);

            pm->_addSourceDictionary(eParent, parentSetInputParams)
            ->_addSourceDictionary(eOutput, srcSetOutputParams)
            ->_addSourceDictionary(eEvent, event->properties)
            ->_applyMapping(targetMap);
        }
    };
    applyMapping(t.mappingToParentStatesLocal, localParameters, false);
    applyMapping(t.mappingToParentStatesOutput, outputParameters, false);
    applyMapping(t.mappingToNextStatesInput, t.destinationState->inputParameters, t.destinationState->greedyInputDictionary);
    ARMARX_DEBUG << "The following input parameters for state " << getStateName() << " are available: " << StateUtilFunctions::getDictionaryString(inputParameters);
    std::string paramCheckOutput;

    if (!checkForCompleteParameters(t.destinationState->inputParameters, &paramCheckOutput))
    {
        error.errorType = eTransitionErrorInput;
        error.infos.push_back(t.destinationState->stateName);
        error.infos.push_back(paramCheckOutput);
        __printTransitionError(error, event);
        return false;
    }

    return true;
}

void StateController::__printTransitionError(const TransitionError& transitionError, const EventPtr& event) const
{
    switch (transitionError.errorType)
    {
        case eTransitionErrorInput:
            ARMARX_WARNING << "Could not execute transition!\nReturned reason: " + StateUtilFunctions::transitionErrorToString(transitionError.errorType) + "\nExplanations:\n"
                           << "\t- The input parameters of the destination state were not fully set.\n\tDestination state: '" << transitionError.infos.at(0) << "'\n" <<
                           transitionError.infos.at(1);
            break;

        case eTransitionErrorUnexpectedEvent:
            ARMARX_WARNING << "Could not execute transition!\nReturned reason: " + StateUtilFunctions::transitionErrorToString(transitionError.errorType) + "\nExplanations:\n"
                           << "\t- The state '" << stateName << "' does not expect the event '" << event->eventName << "' while in substate '" << activeSubstate->stateName << "'.\n" << flush;
            break;

        default:
            ARMARX_WARNING << "Could not execute transition!\nReturned reason: " + StateUtilFunctions::transitionErrorToString(transitionError.errorType) << flush;
    }
}


std::string StateController::getParentStateName()
{
    if (__getParentState())
    {
        return __getParentState()->getStateName();
    }
    else
    {
        return "TopLevel";
    }
}


void StateController::__processEvent(const EventPtr event, bool buffered)
{
    STATEINFO << "TRANSITION due to Event '" << event->eventName  << "' (EventReceiver: '" << event->eventReceiverName << "')..." << flush;
    boost::recursive_mutex::scoped_lock lock(__processEventMutex);

    if (!initialized)
    {
        throw exceptions::local::eNotInitialized();
    }

    //check if the current state is breakable and if any parent state has events to process
    if (!buffered
        && __hasActiveSubstate() && !activeSubstate->unbreakable
        && !__getUnbreakableBufferStati())
    {
        ARMARX_WARNING << "a parent state has events to process. this event will be skipped" << flush;
        return;
    }

    TransitionError transitionError;
    transitionError.errorType = eTransitionErrorUndefined;

    TransitionIceBase t;

    if (__findValidTransition(event, activeSubstate, t, transitionError))
    {
        StateControllerPtr src = StateControllerPtr::dynamicCast(activeSubstate);

        if (src && src->__hasSubstates() && src->__hasActiveSubstate())
        {
            ARMARX_INFO << "<<<< Trying to break substates of " << src->stateName << flush;

            // Check if the active substate is unbreakable
            if (src->__breakActiveSubstate(event))
            {
                src->_baseOnExit();
            }
            else  // substate of src unbreakable
            {
                HiddenTimedMutex::ScopedLock lock(__eventUnbreakableBufferMutex);
                __unbreakableBuffer.push(event);
                __notifyEventBufferedDueToUnbreakableState();

                ARMARX_INFO << "got event while in unbreakable substate: parent stateName:" <<  stateName << flush;
                return;
            }
        }
        else if (src)
        {
            src->_baseOnExit();
        }

        if (t.fromAll)
        {
            // required by StatechartLogger
            // source state is empty if event is received as fromAll
            t.sourceState = activeSubstate;
        }

        // set to NULL in case mapping could not be applied, so onExit wont be called twice
        activeSubstate = NULL;


        if (__applyMappings(src, t, event, transitionError))
        {
            activeSubstate = t.destinationState;
            StateControllerPtr::dynamicCast(t.destinationState)->_baseOnEnter();

            //check if unbreakable state has caused a lock
            if (src && src->unbreakable && src->eventsDelayed)
            {
                if (__getParentState() &&  !__getParentState()->__getUnbreakableBufferStati())
                {
                    // check if next state is unbreakable as well
                    if (t.destinationState->unbreakable)
                    {
                        t.destinationState->eventsDelayed = true;
                    }
                    else
                    {
                        __getParentState()->__processBufferedEvents();
                    }
                }
            }

            if (t.sourceState && activeSubstate)
            {
                localProfiler->logStatechartTransition(getParentStateName(), t.sourceState->stateName, t.destinationState->stateName);
            }

            //        if(activeSubstate){// process event queue of active substate //move to after loop?{
            //            //its a new active state anyway, should not have any events? -> if OnEnter() contains enqueueEvent(), it will be inserted in the buffer of the active substate -> change to parent state?
            //            StateControllerPtr::dynamicCast(activeSubstate)->__processEvents();
            //        }
            //__propagateUpdate(); //<-- needs to be a more intelligent update (do not send all data every time) or the update has to be requested from outside

            // Transition done
            return;
        }
    }

    //no fitting transition found->throw
    if (activeSubstate)
    {
        __printTransitionError(transitionError, event);
        //throw eUnexpectedEvent(event, activeSubstate);
    }
    else
    {
        ARMARX_WARNING << "The state '" << stateName << "' does not have an activeSubstate and therefore cannot process any events." << flush;
    }
}

bool StateController::__findValidTransition(const EventPtr& event, const StateIceBasePtr& sourceState, TransitionIceBase& resultTransition, TransitionError& error) const
{
    // loop through transition table to find the correct transition fitting to the event, activeState and DestinationState

    error.errorType = eTransitionNoError;

    // first check if there is a transition fitting to the input

    int selectedTransitionIndex = -1;

    for (unsigned int i = 0; i < transitions.size(); i++)
    {
        const TransitionIceBase& transition = transitions.at(i);

        if (
            (transition.sourceState == sourceState || transition.fromAll) // correct source state
            && transition.evt->eventName == event->eventName // correct event
            && (sourceState->stateName == event->eventReceiverName || event->eventReceiverName == EVENTTOALL) // correct eventreceiver
        )
        {
            selectedTransitionIndex = i;
            break;
        }
    }

    if (selectedTransitionIndex == -1)
    {
        error.errorType = eTransitionErrorUnexpectedEvent;
        return false;
    }

    const TransitionIceBase& transition = transitions.at(selectedTransitionIndex);

    resultTransition = transition;
    error.errorType = eTransitionNoError;
    return true;
}

TransitionError StateController::__validateTransition(const TransitionIceBase& transition, const EventPtr event, const StateIceBasePtr& sourceState, const StateIceBasePtr& destinationState) const
{
    TransitionError error;

    if (
        (transition.sourceState != sourceState && !transition.fromAll) // wrong source state
        || transition.evt->eventName != event->eventName // wrong event
        || (sourceState && sourceState->stateName != event->eventReceiverName && event->eventReceiverName != EVENTTOALL) // wrong eventreceiver
    )
    {
        error.errorType = eTransitionErrorUnexpectedEvent;
        return error;
    }


    if (transition.sourceState && transition.fromAll) // this should never happen
    {
        error.errorType = eTransitionErrorUndefined;
        return error;
    }


    // get source dictionaries and apply the mapping
    PMPtr transitionMapping = PMPtr::dynamicCast(transition.mappingToNextStatesInput);

    if (transitionMapping && sourceState)
    {
        const StringVariantContainerBaseMap parentSetInputParams = __getSetInputAndLocalParameters();
        const StringVariantContainerBaseMap srcSetOutputParams = StateUtilFunctions::getSetValues(sourceState->outputParameters);
        transitionMapping->setTargetDictToGreedy(transition.destinationState->greedyInputDictionary);
        StateParameterMap inputCopy;
        StateUtilFunctions::copyDictionary(destinationState->inputParameters, inputCopy);
        transitionMapping->_addSourceDictionary(eParent, parentSetInputParams)
        ->_addSourceDictionary(eOutput, srcSetOutputParams)
        ->_addSourceDictionary(eEvent, event->properties)
        ->_applyMapping(inputCopy);
        std::string paramCheckOutput;

        if (!checkForCompleteParameters(inputCopy, &paramCheckOutput))
        {
            error.errorType = eTransitionErrorInput;
            error.infos.push_back(destinationState->stateName + ": " + paramCheckOutput);
            return error;
        }
    }

    error.errorType = eTransitionNoError;
    return error;
}

void StateController::__notifyEventBufferedDueToUnbreakableState(bool eventBuffered)
{
    __eventBufferedDueToUnbreakableState = eventBuffered;

    for (unsigned int i = 0; i < subStateList.size(); ++i)
    {
        StateControllerPtr state = StateControllerPtr::dynamicCast(subStateList.at(i));

        if (state->__getUnbreakableBufferSize() > 0) // check if an event in this state is buffered. if it is, the eventBuffered must be set to true, otherwise dont change it
        {
            eventBuffered = true;
        }

        StateControllerPtr::dynamicCast(subStateList.at(i))->__notifyEventBufferedDueToUnbreakableState(eventBuffered);
    }
}



StateControllerPtr StateController::__getParentState() const
{
    StateControllerPtr ptr = StateControllerPtr::dynamicCast(__parentState);

    if (!ptr && __parentState)
    {
        throw LocalException("Could not cast parentState into StateControllerPtr");
    }

    return ptr;
}

bool StateController::__breakActiveSubstate(const EventPtr event)
{
    bool result = true;
    boost::recursive_mutex::scoped_lock lock(__processEventMutex);

    if (__hasActiveSubstate())
    {
        result = StateControllerPtr::dynamicCast(activeSubstate)->_baseOnBreak(event);

        if (result) //active substate is broken -> set to NULL
        {
            activeSubstate = NULL;
        }
    }

    return result;
}




void
StateController::__processBufferedEvents()
{
    while (__getUnbreakableBufferSize() > 0)
    {
        ARMARX_VERBOSE << "processing " << __unbreakableBuffer.front()->eventName << " in unbreakableBuffer  - eventprocessorstate:" << stateName << " - activeState: " << activeSubstate->stateName << flush;
        __eventUnbreakableBufferMutex.lock();
        EventPtr event = __unbreakableBuffer.front();
        __unbreakableBuffer.pop();
        __eventUnbreakableBufferMutex.unlock();
        __processEvent(event, true);

        if (__getUnbreakableBufferSize() == 0)
        {
            __notifyEventBufferedDueToUnbreakableState(false);
        }
    }

    if (__getParentState())
    {
        StateControllerPtr::dynamicCast(__getParentState())->__processBufferedEvents();
    }
}


void StateController::__waitForRemoteStates() const
{
    for (unsigned int i = 0; i < subStateList.size(); ++i)
    {
        RemoteStatePtr remoteStatePtr = RemoteStatePtr::dynamicCast(subStateList.at(i));

        if (remoteStatePtr)
        {
            ARMARX_INFO << "Waiting for RemoteState " << remoteStatePtr->getName();

            while (!(remoteStatePtr->getState() >= eManagedIceObjectStarted))
            {
                usleep(10);
            }

            ARMARX_INFO << "RemoteState " << remoteStatePtr->getName() << "started.";
            break;
        }
    }
}

bool StateController::__checkExistenceOfTransition(const TransitionIceBase& transition)
{
    for (unsigned int i = 0; i < transitions.size(); ++i)
    {
        if (transitions[i].evt->eventName == transition.evt->eventName
            && (transition.fromAll || transitions[i].sourceState == transition.sourceState)
            &&  transitions[i].destinationState == transition.destinationState
           )
        {
            return true;
        }
    }

    return false;
}

void
StateController::_baseOnEnter()
{
    if (!initialized)
    {
        throw exceptions::local::eNotInitialized();
    }

    {
        HiddenTimedMutex::ScopedLock lock(__stateMutex);

        if (__runningTask)
        {
            try
            {
                if (__runningTask->isRunning())
                {
                    ARMARX_VERBOSE << "Waiting for running task of " << stateName << " to finish";
                }

                __runningTask->stop(); // this blocks until the thread has finished
            }
            catch (IceUtil::ThreadSyscallException& e)
            {

            }
        }

        eventsDelayed = false;

        //reset local & output parameters, so that they are NOT set in case we have been in this state before
        StateUtilFunctions::unsetParameters(localParameters);
        StateUtilFunctions::unsetParameters(outputParameters);

        // std::ARMARX_INFO << StateUtil::getDictionaryString(inputParameters);
        std::string paramCheckOutput;

        if (!checkForCompleteParameters(inputParameters, &paramCheckOutput))
        {
            throw LocalException("Not all required inputparameters of the state '" + stateName + "' are set:\n" + paramCheckOutput);
        }

        // overwrite status of last visit
        cancelEnteringSubstates = false;

        triggeredEndstateEvent = NULL;

        visitCounter++;

        STATEINFO << "Entering State '" << getLocalHierarchyString() << "' (id: " << localUniqueId << ")" << flush;
        IceUtil::Time executionStart = IceUtil::Time::now();

        try
        {
            setStatePhase(eEntering);
            localProfiler->logEvent(Profiler::Profiler::EventType::eFunctionStart, getParentStateName(), getStateName());
            onEnter();
        }
        catch (exceptions::local::eStateAlreadyLeft& e)
        {
        }
        catch (...)
        {
            ARMARX_ERROR << "onEnter() of State " << globalStateIdentifier << " of class " << stateClassName << " failed. Ignoring substates and sending Failure..." << flush;
            handleExceptions();
            lock.unlock();
            setStatePhase(eEntered);
            __enqueueEvent(new Failure(stateName));
            return;

        }

        setStatePhase(eEntered);

        IceUtil::Time duration = IceUtil::Time::now() - executionStart;

        if (duration.toMilliSeconds() > MAX_USER_CODE_DURATION)
        {
            ARMARX_WARNING << "onEnter() of state '" + stateName + "' took more than " << MAX_USER_CODE_DURATION << " ms (In fact: " << duration.toMilliSeconds() << " ms).  The onEnter() method should not calculate complex operations." << flush;
        }
    }

    this->notifyActiveStateChanged(StateIceBasePtr::dynamicCast(this));
    {
        HiddenTimedMutex::ScopedLock lock(__stateMutex);

        if (!cancelEnteringSubstates)
        {
            if (__hasSubstates())
            {
                cancelEnteringSubstates = false;

                if (!initState._ptr)
                {
                    ARMARX_WARNING << "No initial substate set in '" << stateName << "'" << flush;
                }
                else
                {
                    StringVariantContainerBaseMap combinedMap =  __getSetInputAndLocalParameters();
                    ARMARX_DEBUG << "Source for initial state: " << StateUtilFunctions::getDictionaryString(combinedMap);

                    if (initialStateMapping)
                    {
                        PMPtr mapping = ParameterMappingPtr::dynamicCast(initialStateMapping);
                        mapping->_addSourceDictionary(eParent, combinedMap)
                        ->setTargetDictToGreedy(initState->greedyInputDictionary)
                        ->_applyMapping(initState->inputParameters);
                    }

                    activeSubstate = initState;
                    lock.unlock();
                    StateControllerPtr::dynamicCast(initState)->_baseOnEnter();
                }
            }
        }
        else
        {
            STATEINFO << "Entering substates of " << stateName << " has been canceled";
        }
    }

    // triggers to run the run()-Function in a separate thread
    _startRun();
}

void
StateController::_startRun()
{
    HiddenTimedMutex::ScopedLock lock(__stateMutex);

    if (!__useRunFunction)
    {
        return;
    }

    // only execute if still active state and not changed to another state in onEnter()
    if (!__parentState || __parentState->activeSubstate.get() == this)
    {
        __runningTask = new RunningTask<StateController>(this, &StateController::_baseRun, stateClassName + "RunningTask");
        __runningTask->start();
    }
}

void
StateController::_baseRun()
{
    try
    {
        run();
    }
    catch (exceptions::local::eStateAlreadyLeft& e)
    {
    }
    catch (...)
    {
        if (getStatePhase() == eEntered)
        {
            ARMARX_ERROR_S << "run() of State " << globalStateIdentifier << " of class " << stateClassName << " failed for unhandled reason. Ignoring substates and sending Failure..." << flush;

            handleExceptions();

            __enqueueEvent(new Failure(stateName));
        }

        return;
    }
}

void
StateController::_baseOnExit()
{
    HiddenTimedMutex::ScopedLock lock(__stateMutex);

    // substates must not be entered if onexit has already been called
    cancelEnteringSubstates = true;

    if (__runningTask)
    {
        __runningTask->stop(false);
        //runningTask = NULL;
    }

    if (!initialized)
    {
        throw exceptions::local::eNotInitialized();
    }

    if (__hasActiveSubstate())
    {
        throw exceptions::local::eStatechartLogicError("baseOnExit was called before substates were finished - baseOnBreak must be called instead. ActiveSubstate: " + activeSubstate->stateName);
    }


    STATEINFO << "Leaving State '" <<  getLocalHierarchyString() << "' (id: " << localUniqueId << ")" << flush;

    IceUtil::Time executionStart = IceUtil::Time::now();

    try
    {
        setStatePhase(eExiting);
        onExit();
        localProfiler->logEvent(Profiler::Profiler::EventType::eFunctionReturn, getParentStateName(), getStateName());
    }
    catch (...)
    {
        ARMARX_ERROR << "onExit() of State " << globalStateIdentifier << " of class " << stateClassName << " failed." << flush;
        handleExceptions();
    }

    IceUtil::Time duration = IceUtil::Time::now() - executionStart;
    _removeInstalledConditions();


    setStatePhase(eExited);



    if (duration.toMilliSeconds() > MAX_USER_CODE_DURATION)
    {
        ARMARX_WARNING << "onExit() of state '" + stateName + "' took more than " << MAX_USER_CODE_DURATION << " ms (In fact: " << duration.toMilliSeconds() << " ms).  The onEnter() method should not calculate complex operations." << flush;
    }

    std::string paramCheckOutput;

    if (!checkForCompleteParameters(getOutputParameters(), &paramCheckOutput))
    {
        throw LocalException("Not all required Outputparameters of state '" + stateName + "' are set:\n" + paramCheckOutput);
    }

    //reset input parameters, so that they are NOT set when we reenter this state
    StateUtilFunctions::unsetParameters(inputParameters);
    __copyDefaultValuesToInput();

    if (!configFile.empty()) // reapply parameters from configfile, if configfile is set
    {
        __paramLoader.apply(inputParameters);
    }
}


bool
StateController::_baseOnBreak(const EventPtr evt)
{
    HiddenTimedMutex::ScopedLock lock(__stateMutex);

    if (__runningTask)
    {
        __runningTask->stop(false);
        //runningTask = NULL;
    }

    if (!initialized)
    {
        throw exceptions::local::eNotInitialized();
    }



    bool result = true;

    if (unbreakable)
    {
        eventsDelayed = true;
        result = false;
    }
    else if (subStateList.size() > 0 && __hasActiveSubstate())
    {
        // Check if the active substate is unbreakable
        if (activeSubstate->unbreakable)
        {
            // signal to upper states that this state cannot be broken
            activeSubstate->eventsDelayed = true;
            result = false;
        }
        else
        {
            result = __breakActiveSubstate(evt);
        }
    }

    if (result)
    {
        STATEINFO << "Breaking State '" <<  getLocalHierarchyString() << "' (id: " << localUniqueId << ")" << flush;

        if (__runningTask)
        {
            __runningTask->stop(false);
            //runningTask = NULL;
        }

        IceUtil::Time executionStart = IceUtil::Time::now();

        try
        {
            setStatePhase(eBreaking);
            onBreak();
            localProfiler->logEvent(Profiler::Profiler::EventType::eFunctionBreak, getParentStateName(), getStateName());
            setStatePhase(eDefined);
        }
        catch (...)
        {
            ARMARX_ERROR << "onBreak() of State " << globalStateIdentifier << " of class " << stateClassName << " failed. Ignoring substates and sending Failure..." << flush;

            handleExceptions();
        }

        _removeInstalledConditions();

        IceUtil::Time duration = IceUtil::Time::now() - executionStart;

        if (duration.toMilliSeconds() > MAX_USER_CODE_DURATION)
        {
            ARMARX_WARNING << "onEnter() of state '" + stateName + "' took more than " << MAX_USER_CODE_DURATION << " ms (In fact: " << duration.toMilliSeconds() << " ms).  The onEnter() method should not calculate complex operations." << flush;
        }
    }

    if (result)
    {
        //reset input parameters, so that they are NOT set when we reenter this state
        StateUtilFunctions::unsetParameters(inputParameters);
        __copyDefaultValuesToInput();

        if (!configFile.empty()) // reapply parameters from configfile, if configfile is set
        {
            __paramLoader.apply(inputParameters);
        }

    }

    return result;

}


void StateController::notifyActiveStateChanged(const StateIceBasePtr& newActiveState)
{
    for (StateListenerPrx listener : stateListenerMap)
    {
        listener->begin_activeStateChanged(newActiveState);
    }
}


void StateController::notifyActiveStateChangedRecursive(const StateIceBasePtr& newActiveState)
{
    notifyActiveStateChanged(newActiveState);

    for (StateIceBasePtr iceState : subStateList)
    {
        StateControllerPtr stateController = StateControllerPtr::dynamicCast(iceState);

        if (stateController)
        {
            stateController->notifyActiveStateChanged(newActiveState);
        }
    }
}


void StateController::setProfilerRecursive(Profiler::ProfilerPtr profiler, int recursiveLevels)
{
    localProfiler = profiler;

    if (recursiveLevels < 0)
    {
        // fall through: the value -1 means that all substates should be profiled
    }
    else if (recursiveLevels == 0)
    {
        return;
    }
    else if (recursiveLevels > 0)
    {
        recursiveLevels--;
    }

    for (StateIceBasePtr state : subStateList)
    {
        StateControllerPtr stateController = StateControllerPtr::dynamicCast(state);

        if (stateController)
        {
            stateController->setProfilerRecursive(profiler, recursiveLevels);
        }
    }
}


void StateController::registerStateListener(const StateListenerPrx& listener)
{
    stateListenerMap.insert(listener);
}


void StateController::unregisterStateListener(const StateListenerPrx& listener)
{
    stateListenerMap.erase(listener);
}


void StateController::unregisterStateListenerRecursive(const StateListenerPrx& listener)
{
    unregisterStateListener(listener);

    StateList::iterator it = subStateList.begin();

    while (it != subStateList.end())
    {
        StateControllerPtr stateController = StateControllerPtr::dynamicCast(*it);

        if (stateController)
        {
            stateController->unregisterStateListenerRecursive(listener);
        }

        it++;
    }
}


bool StateController::isRunningTaskStopped() const
{
    if (!__runningTask)
    {
        return true;
    }

    return __runningTask->isStopped();
}


bool
StateController::__getUnbreakableBufferStati() const
{
    // commented due to performance issues
    //        const StateBase * curParentState = this;
    //        while(curParentState)
    //        {
    //            if(curParentState->__getUnbreakableBufferSize() > 0){
    //                return false;
    //            }
    //            curParentState = curParentState->__parentState;
    //        }
    //        return true;
    return !__eventBufferedDueToUnbreakableState;
}


void StateController::disableRunFunction()
{
    for (unsigned int i = 0; i < subStateList.size(); i++)
    {
        StateControllerPtr::dynamicCast(subStateList[i])->disableRunFunction();
    }

    HiddenTimedMutex::ScopedLock lock(__stateMutex);
    __useRunFunction = false;

    if (__runningTask)
    {
        ARMARX_VERBOSE << "State with name '" << stateName << "' is waiting for the RunFunction";
        __runningTask->stop();
    }

    __runningTask = NULL;
}

