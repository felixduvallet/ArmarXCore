/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "InputParameterLoader.h"

// Statechart Includes
#include "StateUtilFunctions.h"
#include <ArmarXCore/observers/variant/DataFieldIdentifier.h>
//#include <ArmarXCore/core/Pose.h>
#include <ArmarXCore/observers/variant/Variant.h>
#include <ArmarXCore/core/system/ArmarXDataPath.h>

// ArmarX Includes
#include "ArmarXCore/core/logging/Logging.h"

// Ice Includes
#include <IceUtil/UUID.h>
#include <Ice/ObjectAdapter.h>
#include <Ice/ObjectFactory.h>

// boost Includes


using namespace std;

namespace armarx
{


    template <>
    void InputParameterLoader::addElementstoList<VariantDataClass>(SingleTypeVariantListBasePtr& list, const boost::property_tree::ptree& tree, const std::string& xml_key)
    {
        using namespace boost::property_tree;

        std::stringstream currentItemXmlPathStr;
        int i = 0;
        currentItemXmlPathStr << xml_key << ".Item" << i;
        VariantTypeId type = list->getContainerType();

        //        while( (value=tree.get_optional<float>(currentItemXmlPathStr.str())), value.is_initialized()){
        while (tree.get_child_optional(currentItemXmlPathStr.str()).is_initialized())
        {

            std::string typeStr =  Variant::typeToString(type);
            VariantDataClassPtr data;

            if (ic)
            {
                Ice::ObjectFactoryPtr objFac = ic->findObjectFactory(typeStr);
                data = VariantDataClassPtr::dynamicCast(objFac->create(typeStr));
                //VariantDataClass data = var->getClass< armarx::VariantDataClass >();
                std::ostringstream stream;
                xml_parser::write_xml(stream, tree.get_child(currentItemXmlPathStr.str()));
                //ARMARX_ERROR << stream.str();
                Ice::Current c;
                c.adapter = ic->createObjectAdapterWithEndpoints("Dummy" + IceUtil::generateUUID(), "tcp"); // pass adapter to readFromXML so that a proxy can be retrieved in the variant
                data->readFromXML(stream.str(), c);


                list->addElement(new SingleVariant(Variant(data)));
            }
            else
            {
                ARMARX_WARNING << "could not load parameter '" << xml_key << "' - Ice Communicator is null, so cannot use ObjectFactory" << flush;
            }

            i++;
            currentItemXmlPathStr.seekp(0);
            currentItemXmlPathStr << xml_key << ".Item" << i;
        }
    }

    template <>
    void InputParameterLoader::addEmptyElementstoPTree<VariantDataClass>(const Variant& element,  boost::property_tree::ptree& tree, const std::string& xml_key)
    {
        using namespace boost::property_tree;
        std::stringstream sstr;
        int i = 0;
        sstr << xml_key << ".Item" << i;

        std::string typeStr =  Variant::typeToString(element.getType());
        VariantDataClassPtr data;

        if (ic)
        {
            Ice::ObjectFactoryPtr objFac = ic->findObjectFactory(typeStr);

            if (!objFac)
            {
                throw exceptions::local::eNullPointerException("Could not find ObjectFactory for string '" + typeStr + "'");
            }

            data = VariantDataClassPtr::dynamicCast(objFac->create(typeStr));
        }
        else
        {
            ARMARX_WARNING << "could not write parameter '" << xml_key << "' of type " << typeStr << flush;
            return;
        }

        static Ice::Current c;

        if (!c.adapter)
        {
            c.adapter = ic->createObjectAdapterWithEndpoints("Dummy" + IceUtil::generateUUID(), "tcp");    // pass adapter to readFromXML so that a proxy can be retrieved in the variant
        }

        ptree subTree;
        xml_parser::read_xml(data->writeAsXML(), subTree);

        tree.add_child(sstr.str(), subTree);
    }


    InputParameterLoader::InputParameterLoader()
    {
        setTag("InputParameterLoader");

    }

    InputParameterLoader::InputParameterLoader(const InputParameterLoader& source) :
        Logging(source)
    {
        StateUtilFunctions::copyDictionary(source.paramBufferMap, paramBufferMap);
    }

    void InputParameterLoader::setIceCommunicator(Ice::CommunicatorPtr ic)
    {
        this->ic = ic;
    }

    void InputParameterLoader::load(const boost::filesystem::path& configFileName, const std::string stateName, StateParameterMap& map)
    {
        std::string configFileNameStr;

        if (!ArmarXDataPath::getAbsolutePath(configFileName.string(), configFileNameStr))
        {
            ARMARX_WARNING << "Creating standard config file: " << configFileName.string();
            _createEmptyXMLConfigFile(boost::filesystem::path(configFileName), stateName, map);
            return;
        }

        ARMARX_VERBOSE << "Home: " << ArmarXDataPath::getHomePath();
        //        ARMARX_VERBOSE << "CWD: " <<  boost::filesystem::current_path() << flush;

        ARMARX_VERBOSE << "loading StateParameters from file : " << configFileNameStr << flush;

        using namespace boost::property_tree;
        ptree pt;

        try
        {
            xml_parser::read_xml(configFileNameStr, pt);
        }
        catch (xml_parser::xml_parser_error& e)
        {

            ARMARX_WARNING << "Could not load state-configfile '"
                           << configFileNameStr << "'. Required parameters may be unset.\nReason at "
                           << e.filename() << ":" << e.line() << ":\n" << e.message() << flush;
            return;
        }

        load(pt, stateName, map);
    }

    void InputParameterLoader::load(const std::string& xmlContent, const std::string stateName, StateParameterMap& map)
    {
        using namespace boost::property_tree;
        ptree pt;
        std::stringstream stream;
        stream << xmlContent;

        try
        {
            xml_parser::read_xml(stream, pt);
        }
        catch (xml_parser::xml_parser_error& e)
        {

            ARMARX_WARNING << "Could not parse xml-string! Content: " << e.what() << "\n'" << xmlContent << std::endl;

            return;
        }

        load(pt, stateName, map);
    }

    void InputParameterLoader::load(const boost::property_tree::ptree& pt, const std::string stateName, StateParameterMap& map)
    {
        using namespace boost::property_tree;



        StateParameterMap::iterator it = map.begin();

        for (; it != map.end(); ++it)
        {
            std::string xml_key = stateName + ".StateParameters." + it->first ;
            ARMARX_VERBOSE << "Loading Parameter " << xml_key << flush;
            it->second->set = true;

            try
            {
                static Ice::Current c;

                if (!c.adapter)
                {
                    c.adapter = ic->createObjectAdapterWithEndpoints("Dummy" + IceUtil::generateUUID(), "tcp");    // pass adapter to readFromXML so that a proxy can be retrieved in the variant
                }

                std::ostringstream stream;
                xml_parser::write_xml(stream, pt.get_child(xml_key));
                std::string outputXml = stream.str();
                const std::string xmlHeader = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
                size_t headerPosition = outputXml.find(xmlHeader);

                if (outputXml.npos != headerPosition)
                {
                    outputXml.replace(headerPosition, headerPosition + xmlHeader.size(), std::string(""));
                }

                ContainerTypePtr type = it->second->value->getContainerType();

                if (type->subType) // if it is a complex container (not a SingleVariant()), the real container needs to be generated here
                {
                    Ice::ObjectFactoryPtr objFac = ic->findObjectFactory(type->typeId);
                    it->second->value = VariantContainerBasePtr::dynamicCast(objFac->create(type->typeId));
                    it->second->value->setContainerType(type);
                }

                it->second->value->readFromXML(outputXml, c);
                //                if (!it->second->value->getContainerType()->subType)
                //                {

                //                    VariantPtr var = SingleVariantPtr::dynamicCast(it->second->value)->get();
                //                        if(!var)
                //                            throw LocalException("Could not cast to VariantPtr");
                //                        VariantTypeId type = Variant::hashTypeName(it->second->value->getContainerType()->typeId);

                //                        if(type == VariantType::Bool)
                //                            var->setBool(pt.get<bool>(xml_key));
                //                        else if(type == VariantType::String)
                //                            var->setString(pt.get<std::string>(xml_key));
                //                        else if(type == VariantType::Float)
                //                            var->setFloat(pt.get<float>(xml_key));
                //                        else if(type == VariantType::Int)
                //                            var->setInt(pt.get<int>(xml_key));
                //                        else // must be VariantDataClass
                //                        {
                //                            std::string typeStr =  Variant::typeToString(type);
                //                            VariantDataClassPtr data;
                //                            if(ic)
                //                            {
                //                                Ice::ObjectFactoryPtr objFac= ic->findObjectFactory(typeStr);
                //                                if(!objFac)
                //                                    throw exceptions::local::eNullPointerException("Could not find ObjectFactory for string '"+typeStr+"'");
                //                                data = VariantDataClassPtr::dynamicCast(objFac->create(typeStr));
                //                            }
                //                            else {
                //                                ARMARX_WARNING << "could not load parameter '" << xml_key << "'" << flush;
                //                                continue;
                //                            }
                //                            //VariantDataClass data = var->getClass< armarx::VariantDataClass >();
                //                            std::ostringstream stream;
                //                            xml_parser::write_xml(stream, pt.get_child(xml_key));
                //                            static Ice::Current c;
                //                            if(!c.adapter)
                //                                c.adapter = ic->createObjectAdapterWithEndpoints("Dummy"+IceUtil::generateUUID(), "tcp"); // pass adapter to readFromXML so that a proxy can be retrieved in the variant
                //                            data->readFromXML(stream.str(), c);
                //                            var->setClass(data);
                //                        }
                //                }


                // TODO: ChannelRef: ObserverPrx from observername




                //                    case eVariantListParam:
                //                    {
                //                        SingleTypeVariantListBasePtr varList = it->second->value->getVariantList();

                //                        VariantTypeId type = it->second->value->getVariantType();

                //                        if(type == VariantType::Bool)
                //                            addElementstoList<bool>(varList, pt, xml_key);
                //                        else if(type == VariantType::String)
                //                            addElementstoList<std::string>(varList, pt, xml_key);
                //                        else if(type == VariantType::Float)
                //                            addElementstoList<float>(varList, pt, xml_key);
                //                        else if(type == VariantType::Int)
                //                            addElementstoList<int>(varList, pt, xml_key);
                //                        else // must be VariantDataClass
                //                            addElementstoList<VariantDataClass>(varList, pt, xml_key);

                //                        break;
                //                    }
                //                    default:
                //                        it->second->set=false;
                //                        throw LocalException("Unknown Variant Type - couldnt load it from file : '" + it->first + "'->" + it->second->value->getVariant()->getTypeName());

                //                }
            }
            catch (boost::property_tree::ptree_error& e)
            {
                ARMARX_ERROR << "Error while loading parameter '" << xml_key << "'. Reason: " << e.what() << flush;
            }

            paramBufferMap[it->first] = it->second->value->cloneContainer();
        }
    }

    void InputParameterLoader::apply(StateParameterMap& map)
    {
        if (!paramBufferMap.size())
            // throw LocalException("The ParameterBufferMap is empty - maybe you forgot to call load()?");
        {
            ARMARX_WARNING << "The ParameterBufferMap is empty - maybe you forgot to call load()?" << flush;
        }

        StateUtilFunctions::fillDictionary(paramBufferMap, map);
    }

    void
    InputParameterLoader::_createEmptyXMLConfigFile(const boost::filesystem::path& configFile, const string& stateName, const StateParameterMap& map)
    {
        //        using namespace boost::property_tree;

        //        boost::property_tree::ptree pt;
        //        std::string basicXMLPath = stateName + ".StateParameters";


        //        StateParameterMap::const_iterator it = map.begin();
        //        for(; it != map.end(); ++it)
        //        {
        //            std::string xml_key = stateName + ".StateParameters." + it->first ;
        //            ARMARX_VERBOSE << "Writing Parameter " << xml_key << flush;
        //            it->second->set = true;
        //            try{
        //                switch (it->second->value->getParameterType())
        //                {
        //                    case eVariantParam:
        //                    {
        //                        VariantPtr var = VariantPtr::dynamicCast(it->second->value->getVariant());

        //                        VariantTypeId type = it->second->value->getVariantType();

        //                        if(type == VariantType::Bool)
        //                            pt.add(xml_key, false);
        //                        else if(type == VariantType::String)
        //                            pt.add(xml_key, "");

        //                        else if(type == VariantType::Float)
        //                            pt.add(xml_key, 0.0f);
        //                        else if(type == VariantType::Int)
        //                             pt.add(xml_key, 0);
        //                        else // must be VariantDataClass
        //                        {
        //                            std::string typeStr =  Variant::typeToString(type);
        //                            VariantDataClassPtr data;
        //                            if(ic)
        //                            {
        //                                Ice::ObjectFactoryPtr objFac= ic->findObjectFactory(typeStr);
        //                                if(!objFac)
        //                                    throw exceptions::local::eNullPointerException("Could not find ObjectFactory for string '"+typeStr+"'");
        //                                data = VariantDataClassPtr::dynamicCast(objFac->create(typeStr));
        //                            }
        //                            else {
        //                                ARMARX_WARNING << "could not write parameter '" << xml_key << "' of type " << typeStr << flush;
        //                                continue;
        //                            }



        //                            static Ice::Current c;
        //                            if(!c.adapter)
        //                                c.adapter = ic->createObjectAdapterWithEndpoints("Dummy"+IceUtil::generateUUID(), "tcp"); // pass adapter to readFromXML so that a proxy can be retrieved in the variant
        //                            ptree subTree;
        //                            std:: stringstream stream;
        //                            stream << data->writeAsXML();
        //                            xml_parser::read_xml(stream,subTree);
        //                            pt.add_child(xml_key, subTree);
        //                        }



        //                        // TODO: ChannelRef: ObserverPrx from observername


        //                        break;
        //                    }

        //                    case eVariantListParam:
        //                    {
        //                        SingleTypeVariantListBasePtr varList = it->second->value->getVariantList();

        //                        VariantTypeId type = it->second->value->getVariantType();

        //                        if(type == VariantType::Bool)
        //                            addEmptyElementstoPTree<bool>(Variant(false), pt, xml_key);
        //                        else if(type == VariantType::String)
        //                            addEmptyElementstoPTree<std::string>(Variant(""), pt, xml_key);
        //                        else if(type == VariantType::Float)
        //                            addEmptyElementstoPTree<float>(Variant(0.0f), pt, xml_key);
        //                        else if(type == VariantType::Int)
        //                            addEmptyElementstoPTree<int>(Variant(0), pt, xml_key);
        //                        else // must be VariantDataClass
        //                        {
        //                            Variant var;
        //                            var.setType(type);
        //                            addEmptyElementstoPTree<VariantDataClass>(var, pt, xml_key);
        //                        }

        //                        break;
        //                    }
        //                    default:
        //                        it->second->set=false;
        //                        throw LocalException("Unknown Variant Type - couldnt load it from file : '" + it->first + "'->" + it->second->value->getVariant()->getTypeName());

        //                }
        //            }
        //            catch(boost::property_tree::ptree_error &e)
        //            {
        //                ARMARX_ERROR << "Error while writing default parameter '" << xml_key << "'. Reason: " << e.what() << flush;
        //            }

        //        }


        //        try
        //        {
        //            xml_parser::write_xml(configFile.string(), pt);
        //        }
        //        catch(boost::property_tree::ptree_error &e)
        //        {
        //            ARMARX_ERROR << "Error while writing default configfile '" << configFile.string() << "'. Reason: " << e.what() << flush;
        //        }
    }



}
