/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef _ARMARX_DYNAMICREMOTESTATE_H
#define _ARMARX_DYNAMICREMOTESTATE_H

#include "RemoteState.h"

namespace armarx
{
    DEFINEEVENT(LoadingFailed);

    /**
     * \class DynamicRemoteState
     * \brief DynamicRemoteStates can be used to connect to remote statecharts dynamically at runtime
     * \ingroup StatechartGrp
     *
     * A DynamicRemoteState behaves like its parent class RemoteState with the
     * exception that the both proxy and statename of the remote statechart are not
     * known at compile time.
     * Instead, both parameters are passed to the DynamicRemoteState instance
     * at runtime via input parameters defined on the event leading to a
     * transition to this instance.
     *
     * To create a DynamicRemoteState instance in a statechart call the
     * State::addDynamicRemoteState() function in StateBase::defineSubstates().
     *
     * At runtime add the following input parameters to the transition event:
     * \li proxyName (VariantType::String) name of the RemoteStateOfferer instance
     * \li stateName (VariantType::String) name of the remote state to enter
     */
    class DynamicRemoteState :
        virtual public RemoteState
    {
    public:
        /**
         * Initialize all instance variables
         */
        DynamicRemoteState();
        /**
         * Create a copy of \p source
         */
        DynamicRemoteState(const DynamicRemoteState& source);
        /**
         * Assignment operator which currently is set to fail on any invocation
         */
        DynamicRemoteState& operator=(const DynamicRemoteState& source);

        // inherited from Component
        virtual std::string getDefaultName() const;
        virtual void onInitComponent();
        virtual void onConnectComponent();

        // inherited from StateBase
        /**
         * Does the setup of the remote statechart and calls RemoteState::_baseOnEnter()
         * afterwards.
         *
         * The Event LoadingFailed is emitted if connecting to the remote
         * statechart failed.
         *
         * \see StateBase::_baseOnEnter()
         */
        void _baseOnEnter();
        /**
         * Calls RemoteState::_baseOnBreak() and redefines the input parameters
         *
         * \see StateBase::_baseOnBreak()
         */
        void _baseOnExit();
        /**
         * If the call to RemoteState::_baseOnBreak() returns true
         * the input parameters are redefined.
         *
         * \see StateBase::_baseOnBreak()
         */
        bool _baseOnBreak(const EventPtr evt);

        /**
         * \see StateBase::createEmptyCopy()
         */
        StateBasePtr createEmptyCopy() const;
        /**
         * \see StateBase::clone()
         */
        StateBasePtr clone() const;
        /**
         * \see StateBase::defineParameters()
         */
        void defineParameters();
    private:
        bool __checkStatePreconditions();
    };

    typedef IceInternal::Handle<DynamicRemoteState> DynamicRemoteStatePtr;
}

#endif
