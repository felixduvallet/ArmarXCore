/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Statechart
* @author     Mirko Waechter( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#define BOOST_TEST_MODULE ArmarX::Statechart::ParameterLoadingTest
#define ARMARX_BOOST_TEST
#include <ArmarXCore/Test.h>

#include <ArmarXCore/statechart/Statechart.h>
#include <ArmarXCore/statechart/StateParameter.h>
#include <ArmarXCore/observers/variant/ChannelRef.h>
#include <ArmarXCore/observers/ObserverObjectFactories.h>
//#include <ArmarXCore/robotstate/RobotStateObjectFactories.h>
#include <ArmarXCore/core/ArmarXManager.h>

using namespace armarx;




BOOST_AUTO_TEST_CASE(testParameterLoadingChannelRefList)
{
    Ice::CommunicatorPtr ic;
    ic = Ice::initialize();
    ArmarXManager::RegisterKnownObjectFactoriesWithIce(ic);

    InputParameterLoader loader;
    loader.setIceCommunicator(ic);

    std::stringstream xmlContent;
    xmlContent << "<TestState><StateParameters>"
               << "<ChannelRefList>"
               << "<Item0><observerName>KinematicUnitObserver</observerName><channelName>left_head</channelName></Item0>"
               << "<Item1><observerName>KinematicUnitObserver</observerName><channelName>right_head</channelName></Item1>"
               << "</ChannelRefList>"
               << "</TestState></StateParameters>";

    StateParameterMap params;
    params["ChannelRefList"] = StateParameter::create();
    params["ChannelRefList"]->value = new SingleTypeVariantList(VariantType::ChannelRef);

    loader.load(xmlContent.str(), "TestState", params);
    SingleTypeVariantListPtr list1 = SingleTypeVariantListPtr::dynamicCast(params["ChannelRefList"]->value);

    BOOST_CHECK(list1->getElement<SingleVariant>(0)->get()->getClass<ChannelRef>()->getChannelName() == "left_head");
    BOOST_CHECK(list1->getElement<SingleVariant>(1)->get()->getClass<ChannelRef>()->getObserverName() == "KinematicUnitObserver");
    BOOST_CHECK(params["ChannelRefList"]->set == true);


}

//BOOST_AUTO_TEST_CASE(testParameterLoadingFloatList)
//{
//    Ice::CommunicatorPtr ic;
//    ic = Ice::initialize();
//    ObserverObjectFactories::addFactories(ic);



//    InputParameterLoader loader;
//    loader.setIceCommunicator(ic);

//    std::string listName = "floatList";
//    std::stringstream xmlContent;
//    xmlContent << "<TestState><StateParameters>"
//            <<"<" << listName << ">"
//           <<"<Item0>1.3</Item0>"
//           <<"<Item1>1.4</Item1>"
//          <<"</" << listName << ">"
//            <<"</TestState></StateParameters>";

//    StateParameterMap params;
//    params[listName] = StateParameter::create();
//    params[listName]->value = new VariantListParameter();
//    params[listName]->value->setVariantList(new SingleTypeVariantList(VariantType::Float));

//    loader.load(xmlContent.str(), "TestState", params);
//    SingleTypeVariantListPtr list = SingleTypeVariantListPtr::dynamicCast(params[listName]->value->getVariantList());

//    BOOST_CHECK(list->getVariant(0)->getFloat() == 1.3f);
//    BOOST_CHECK(list->getVariant(1)->getFloat() == 1.4f);
//    BOOST_CHECK(params[listName]->set == true);


//}

//BOOST_AUTO_TEST_CASE(testCreateEmptyParameterConfigFile)
//{
//    Ice::CommunicatorPtr ic;
//    ic = Ice::initialize();
//    ObserverObjectFactories::addFactories(ic);
//    RobotStateObjectFactories::addFactories(ic);
//    std::string stateconfigFile = "./StateConfigTestFile.xml";
//    boost::filesystem::remove(stateconfigFile);


//    InputParameterLoader loader;
//    loader.setIceCommunicator(ic);

//    StateParameterMap params;
//    std::string paramName = "jointValue";
//    params[paramName] = StateParameter::create();
//    params[paramName]->value = new VariantParameter();
//    params[paramName]->value->getVariant()->setType(VariantType::Float);
//    paramName = "ChannelRef";
//    params[paramName] = StateParameter::create();
//    params[paramName]->value = new VariantParameter();
//    params[paramName]->value->getVariant()->setType(VariantType::ChannelRef);


//    loader.load(boost::filesystem::path(stateconfigFile), "TestState", params);

//    BOOST_CHECK(boost::filesystem::exists(stateconfigFile) == true);
//    boost::filesystem::remove(stateconfigFile);

//}

//BOOST_AUTO_TEST_CASE(testHasMappingEntry2)
//{
//    Mapping mapping;
//    mapping.insert(std::pair<std::string, std::string>("State2.angle", "State1.angle"));
//    mapping.insert(std::pair<std::string, std::string>("State2.head.*", "State1.head.*"));


//    StringVariantBaseMap mapSource;
//    mapSource.insert(std::pair<std::string, VariantBasePtr>("State1.angle",new Variant(5.5f)));
//    mapSource.insert(std::pair<std::string, VariantBasePtr>("State1.head.angle",new Variant(5.5f)));
//    StringVariantBaseMap::const_iterator it;

//    std::string  keyDestination = "State2.head.direction";
//    it =  PM::hasMappingEntry(mapping, keyDestination, mapSource);
//    BOOST_CHECK(it == mapSource.end());
//}


//BOOST_AUTO_TEST_CASE(testHasMappingEntry3)
//{
//    Mapping mapping;
//    mapping.insert(std::pair<std::string, std::string>("State2.angle", "State1.angle"));
//    mapping.insert(std::pair<std::string, std::string>("State2.head.*", "State1.head.*"));


//    StringVariantBaseMap mapSource;
//    mapSource.insert(std::pair<std::string, VariantBasePtr>("State1.angle",new Variant(5.5f)));
//    mapSource.insert(std::pair<std::string, VariantBasePtr>("State1.head.angle",new Variant(5.5f)));
//    StringVariantBaseMap::const_iterator it;

//    std::string  keyDestination = "State2.head.angle";
//    it =  PM::hasMappingEntry(mapping, keyDestination, mapSource);
//    BOOST_CHECK(it != mapSource.end());
//}

//BOOST_AUTO_TEST_CASE(testHasMappingEntry4)
//{
//    ParameterMappingPtr mapping = createMapping();
//    mapping->addTuple("State1.head.in.*", "State2.head.in.*");


//    StringVariantBaseMap mapSource;
//    mapSource.insert(std::pair<std::string, VariantBasePtr>("State1.in.angle",new Variant(5.5f)));
//    mapSource.insert(std::pair<std::string, VariantBasePtr>("State1.head.in.angle",new Variant(5.5f)));
//    StringVariantBaseMap::const_iterator it;

//    std::string  keyDestination = "State2.head.in.angle";
//    it =  PM::hasMappingEntry(mapping->paramMapping, keyDestination, mapSource);
//    BOOST_CHECK(it != mapSource.end());
//}


//BOOST_AUTO_TEST_CASE(testApplyMapping)
//{
//    ParameterMappingPtr mapping = createMapping();
//    mapping->addTuple("*", "State2.in.*");


//    StringVariantBaseMap mapSource;
//    mapSource.insert(std::pair<std::string, VariantBasePtr>("angle",new Variant(5.5f)));
////    mapSource.insert(std::pair<std::string, VariantBasePtr>("State1.head.in.angle",new Variant(5.5f)));

//    StateParameterMap mapDest;
//    StateParameter param;
//    param.value = new Variant(1.5f);
//    param.defaultValue = new Variant(1.5f);
//    param.optionalParam = false;
//    param.set = false;
//    mapDest.insert(std::pair<std::string, StateParameter>("State2.in.angle",param));

//    mapping->applyMapping(&mapSource, NULL, "", NULL, mapDest, false);
//    BOOST_CHECK(mapDest.size()>0);
//}

//BOOST_AUTO_TEST_CASE(testaddTuple)
//{
//    ParameterMappingPtr mapping = createMapping();
//    BOOST_CHECK_NO_THROW(
//                mapping->addTuple( "State2.head.*", "State1.head.*")

//                );
//    BOOST_CHECK_THROW(
//                mapping->addTuple("State2.head.*", "State1.head.direction"), armarx::LocalException
//                );
//}
