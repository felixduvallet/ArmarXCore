/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Statechart
* @author     Mirko Waechter( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include <ArmarXCore/statechart/Statechart.h>
#include <ArmarXCore/core/test/IceTestHelper.h>

#include <ArmarXCore/core/system/FactoryCollectionBase.h>
#include <ArmarXCore/observers/ConditionHandler.h>
#include <ArmarXCore/observers/SystemObserver.h>

using namespace armarx;


class StatechartEnvironment
{
public:
    StatechartEnvironment(const std::string& testName, int registryPort = 11220, bool addObjects = true)
    {
        Ice::PropertiesPtr properties = Ice::createProperties();

        iceTestHelper = new IceTestHelper(registryPort, registryPort + 1);
        iceTestHelper->startEnvironment();
        manager = new TestArmarXManager(testName, iceTestHelper->getCommunicator(), properties);

        if (addObjects)
        {
            conditionHandler = manager->createComponentAndRun<ConditionHandler, ConditionHandlerInterfacePrx>("ArmarX", "ConditionHandler");
            systemObserver = manager->createComponentAndRun<SystemObserver, SystemObserverInterfacePrx>("ArmarX", "SystemObserver");
        }

    }
    ~StatechartEnvironment()
    {
        manager->shutdown();
    }

    ConditionHandlerInterfacePrx conditionHandler;
    SystemObserverInterfacePrx systemObserver;
    TestArmarXManagerPtr manager;
    IceTestHelperPtr iceTestHelper;

};

typedef boost::shared_ptr<StatechartEnvironment> StatechartEnvironmentPtr;

