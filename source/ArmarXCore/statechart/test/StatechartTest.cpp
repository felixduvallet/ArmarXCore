/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Statechart
* @author     Mirko Waechter( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#define BOOST_TEST_MODULE ArmarX::Statechart::StatechartTest
#define ARMARX_BOOST_TEST
#include <ArmarXCore/Test.h>

#include <ArmarXCore/statechart/Statechart.h>
#include <ArmarXCore/core/test/IceTestHelper.h>
#include <ArmarXCore/core/system/DynamicLibrary.h>
#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <ArmarXCore/core/system/FactoryCollectionBase.h>
#include <ArmarXCore/observers/variant/TimestampVariant.h>
using namespace armarx;


// Define Events before the first state they are used in
DEFINEEVENT(EvInit)  // this macro declares a new event-class derived vom Event, to have a compiletime check for typos in events
DEFINEEVENT(EvNext)

struct StateResult;
struct StateRun;
struct Statechart_StateAdditionTest : StateTemplate<Statechart_StateAdditionTest>
{

    void defineState()
    {
        //        setStateName("AdditionTest");
    }

    void defineSubstates()
    {
        //add substates

        StatePtr stateRun = addState<StateRun>("Running");
        setInitState(stateRun, createMapping()
                     ->mapFromParent("x", "x")
                     ->mapFromParent("y", "y")
                     ->setSourcePriority(3, eOutput)
                     ->setSourcePriority(2, eParent));
        StatePtr stateResult = addState<StateResult>("Result");
        StatePtr stateSuccess = addState<SuccessState>("Success"); // preimplemented state derived from FinalStateBase, that sends triggers a transition on the upper state

        PMPtr outputMapping = PM::createMapping()->mapFromOutput("result", "mappedresult");
        PMPtr localMapping = PM::createMapping()->mapFromOutput("result", "localmappingtest");


        // add transitions
        addTransition<EvNext>(stateRun, stateResult,
                              createMapping()->mapFromOutput("*", "my.*"),
                              localMapping,
                              outputMapping);
        addTransition<EvNext>(stateResult, stateSuccess,
                              createMapping()->mapFromOutput("my.*", "*"),
                              localMapping,
                              outputMapping);


        // ...add more transitions
    }

    void defineParameters()
    {
        //setConfigFile("StateConfigs/Parameters.xml"); //tries to fill the inputParameters with the values found in the xml-file

        // add input
        addToInput("x", VariantType::Float, true);
        addToInput("y", VariantType::Float, true);
        addToLocal("localmappingtest", VariantType::Float);
        // add output
        addToOutput("result", VariantType::Float, true);
        addToOutput("mappedresult", VariantType::Float, true);
    }
    void onExit()
    {

        BOOST_CHECK(getLocal<float>("localmappingtest") == 7.f);
        ARMARX_IMPORTANT << "local ammping test: " << getLocal<float>("localmappingtest") ;
    }

};

struct StateRun : StateTemplate<StateRun>
{
    void defineParameters()
    {
        //add input
        addToInput("x", VariantType::Float, true);
        addToInput("y", VariantType::Float, true);

        // add output
        addToOutput("result", VariantType::Float, true);
        addToOutput("time", VariantType::Timestamp, true);
        addToOutput("stringlist", VariantType::List(VariantType::String), true);
        addToOutput("timelist", VariantType::List(VariantType::Timestamp), true);
        addToOutput("twolevellist", VariantType::List(VariantType::Map(VariantType::Int)), true);

    }

    void onEnter()
    {
        TimestampVariantPtr t = TimestampVariant::nowPtr();
        std::vector<TimestampVariantPtr> timevec;
        timevec.push_back(t);
        SingleTypeVariantListPtr timelist = SingleTypeVariantList::FromStdVector<TimestampVariantPtr>(timevec);
        setOutput("timelist", *timelist);
        SingleTypeVariantListPtr timelist2 = getOutput<SingleTypeVariantList>("timelist");
        BOOST_CHECK(timelist2->toStdVector<TimestampVariantPtr>().at(0)->getTimestamp() ==
                    t->getTimestamp());

        std::vector<std::string> strvec;
        strvec.push_back("mystring");
        SingleTypeVariantListPtr stringlist = SingleTypeVariantList::FromStdVector<std::string>(strvec);
        setOutput("stringlist", *stringlist);
        SingleTypeVariantListPtr stringlist2 = getOutput<SingleTypeVariantList>("stringlist");
        BOOST_CHECK(stringlist2->toStdVector<std::string>().at(0) == "mystring");

        std::vector<StringValueMapPtr> stdtllm;
        stdtllm.push_back(new StringValueMap());
        stdtllm.at(0)->addVariant("mykey", 3);
        SingleTypeVariantListPtr tllm = SingleTypeVariantList::FromContainerStdVector(stdtllm);
        setOutput("twolevellist", *tllm);
        SingleTypeVariantListPtr ttlm2 = getOutput<SingleTypeVariantList>("twolevellist");
        BOOST_CHECK(ttlm2->toContainerStdVector<StringValueMapPtr>().at(0)->toStdMap<int>()["mykey"] == 3);
        ARMARX_IMPORTANT_S << "ttlm: " << ttlm2->toContainerStdVector<StringValueMapPtr>().at(0)->toStdMap<int>()["mykey"];


        ARMARX_IMPORTANT_S << "t: " << t->output();
        setOutput("time", t);
        ARMARX_IMPORTANT_S << getOutput<TimestampVariantPtr>("time")->output();

        setOutput("result",  getInput<float>("x") + getInput<float>("y"));
        sendEvent<EvNext>();
    }

};

struct StateResult : StateTemplate<StateResult>
{
    void defineParameters()
    {
        //add input
        addToInput("my.result", VariantType::Float, false);
        // add output
        addToOutput("my.result", VariantType::Float, true);
    }
    void onEnter()
    {
        setOutput("my.result", getInput<float>("my.result"));
        sendEvent<EvNext>();
    }

};

BOOST_AUTO_TEST_CASE(StatechartParameterMappingAndTransitionTest)
{
    StatePtr statechart;
    StatechartManager manager;

    try
    {
        statechart = Statechart_StateAdditionTest::createInstance();
        statechart->init(NULL, &manager);

        BOOST_CHECK(statechart->isInitialized());
        StringVariantContainerBaseMap input;
        input["x"] = new SingleVariant(3.f);
        input["y"] = new SingleVariant(4.f);
        manager.setToplevelState(statechart, input);
        manager.start();
        statechart->waitForStateToFinish();
        BOOST_CHECK(statechart->getOutput<float>("result") == 7.f);

    }
    catch (...)
    {
        handleExceptions();
    }

    //    ARMARX_INFO_S << "StatechartParameterMappingAndTransitionTest";

}

struct StateRunFunction : StateTemplate<StateRunFunction>
{

    void defineParameters()
    {
        addToLocal("bool", VariantType::Bool);
    }

    void run()
    {
        //Variant var(ChannelRefPtr(new ChannelRef(NULL,"")));
        ARMARX_VERBOSE << "run()";
        //setLocal("test",var);
        setLocal("bool", true);
        getLocal<bool>("bool");
        sendEvent<Success>();
    }
};

struct Statechart_StateRunFunctionTest : StateTemplate<Statechart_StateRunFunctionTest>
{

    void defineSubstates()
    {
        StatePtr run = addState<StateRunFunction>("StateRunFunction");
        StatePtr success = addState<SuccessState>("StateSuccess");

        setInitState(run);

        addTransition<Success>(run, success);
    }



};



BOOST_AUTO_TEST_CASE(StatechartRunFunctionTest)
{
    StatePtr statechart;
    StatechartManager manager;

    try
    {
        statechart = Statechart_StateRunFunctionTest::createInstance();
        statechart->init(NULL, &manager);



        manager.setToplevelState(statechart);
        manager.start();
        statechart->waitForStateToFinish();
        manager.shutdown();
        //        ARMARX_INFO_S << ThreadList::getApplicationThreadList()->getRunningTaskNames()[0] << std::endl;
        //        ARMARX_INFO_S << ThreadList::getApplicationThreadList()->getRunningTaskNames()[1] << std::endl;
        //        BOOST_CHECK_EQUAL((unsigned int)1, ThreadList::getApplicationThreadList()->getRunningTaskNames().size());
    }
    catch (...)
    {
        handleExceptions();
    }

    BOOST_CHECK(true);

}

class FactoryCollectionBaseDummy : public FactoryCollectionBase
{


    // FactoryCollectionBase interface
public:
    ObjectFactoryMap getFactories();
};

ObjectFactoryMap FactoryCollectionBaseDummy::getFactories()
{
    return ObjectFactoryMap();
}

BOOST_FIXTURE_TEST_CASE(testDynamicFactoryLoad, FactoryCollectionBaseDummy)
{

    std::string relPath = "ArmarXCore/build/lib/libArmarXCoreEigen3Variants.so";
    std::string path;

    if (!ArmarXDataPath::getAbsolutePath(relPath, path))
    {
        return;
    }

    unsigned int sizeBefore = PreregistrationList().size();

    std::cout << "Size before: " << sizeBefore << std::endl;
    DynamicLibrary lib;
    lib.load(path);

    for (size_t i = 0; i < PreregistrationList().size(); i++)
    {
        std::cout << PreregistrationList().at(i)->getFactories().begin()->first << std::endl;
    }

    BOOST_CHECK(sizeBefore != PreregistrationList().size());



}






BOOST_AUTO_TEST_CASE(StatechartExtendedParameterMappingAndTransitionTest)
{
    StatePtr statechart;
    StatechartManager manager;

    try
    {
        statechart = Statechart_StateAdditionTest::createInstance();
        statechart->init(NULL, &manager);

        BOOST_CHECK(statechart->isInitialized());
        StringVariantContainerBaseMap input;
        input["x"] = new SingleVariant(3.f);
        input["y"] = new SingleVariant(4.f);
        manager.setToplevelState(statechart, input);
        manager.start();
        statechart->waitForStateToFinish();
        BOOST_CHECK(statechart->getOutput<float>("result") == 7.f);
        BOOST_CHECK(statechart->getOutput<float>("mappedresult") == 7.f);

    }
    catch (...)
    {
        handleExceptions();
    }

    //    ARMARX_INFO_S << "StatechartParameterMappingAndTransitionTest";

}
