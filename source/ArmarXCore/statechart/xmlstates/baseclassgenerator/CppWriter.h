#ifndef CPPWRITER_H
#define CPPWRITER_H

#include <string>
#include <sstream>
#include <boost/shared_ptr.hpp>

namespace armarx
{
    class CppWriter;
    typedef boost::shared_ptr<CppWriter> CppWriterPtr;

    class CppWriter
    {
    public:
        enum LineType {StartBlock, EndBlock, Normal, Empty};

        CppWriter();

        void startBlock();
        void startBlock(const std::string& line);
        void endBlock();
        void endBlock(const std::string& line);
        void endBlockComment(std::string comment);
        void line();
        void line(const std::string& line);
        void line(const std::string& line, int indentDelta);

        std::string getString();
        LineType getLastLineType();

    private:
        void lineInternal(const std::string& line, int indent);

    private:
        std::stringstream ss;
        int indent;
        std::string indentChars;
        LineType lastLineType;
    };
}

#endif // CPPWRITER_H
