#ifndef DOXTRANSITIONGRAPH_H
#define DOXTRANSITIONGRAPH_H

#include <boost/shared_ptr.hpp>
#include <vector>
#include <string>
#include "DoxEntry.h"

namespace armarx
{
    class DoxTransition;

    typedef boost::shared_ptr<DoxTransition> DoxTransitionPtr;

    class DoxTransition
    {
    public:
        DoxTransition(std::string from, std::string to, std::string eventName);
        std::string toString();
    private:
        std::string from;
        std::string to;
        std::string eventName;
    };

    class DoxTransitionGraphNode;
    typedef boost::shared_ptr<DoxTransitionGraphNode> DoxTransitionGraphNodePtr;

    class DoxTransitionGraphNode
    {
    public:
        DoxTransitionGraphNode(const std::string& name, const std::string& attrs);
        std::string toString();
    private:
        std::string name;
        std::string attrs;
    };

    class DoxTransitionGraph;

    typedef boost::shared_ptr<DoxTransitionGraph> DoxTransitionGraphPtr;

    class DoxTransitionGraph : public DoxEntry
    {
    public:
        DoxTransitionGraph();
        void addTransition(DoxTransitionPtr transition);
        std::string getDoxString();
        void addTransition(const std::string& from, const std::string& to, const std::string& eventName);
        void addNode(const std::string& name, const std::string& attrs);
        int transitionCount();
    private:
        std::vector<DoxTransitionPtr> transitions;
        std::vector<DoxTransitionGraphNodePtr> nodes;

    };
}



#endif
