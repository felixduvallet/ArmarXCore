#ifndef XMLSTATEWRAPPERGENERATOR_H
#define XMLSTATEWRAPPERGENERATOR_H

#include "CppClass.h"
#include "VariantInfo.h"
#include "DoxDoc.h"
#include "DoxLine.h"
#include "DoxTable.h"
#include "DoxTransitiongraph.h"

#include <set>
#include <string>

#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlReader.h>
#include <Ice/Communicator.h>

namespace armarx
{
    class XmlStateBaseClassGenerator
    {
    public:
        XmlStateBaseClassGenerator();

        static std::string GenerateCpp(std::vector<std::string> namespaces, RapidXmlReaderPtr reader, const std::vector<std::string>& proxies, bool contextGenerationEnabled, std::string groupName, VariantInfoPtr variantInfo);

    private:
        static CppClassPtr BuildClass(std::vector<std::string> namespaces, RapidXmlReaderNode stateNode, std::vector<std::string> proxies, bool contextGenerationEnabled, std::string groupName, VariantInfoPtr variantInfo);
        static std::set<std::string> GetUsedVariantTypes(RapidXmlReaderNode stateNode);
        static void AddGet(RapidXmlReaderNode node, CppClassPtr targetClass, VariantInfoPtr variantInfo, const std::string& parentGetMethodName);
        static void AddIsSet(RapidXmlReaderNode node, CppClassPtr targetClass, VariantInfoPtr variantInfo, const std::string& parentTestParamMethodName);
        static void AddSet(RapidXmlReaderNode node, CppClassPtr targetClass, VariantInfoPtr variantInfo, const std::string& parentGetMethodName);
        static std::string fmt(const std::string& fmt, const std::string& arg1);
        static std::string fmt(const std::string& fmt, const std::string& arg1, const std::string& arg2);
        static std::string fmt(const std::string& fmt, const std::string& arg1, const std::string& arg2, const std::string& arg3);
        static std::string generateDocString(RapidXmlReaderNode stateNode, const std::string& packageName, const std::vector<std::string>& namespaces, bool isPublicState);
        static std::string generateParameterTableString(RapidXmlReaderNode stateNode);
        static std::string generateDotGraphString(RapidXmlReaderNode stateNode);
        static DoxDocPtr generateDoxDoc(RapidXmlReaderNode stateNode, const std::string& packageName, std::vector<std::string>& namespaces, bool isPublicState, VariantInfoPtr variantInfo, Ice::CommunicatorPtr communicator);
    };
}

#endif // XMLSTATEWRAPPERGENERATOR_H
