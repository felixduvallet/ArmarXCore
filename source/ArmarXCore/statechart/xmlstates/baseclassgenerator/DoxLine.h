#ifndef DOXLINE_H
#define DOXLINE_H

#include <boost/shared_ptr.hpp>
#include <string>
#include <vector>
#include "DoxEntry.h"

namespace armarx
{

    class DoxLine;
    typedef boost::shared_ptr<DoxLine> DoxLinePtr;


    class DoxLine : public DoxEntry
    {
    public:
        DoxLine(const std::string& line);
        void setLine(const std::string& line);
        std::string getDoxString();
    private:
        std::string line;
    };
}

#endif
