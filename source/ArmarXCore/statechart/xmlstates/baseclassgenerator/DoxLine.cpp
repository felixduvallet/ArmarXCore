#include "DoxLine.h"

using namespace armarx;

DoxLine::DoxLine(const std::string& line)
{
    this->line = line;
}

void DoxLine::setLine(const std::string& line)
{
    this->line = line;
}

std::string DoxLine::getDoxString()
{
    return line;
}
