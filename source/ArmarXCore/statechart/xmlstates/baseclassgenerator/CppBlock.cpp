#include "CppBlock.h"

using namespace armarx;

CppBlock::CppBlock()
{
}

void CppBlock::writeCpp(CppWriterPtr writer)
{
    writer->startBlock();

    for (std::vector<std::string>::iterator it = lines.begin(); it != lines.end(); it++)
    {
        writer->line(*it);
    }

    writer->endBlock();
}

std::string CppBlock::getAsSingleLine()
{
    std::stringstream ss;
    ss << "{ ";

    for (std::string line : lines)
    {
        ss << line << " ";
    }

    ss << "}";
    return ss.str();
}

void CppBlock::addLine(const std::string& line)
{
    lines.push_back(line);
}

void CppBlock::addLine(const boost::basic_format<char>& line)
{
    lines.push_back(boost::str(line));
}
