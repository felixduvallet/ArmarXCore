#include "DoxTransitiongraph.h"
#include <sstream>

using namespace armarx;


DoxTransitionGraph::DoxTransitionGraph()
{ }

void DoxTransitionGraph::addTransition(DoxTransitionPtr transition)
{
    transitions.push_back(transition);
}
void DoxTransitionGraph::addTransition(const std::string& from, const std::string& to, const std::string& eventName)
{
    DoxTransitionPtr transition(new DoxTransition(from, to, eventName));
    transitions.push_back(transition);
}

void DoxTransitionGraph::addNode(const std::string& name, const std::string& attrs)
{
    DoxTransitionGraphNodePtr node(new DoxTransitionGraphNode(name, attrs));
    nodes.push_back(node);
}

int DoxTransitionGraph::transitionCount()
{
    return transitions.size();
}

std::string DoxTransitionGraph::getDoxString()
{
    std::stringstream ss;
    ss << "@dot\ndigraph States {\n";

    for (DoxTransitionGraphNodePtr node : nodes)
    {
        ss << "  " << node->toString() << "\n";
    }

    for (DoxTransitionPtr transition : transitions)
    {
        ss << "  " << transition->toString() << "\n";
    }

    ss << "}\n@enddot";
    return ss.str();
}


DoxTransition::DoxTransition(std::string from, std::string to, std::string eventName)
{
    this->from = from;
    this->to = to;
    this->eventName = eventName;
}

std::string DoxTransition::toString()
{
    return "\"" + from + "\" -> \"" + to + "\" [ label=\"" + eventName + "\" ];";
}


DoxTransitionGraphNode::DoxTransitionGraphNode(const std::string& name, const std::string& shape)
    : name(name), attrs(shape)
{ }

std::string DoxTransitionGraphNode::toString()
{
    return "\"" + name + "\" [" + attrs + "]";
}
