#include "XmlStateBaseClassGenerator.h"

#include <ArmarXCore/core/system/ArmarXDataPath.h>
//#include <ArmarXCore/core/system/LibRegistryBase.h>
#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>

#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/observers/variant/VariantContainer.h>

using namespace armarx;
using namespace boost;

XmlStateBaseClassGenerator::XmlStateBaseClassGenerator()
{

}


std::string XmlStateBaseClassGenerator::GenerateCpp(std::vector<std::string> namespaces, RapidXmlReaderPtr reader, const std::vector<std::string>& proxies, bool contextGenerationEnabled, std::string groupName, VariantInfoPtr variantInfo)
{
    CppClassPtr cppClass = BuildClass(namespaces, reader->getRoot("State"), proxies, contextGenerationEnabled, groupName, variantInfo);
    CppWriterPtr writer(new CppWriter());
    cppClass->writeCpp(writer);
    return writer->getString();
}

CppClassPtr XmlStateBaseClassGenerator::BuildClass(std::vector<std::string> namespaces, RapidXmlReaderNode stateNode, std::vector<std::string> proxies, bool contextGenerationEnabled, std::string groupName, VariantInfoPtr variantInfo)
{
    std::string className = stateNode.attribute_value("name");
    std::string baseClassName = className + "GeneratedBase";

    for (std::string & ns : namespaces)
    {
        ns = boost::replace_all_copy(ns, "-", "_");
    }

    CppClassPtr cppClass(new CppClass(namespaces, baseClassName, "template<typename StateType>"));

    // auto docString = generateDocString(stateNode, packageName, namespaces, isPublicState);
    // cppClass->addClassDoc(docString);
    //    DoxDocPtr doxDoc  = generateDoxDoc(stateNode, packageName, namespaces, isPublicState, variantInfo, communicator);
    //    cppClass->addClassDoc(doxDoc);

    std::stringstream ss;
    ss << "_ARMARX_XMLUSERCODE_";

    for (auto ns : namespaces)
    {
        ss << boost::to_upper_copy(ns) << "_";
    }

    ss << boost::to_upper_copy(baseClassName) << "_H";
    cppClass->setIncludeGuard(ss.str());

    cppClass->addInclude("<ArmarXCore/statechart/xmlstates/XMLState.h>");

    if (contextGenerationEnabled)
    {
        /*std::stringstream ssNesting;
        for(int i = 0; i < nestingLevel; i++)
        {
            ssNesting << "../";
        }
        cppClass->addInclude(fmt("\"%s%sStatechartContext.generated.h\"", ssNesting.str(), groupName));*/
        cppClass->addInclude(fmt("\"%sStatechartContext.generated.h\"", groupName));
    }

    std::set<VariantInfo::LibEntryPtr> usedLibs;
    std::set<std::string> usedInnerNonBasicVariantDataTypes;

    for (std::string typeName : GetUsedVariantTypes(stateNode))
    {
        ContainerTypePtr type = VariantContainerType::FromString(typeName);

        // Find the inner type, all outer types are containers. If there is no container type->subType is null
        while (type->subType)
        {
            type = type->subType;
        }

        if (!variantInfo->isBasic(type->typeId))
        {
            usedInnerNonBasicVariantDataTypes.insert(variantInfo->getDataTypeName(type->typeId));
        }

        VariantInfo::LibEntryPtr lib = variantInfo->findLibByVariant(type->typeId);

        if (lib)
        {
            usedLibs.insert(lib);
        }
        else
        {
            cppClass->addInclude("MISSING INCLUDE FOR " + type->typeId);
        }
    }

    std::vector<VariantInfo::LibEntryPtr> usedLibsVector(usedLibs.begin(), usedLibs.end());
    std::sort(usedLibsVector.begin(), usedLibsVector.end(), [](VariantInfo::LibEntryPtr a, VariantInfo::LibEntryPtr b)
    {
        return a->getName().compare(b->getName()) < 0;
    });

    std::vector<std::string> usedInnerNonBasicVariantDataTypesVector(usedInnerNonBasicVariantDataTypes.begin(), usedInnerNonBasicVariantDataTypes.end());
    std::sort(usedInnerNonBasicVariantDataTypesVector.begin(), usedInnerNonBasicVariantDataTypesVector.end(), [](std::string a, std::string b)
    {
        return a.compare(b) > 0;
    });

    for (VariantInfo::LibEntryPtr lib : usedLibsVector)
    {
        for (std::string include : lib->getFactoryIncludes())
        {
            cppClass->addInclude(fmt("<%s>", include));
        }
    }

    cppClass->addInherit("virtual public XMLStateTemplate < StateType >");
    cppClass->addInherit("public XMLStateFactoryBase");

    CppClassPtr inClass = cppClass->addInnerClass(className + "In");
    CppClassPtr localClass = cppClass->addInnerClass(className + "Local");
    CppClassPtr outClass = cppClass->addInnerClass(className + "Out");
    cppClass->addProtectedField(format("const %s in;") % inClass->getName());
    cppClass->addProtectedField(format("const %s local;") % localClass->getName());
    cppClass->addProtectedField(format("const %s out;") % outClass->getName());

    inClass->addPrivateField(format("%s<StateType> *parent;") % baseClassName);
    localClass->addPrivateField(format("%s<StateType> *parent;") % baseClassName);
    outClass->addPrivateField(format("%s<StateType> *parent;") % baseClassName);

    inClass->addCtor(fmt("%s<StateType> *parent", baseClassName))->addInitListEntry("parent", "parent");
    localClass->addCtor(fmt("%s<StateType> *parent", baseClassName))->addInitListEntry("parent", "parent");
    outClass->addCtor(fmt("%s<StateType> *parent", baseClassName))->addInitListEntry("parent", "parent");

    CppCtorPtr ctor = cppClass->addCtor("const XMLStateConstructorParams& stateData");
    ctor->addInitListEntry("XMLStateTemplate < StateType > ", "stateData");
    ctor->addInitListEntry("in", fmt("%s(this)", inClass->getName()));
    ctor->addInitListEntry("local", fmt("%s(this)", localClass->getName()));
    ctor->addInitListEntry("out", fmt("%s(this)", outClass->getName()));

    CppCtorPtr copyCtor = cppClass->addCtor(fmt("const %s &source", baseClassName));
    copyCtor->addInitListEntry("IceUtil::Shared", "source");
    copyCtor->addInitListEntry("armarx::StateIceBase", "source");
    copyCtor->addInitListEntry("armarx::StateBase", "source");
    copyCtor->addInitListEntry("armarx::StateController", "source");
    copyCtor->addInitListEntry("armarx::State", "source");
    copyCtor->addInitListEntry("XMLStateTemplate < StateType > ", "source");
    copyCtor->addInitListEntry("in", fmt("%s(this)", inClass->getName()));
    copyCtor->addInitListEntry("local", fmt("%s(this)", localClass->getName()));
    copyCtor->addInitListEntry("out", fmt("%s(this)", outClass->getName()));


    for (RapidXmlReaderNode p : stateNode.nodes("Events", "Event"))
    {
        std::string name = p.attribute_value("name");
        CppMethodPtr emitEvent = cppClass->addMethod(fmt("void emit%s()", name));
        emitEvent->addLine(fmt("State::sendEvent(State::createEvent(\"%s\"));", name));
        CppMethodPtr installCondition = cppClass->addMethod(fmt("void installConditionFor%s(const Term& condition, const std::string& desc = \"\")", name));
        installCondition->addLine(fmt("State::installCondition(condition, State::createEvent(\"%s\"), desc);", name));
        CppMethodPtr createEvent = cppClass->addMethod(fmt("::armarx::EventPtr createEvent%s()", name));
        createEvent->addLine(fmt("return State::createEvent(\"%s\");", name));
    }

    for (RapidXmlReaderNode p : stateNode.nodes("InputParameters", "Parameter"))
    {
        AddGet(p, inClass, variantInfo, "getInput");
        AddIsSet(p, inClass, variantInfo, "isInputParameterSet");
    }

    for (RapidXmlReaderNode p : stateNode.nodes("LocalParameters", "Parameter"))
    {
        AddGet(p, localClass, variantInfo, "getLocal");
        AddSet(p, localClass, variantInfo, "setLocal");
        AddIsSet(p, localClass, variantInfo, "isLocalParameterSet");
    }

    for (RapidXmlReaderNode p : stateNode.nodes("OutputParameters", "Parameter"))
    {
        AddGet(p, outClass, variantInfo, "getOutput");
        AddSet(p, outClass, variantInfo, "setOutput");
        AddIsSet(p, outClass, variantInfo, "isOutputParameterSet");
    }

    if (contextGenerationEnabled)
    {
        for (std::string proxyId : proxies)
        {
            VariantInfo::ProxyEntryPtr p = variantInfo->getProxyEntry(proxyId);

            if (p)
            {
                std::string name = p->getMemberName();
                name[0] = toupper(name[0]);
                CppMethodPtr getProxy = cppClass->addMethod(fmt("%s get%s() const", p->getTypeName(), name));
                getProxy->addLine(fmt("return StateBase::getContext<%sStatechartContext>()->%s();", groupName, p->getGetterName()));
                std::string include = fmt("<%s>", p->getIncludePath());

                if (!cppClass->hasInclude(include))
                {
                    cppClass->addInclude(include);
                }

                for (std::pair<std::string, std::string> method : p->getStateMethods())
                {
                    cppClass->addMethod(method.first)->addLine(boost::replace_all_copy(method.second, "%getContext%", fmt("StateBase::getContext<%sStatechartContext>()", groupName)));
                }
            }
            else
            {
                ARMARX_WARNING_S << "Cannot find proxy info for " << proxyId << " for state " << className;
                ARMARX_WARNING_S << "VariantInfo: " << variantInfo->getDebugInfo();
            }
        }
    }

    CppMethodPtr getName(new CppMethod("static std::string GetName()"));
    getName->addLine(fmt("return \"%s\";", className));
    cppClass->addMethod(getName);


    if (usedInnerNonBasicVariantDataTypesVector.size() > 0)
    {
        // Add special method that uses all non-basic types to force compiler/linker to include/load all libraries in binary (ensure GCC does not optimise it out).
        CppMethodPtr forceLibLoading(new CppMethod("void __attribute__((used)) __forceLibLoading()"));
        forceLibLoading->addLine("// Do not call this method.");
        forceLibLoading->addLine("// The sole purpose of this method is to force the compiler/linker to include all libraries.");

        int typeNr = 1;

        for (std::string innerNonBasicVariantDataType : usedInnerNonBasicVariantDataTypesVector)
        {
            forceLibLoading->addLine(fmt("%s type%s;", innerNonBasicVariantDataType, std::to_string(typeNr)));
            typeNr++;
        }

        cppClass->addMethod(forceLibLoading);
    }

    /*CppMethodPtr createInstance(new CppMethod("static XMLStateFactoryBasePtr CreateInstance(XMLStateConstructorParams stateData)"));
    createInstance->addLine(fmt("return XMLStateFactoryBasePtr(new %s(stateData));", className));
    cppClass->addMethod(createInstance);*/

    return cppClass;
}

std::set<std::string> XmlStateBaseClassGenerator::GetUsedVariantTypes(RapidXmlReaderNode stateNode)
{
    std::set<std::string> usedVariantTypes;

    for (RapidXmlReaderNode p : stateNode.nodes("InputParameters", "Parameter"))
    {
        std::string type = p.attribute_value("type");
        usedVariantTypes.insert(type);
    }

    for (RapidXmlReaderNode p : stateNode.nodes("LocalParameters", "Parameter"))
    {
        std::string type = p.attribute_value("type");
        usedVariantTypes.insert(type);
    }

    for (RapidXmlReaderNode p : stateNode.nodes("OutputParameters", "Parameter"))
    {
        std::string type = p.attribute_value("type");
        usedVariantTypes.insert(type);
    }

    return usedVariantTypes;
}


void XmlStateBaseClassGenerator::AddGet(RapidXmlReaderNode node, CppClassPtr targetClass, VariantInfoPtr variantInfo, const std::string& parentGetMethodName)
{
    std::string name = node.attribute_value("name");
    std::string type = node.attribute_value("type");
    std::string doc = node.first_node_value_or_default("Description", "");
    ContainerTypePtr containerInfo = VariantContainerType::FromString(type);

    // No Container, generated output:
    // [returnType] get[Name]() { return parent->[getInput|getLocal|getOutput]<[dataType]>("[Name]"); }
    if (!containerInfo->subType)
    {
        std::string dataType = variantInfo->getDataTypeName(type);
        std::string returnType = variantInfo->getReturnTypeName(type);
        CppMethodPtr getMethod = targetClass->addMethod(format("%s get%s() const") % returnType % name, doc);
        getMethod->addLine(format("return parent->State::%s< %s>(\"%s\");") % parentGetMethodName % dataType % name);
    }
    // Single Container, i.e. vector<int>, generated output:
    // [std-container]<[innerReturnType]> get[Name]() { return parent->[getInput|getLocal|getOutput]<[Variant-ContainerType]>("[Name]")->[variant-container-specific-to-std-container-method]<[innerDataType]>(); }
    // all containers are suppoted, as long as these containers are: "::armarx::StringValueMapBase" OR "::armarx::SingleTypeVariantListBase"
    // NOTE: innerDataType == innerReturnType for basic types, innerReturnType = innerDataType + "Ptr" for non basic types, see variantInfo->getDataTypeName and variantInfo->getReturnTypeName
    // ALSO NOTE: the + "Ptr" is hardcoded, so if the convention of a typedef for a ice-pointer is not met invalid code is generated.
    // examples:
    // std::vector<int> getMyParam1() { return parent->getInput< ::armarx::SingleTypeVariantList>()->toStdVector<int>(); }
    else if (!containerInfo->subType->subType)
    {
        std::string innerDataType = variantInfo->getDataTypeName(containerInfo->subType->typeId);
        std::string innerReturnType = variantInfo->getReturnTypeName(containerInfo->subType->typeId);
        std::string containerType = "NOT_FOUND";
        std::string toStdContainerMethodName = "NOT_FOUND";
        std::string returnType = "NOT_FOUND";

        if (containerInfo->typeId == "::armarx::StringValueMapBase")
        {
            containerType = "::armarx::StringValueMap";
            toStdContainerMethodName = "toStdMap";
            returnType = fmt("std::map<std::string, %s>", innerReturnType);
        }
        else if (containerInfo->typeId == "::armarx::SingleTypeVariantListBase")
        {
            containerType = "::armarx::SingleTypeVariantList";
            toStdContainerMethodName = "toStdVector";
            returnType = fmt("std::vector< %s>", innerReturnType);
        }

        CppMethodPtr getMethod = targetClass->addMethod(format("%s get%s() const") % returnType % name, doc);
        getMethod->addLine(format("return parent->State::%s< %s>(\"%s\")->%s::%s< %s>();") % parentGetMethodName % containerType % name % containerType % toStdContainerMethodName % innerReturnType);

    }
    // 2 or more level container: WHY U EVEN USING THIS?
    // but it is supported nonetheless
    else
    {
        std::string innerDataType = "NOT_FOUND";
        std::string innerReturnType = "NOT_FOUND";

        if (containerInfo->subType->typeId == "::armarx::StringValueMapBase")
        {
            innerDataType = "::armarx::StringValueMapPtr";
            innerReturnType = "::armarx::StringValueMapPtr";
        }
        else if (containerInfo->subType->typeId == "::armarx::SingleTypeVariantListBase")
        {
            innerDataType = "::armarx::SingleTypeVariantListPtr";
            innerReturnType = "::armarx::SingleTypeVariantListPtr";
        }

        std::string containerType = "NOT_FOUND";
        std::string toStdContainerMethodName = "NOT_FOUND";
        std::string returnType = "NOT_FOUND";

        if (containerInfo->typeId == "::armarx::StringValueMapBase")
        {
            containerType = "::armarx::StringValueMap";
            toStdContainerMethodName = "toContainerStdMap";
            returnType = fmt("std::map<std::string, %s>", innerReturnType);
        }
        else if (containerInfo->typeId == "::armarx::SingleTypeVariantListBase")
        {
            containerType = "::armarx::SingleTypeVariantList";
            toStdContainerMethodName = "toContainerStdVector";
            returnType = fmt("std::vector< %s>", innerReturnType);
        }

        CppMethodPtr getMethod = targetClass->addMethod(format("%s get%s() const") % returnType % name, doc);
        getMethod->addLine(format("return parent->State::%s< %s>(\"%s\")->%s::%s< %s>();") % parentGetMethodName % containerType % name % containerType % toStdContainerMethodName % innerDataType);
    }
}

void XmlStateBaseClassGenerator::AddIsSet(RapidXmlReaderNode node, CppClassPtr targetClass, VariantInfoPtr variantInfo, const std::string& parentTestParamMethodName)
{
    std::string name = node.attribute_value("name");

    CppMethodPtr getMethod = targetClass->addMethod(format("bool is%sSet() const") % name);
    getMethod->addLine(format("return parent->State::%s(\"%s\");") % parentTestParamMethodName % name);
}

void XmlStateBaseClassGenerator::AddSet(RapidXmlReaderNode node, CppClassPtr targetClass, VariantInfoPtr variantInfo, const std::string& parentSetMethodName)
{
    std::string name = node.attribute_value("name");
    std::string type = node.attribute_value("type");
    std::string doc = node.first_node_value_or_default("Description", "");
    ContainerTypePtr containerInfo = VariantContainerType::FromString(type);

    // No Container
    if (!containerInfo->subType)
    {
        // generate for non-pointer
        std::string dataType = variantInfo->getDataTypeName(containerInfo->typeId);
        CppMethodPtr setMethod = targetClass->addMethod(format("void set%s(const %s & value) const") % name % dataType, doc);
        setMethod->addLine(format("parent->State::%s(\"%s\", value);") % parentSetMethodName % name);

        if (!variantInfo->isBasic(containerInfo->typeId))
        {
            // generate for pointer if type is non-basic
            std::string pointerType = variantInfo->getReturnTypeName(containerInfo->typeId);
            CppMethodPtr ptrSetMethod = targetClass->addMethod(format("void set%s(const %s & value) const") % name % pointerType);
            ptrSetMethod->addLine(format("parent->State::%s(\"%s\", *value);") % parentSetMethodName % name);
        }
    }
    else if (!containerInfo->subType->subType)
    {
        //std::string innerDataType = variantInfo->getDataTypeName(containerInfo->subType->typeId);
        std::string innerParamType = variantInfo->getReturnTypeName(containerInfo->subType->typeId);
        std::string fromStdContainerMethod = "NOT_FOUND";
        std::string paramType = "NOT_FOUND";
        std::string containerType = "NOT_FOUND";

        if (containerInfo->typeId == "::armarx::StringValueMapBase")
        {
            containerType = "::armarx::StringValueMapPtr";
            fromStdContainerMethod = fmt("::armarx::StringValueMap::FromStdMap< %s>", innerParamType);
            paramType = fmt("std::map<std::string, %s>", innerParamType);
        }
        else if (containerInfo->typeId == "::armarx::SingleTypeVariantListBase")
        {
            containerType = "::armarx::SingleTypeVariantListPtr";
            fromStdContainerMethod = fmt("::armarx::SingleTypeVariantList::FromStdVector< %s>", innerParamType);
            paramType = fmt("std::vector< %s>", innerParamType);
        }

        CppMethodPtr setMethod = targetClass->addMethod(format("void set%s(const %s & value) const") % name % paramType, doc);
        setMethod->addLine(format("%s container = %s(value);") % containerType % fromStdContainerMethod);
        setMethod->addLine(format("parent->State::%s(\"%s\", *container);") % parentSetMethodName % name);
    }
    else
    {
        //std::string innerDataType = "NOT_FOUND";
        std::string innerParamType = "NOT_FOUND";

        if (containerInfo->subType->typeId == "::armarx::StringValueMapBase")
        {
            //innerDataType = "::armarx::StringValueMapPtr";
            innerParamType = "::armarx::StringValueMapPtr";
        }
        else if (containerInfo->subType->typeId == "::armarx::SingleTypeVariantListBase")
        {
            //innerDataType = "::armarx::SingleTypeVariantListPtr";
            innerParamType = "::armarx::SingleTypeVariantListPtr";
        }

        std::string fromStdContainerMethod = "NOT_FOUND";
        std::string paramType = "NOT_FOUND";
        std::string containerType = "NOT_FOUND";

        if (containerInfo->typeId == "::armarx::StringValueMapBase")
        {
            containerType = "::armarx::StringValueMapPtr";
            fromStdContainerMethod = fmt("::armarx::StringValueMap::FromContainerStdMap< %s>", innerParamType);
            paramType = fmt("std::map<std::string, %s>", innerParamType);
        }
        else if (containerInfo->typeId == "::armarx::SingleTypeVariantListBase")
        {
            containerType = "::armarx::SingleTypeVariantListPtr";
            fromStdContainerMethod = fmt("::armarx::SingleTypeVariantList::FromContainerStdVector< %s>", innerParamType);
            paramType = fmt("std::vector< %s>", innerParamType);
        }

        CppMethodPtr setMethod = targetClass->addMethod(format("void set%s(const %s & value) const") % name % paramType, doc);
        setMethod->addLine(format("%s container = %s(value);") % containerType % fromStdContainerMethod);
        setMethod->addLine(format("parent->State::%s(\"%s\", *container);") % parentSetMethodName % name);
    }

}

std::string XmlStateBaseClassGenerator::fmt(const std::string& fmt, const std::string& arg1)
{
    return str(format(fmt) % arg1);
}

std::string XmlStateBaseClassGenerator::fmt(const std::string& fmt, const std::string& arg1, const std::string& arg2)
{
    return str(format(fmt) % arg1 % arg2);
}

std::string XmlStateBaseClassGenerator::fmt(const std::string& fmt, const std::string& arg1, const std::string& arg2, const std::string& arg3)
{
    return str(format(fmt) % arg1 % arg2 % arg3);
}

std::string XmlStateBaseClassGenerator::generateDocString(RapidXmlReaderNode stateNode, const std::string& packageName, const std::vector<std::string>& namespaces, bool isPublicState)
{
    std::string className = stateNode.attribute_value("name");
    auto docNode = stateNode.first_node("Description");
    std::string docString;
    docString += "@class " + className + "GeneratedBase\n";

    if (namespaces.size() != 0)
    {
        docString += "@ingroup " + packageName + "-" + *namespaces.rbegin() + (isPublicState ? "" : "-Substates") + "\n";
    }

    if (docNode.is_valid())
    {
        docString += docNode.value();
    }

    docString += generateParameterTableString(stateNode);

    auto graphString = generateDotGraphString(stateNode);

    if (!graphString.empty())
    {
        docString += "\n\n" + graphString;
    }

    return docString;
}

std::string XmlStateBaseClassGenerator::generateParameterTableString(RapidXmlReaderNode stateNode)
{
    std::string result;


    /**
        for (RapidXmlReaderNode inputParameter : stateNode.nodes("InputParameters", "Parameter"))
        {
            result +=
        }



        auto inputParameters = stateNode.first_node("InputParameters");
        if(inputParameters.first_node("Parameter").is_valid())
        {
            result = "\n \n Input parameters: \n";
            result += "name | type | description \n";
            for (RapidXmlReaderNode curParameterNode = inputParameters.first_node();
                 curParameterNode.is_valid();
                 curParameterNode = curParameterNode.next_sibling("Parameter"))
            {
                result
            }

        }
    */


    return result;


}


std::string XmlStateBaseClassGenerator::generateDotGraphString(RapidXmlReaderNode stateNode)
{
    std::string result;
    auto transitions = stateNode.first_node("Transitions");

    if (transitions.first_node("Transition").is_valid())
    {

        result = "\n \nTransitiongraph of the substates:\n";
        result += "@dot\n\tdigraph States {\n";

        for (RapidXmlReaderNode curTransitionNode = transitions.first_node("Transition");
             curTransitionNode.is_valid();
             curTransitionNode = curTransitionNode.next_sibling("Transition"))
        {
            if (curTransitionNode.has_attribute("from")
                && curTransitionNode.has_attribute("to")
                && curTransitionNode.has_attribute("eventName"))
            {
                result += "\t\t" + curTransitionNode.attribute_value("from") + "->" +
                          curTransitionNode.attribute_value("to")
                          + "[ label=\"" + curTransitionNode.attribute_value("eventName") + "\" ];\n";
            }
        }

        result += "}\n@enddot";
        return result;
    }

    return "";
}

DoxDocPtr XmlStateBaseClassGenerator::generateDoxDoc(RapidXmlReaderNode stateNode, const std::string& packageName, std::vector<std::string>& namespaces, bool isPublicState, VariantInfoPtr variantInfo, Ice::CommunicatorPtr communicator)
{
    std::string className = stateNode.attribute_value("name");
    DoxDocPtr doc(new DoxDoc());

    doc->addLine(fmt("@defgroup %s-%s-State %s", packageName, className, className));
    std::string doxGroupName = fmt("%s-%s", packageName, namespaces.at(namespaces.size() - 1));

    if (!isPublicState)
    {
        doxGroupName += "-Substates";
    }

    doc->addLine(fmt("@ingroup %s\n", doxGroupName));

    std::string doxBrief;
    std::string doxDescription = stateNode.first_node_value_or_default("Description", "");

    if (doxDescription.size() > 0)
    {
        std::vector<std::string> descLines;
        boost::split(descLines, doxDescription, boost::is_any_of("."));
        doxBrief = descLines[0];
        doc->addLine(fmt("@brief %s\n", doxBrief));
    }
    else
    {
        doxBrief = fmt("@brief Brief description of state %s\n", className);
        doc->addLine(doxBrief);
    }


    // input parameters
    RapidXmlReaderNode inputParameters = stateNode.first_node("InputParameters");

    if (inputParameters.first_node("Parameter").is_valid())
    {
        doc->addLine("Input parameters:");
        doc->addParameterTable(inputParameters, variantInfo, communicator);
    }

    // local parameters
    RapidXmlReaderNode localParameters = stateNode.first_node("LocalParameters");

    if (localParameters.first_node("Parameter").is_valid())
    {
        doc->addLine("Local parameters:");
        doc->addParameterTable(localParameters, variantInfo, communicator);
    }

    // output parameters
    RapidXmlReaderNode outputParameters = stateNode.first_node("OutputParameters");

    if (outputParameters.first_node("Parameter").is_valid())
    {
        doc->addLine("Output parameters");
        doc->addParameterTable(outputParameters, variantInfo, communicator);
    }

    RapidXmlReaderNode transitions = stateNode.first_node("Transitions");

    if (transitions.first_node("Transition").is_valid())
    {
        doc->addLine("Transition graph");
        doc->addTransitionGraph(transitions);
    }


    doc->addLine(fmt("@class %sGeneratedBase", className));

    if (namespaces.size() != 0)
    {
        //doc->addLine(fmt("@ingroup %s-%s%s", packageName, namespaces.at(namespaces.size() - 1), isPublicState ? "" : "-Substates"));
        //doc->addLine(fmt("@ingroup %s", doxGroupName));
        doc->addLine(fmt("@ingroup %s-%s-State", packageName, className));
    }

    doc->addLine(fmt("For a documentation of this state see @ref %s-%s-State", packageName, className));

    if (doxBrief.size() > 0)
    {
        doc->addLine(doxBrief);
    }

    doc->addLine(doxDescription.substr(0, 80));


    return doc;
}
