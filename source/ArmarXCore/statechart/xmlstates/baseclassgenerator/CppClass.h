#ifndef CPPCLASS_H
#define CPPCLASS_H

#include "CppMethod.h"


namespace armarx
{
    class CppClass;
    typedef boost::shared_ptr<CppClass> CppClassPtr;
    class CppCtor;
    typedef boost::shared_ptr<CppCtor> CppCtorPtr;

    class CppCtor
    {
    public:
        CppCtor(const std::string& header);

        void addInitListEntry(const std::string& target, const std::string& expr);
        void addLine(const std::string& line);
        void addLine(const boost::basic_format<char>& line);

        void writeCpp(CppWriterPtr writer);

    private:
        std::string header;
        std::vector<std::string> initList;
        CppBlockPtr block;
    };

    class CppClass
    {

    public:
        class EmptyLineHelper;
        typedef boost::shared_ptr<EmptyLineHelper> EmptyLineHelperPtr;

        class EmptyLineHelper
        {
        public:
            EmptyLineHelper(CppWriterPtr writer)
                : writer(writer) {}
            void addEmptyLine()
            {
                switch (writer->getLastLineType())
                {
                    case CppWriter::Normal:
                    case CppWriter::EndBlock:
                        writer->line();
                        break;

                    case CppWriter::Empty:
                    case CppWriter::StartBlock:
                        // no empty line needed
                        break;
                }
            }

            void line(std::string line, int indentDelta = 0)
            {
                addEmptyLine();
                writer->line(line, indentDelta);
            }

        private:
            CppWriterPtr writer;
        };

        CppClass(std::vector<std::string> namespaces, std::string name, std::string templates = "");

        void writeCpp(CppWriterPtr writer);
        void addMethod(CppMethodPtr method);
        CppMethodPtr addMethod(const std::string& header, const std::string& doc = "");
        CppMethodPtr addMethod(const boost::basic_format<char>& header, const std::string& doc = "");
        CppCtorPtr addCtor(const std::string arguments);
        void addPrivateField(const std::string& field);
        void addPrivateField(const boost::basic_format<char>& field);
        void addProtectedField(const std::string& field);
        void addProtectedField(const boost::basic_format<char>& field);
        CppClassPtr addInnerClass(std::string name);
        void addInherit(const std::string& inherit);
        void addInherit(const boost::basic_format<char>& inherit);
        void addInclude(const std::string include);
        void addClassDoc(const std::string& doc);
        void addClassDoc(DoxDocPtr docPtr);
        bool hasInclude(const std::string include);

        std::string getName() const;
        void setIncludeGuard(const std::string& includeGuard);

        void convertToTextWithMaxWidth(unsigned int maxTextWidth, const std::vector<std::string>& docLines, std::vector<std::string>& linesOut);
    private:
        void addDoc(CppWriterPtr writer);

        std::vector<std::string> namespaces;
        std::string name;
        std::string templates;
        std::vector<CppMethodPtr> methods;
        std::vector<std::string> privateFields;
        std::vector<std::string> protectedFields;
        std::vector<std::string> inherit;
        std::vector<CppClassPtr> innerClasses;
        std::vector<CppCtorPtr> ctors;
        std::vector<std::string> includes;
        std::string includeGuard;
        std::string docString;
        DoxDocPtr doc;

    };
}

#endif // CPPCLASS_H
