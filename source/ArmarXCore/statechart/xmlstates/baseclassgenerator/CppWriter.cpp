#include "CppWriter.h"

using namespace armarx;

CppWriter::CppWriter()
{
    indent = 0;
    indentChars = "    ";
    lastLineType = Empty;
}

void CppWriter::startBlock()
{
    this->line("{");
    indent++;
    lastLineType = StartBlock;
}

void CppWriter::startBlock(const std::string& line)
{
    this->line(line);
    this->line("{");
    indent++;
    lastLineType = StartBlock;
}

void CppWriter::endBlock()
{
    indent--;
    this->line("}");
    lastLineType = EndBlock;
}

void CppWriter::endBlock(const std::string& line)
{
    indent--;
    this->line(std::string("}") + line);
    lastLineType = EndBlock;
}

void CppWriter::endBlockComment(std::string comment)
{
    indent--;
    this->line(std::string("} // ") + comment);
    lastLineType = EndBlock;
}

void CppWriter::line()
{
    lineInternal("", indent);
}

void CppWriter::line(const std::string& line)
{
    lineInternal(line, indent);
}

void CppWriter::line(const std::string& line, int indentDelta)
{
    lineInternal(line, indent + indentDelta);
}

std::string CppWriter::getString()
{
    return ss.str();
}

CppWriter::LineType CppWriter::getLastLineType()
{
    return lastLineType;
}

void CppWriter::lineInternal(const std::string& line, int indent)
{
    for (int i = 0; i < indent; i++)
    {
        ss << indentChars;
    }

    ss << line << std::endl;
    lastLineType = line.empty() ? Empty : Normal;
}
