#include "DoxTable.h"
#include <sstream>

using namespace armarx;


DoxTable::DoxTable(const std::vector<std::string>& header)
{
    this->header = header;
}

void DoxTable::addRow(const std::vector<std::string>& row)
{
    cells.push_back(row);
}

int DoxTable::rows()
{
    return cells.size();
}

std::string DoxTable::getDoxString()
{
    std::stringstream ss;

    /*ss << "| a | b | c |\n"
       << "|---|---|---|\n"
       << "| 1 | 2 | 3 |\n"
       << "| 4 | 5 | 6 |\n";*/
    ss << "@htmlonly\n";
    ss << "<table  class=\"doxtable\">\n";
    ss << "  <tr>\n";

    for (std::string s : header)
    {
        ss << "    <th>" << s << "</th>\n";

    }

    ss << "  </tr>\n";

    for (std::vector<std::string> row : cells)
    {
        ss << "  <tr>\n";

        for (std::string cell : row)
        {
            ss << "    <td>" << cell << "</td>\n";
        }

        ss << "  </tr>\n";
    }

    ss << "</table>\n";

    ss << "@endhtmlonly";

    return ss.str();
    /*
    std::stringstream ss_header;
    std::stringstream ss_line;


    for (std::string s : header)
    {
        ss_header << "|" << s << "|";
        ss_line << "|-";

    }
    ss_header << "|\n";
    ss_line << "|\n";

    std::stringstream ss;
    ss << ss_header.str() << ss_line.str();


    for (std::vector<std::string> row : cells)
    {
        for (std::string cell : row)
        {
            ss << "| " << cell << " ";
        }
        ss << "|\n";
    }

    ss << "@endhtmlonly";

    return ss.str();*/

}
