/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "XMLState.h"

#include <ArmarXCore/util/json/JSONObject.h>
#include <fstream>

using namespace armarx;
using namespace rapidxml;




NoUserCodeState::SubClassRegistry NoUserCodeState::Registry(NoUserCodeState::GetName(), &NoUserCodeState::CreateInstance);

NoUserCodeState::NoUserCodeState(XMLStateConstructorParams stateData) :
    XMLStateTemplate<NoUserCodeState>(stateData)

{

}



XMLStateFactoryBasePtr NoUserCodeState::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new NoUserCodeState(stateData));
}




StateParameterDeserialization::StateParameterDeserialization(RapidXmlReaderNode parameterNode, Ice::CommunicatorPtr ic, StatechartProfiles::StatechartProfilePtr selectedProfile)
{
    name = parameterNode.attribute_value("name");
    optional = parameterNode.attribute_value("optional") == "no" ? false : true;
    typeStr = parameterNode.attribute_value("type");
    typePtr = VariantContainerType::FromString(typeStr);

    std::string defaultValueJsonString = parameterNode.attribute_value_or_default("default", "");
    std::map<std::string, std::string> defaultValueMap;

    for (RapidXmlReaderNode defaultValueNode : parameterNode.nodes("DefaultValue"))
    {
        if (defaultValueNode.has_attribute("profile"))
        {
            defaultValueMap.insert(std::make_pair(defaultValueNode.attribute_value("profile"), defaultValueNode.attribute_value("value")));
        }
        else
        {
            defaultValueJsonString = defaultValueNode.attribute_value("value");
        }
    }

    StatechartProfiles::StatechartProfilePtr profile = selectedProfile;

    while (profile)
    {
        if (defaultValueMap.find(profile->getName()) != defaultValueMap.end())
        {
            defaultValueJsonString = defaultValueMap.at(profile->getName());
            break;
        }

        profile = profile->getParent();
    }

    JSONObjectPtr json(new JSONObject(ic));

    if (!defaultValueJsonString.empty())
    {
        try
        {
            json->fromString(defaultValueJsonString);
            container = VariantContainerBasePtr::dynamicCast(json->deserializeIceObject());

            if (!container)
            {
                ARMARX_WARNING_S << "Could not deserialize param " << name;
            }

        }
        catch (JSONException& e)
        {
            ARMARX_WARNING_S << "Could not read default value for param " << name;
        }
    }

}
