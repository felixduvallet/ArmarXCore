#include "StateGroupDocGenerator.h"

#include <fstream>

#include <boost/filesystem.hpp>

#include <ArmarXCore/core/exceptions/local/FileIOException.h>
#include <ArmarXCore/statechart/xmlstates/baseclassgenerator/DoxTransitiongraph.h>
#include <boost/algorithm/string.hpp>
#include <boost/format.hpp>

namespace armarx
{

    StatechartGroupDocGenerator::StatechartGroupDocGenerator()
    { }



    std::set<std::string> StatechartGroupDocGenerator::getUsedProfiles(const RapidXmlReaderNode& stateNode) const
    {
        std::set<std::string> usedProfiles;

        for (RapidXmlReaderNode defaultNode : stateNode.nodes("InputParameters", "Parameter", "DefaultValue"))
        {
            usedProfiles.insert(defaultNode.attribute_value_or_default("profile", "Root"));
        }

        for (RapidXmlReaderNode defaultNode : stateNode.nodes("OutputParameters", "Parameter", "DefaultValue"))
        {
            usedProfiles.insert(defaultNode.attribute_value_or_default("profile", "Root"));
        }

        for (RapidXmlReaderNode defaultNode : stateNode.nodes("LocalParameters", "Parameter", "DefaultValue"))
        {
            usedProfiles.insert(defaultNode.attribute_value_or_default("profile", "Root"));
        }

        return usedProfiles;
    }

    std::string StatechartGroupDocGenerator::unescapeString(std::string str) const
    {
        std::string res;
        std::string::const_iterator it = str.begin();

        while (it != str.end())
        {
            char c = *it++;

            if (c == '\\' && it != str.end())
            {
                switch (*it++)
                {
                    case '\\':
                        c = '\\';
                        break;

                    case 'r':
                        c = '\r';
                        break;

                    case 'n':
                        c = '\n';
                        break;

                    case 't':
                        c = '\t';
                        break;

                        // all other escapes
                    default:
                        // invalid escape sequence - skip it. alternatively you can copy it as is, throw an exception...
                        continue;
                }
            }

            res += c;
        }

        return res;
    }

    std::string StatechartGroupDocGenerator::nToBr(std::string str) const
    {
        return boost::replace_all_copy(str, "\n", "<br />\n");
    }

    DoxTablePtr StatechartGroupDocGenerator::buildParameterTable(RapidXmlReaderNode parametersNode) const
    {
        std::set<std::string> usedProfiles;

        for (RapidXmlReaderNode defaultNode : parametersNode.nodes("Parameter", "DefaultValue"))
        {
            usedProfiles.insert(defaultNode.attribute_value_or_default("profile", "Root"));
        }

        std::vector<std::string> header;

        header.push_back("Name");
        header.push_back("Type");
        header.push_back("Description");
        std::copy(usedProfiles.begin(), usedProfiles.end(), std::back_inserter(header));
        DoxTablePtr table(new DoxTable(header));

        for (RapidXmlReaderNode parameterNode : parametersNode.nodes("Parameter"))
        {
            std::vector<std::string> row(header.size());
            row.at(0) = parameterNode.attribute_value("name");
            row.at(1) = parameterNode.has_attribute("docType") ? parameterNode.attribute_value("docType") : parameterNode.attribute_value("type");
            row.at(2) = nToBr(parameterNode.first_node_value_or_default("Description", ""));

            for (RapidXmlReaderNode defaultNode : parameterNode.nodes("DefaultValue"))
            {
                int index = std::find(header.begin(), header.end(), defaultNode.attribute_value_or_default("profile", "Root")) - header.begin();
                std::string defaultValue = defaultNode.has_attribute("docValue") ? defaultNode.attribute_value("docValue") : defaultNode.attribute_value("value");
                row.at(index) = nToBr(unescapeString(defaultValue));
            }

            table->addRow(row);
        }

        return table;
    }
    void StatechartGroupDocGenerator::addParameterTable(const DoxDocPtr& doc, const RapidXmlReaderNode& stateNode, std::string name, const char* nodeName) const
    {
        DoxTablePtr table = buildParameterTable(stateNode.first_node(nodeName));

        if (table->rows() == 0)
        {
            doc->addLine("No " + name + ".<br />");
        }
        else
        {
            doc->addLine(name + ":");
            doc->addEntry(table);
        }
    }

    std::string StatechartGroupDocGenerator::getNodeAttrsFromStateType(const std::string& nodeType) const
    {
        if (nodeType == "EndState")
        {
            return "shape=box,style=filled,fillcolor=\"#FFFF96\"";
        }

        if (nodeType == "RemoteState")
        {
            return "shape=box,style=filled,fillcolor=\"#96FFFA\"";
        }

        if (nodeType == "DynamicRemoteState")
        {
            return "shape=box,style=filled,fillcolor=\"#B147EA\"";
        }

        return "shape=box,style=filled,fillcolor=\"#B3D7FF\"";
    }

    std::string StatechartGroupDocGenerator::generateDocString(const StatechartGroupXmlReaderPtr& reader) const
    {
        DoxDocPtr doc(new DoxDoc);
        std::string packageName = reader->getPackageName();
        std::string groupName = reader->getGroupName();
        doc->addLine(fmt("@defgroup %s-%s %s", packageName, groupName, groupName));
        doc->addLine(fmt("@ingroup Statecharts  %s-Statecharts", packageName));
        doc->addLine(nToBr(reader->getDescription()));


        bool anyPrivateStateFound = false;

        for (std::string statefile : reader->getStateFilepaths())
        {
            if (reader->getStateVisibility(statefile) == StatechartGroupXmlReader::ePrivate)
            {
                anyPrivateStateFound = true;
            }

        }

        if (anyPrivateStateFound)
        {
            doc->addLine();
            doc->addLine(fmt("@defgroup %s-%s-PrivateStates %s Private States", packageName, groupName, groupName));
            doc->addLine(fmt("@ingroup %s-%s", packageName, groupName));
        }

        for (std::string statefile : reader->getStateFilepaths())
        {
            RapidXmlReaderPtr stateReader = RapidXmlReader::FromFile(statefile);
            RapidXmlReaderNode stateNode = stateReader->getRoot("State");
            std::string stateName = stateNode.attribute_value("name");
            std::string stateUuid = stateNode.attribute_value("uuid");

            doc->addLine();
            //doc->addLine(fmt("@defgroup %s-%s-%s %s", packageName, groupName, stateName, stateName));
            doc->addLine(fmt("@defgroup State-%s %s", stateUuid, stateName));
            doc->addLine(fmt("@ingroup %s-%s%s", packageName, groupName, reader->getStateVisibility(statefile) == StatechartGroupXmlReader::ePublic ? "" : "-PrivateStates"));
            doc->addLine("@brief " + stateNode.first_node_value_or_default("Description", "This state has no description."));
            doc->addLine();

            addParameterTable(doc, stateNode, "\\b Input \\b Parameters", "InputParameters");
            addParameterTable(doc, stateNode, "\\b Local \\b Parameters", "LocalParameters");
            addParameterTable(doc, stateNode, "\\b Output \\b Parameters", "OutputParameters");

            doc->addLine("<br/>\\b State \\b used by:<br />");

            if (usageMap.count(stateUuid) > 0)
            {
                std::set<std::string> users = usageMap.find(stateUuid)->second;

                for (std::string uuid : users)
                {
                    doc->addLine(fmt("@ref State-%s <br />", uuid));
                }
            }

            doc->addLine("<br/>\\b Substates:<br />");

            for (RapidXmlReaderNode substateNode : stateNode.nodes("Substates", 0))
            {
                if (substateNode.name() == "LocalState" || substateNode.name() == "RemoteState" || substateNode.name() == "DynamicRemoteState")
                {
                    std::string name = substateNode.attribute_value("name");
                    std::string refuuid = substateNode.attribute_value("refuuid");
                    std::map<std::string, StateInfo>::const_iterator it = uuidToStateInfoMap.find(refuuid);

                    if (it == uuidToStateInfoMap.end())
                    {
                        doc->addLine(fmt("@ref State-%s \"%s (External Package)\"<br />", refuuid, name));
                    }
                    else
                    {
                        StateInfo stateInfo = it->second;

                        if (stateInfo.group == groupName && stateInfo.state == name)
                        {
                            doc->addLine(fmt("@ref State-%s<br />", refuuid));
                        }
                        else if (stateInfo.group == groupName)
                        {
                            doc->addLine(fmt("%s -> @ref State-%s \"%s\"<br />", name, refuuid, stateInfo.state));
                        }
                        else
                        {
                            doc->addLine(fmt("%s -> @ref State-%s \"%s-%s\"<br />", name, refuuid, stateInfo.group, stateInfo.state));
                        }
                    }
                }
            }

            doc->addLine("<br/>\\b Statechart \\b layout:");
            DoxTransitionGraphPtr graph(new DoxTransitionGraph());
            graph->addNode("", "shape=point");
            RapidXmlReaderNode startStateNode = stateNode.first_node("StartState");

            if (startStateNode.is_valid())
            {
                graph->addTransition("", startStateNode.attribute_value("substateName"), "");
            }

            for (RapidXmlReaderNode substateNode : stateNode.nodes("Substates", 0))
            {
                std::string shape = getNodeAttrsFromStateType(substateNode.name());
                graph->addNode(substateNode.attribute_value("name"), shape);
            }

            for (RapidXmlReaderNode transition : stateNode.nodes("Transitions", "Transition"))
            {
                if (transition.has_attribute("from") && transition.has_attribute("to") && transition.has_attribute("eventName"))
                {
                    std::string from = transition.attribute_value_or_default("from", "");
                    std::string to = transition.attribute_value_or_default("to", "");
                    std::string eventName = transition.attribute_value_or_default("eventName", "");

                    graph->addTransition(from, to, eventName);
                }
            }

            if (graph->transitionCount() > 0)
            {
                doc->addEntry(graph);
            }


            /*std::vector<std::string> header;

            header.push_back("Name");
            header.push_back("Type");
            header.push_back("Description");
            std::copy(usedProfiles.begin(), usedProfiles.end(), std::back_inserter(header));
            DoxTablePtr table(new DoxTable(header));

            for(RapidXmlReaderNode parameterNode : stateNode.nodes("InputParameters", "Parameter"))
            {
                std::vector<std::string> row(header.size());
                row.at(0) = parameterNode.attribute_value("name");
                row.at(1) = parameterNode.attribute_value("type");
                row.at(2) = parameterNode.first_node_value_or_default("Description", "");
                for(RapidXmlReaderNode defaultNode : parameterNode.nodes("DefaultValue"))
                {
                    int index = std::find(header.begin(), header.end(), defaultNode.attribute_value_or_default("profile", "Root")) - header.begin();
                    row.at(index) = defaultNode.has_attribute("docValue") ? defaultNode.attribute_value("docValue") : defaultNode.attribute_value("value");
                }
                table->addRow(row);
            }

            doc->addEntry(table);*/
        }


        CppWriterPtr writer(new CppWriter);
        doc->writeDoc(writer);
        return writer->getString();
    }

    std::string StatechartGroupDocGenerator::fmt(const std::string& fmt, const std::string& arg1) const
    {
        return boost::str(boost::format(fmt) % arg1);
    }

    std::string StatechartGroupDocGenerator::fmt(const std::string& fmt, const std::string& arg1, const std::string& arg2) const
    {
        return boost::str(boost::format(fmt) % arg1 % arg2);
    }

    std::string StatechartGroupDocGenerator::fmt(const std::string& fmt, const std::string& arg1, const std::string& arg2, const std::string& arg3) const
    {
        return boost::str(boost::format(fmt) % arg1 % arg2 % arg3);
    }

    std::string StatechartGroupDocGenerator::fmt(const std::string& fmt, const std::string& arg1, const std::string& arg2, const std::string& arg3, const std::string& arg4) const
    {
        return boost::str(boost::format(fmt) % arg1 % arg2 % arg3 % arg4);
    }

    void StatechartGroupDocGenerator::generateDoxygenFile(const std::string& groupDefinitionFilePath)
    {
        StatechartGroupXmlReaderPtr reader(new StatechartGroupXmlReader(StatechartProfiles::StatechartProfilePtr()));
        reader->readXml(boost::filesystem::path(groupDefinitionFilePath));
        CMakePackageFinder finder = getFinder(reader);

        boost::filesystem::path doxyFile = finder.getBuildDir();

        if (!outputPath.empty())
        {
            doxyFile = outputPath;
        }

        if (!doxyFile.string().empty())
        {
            doxyFile /= "doxygen";
        }
        else
        {
            doxyFile = ".";
        }

        doxyFile /= reader->getGroupName() + ".dox";

        std::ofstream file;
        file.open(doxyFile.string().c_str());

        if (!file.is_open())
        {
            throw armarx::exceptions::local::FileOpenException(doxyFile.string());
        }

        std::string docstr = generateDocString(reader);
        file << docstr;

        ARMARX_INFO_S << "wrote " << docstr.size() << " bytes to " << doxyFile.string();

    }

    void StatechartGroupDocGenerator::buildReferenceIndex(const std::vector<std::string>& groups)
    {
        for (std::string groupDefinitionFilePath : groups)
        {
            StatechartGroupXmlReaderPtr reader(new StatechartGroupXmlReader(StatechartProfiles::StatechartProfilePtr()));
            reader->readXml(boost::filesystem::path(groupDefinitionFilePath));

            for (std::string statefile : reader->getStateFilepaths())
            {
                RapidXmlReaderPtr stateReader = RapidXmlReader::FromFile(statefile);
                RapidXmlReaderNode stateNode = stateReader->getRoot("State");
                std::string stateName = stateNode.attribute_value("name");
                std::string stateUuid = stateNode.attribute_value("uuid");
                uuidToStateInfoMap.insert(std::make_pair(stateUuid, StateInfo(reader->getPackageName(), reader->getGroupName(), stateName)));

                for (RapidXmlReaderNode substateNode : stateNode.nodes("Substates", 0))
                {
                    if (substateNode.name() == "LocalState" || substateNode.name() == "RemoteState" || substateNode.name() == "DynamicRemoteState")
                    {
                        //std::string name = substateNode.attribute_value("name");
                        std::string refuuid = substateNode.attribute_value("refuuid");

                        if (usageMap.count(refuuid) == 0)
                        {
                            usageMap.insert(std::make_pair(refuuid, std::set<std::string>()));
                        }

                        usageMap[refuuid].insert(stateUuid);
                    }
                }
            }
        }
    }

    void StatechartGroupDocGenerator::generateDoxygenFiles(const std::vector<std::string>& groups)
    {
        for (std::string groupDefinitionFilePath : groups)
        {
            generateDoxygenFile(groupDefinitionFilePath);
        }
    }

    void StatechartGroupDocGenerator::setOutputpath(const std::string& outputPath)
    {
        this->outputPath = outputPath;
    }

    std::vector<std::string> StatechartGroupDocGenerator::FindAllStatechartGroupDefinitions(const boost::filesystem::path& path)
    {
        std::vector<std::string> result;

        try
        {
            int i = 0;

            for (boost::filesystem::recursive_directory_iterator end, dir(path);
                 dir != end ; ++dir, i++)
            {
                if (dir->path().extension() == ".scgxml" && dir->path().string().find("deprecated") == std::string::npos)
                {
                    result.push_back(dir->path().c_str());
                }

                if (i % 100 == 0)
                {
                    ARMARX_INFO_S << "Scanning file " << i << ": " << dir->path().c_str();
                }
            }
        }
        catch (std::exception& e)
        {
            ARMARX_WARNING_S << "Invalid filepath: " << e.what();
        }

        return result;
    }

    CMakePackageFinder StatechartGroupDocGenerator::getFinder(const StatechartGroupXmlReaderPtr& reader)
    {
        auto itFinders = finders.find(reader->getPackageName());

        if (itFinders == finders.end())
        {
            itFinders = finders.insert(std::make_pair(reader->getPackageName(), CMakePackageFinder(reader->getPackageName()))).first;
        }

        return itFinders->second;
    }


} // namespace armarx
