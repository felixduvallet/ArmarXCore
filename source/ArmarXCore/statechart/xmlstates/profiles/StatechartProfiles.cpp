#include "StatechartProfiles.h"

#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>
#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>
#include <ArmarXCore/core/logging/Logging.h>


using namespace armarx;

StatechartProfiles::StatechartProfiles()
    : rootProfile(new StatechartProfile(StatechartProfiles::GetRootName(), StatechartProfilePtr()))
{
}

void StatechartProfiles::readStatechartProfiles(RapidXmlReaderPtr reader)
{
    RapidXmlReaderNode node = reader->getRoot("Profiles");
    rootProfile->readFromXml(node);
}



StatechartProfilesPtr StatechartProfiles::ReadProfileFiles(const std::vector<std::string>& packages)
{
    StatechartProfilesPtr profiles(new StatechartProfiles());

    for (std::string project : packages)
    {
        boost::filesystem::path profilesFile(CMakePackageFinder(project).getDataDir().c_str());
        profilesFile /= project;
        profilesFile /= "StatechartProfiles-" + project + ".xml";

        if (boost::filesystem::exists(profilesFile))
        {
            try
            {
                RapidXmlReaderPtr xmlReader = RapidXmlReader::FromFile(profilesFile.string());
                profiles->readStatechartProfiles(xmlReader);
            }
            catch (std::exception& e)
            {
                ARMARX_ERROR_S << "Reading " << profilesFile.string() << " failed: " << e.what();
            }
        }
        else
        {
            ARMARX_INFO_S << "StatechartProfiles File not found for project " << project << ": " << profilesFile.string();
        }
    }

    return profiles;
}

std::vector<StatechartProfiles::StatechartProfilePtr> StatechartProfiles::getAllLeaves()
{
    std::vector<StatechartProfiles::StatechartProfilePtr> profiles;
    getAllLeaves(rootProfile, profiles);
    return profiles;
}

StatechartProfiles::StatechartProfilePtr StatechartProfiles::getProfileByName(std::string name)
{
    if (name == "")
    {
        return rootProfile;
    }

    return rootProfile->findByNameRecursive(name);
}

void StatechartProfiles::getAllLeaves(StatechartProfiles::StatechartProfilePtr currentProfile, std::vector<StatechartProfiles::StatechartProfilePtr>& profiles)
{
    if (currentProfile->children.size() == 0)
    {
        profiles.push_back(currentProfile);
    }
    else
    {
        for (StatechartProfilePtr p : currentProfile->children)
        {
            getAllLeaves(p, profiles);
        }
    }
}


StatechartProfiles::StatechartProfile::StatechartProfile(const std::string& name, StatechartProfilePtr parent)
{
    this->name = name;
    this->parent = parent;
}

void StatechartProfiles::StatechartProfile::readFromXml(RapidXmlReaderNode node)
{
    for (RapidXmlReaderNode packageNode : node.nodes("Package"))
    {
        std::string packageName = packageNode.attribute_value("name");

        if (std::find(packages.begin(), packages.end(), packageName) == packages.end())
        {
            packages.push_back(packageName);
        }
    }

    for (RapidXmlReaderNode profileNode : node.nodes("Profile"))
    {
        std::string name = profileNode.attribute_value("name");
        StatechartProfilePtr profile = getChildByName(name); // Check if profile has already been read previously. Merge profiles if necessary.

        if (profile)
        {
            ARMARX_INFO_S << "Merging Profile " << name;
        }
        else
        {
            profile.reset(new StatechartProfile(profileNode.attribute_value("name"), shared_from_this()));
            children.push_back(profile);
        }

        profile->readFromXml(profileNode);
    }
}

bool StatechartProfiles::StatechartProfile::isLeaf()
{
    return children.size() == 0;
}

bool StatechartProfiles::StatechartProfile::isRoot()
{
    return !parent.lock();
}

std::vector<std::string> StatechartProfiles::StatechartProfile::getAllPackages()
{
    std::vector<std::string> packages;
    StatechartProfilePtr parent = this->parent.lock();

    if (parent)
    {
        packages = parent->getAllPackages();
    }

    for (std::string package : this->packages)
    {
        if (std::find(packages.begin(), packages.end(), package) == packages.end())
        {
            packages.push_back(package);
        }
    }

    return packages;
}

std::string StatechartProfiles::StatechartProfile::getName()
{
    return name;
}

std::string StatechartProfiles::StatechartProfile::getFullName()
{
    StatechartProfilePtr parent = this->parent.lock();

    if (!parent)
    {
        return name;    // root node returns "ROOT"
    }

    if (parent->isRoot())
    {
        return name;    // avoid adding "::" in the front
    }

    return parent->getFullName() + "::" + name;
}

StatechartProfiles::StatechartProfilePtr StatechartProfiles::StatechartProfile::findByNameRecursive(std::string name)
{
    if (this->name == name)
    {
        return shared_from_this();
    }

    for (StatechartProfilePtr p : children)
    {
        StatechartProfilePtr match = p->findByNameRecursive(name);

        if (match)
        {
            return match;
        }
    }

    return StatechartProfilePtr();
}

StatechartProfiles::StatechartProfilePtr StatechartProfiles::StatechartProfile::getChildByName(std::string name)
{
    for (StatechartProfilePtr p : children)
    {
        if (p->name == name)
        {
            return p;
        }
    }

    return StatechartProfilePtr();
}

StatechartProfiles::StatechartProfilePtr StatechartProfiles::StatechartProfile::getParent()
{
    return parent.lock();
}

int StatechartProfiles::StatechartProfile::getNesting()
{
    return isRoot() ? 1 : getParent()->getNesting() + 1;
}
