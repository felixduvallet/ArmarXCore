#ifndef StatechartProfiles_H
#define StatechartProfiles_H

#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlReader.h>
#include <vector>
#include <string>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>

namespace armarx
{
    class StatechartProfiles;
    typedef boost::shared_ptr<StatechartProfiles> StatechartProfilesPtr;

    class StatechartProfiles
    {
    public:
        static std::string GetRootName()
        {
            return "Root";
        }

        class StatechartProfile;
        typedef boost::shared_ptr<StatechartProfile> StatechartProfilePtr;
        typedef boost::weak_ptr<StatechartProfile> StatechartProfileWeakPtr;

        class StatechartProfile
            : public boost::enable_shared_from_this<StatechartProfile>
        {
            friend class StatechartProfiles;

        public:
            StatechartProfile(const std::string& name, StatechartProfilePtr parent);
            void readFromXml(RapidXmlReaderNode node);
            bool isLeaf();
            bool isRoot();
            std::vector<std::string> getAllPackages();
            std::string getName();
            std::string getFullName();
            StatechartProfilePtr findByNameRecursive(std::string name);
            StatechartProfilePtr getChildByName(std::string name);
            StatechartProfilePtr getParent();
            int getNesting();

        private:

            StatechartProfileWeakPtr parent;
            std::vector<StatechartProfilePtr> children;
            std::vector<std::string> packages;
            std::string name;
        };

    public:
        StatechartProfiles();
        void readStatechartProfiles(RapidXmlReaderPtr reader);

        static StatechartProfilesPtr ReadProfileFiles(const std::vector<std::string>& packages);

        std::vector<StatechartProfilePtr> getAllLeaves();
        StatechartProfilePtr getProfileByName(std::string name);

    private:
        StatechartProfilePtr rootProfile;
        void getAllLeaves(StatechartProfilePtr currentProfile, std::vector<StatechartProfilePtr>& profiles);
    };
}

#endif // StatechartProfiles_H
