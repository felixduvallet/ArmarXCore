/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef _ARMARX_XMLSTATECOMPONENT_H
#define _ARMARX_XMLSTATECOMPONENT_H

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/system/DynamicLibrary.h>
#include <ArmarXCore/statechart/xmlstates/profiles/StatechartProfiles.h>
//#include <ArmarXCore/statechart/StatechartContext.h>
//#include "XMLRemoteStateOfferer.h"

#define STATEOFFERERSUFFIX "RemoteStateOfferer"

namespace armarx
{

    struct XMLStateOffererFactoryBase;
    typedef  IceInternal::Handle<XMLStateOffererFactoryBase > XMLStateOffererFactoryBasePtr;

    class XMLStateComponentProperties :
        public ComponentPropertyDefinitions
    {
    public:
        XMLStateComponentProperties(std::string prefix):
            ComponentPropertyDefinitions(prefix)
        {
            defineRequiredProperty<std::string>("XMLStatechartGroupDefinitionFile", "Path to statechart group definition file (*.scgxml) - relative to projects source dir");
            defineOptionalProperty<std::string>("XMLStatechartProfile", "", "Profile to use for state execution. Should be the same for all Statechart Groups.");
            //defineOptionalProperty<std::string>("XMLStatechartProfileDefinitionFile", "", "Path to StatechartProfiles-<ProjectName>.xml file.");
        }
    };

    /**
     * @defgroup Component-XMLStateComponent XMLStateComponent
     * @ingroup ArmarXCore-Components
     * TODO: Write description
     *
     * @class XMLStateComponent
     * @ingroup Component-XMLStateComponent
     */
    class XMLStateComponent :
        public Component
    {
    public:
        XMLStateComponent();

        // PropertyUser interface
        PropertyDefinitionsPtr createPropertyDefinitions();


        // ManagedIceObject interface
        bool loadLib(std::string libPath);
    protected:
        void onInitComponent();
        void onConnectComponent();
        std::string getDefaultName() const;

        std::map<std::string, DynamicLibraryPtr> libraries;
        std::string uuid;
        std::string offererName;
        XMLStateOffererFactoryBasePtr offerer;
        StatechartProfilesPtr profiles;
    };

}

#endif
