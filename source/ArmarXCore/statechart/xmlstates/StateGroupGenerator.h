#ifndef ARMARX_STATEGROUPGENERATOR_H
#define ARMARX_STATEGROUPGENERATOR_H

#include "GroupXmlReader.h"

#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>
#include <ArmarXCore/statechart/xmlstates/baseclassgenerator/VariantInfo.h>
#include <boost/filesystem.hpp>

namespace armarx
{

    class StatechartGroupGenerator
    {
    public:
        static void generateStatechartFiles(const std::string& statechartsPath);

        static void generateStateFile(const std::string& statechartGroupXmlFilePath, const std::string& statePath, const std::string& packagePath);
        static void generateStatechartContextFile(const std::string& statechartGroupXmlFilePath, const std::string& packagePath);

        static bool generateStateFile(const std::string& stateName, RapidXmlReaderPtr reader, boost::filesystem::path buildDir, const std::string& packageName, const std::string& groupName, const std::vector<std::string>& proxies, bool contextGenerationEnabled, const VariantInfoPtr& variantInfo, bool forceRewrite);
        static bool generateStatechartContextFile(boost::filesystem::path buildDir, const std::string& packageName, const std::string& groupName, const std::vector<std::string>& proxies, const VariantInfoPtr& variantInfo, bool forceRewrite);

        static bool writeFileContentsIfChanged(const std::string& path, const std::string& contents);
        static void writeFileContents(const std::string& path, const std::string& contents);

    };

} // namespace armarx

#endif // ARMARX_STATEGROUPDOCGENERATOR_H
