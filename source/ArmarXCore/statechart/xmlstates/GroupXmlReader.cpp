#include "GroupXmlReader.h"


#include <string>

#include <fstream>
#include <streambuf>
#include <stdexcept>
#include <boost/filesystem.hpp>

#include <ArmarXCore/core/exceptions/local/FileIOException.h>

using namespace armarx;
using namespace rapidxml;


StatechartGroupXmlReader::StatechartGroupXmlReader(const StatechartProfiles::StatechartProfilePtr& selectedProfile)
    : selectedProfile(selectedProfile)
{

}

void StatechartGroupXmlReader::readXml(const boost::filesystem::path& groupDefinitionFile)
{
    RapidXmlReaderPtr wrapper = RapidXmlReader::FromFile(groupDefinitionFile.string());
    this->groupDefinitionFile = groupDefinitionFile;
    basePath = groupDefinitionFile;
    basePath = basePath.remove_filename();
    //ARMARX_IMPORTANT_S << "basePath: " << basePath.string();
    readXml(wrapper);
}

/*void StatechartGroupXmlReader::readXml(const std::string &groupDefinitionXMLString)
{
    readXml(RapidXmlReader::FromXmlString(groupDefinitionXMLString));
}*/

void StatechartGroupXmlReader::readXml(RapidXmlReaderPtr reader)
{

    RapidXmlReaderNode xmlGroupNode = reader->getRoot("StatechartGroup");
    groupName = xmlGroupNode.attribute_value("name");
    description = xmlGroupNode.first_node_value_or_default("Description", "");
    packageName = xmlGroupNode.attribute_value("package");
    generateContext = xmlGroupNode.attribute_as_optional_bool("generateContext", "true", "false", false);

    for (RapidXmlReaderNode proxyNode : xmlGroupNode.nodes("Proxies", "Proxy"))
    {
        proxies.push_back(proxyNode.attribute_value("value"));
    }

    ReadChildren(xmlGroupNode, basePath, 0);
}

boost::filesystem::path StatechartGroupXmlReader::getGroupDefinitionFilePath() const
{
    return groupDefinitionFile;
}


void StatechartGroupXmlReader::ReadChildren(RapidXmlReaderNode xmlNode, const boost::filesystem::path& path, int nesting)
{
    for (RapidXmlReaderNode xmlFolderNode = xmlNode.first_node("Folder"); xmlFolderNode.is_valid(); xmlFolderNode = xmlFolderNode.next_sibling("Folder"))
    {
        boost::filesystem::path basename(xmlFolderNode.attribute_value("basename"));

        ReadChildren(xmlFolderNode, path / basename, nesting + 1);
    }

    for (RapidXmlReaderNode xmlStateNode = xmlNode.first_node("State"); xmlStateNode.is_valid(); xmlStateNode = xmlStateNode.next_sibling("State"))
    {
        boost::filesystem::path filename(xmlStateNode.attribute_value("filename").c_str());
        std::string fullpath = (path / filename).string();
        allstateFilePaths.push_back(fullpath);
        std::string visibility = xmlStateNode.attribute_value_or_default("visibility", "");
        stateVisibilityMap[fullpath] = visibility == "public" ? ePublic : ePrivate;
        stateNestingMap[fullpath] = nesting;
    }

}
std::string StatechartGroupXmlReader::getPackageName() const
{
    return packageName;
}

bool StatechartGroupXmlReader::contextGenerationEnabled() const
{
    return generateContext;
}

StatechartGroupXmlReader::StateVisibility StatechartGroupXmlReader::getStateVisibility(const std::string& filepath) const
{
    auto it = stateVisibilityMap.find(filepath);

    if (it == stateVisibilityMap.end())
    {
        throw LocalException("State ") << filepath << " not found.";
    }

    return it->second;
}

int StatechartGroupXmlReader::getStateNestingLevel(std::string filepath) const
{
    filepath = ArmarXDataPath::cleanPath(filepath);
    auto it = stateNestingMap.find(filepath);

    if (it == stateNestingMap.end())
    {
        std::stringstream ss;

        for (std::pair<std::string, int> pair : stateNestingMap)
        {
            ss << "\n" << pair.first;
        }

        throw LocalException("State File ") << filepath << " not found in nesting map. States in map (" << stateNestingMap.size() << "):" << ss.str();
    }

    return it->second;
}

std::vector<std::string> StatechartGroupXmlReader::getProxies() const
{
    return proxies;
}

std::string StatechartGroupXmlReader::getDescription() const
{
    return description;
}

