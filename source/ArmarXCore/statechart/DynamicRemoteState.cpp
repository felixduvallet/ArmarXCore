/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::statechart
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "DynamicRemoteState.h"

#include <IceUtil/IceUtil.h>
#include <ArmarXCore/core/ArmarXObjectScheduler.h>
#include <ArmarXCore/core/ArmarXManager.h>

using namespace armarx;



DynamicRemoteState::DynamicRemoteState() :
    RemoteState()
{
    stateType = eDynamicRemoteState;
    stateName = "DynamicRemoteState";
    greedyInputDictionary = true;
}

DynamicRemoteState::DynamicRemoteState(const DynamicRemoteState& source) :
    IceUtil::Shared(source),
    Ice::Object(source),
    StateIceBase(source),
    RemoteStateInterface(source),
    RemoteStateIceBase(source),
    Logging(source),
    StateBase(source),
    StateController(source),
    ManagedIceObject(source),
    RemoteState(source)

{
    stateType = eDynamicRemoteState;
    stateName = "DynamicRemoteState";
    greedyInputDictionary = true;
}

DynamicRemoteState& DynamicRemoteState::operator =(const DynamicRemoteState& source)
{
    assert(0);
    stateName = "DynamicRemoteState";
    stateType = eDynamicRemoteState;
    greedyInputDictionary = true;

    return *this;
}

std::string DynamicRemoteState::getDefaultName() const
{
    std::stringstream str;
    str << localUniqueId << "_" << globalStateIdentifier;
    return str.str();
}

void DynamicRemoteState::onInitComponent()
{
}


void DynamicRemoteState::onConnectComponent()
{
    // get proxy to self/this
    myProxy = RemoteStateIceBasePrx::checkedCast(getProxy());
}


bool DynamicRemoteState::__checkStatePreconditions()
{

    StateParameterMap::iterator itProxy = inputParameters.find("proxyName");
    StateParameterMap::iterator itState = inputParameters.find("stateName");

    if (itProxy == inputParameters.end())
    {
        ARMARX_WARNING << "'proxyName' is not specified in the input parameters of the DynamicRemoteState '" + stateName + "'." << flush;
        return false;
    }

    if (itState == inputParameters.end())
    {
        ARMARX_WARNING << "'stateName' is not specified in the input parameters of the DynamicRemoteState '" + stateName + "'." << flush;
        return false;
    }

    if (!itProxy->second->set)
    {
        ARMARX_WARNING << "'proxyName' is not set in the input parameters of the DynamicRemoteState '" + stateName + "'." << flush;
        return false;
    }

    if (!itState->second->set)
    {
        ARMARX_WARNING << ("'stateName' is not set in the input parameters of the DynamicRemoteState '" + stateName + "'.") << flush;
        return false;
    }

    if (!proxyName.empty())
    {
        removeProxyDependency(proxyName);
    }

    SingleVariantPtr sVar = SingleVariantPtr::dynamicCast(itProxy->second->value);
    proxyName = sVar->get()->getString();

    if (proxyName.empty())
    {
        //            throw LocalException("'proxyName' parameter is empty.");
        ARMARX_WARNING << "'proxyName' parameter is empty." << flush;
        return false;
    }

    sVar = SingleVariantPtr::dynamicCast(itState->second->value);
    std::string tempStateStr = sVar->get()->getString();

    if (tempStateStr.empty())
    {
        //            throw LocalException("'stateName' parameter is empty.");
        ARMARX_WARNING << ("'stateName' parameter is empty.") << flush;
        return false;
    }

    stateClassName = tempStateStr;
    stateName = tempStateStr;


    //    StateParameterMap remoteInput;
    if (!getObjectScheduler()->waitForObjectStateMinimum(eManagedIceObjectStarted))
    {
        return false;
    }

    try
    {
        stateOffererPrx = getProxy<RemoteStateOffererIceBasePrx>(proxyName, true);
        //        remoteInput = stateOffererPrx->getRemoteInputParameters(stateClassName);
    }
    catch (const Ice::NotRegisteredException&)
    {
        ARMARX_WARNING << "The Component with name '" + proxyName + "' is not registered in Ice." << flush;
        return false;
    }
    catch (std::exception& e)
    {
        ARMARX_WARNING << "Could not get proxy for DynamicRemotestate with: proxyName: '" + proxyName + "' and stateName: '" + stateName + "'!\n\n" << e.what();
        return false;
    }


    globalStateIdentifier.erase(globalStateIdentifier.find_last_of(">") - 1);

    // get the remotestate inputparameters and fill them with the already set (greedy) input parameters of this dynamic remote state
    //    StateParameterMap tempMap;
    //    StateUtilFunctions::copyDictionary(inputParameters, tempMap);
    //    StateUtilFunctions::copyDictionary(remoteInput, inputParameters);
    //    StateUtilFunctions::fillDictionary(StateUtilFunctions::getSetValues(tempMap), inputParameters);
    //    std::string paramCheckOutput;


    //    if (!StateUtilFunctions::checkForCompleteParameters(inputParameters, &paramCheckOutput))
    //    {
    //        ARMARX_WARNING << ("Not all required inputparameters of the Dynamic Remote State '" + stateName + "' are set.\n" + paramCheckOutput);
    //        return false;
    //    }


    itProxy->second->set = false;
    itState->second->set = false;

    // myProxy is set in onConnectComponent() which is running in another thread, to avoid race conditions, wait for it
    if (!getObjectScheduler()->waitForObjectState(eManagedIceObjectStarted, 10000))
    {
        ARMARX_WARNING << ("Waited more than 10 seconds for dynamic remote state to be ready - bailing out");
        return false;
    }

    return true;
}

void
DynamicRemoteState::_baseOnEnter()
{

    bool loadingSuccessfull = __checkStatePreconditions();

    if (!loadingSuccessfull)
    {
        EventPtr evt = new LoadingFailed(EVENTTOALL);
        __enqueueEvent(evt);
        return;
    }

    // this is usually done in RemoteState::onConnectComponent()
    // but in DynamicRemoteState it must be done here because the proxy name
    // is only available in the Event sent to this state
    remoteStateId = stateOffererPrx->createRemoteStateInstance(stateClassName, myProxy, globalStateIdentifier, stateName);
    globalStateIdentifier += "->" + stateName;
    StateIceBasePtr state = stateOffererPrx->getStatechartInstance(remoteStateId);
    subStateList = state->subStateList;
    transitions = state->transitions;

    RemoteState::_baseOnEnter();

}

void DynamicRemoteState::_baseOnExit()
{
    RemoteState::_baseOnExit();
    inputParameters.clear();
    removeProxyDependency(proxyName);
    // the input parameters have been deleted due to assignment of the remote inputparameters, re-insert them
    defineParameters();
}

bool DynamicRemoteState::_baseOnBreak(const EventPtr evt)
{

    bool result = RemoteState::_baseOnBreak(evt);

    if (result)
    {
        inputParameters.clear();
        // the input parameters have been deleted due to assignment of the remote inputparameters, re-insert them
        defineParameters();
    }

    return result;
}

StateBasePtr DynamicRemoteState::createEmptyCopy() const
{
    RemoteStatePtr result = RemoteStatePtr::dynamicCast(ManagedIceObject::create<DynamicRemoteState>());
    result->setName(result->getDefaultName() + IceUtil::generateUUID());
    getArmarXManager()->addObject(ManagedIceObjectPtr::dynamicCast(result));
    return result;
}
StateBasePtr DynamicRemoteState::clone() const
{
    RemoteStatePtr result = new DynamicRemoteState(*this);
    result->setName(result->getDefaultName() + IceUtil::generateUUID());
    getArmarXManager()->addObject(ManagedIceObjectPtr::dynamicCast(result));

    return result;
}

void DynamicRemoteState::defineParameters()
{
    addParameter(inputParameters, "proxyName", VariantType::String, false);
    addParameter(inputParameters, "stateName", VariantType::String, false);
}
