/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::StatechartExamples::InstallConditionExample
* @author     Mirko Waechter (mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef _ARMARX_COMPONENT_InstallConditionExample_InstallConditionExample_H
#define _ARMARX_COMPONENT_InstallConditionExample_InstallConditionExample_H

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>
#include <ArmarXCore/statechart/Statechart.h>

namespace armarx
{

    class ARMARXCOMPONENT_IMPORT_EXPORT InstallConditionExample :
        virtual public StatechartContext
    {
    public:
        // inherited from Component

        virtual std::string getDefaultName() const
        {
            return "GraspPositionReached";
        }
        virtual void onInitStatechart();
        virtual void onConnectStatechart();



    };

    DEFINEEVENT(EvConditionReached)
    struct stateInstallCondition;
    struct StatechartInstallConditionExample : StateTemplate<StatechartInstallConditionExample>
    {

        void defineState()
        {
        }

        void defineSubstates()
        {
            // add substates

            setInitState(addState<stateInstallCondition>("ApproachGraspPosition"));
            StatePtr final = addState<SuccessState>("Success");

            // add transition
            ParameterMappingPtr mapping = ParameterMapping::createMapping()->mapFromOutput("hand_x", "position_x");
            addTransition<EvConditionReached>(getInitState(),
                                              final, mapping);
        }

        void defineParameters()
        {
            addToInput("hand_x", VariantType::Float, true);
            addToOutput("hand_current_x", VariantType::Float, true);
        }


        void onEnter()
        {
        }
    };



    struct stateInstallCondition : StateTemplate<stateInstallCondition>
    {
        ConditionIdentifier condId;
        void onEnter()
        {
            Literal checkAngles1("Armar4Observer.jointangles.LeftArm_Joint1", "larger", Literal::createParameterList(1.5f));
            Literal checkAngles2("Armar4Observer.jointangles.LeftArm_Joint2", "smaller", Literal::createParameterList(1.f));
            Literal checkAngles3("Armar4Observer.jointangles.Torso_Joint2", "larger", Literal::createParameterList(0.5f));
            condId = installCondition<EvConditionReached>((!checkAngles1 || !checkAngles2) &&  checkAngles3);
        }
        void onExit()
        {

            removeCondition(condId);
        }


    };




}

#endif
