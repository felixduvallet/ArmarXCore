/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::StatechartExamples::StateParameterExample
* @author     Mirko Waechter (mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef _ARMARX_COMPONENT_StateParameterExample_StateParameterExample_H
#define _ARMARX_COMPONENT_StateParameterExample_StateParameterExample_H

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>
#include <ArmarXCore/statechart/Statechart.h>

namespace armarx
{
    class ARMARXCOMPONENT_IMPORT_EXPORT StateParameterExample :
        virtual public StatechartContext
    {
    public:
        // inherited from Component
        virtual std::string getDefaultName() const
        {
            return "StateParameterExample";
        };
        virtual void onInitStatechart();
        virtual void onConnectStatechart();
        void run();
    };

    // Define Events before the first state they are used in
    DEFINEEVENT(EvInit)  // this macro declares a new event-class derived vom Event, to have a compiletime check for typos in events
    DEFINEEVENT(EvNext)

    struct StateResult;
    struct Statechart_StateParameterExample : StateTemplate<Statechart_StateParameterExample>
    {

        void defineState();

        void defineSubstates();
        void defineParameters();

        void onEnter();
    };

    struct StateRun : StateTemplate<StateRun>
    {
        ConditionIdentifier condId;
        void defineParameters();
        void onEnter();
        void onBreak();

        void onExit();
    };

    struct StateResult : StateTemplate<StateResult>
    {
        void defineParameters();
        void onEnter();

    };

}

#endif
