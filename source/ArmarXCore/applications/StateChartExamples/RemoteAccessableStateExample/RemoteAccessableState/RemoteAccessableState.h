/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::StatechartExamples::RemoteAccessableStateExample::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       20122012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef _ARMARX_COMPONENT_RemoteAccessableStateExample_RemoteAccessableStateExample_H
#define _ARMARX_COMPONENT_RemoteAccessableStateExample_RemoteAccessableStateExample_H


#include <ArmarXCore/core/system/ImportExportComponent.h>
#include <ArmarXCore/statechart/Statechart.h>

namespace armarx
{
    class ARMARXCOMPONENT_IMPORT_EXPORT RemoteAccessableState :
        virtual public RemoteStateOfferer<StatechartContext>
    {
    public:
        // inherited from Component
        std::string getStateOffererName() const
        {
            return "RemoteAccessable";
        }
        // inherited from RemoteStateOfferer
        void onInitRemoteStateOfferer();
    };

    struct Stateadd_x_to_y : StateTemplate<Stateadd_x_to_y>
    {
        void defineParameters()
        {
            addToInput("x", VariantType::Float, false);
            addToInput("y", VariantType::Float, false);

            addToOutput("result", VariantType::Float, false);
        }

        void defineSubstates()
        {
            setInitState(addState<State>("DummySubstate1"));
            addState<State>("DummySubstate2");

        }

        void onEnter()
        {
            float result = getInput<float>("x") +
                           getInput<float>("y");
            setOutput("result", result);
            ARMARX_INFO << "result: " << result << std::endl;
            sendEvent<Success>();
        }


    };


}

#endif
