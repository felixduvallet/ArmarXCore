/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Core::application::DummyUnitApp
 * @author     Peter Kaiser (peter dot kaiser at kit dot edu)
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ExampleUnitApp.h"

//! [ObserversDocumentation UnitApplication2]
int main(int argc, char* argv[])
{
    armarx::ApplicationPtr app = armarx::Application::createInstance <armarx::ExampleUnitApp>();
    app->setName("ExampleUnit");

    return app->main(argc, argv);
}
//! [ObserversDocumentation UnitApplication2]
