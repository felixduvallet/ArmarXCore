/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author    Kai Welke (kai dot welke at kit dot edu)
 * @date       2012
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_CORE_ARMARXDUMMYMANAGER_H
#define _ARMARX_CORE_ARMARXDUMMYMANAGER_H

#include <ArmarXCore/core/ManagedIceObjectRegistryInterface.h>
#include <ArmarXCore/core/ManagedIceObject.h>

#include <string>
#include <map>

namespace armarx
{
    /**
     * ArmarXDummyManager smart pointer type
     */
    class ArmarXDummyManager;
    typedef IceUtil::Handle<ArmarXDummyManager> ArmarXDummyManagerPtr;

    /**
     * @class ArmarXDummyManager
     * @brief Handles help and documentation generation but does not provide Ice functionality.
     * @ingroup DistributedProcessingSub
     *
     * The ArmarXDummyManager implements the ManagedIceObjectRegistryInterface and thus provides all ManagedIceObject adding and removal
     * functionality. In contrast to the ArmarXManager, no Ice setup is performed and the objects are not started.
     *
     * This manager is used in order to parse the options of all ManagedIceObject added to the ArmarXManager for the help and
     * automatic documentation generation.
     */
    class ArmarXDummyManager
        : public ManagedIceObjectRegistryInterface
    {
        typedef std::map<std::string, ManagedIceObjectPtr> ObjectList;
    public:
        /**
         * Add a ManagedIceObject to the dummy manager. Takes care of the ManagedIceObject
         * lifcycle using and ArmarXObjectScheduler. addObject is
         * thread-safe and can be called from any method in order to dynamically
         * add ManagedIceObjects (as e.g. required for GUI).
         *
         * @param object      object to add
         */
        virtual void addObject(ManagedIceObjectPtr object, bool addWithOwnAdapter = true)
        {
            objects.insert(std::make_pair(object->getName(), object));
        }


        /**
         * Removes an object from the registry.
         *
         * This version waits until all calls to this component are finished and
         * the has been completely removed.
         *
         * @param object object to remove
         */
        virtual void removeObjectBlocking(ManagedIceObjectPtr object)
        {
            objects.erase(object->getName());
        }

        /**
         * Removes an object from the manageregistry.
         *
         * This version waits until all calls to this component are finished and
         * the has been completely removed.
         *
         * @param objectName name of the object to remove
         */
        virtual void removeObjectBlocking(const std::string& objectName)
        {
            objects.erase(objectName);
        }

        /**
         * Removes an object from the registry.
         *
         * This version returns immediatly and does <b>not</b> wait for all call to this
         * to finish.
         *
         * @param object object to remove
         */
        virtual void removeObjectNonBlocking(ManagedIceObjectPtr object)
        {
            objects.erase(object->getName());
        }

        /**
         * Removes an object from the registry.
         *
         * This version returns immediatly and does <b>not</b> wait for all call to this
         * to finish.
         *
         * @param objectName name of the object to remove
         */
        virtual void removeObjectNonBlocking(const std::string& objectName)
        {
            objects.erase(objectName);
        }

        /**
         * Retrieve pointers to all ManagedIceObject.
         *
         * @return vector of pointers
         */
        std::vector<ManagedIceObjectPtr> getManagedObjects()
        {
            std::vector<ManagedIceObjectPtr> result;

            ObjectList::iterator iter = objects.begin();

            while (iter != objects.end())
            {
                result.push_back(iter->second);
                iter++;
            }

            return result;
        }

    private:
        // list of added objects
        ObjectList objects;

    };
}

#endif
