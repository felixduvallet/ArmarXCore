/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef _ARMARX_DYNAMICLIBRARY_H
#define _ARMARX_DYNAMICLIBRARY_H


#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <ArmarXCore/core/exceptions/local/DynamicLibraryException.h>

#include <boost/filesystem.hpp>

#ifndef _WIN32
#include <dlfcn.h>
#endif

namespace armarx
{

    /**
     * @class DynamicLibrary
     * @ingroup core-utility
     * @brief The DynamicLibrary class provides a mechanism to load libraries at runtime.
     *
     * A quick usage example looks as follows:
     * @snippet ArmarXCore/core/test/DynamicLibraryTest.cpp DynamicLibrary DynamicLibraryUsage
     */
    class DynamicLibrary :
        public Logging
    {
    public:
        DynamicLibrary();
        virtual ~DynamicLibrary();
        /**
         * @brief Loads a shared library from the specified path. Throws
         * exceptions::local::DynamicLibraryException on error.
         *
         * @param libPath Path to the library. Can be relative to ArmarXDataPath's
         * paths.
         * @throw exceptions::local::DynamicLibraryException
         * @see ArmarXDataPath
         */
        void load(boost::filesystem::path libPath);

        /**
         * @brief Unloads library. User has to make sure that all references to
         * the symbols in this library are deleted!
         * @throw  exceptions::local::DynamicLibraryException
         */
        void unload();

        /**
         * @brief Reloads the current library.
         *
         * Useful, if the library has changed since last loading.
         * @throw  exceptions::local::DynamicLibraryException
         */
        void reload();

        /**
         * @brief Checks if a library is currently loaded.
         * @return true if a library is loaded.
         */
        bool isLibraryLoaded() throw();
#ifndef WIN32
        /**
         * @brief Sets the shared library opening flags and reloads the library,
         * if a library is already loaded.
         * @param newFlags flags of ::dlopen()
         * @throw  exceptions::local::DynamicLibraryException
         */
        void setFlags(int newFlags);
#endif

        /** @brief Retrieves a symbol (mostly a function) from the loaded library.
         * The requested function needs to be exported with "extern "C"".
         *  @tparam SymbolType type of the requested symbol
         *  @param functionName name of the requested function
         *  @throw  exceptions::local::DynamicLibraryException
         *  @return requested symbol
         */
        template <class SymbolType>
        SymbolType getSymbol(const std::string& functionName);

        boost::filesystem::path getLibraryFilename() throw();
        std::string getErrorMessage() throw();
    private:
#ifdef WIN32

#else
        void* handle;
        int flags;
#endif
        boost::filesystem::path libPath;
        std::string lastError;
    };
    typedef boost::shared_ptr<DynamicLibrary> DynamicLibraryPtr;


    // /////////////////////////////
    // / Template Implementation ///
    // /////////////////////////////



    template <class SymbolType>
    SymbolType DynamicLibrary::getSymbol(const std::string& functionName)
    {
        if (!handle)
        {
            throw exceptions::local::DynamicLibraryException("No library loaded - cannot fetch a symbol");
        }

#ifndef _WIN32
        dlerror(); // clear error message
        void* resultRaw = dlsym(handle, functionName.c_str());
#endif

        if (!resultRaw)
        {
#ifndef _WIN32
            const char* error = dlerror();
#endif

            if (error)
            {
                lastError = error;
                throw exceptions::local::DynamicLibraryException(lastError);
            }
        }

        SymbolType result = (SymbolType)(resultRaw);

        if (!result)
        {
            lastError = "Could not cast into desired function type";
            throw exceptions::local::DynamicLibraryException(lastError);
        }

        return result;

    }
}

#endif
