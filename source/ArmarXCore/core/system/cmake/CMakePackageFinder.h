/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef _ARMARX_CMakePackageFinder_H
#define _ARMARX_CMakePackageFinder_H

#include <string>
#include <vector>
#include <map>
#include <boost/filesystem/path.hpp>

namespace armarx
{

    /**
     * @class CMakePackageFinder
     * @ingroup core-utility
     * @brief The CMakePackageFinder class provides an interface to the CMake Package
     * finder capabilities.
     *
     * This class calls the CMake script FindPackageX.cmake to retrieve information
     * about a specific ArmarX package.
     * The available variables and their values are written to stdout and parsed by
     * CMakePackageFinder::_ParseString().
     * The values of the variables can be retieved via CMakePackageFinder::getVar() or
     * their specific convenience functions like getBuildDir().
     */
    class CMakePackageFinder
    {
    public:

        /**
         * @brief The package with name packageName is searched with cmake during construction of this
         * class. Query the values with the class functions.
         * @param packageName Name of the ArmarX package to find.
         * @param packagePath Optional path to the package. Use if a specific instance of the package should be used.
         */
        CMakePackageFinder(const std::string& packageName, const boost::filesystem::path& packagePath = "");

        /**
         * @brief Static function to find the include path with cmake. Does use cmake built in functions and
         * not the ArmarX cmake find script.
         * @param packageName
         * @return
         */
        static std::vector<std::string> FindPackageIncludePathList(const std::string& packageName);
        static std::string FindPackageIncludePaths(const std::string& packageName);
        static std::string FindPackageLibs(const std::string& packageName);

        static std::string ExecCommand(const std::string& command, int& result);

        /**
         * @brief Returns the name of the given package. Same name as given in constructor.
         */
        std::string getName() const;

        /**
         * @brief Returns whether or not this package was found with cmake.
         */
        bool packageFound() const;
        const std::map<std::string, std::string>& getVars() const;

        /**
         * @brief Returns the content of a CMake variable. Usually there exists a dedicated function for all variables e.g. getIncludePaths()
         * @param varName Name of the CMake Variable
         * @return Content of the variable.
         */
        std::string getVar(const std::string& varName) const;

        /**
         * @brief Returns the path needed to find this package with cmake, i.e. the dir where ${PACKAGENAME}Config.cmake resides.
         */
        std::string getConfigDir() const
        {
            return getVar("PACKAGE_CONFIG_DIR");
        }

        /**
         * @brief Returns the include paths separated by semi-colons.
         */
        std::string getIncludePaths() const
        {
            return getVar("INCLUDE_DIRS");
        }

        /**
         * @brief Return the include paths in a vector. Same as getIncludePaths() but already splitted.
         */
        std::vector<std::string> getIncludePathList() const;

        /**
         * @brief Returns the library paths seperated by semi-colons.
         */
        std::string getLibraryPaths() const
        {
            return getVar("LIBRARY_DIRS");
        }
        std::string getLibs() const
        {
            return getVar("LIBRARIES");
        }
        std::string getBuildDir() const
        {
            return getVar("BUILD_DIR");
        }
        /**
         * @brief Returns the top level path of the package. Does not work for installed packages, so use with caution.
         */
        std::string getPackageDir() const
        {
            return getVar("PROJECT_BASE_DIR");
        }
        std::string getCMakeDir() const
        {
            return getVar("CMAKE_DIR");
        }
        std::string getBinaryDir() const
        {
            return getVar("BINARY_DIR");
        }
        std::string getExecutables() const
        {
            return getVar("EXECUTABLE");
        }
        std::string getInterfacePaths() const
        {
            return getVar("INTERFACE_DIRS");
        }
        std::string getDataDir() const
        {
            return getVar("DATA_DIR");
        }
        std::string getScenariosDir() const
        {
            return getVar("SCENARIOS_DIR");
        }
        std::vector<std::string> getDependencies() const;
        std::map<std::string, std::string> getDependencyPaths() const;
    protected:
        std::vector<std::string> extractVariables(const std::string& cmakeVarString);
        static bool _ParseString(const std::string& input, std::string& varName, std::string& content);
        static void _CreateSharedMutex();

        bool found;
        std::map<std::string, std::string> vars;
        std::string packageName;
    };

}

#endif
