/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "CPULoadWatcher.h"

#include <boost/algorithm/string/trim.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/tokenizer.hpp>
#include <boost/foreach.hpp>
#include <boost/typeof/typeof.hpp>
#include <fstream>

#ifndef _WIN32
#include <sys/utsname.h>
#include <unistd.h>
#include <time.h>
#endif




using namespace armarx;

CPULoadWatcher::CPULoadWatcher(Profiler::ProfilerPtr profiler) :
    profiler(profiler)
{
    long pid = LogSender::getProcessId();
    Hertz = sysconf(_SC_CLK_TCK);
    std::stringstream ss;
    ss << "/proc/" << pid << "/stat";
    processCpuDataFilename = ss.str();
    processCpuUsage.processId = pid;
    cpuUsageFromProcFile();
}

void CPULoadWatcher::start()
{
    if (!watcherTask)
    {
        watcherTask = new PeriodicTask<CPULoadWatcher>(this, &CPULoadWatcher::watch, 300, false);
    }

    watcherTask->start();
}

void CPULoadWatcher::stop()
{
    watcherTask->stop();
    watcherTask = NULL;
}

void CPULoadWatcher::addThread(int processId, int threadId)
{
    ScopedLock lock(threadWatchListMutex);
    ThreadUsage usage;
    usage.processId = processId;
    usage.threadId = threadId;
    usage.lastJiffies = GetThreadJiffies(processId, threadId);
    usage.lastUpdate = IceUtil::Time::now();
    usage.load = 0;
    threadWatchList.insert(usage);
}

void CPULoadWatcher::addThread(int threadId)
{
    addThread(LogSender::getProcessId(), threadId);
}

void CPULoadWatcher::watch()
{
    runThreadWatchList();
    cpuUsageFromProcFile();
    reportCpuUsage();
}

void CPULoadWatcher::runThreadWatchList()
{
    ScopedLock lock(threadWatchListMutex);
    boost::unordered_set<ThreadUsage> newset;
    boost::unordered_set<ThreadUsage>::iterator it =  threadWatchList.begin();

    for (; it != threadWatchList.end(); it++)
    {
        ThreadUsage usage = *it;
        IceUtil::Time queryTime = IceUtil::Time::now();
        int curJiffies = GetThreadJiffies(usage.processId, usage.threadId);

        if (GetHertz() != -1)
        {
            usage.load = (curJiffies - usage.lastJiffies) / ((queryTime - usage.lastUpdate).toSecondsDouble() * GetHertz());
        }
        else
        {
            usage.load = 0;
        }

        usage.lastJiffies = curJiffies;
        usage.lastUpdate = queryTime;
        newset.insert(usage);
    }

    threadWatchList.swap(newset);
}


#if !defined(_WIN32) && !defined(__APPLE__)
int CPULoadWatcher::GetHertz()
{
    static Mutex mutex;
    ScopedLock lock(mutex);
    static int value = -2;

    if (value == -2) // init value
    {
        static std::string _release;

        if (_release.empty())
        {
            struct utsname utsinfo;
            uname(&utsinfo);
            _release = utsinfo.release;
        }

        std::string fileName = "/boot/config-" + _release;
        std::ifstream configFile(fileName.c_str());

        //    procFile.open(fileName, ios::out);
        if (!configFile.is_open())
        {
            ARMARX_WARNING_S << "Cannot open "  << fileName << " for reading the cpu hertz value";
            value = -1;
        }
        else
        {
            value = -1;
            std::string line;
            const std::string searchString = "CONFIG_HZ=";

            while (getline(configFile, line))
            {

                std::size_t pos = line.find(searchString);

                if (pos != std::string::npos)
                {
                    std::string hzString = line.substr(pos + searchString.length());
                    value = atoi(hzString.c_str());
                    break;
                }
            }
        }
    }

    return value;


}

void CPULoadWatcher::cpuUsageFromProcFile()
{
    std::string line;

    std::ifstream file(processCpuDataFilename);

    if (!file.is_open())
    {
        ARMARX_WARNING_S << "File " << processCpuDataFilename << " does not exist";
    }


    std::getline(file, line);
    file.close();

    std::vector<std::string> pidElements;
    boost::split(pidElements, line, ::isspace);

    int currentUtime = boost::lexical_cast<int>(pidElements.at(eUTime));
    int currentStime = boost::lexical_cast<int>(pidElements.at(eSTime));

    int currentCUtime = boost::lexical_cast<int>(pidElements.at(eCUTIME));
    int currentCStime = boost::lexical_cast<int>(pidElements.at(eCSTIME));
    {
        ScopedLock(processCpuUsageMutex);

        IceUtil::Time queryTime = IceUtil::Time::now();

        processCpuUsage.proc_total_time = 100 * (double)((currentStime + currentCStime - processCpuUsage.lastStime - processCpuUsage.lastCStime) + (currentUtime + currentCUtime - processCpuUsage.lastCUtime - processCpuUsage.lastUtime)) / ((queryTime - processCpuUsage.lastUpdate).toSecondsDouble() * Hertz);

        processCpuUsage.lastUpdate = queryTime;
        processCpuUsage.lastUtime = currentUtime;
        processCpuUsage.lastStime = currentStime;

        processCpuUsage.lastCUtime = currentCUtime;
        processCpuUsage.lastCStime = currentCStime;
    }
}

void CPULoadWatcher::reportCpuUsage()
{
    ScopedLock(processCpuUsageMutex);
    profiler->logProcessCpuUsage(processCpuUsage.proc_total_time);
}

int CPULoadWatcher::GetThreadJiffies(int processId, int threadId)
{
    std::map<int, int> result = GetThreadListJiffies(processId, std::vector<int>(1, threadId));
    std::map<int, int>::iterator it = result.find(threadId);

    if (it == result.end())
    {
        return 0;
    }

    return it->second;
}

std::map<int, int> CPULoadWatcher::GetThreadListJiffies(int processId, std::vector<int> threadIds)
{
    std::map<int, int> resultMap;
    BOOST_FOREACH(int threadId, threadIds)
    {
        resultMap[threadId] = 0;
        std::stringstream fileName;
        fileName << "/proc/" << threadId << "/task/" <<  threadId << "/stat";
        std::ifstream statFile(fileName.str().c_str());

        if (!statFile.is_open())
        {
            ARMARX_WARNING_S << "Cannot open "  << fileName.str() << " for reading the hertz value";
            resultMap[threadId] = 0;
            continue;
        }

        std::string line;

        while (getline(statFile, line))
        {
            boost::algorithm::trim(line);
            using namespace boost;

            char_separator<char> sep(" ");
            tokenizer< char_separator<char> > tokens(line, sep);
            std::vector<std::string> stringVec;
            stringVec.assign(tokens.begin(), tokens.end());
            int userTimeJiffies = atoi(stringVec.at(13).c_str());
            int kernelTimeJiffies = atoi(stringVec.at(14).c_str());

            resultMap[threadId] = userTimeJiffies + kernelTimeJiffies;
        }
    }

    return resultMap;
}
#else
int CPULoadWatcher::GetHertz()
{
    return -1;
}

int CPULoadWatcher::GetThreadJiffies(int processId, int threadId)
{
    return 0;
}

std::map<int, int> CPULoadWatcher::GetThreadListJiffies(int processId, std::vector<int> threadIds)
{
    return std::map<int, int>();
}

#endif

bool ThreadUsage::operator <(const ThreadUsage& rhs) const
{
    if (processId < rhs.processId)
    {
        return true;
    }

    if (threadId < rhs.threadId)
    {
        return true;
    }

    return false;
}

bool ThreadUsage::operator ==(const ThreadUsage& rhs) const
{
    if (processId == rhs.processId && threadId == rhs.threadId)
    {
        return true;
    }

    return false;
}


double CPULoadWatcher::getThreadLoad(int threadId)
{
    return getThreadLoad(LogSender::getProcessId(), threadId);
}


double CPULoadWatcher::getThreadLoad(int processId, int threadId)
{
    ThreadUsage query;
    query.processId = processId;
    query.threadId = threadId;
    ScopedLock lock(threadWatchListMutex);
    BOOST_AUTO(it, threadWatchList.find(query));

    if (it != threadWatchList.end())
    {
        return it->load * 100;
    }
    else
    {
        return 0.0;
    }
}

armarx::CpuUsage CPULoadWatcher::getProcessCpuUsage()
{
    ScopedLock(processCpuUsageMutex);
    return processCpuUsage;
}


void CPULoadWatcher::removeThread(int processId, int threadId)
{
    ThreadUsage query;
    query.processId = processId;
    query.threadId = threadId;
    ScopedLock lock(threadWatchListMutex);
    threadWatchList.erase(query);
}

void CPULoadWatcher::removeThread(int threadId)
{
    removeThread(LogSender::getProcessId(), threadId);
}
