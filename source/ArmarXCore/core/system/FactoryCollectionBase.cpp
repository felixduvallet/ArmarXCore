/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package
* @author     Mirko Waechter( waechter at kit dot edu)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "FactoryCollectionBase.h"
using namespace armarx;

FactoryCollectionBase::FactoryCollectionBase()
{
}

std::vector<FactoryCollectionBasePtr>& FactoryCollectionBase::PreregistrationList()
{

    //    ScopedLock lock(registrationListMutex());
    static std::vector<FactoryCollectionBasePtr> list;
    return list;
}

Mutex& FactoryCollectionBase::RegistrationListMutex()
{
    static Mutex mutex;
    return mutex;
}



FactoryCollectionBaseCleanUp::FactoryCollectionBaseCleanUp(FactoryCollectionBasePtr factoryToRemoveOnDesctruction)
{
    factoryCollection = factoryToRemoveOnDesctruction;
}

FactoryCollectionBaseCleanUp::~FactoryCollectionBaseCleanUp()
{
    ScopedLock lock(FactoryCollectionBase::RegistrationListMutex());

    //    std::cout << "FactoryCollectionBaseCleanUp" << std::endl;
    for (unsigned int i = 0; i < FactoryCollectionBase::PreregistrationList().size(); i++)
    {
        if (FactoryCollectionBase::PreregistrationList().at(i).get() == factoryCollection.get())
        {
            //            std::cout << "erasing: " <<factoryCollection->getFactories().begin()->first << " and co." << std::endl;
            FactoryCollectionBase::PreregistrationList().erase(FactoryCollectionBase::PreregistrationList().begin() + i);
        }
    }
}
