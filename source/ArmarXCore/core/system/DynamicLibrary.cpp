/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "DynamicLibrary.h"

using namespace armarx;

DynamicLibrary::DynamicLibrary() :
    handle(0)
{
#ifndef _WIN32
    flags = RTLD_NOW | RTLD_GLOBAL;
#endif
}

DynamicLibrary::~DynamicLibrary()
{
    try
    {
        unload();
    }
    catch (exceptions::local::DynamicLibraryException& e)
    {
        ARMARX_ERROR << "Error while unloading dynamic library: " << lastError;
    }
}

void DynamicLibrary::load(boost::filesystem::path libPath)
{
    if (handle)
    {
        throw exceptions::local::DynamicLibraryException("A library is already loaded. Call unload first and make sure that all references to this library are deleted.");
    }

    // if relative and not found, check at ArmarX locations
    if (libPath.is_relative() && !boost::filesystem::exists(libPath))
    {
        std::string absolutePath;

        if (ArmarXDataPath::getAbsolutePath(libPath.string(), absolutePath))
        {
            libPath = absolutePath;
        }
    }

    if (!boost::filesystem::exists(libPath))
    {
        throw exceptions::local::DynamicLibraryException("Lib-file not found: " + libPath.string());
    }

#ifndef _WIN32
    handle = dlopen(libPath.c_str(), flags);
#endif


    if (!handle)
    {
#ifndef _WIN32
        const char* error = dlerror();
#endif

        if (error)
        {
            lastError = error;
            throw exceptions::local::DynamicLibraryException(lastError);
        }
        else
        {
            throw exceptions::local::DynamicLibraryException("Unknown error");
        }
    }

    this->libPath = libPath;

}


void DynamicLibrary::unload()
{
    if (!handle)
    {
        return;
    }

    bool result = dlclose(handle) == 0;

    if (!result)
    {
        const char* error = dlerror();

        if (error)
        {
            lastError = error;
            throw exceptions::local::DynamicLibraryException(lastError);
        }
        else
        {
            throw exceptions::local::DynamicLibraryException("Unknown error");
        }
    }

    handle = 0;
}

void DynamicLibrary::reload()
{
    unload();
    load(libPath);
}

bool DynamicLibrary::isLibraryLoaded() throw()
{
    return handle != 0;
}

void DynamicLibrary::setFlags(int newFlags)
{
    flags = newFlags;

    if (isLibraryLoaded())
    {
        reload();
    }
}

boost::filesystem::path DynamicLibrary::getLibraryFilename() throw()
{
    return libPath;
}


std::string DynamicLibrary::getErrorMessage() throw()
{
    return lastError;
}
