/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core
* @author     Nikolaus Vahrenkamp
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "ArmarXDataPath.h"
#include <cstdio>
#include <cstdlib>
#include <sstream>
#include <stdio.h>

#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>

#include <ArmarXCore/core/logging/Logging.h>
#include <boost/regex.hpp>
#include <ArmarXCore/core/util/StringHelpers.h>
using namespace armarx;


std::vector<std::string> armarx::ArmarXDataPath::dataPaths;
bool armarx::ArmarXDataPath::initialized = false;

ArmarXDataPath::ArmarXDataPath()
{
}

bool ArmarXDataPath::getAbsolutePath(const std::string& relativeFilename, std::string& storeAbsoluteFilename, const std::vector<std::string>& additionalSearchPaths, bool verbose)
{
    init();

    boost::filesystem::path fn(relativeFilename);

    // first: check current path
    if (boost::filesystem::exists(fn))
    {
        storeAbsoluteFilename = fn.string();
        return true;
    }

    std::vector<std::string> paths(additionalSearchPaths.begin(), additionalSearchPaths.end());
    paths.insert(paths.begin(), dataPaths.rbegin(), dataPaths.rend());

    for (auto currentPath : paths)
    {
        boost::filesystem::path p(currentPath);

        boost::filesystem::path fnComplete = boost::filesystem::operator/(p, fn);

        if (boost::filesystem::exists(fnComplete))
        {
            storeAbsoluteFilename = fnComplete.string();
            return true;
        }
    }

    if (verbose)
    {
        ARMARX_WARNING_S << "Could not find file '" << relativeFilename << "' in the following paths: " << paths;
    }

    return false;
}


std::string ArmarXDataPath::cleanPath(const std::string& filepathStr)
{
    boost::filesystem::path p(filepathStr);
    boost::filesystem::path result;

    for (boost::filesystem::path::iterator it = p.begin();
         it != p.end();
         ++it)
    {
        if (*it == ".." && it != p.begin())
        {
            // /a/b/.. is not necessarily /a if b is a symbolic link
            bool isSymLink = false;

            try
            {
                isSymLink = boost::filesystem::is_symlink(result);
            }
            catch (boost::filesystem::filesystem_error& e) {}

            if (isSymLink)
            {
                result /= *it;
            }
            // /a/b/../.. is not /a/b/.. under most circumstances
            // We can end up with ..s in our result because of symbolic links
            else if (result.filename() == "..")
            {
                result /= *it;
            }
            // Otherwise it should be safe to resolve the parent
            else
            {
                result = result.parent_path();
            }
        }
        else if (*it == ".")
        {
            // Ignore
        }
        else
        {
            // Just concatenate other path entries
            result /= *it;
        }
    }

    if (*filepathStr.rbegin() == '/')
    {
        return result.string() + "/";
    }
    else if (*filepathStr.rbegin() == '\\')
    {
        return result.string() + "\\";
    }

    return result.string();
}

std::string ArmarXDataPath::getRelativeArmarXPath(const std::string& absolutePathString)
{
    boost::filesystem::path absolutePath(absolutePathString);
    boost::filesystem::path absolutePathPart(absolutePathString);
    boost::filesystem::path relativePath;

    boost::filesystem::path::iterator it = --absolutePath.end();

    while (it != absolutePath.begin())
    {
        for (size_t i = 0; i < dataPaths.size(); i ++)
        {
            boost::filesystem::path p(dataPaths[i]);

            if (boost::filesystem::equivalent(p, absolutePathPart))
            {
                return relativePath.string();
            }
        }

        relativePath = *it / relativePath;
        absolutePathPart = absolutePathPart.parent_path();
        it--;

    }

    throw LocalException("Could not make path relative to any Armarx data path");
    return absolutePathString;
}

void ArmarXDataPath::initDataPaths(const std::string& dataPathList)
{
    addDataPaths(dataPathList);

    // check for standard armarx data path
    if (getHomePath() != "")
    {
        boost::filesystem::path homePath(getHomePath());
        homePath /= "data";
        boost::filesystem::path fn(homePath);

        if (boost::filesystem::exists(fn))
        {
            __addPath(homePath.string());
        }
    }
}

bool ArmarXDataPath::ReplaceEnvVars(std::string& string)
{
    const boost::regex e("\\$\\{([a-zA-Z0-9_]+)\\}");
    boost::match_results<std::string::const_iterator> what;
    boost::regex_search(string, what, e);
    bool found = false;

    for (size_t i = 1; i < what.size(); i += 2)
    {
        std::string var =  what[i];

        //        ARMARX_INFO_S << VAROUT(var);
        if (getenv(var.c_str()))
        {
            string = boost::regex_replace(string, e, std::string(getenv(var.c_str())));
            found = true;
        }
    }

    return found;
}

void ArmarXDataPath::ReplaceVar(std::string& string, const std::string varName, const std::string& varValue)
{
    boost::replace_all(string, std::string("${") + varName + "}", varValue);
}


void ArmarXDataPath::addDataPaths(const std::string& dataPathList)
{
    init();
    __addPaths(dataPathList);
}

std::string ArmarXDataPath::getHomePath()
{
    char* home_path = getenv("ArmarXHome_DIR");

    if (!home_path)
    {
        return std::string();
    }

    return cleanPath(std::string(home_path));
}

void ArmarXDataPath::init()
{
    if (initialized)
    {
        return;
    }

    std::string homePath = getHomePath();

    if (!homePath.empty())
    {
        __addPaths(homePath);
    }

    char* data_path = getenv("ArmarXData_DIRS");

    if (data_path)
    {
        std::string pathStr(data_path);
        __addPaths(pathStr);
    }

    //check the documented variable
    char* data_path_dir = getenv("ArmarXData_DIR");

    if (data_path_dir)
    {
        std::string pathStr(data_path_dir);
        __addPaths(pathStr);
    }

    initialized = true;
}

std::vector<std::string> ArmarXDataPath::getDataPaths()
{
    return dataPaths;
}

bool ArmarXDataPath::__addPaths(const std::string& pathList)
{
    if (pathList == "")
    {
        return false;
    }

    std::vector<std::string> separatedPaths = __separatePaths(pathList);

    if (separatedPaths.size() == 0)
    {
        return false;
    }

    bool ok = true;

    for (int i = separatedPaths.size() - 1; i >= 0 ; i--)
    {
        ArmarXDataPath::ReplaceEnvVars(separatedPaths[i]);
        ok = ok & __addPath(separatedPaths[i]);
    }

    return ok;
}

std::vector<std::string> ArmarXDataPath::__separatePaths(const std::string& pathList)
{
    std::string delimiters = ";";
    std::vector<std::string> tokens;
    boost::split(tokens, pathList, boost::is_any_of(delimiters));
    return tokens;
}


bool ArmarXDataPath::__addPath(const std::string& path)
{
    if (path.empty())
    {
        return false;
    }

    boost::filesystem::path p(path);

    if (!boost::filesystem::is_directory(p) && !boost::filesystem::is_symlink(p))
    {
        ARMARX_WARNING_S << "Not a valid data path:" << path << std::endl;
        return false;
    }
    else
    {
        ARMARX_DEBUG_S << "Adding data path:" << path << std::endl;
        dataPaths.push_back(path);
    }

    return true;
}

