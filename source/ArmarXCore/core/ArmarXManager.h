/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author    Kai Welke (kai dot welke at kit dot edu)
 * @date       2012
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_CORE_ARMARXOBJECTMANAGER_H
#define _ARMARX_CORE_ARMARXOBJECTMANAGER_H

// superclasses
#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/ManagedIceObjectRegistryInterface.h>
#include <ArmarXCore/interface/core/ArmarXManagerInterface.h>
#include <ArmarXCore/core/ArmarXObjectObserver.h>
#include <ArmarXCore/core/util/StringHelpers.h>

// stl
#include <string>
#include <map>
#include <vector>

namespace armarx
{
    // forward declaration
    class ArmarXObjectScheduler;
    typedef IceUtil::Handle<ArmarXObjectScheduler> ArmarXObjectSchedulerPtr;

    class IceManager;
    typedef IceUtil::Handle<IceManager> IceManagerPtr;

    template <class T>
    class PeriodicTask;

    /**
     * IceObjectManager smart pointer type
     */
    class ArmarXManager;
    typedef IceUtil::Handle<ArmarXManager> ArmarXManagerPtr;

    /**
     * @class ArmarXManager
     * @brief Main class of an ArmarX process.
     * @ingroup DistributedProcessingGrp
     *
     * The ArmarXManager is the main class for writing distributed
     * applications in the ArmarX framework.
     * The ArmarXManager takes care of handling ManagedIceObjects.
     * ManagedIceObjects are the core elements of ArmarX distributed applications.
     *
     * Usually, each process creates one ArmarXManager and adds the
     * required ManagedIceObjects to this manager.
     *
     * The ArmarXManager provides and Ice interface which allows
     * retrieving the state and network connectivity of all
     * ManagedIceObject within the Manager.
     * To provide this functionality it holds one instance of Ice::Communicator
     *
     * Finally it also does shutdown handling
     */
    class ArmarXManager:
        virtual public Logging,
        virtual public ArmarXManagerInterface,
    public ManagedIceObjectRegistryInterface
    {
        friend class PeriodicTask<ArmarXManager>;
        friend class ArmarXObjectObserver;

        enum ArmarXManagerState
        {
            eCreated,
            eRunning,
            eShutdownInProgress,
            eShutdown
        };

    public:
        /**
         * ArmarXManager constructor. With this constructor, the
         * ArmarXManager builds its own communicator using the specified port, host and locatorName
         * (default: --Ice.Default.Locator= IceGrid/Locator:tcp -p 4061 -h localhost)
         *
         * @param applicationName name of the application used for registering the manager to Ice and for logging
         * @param port the port of the Ice locator service
         * @param host the host of the Ice locator service
         * @param locatorName the name of the Ice locator service
         * @param args list of arguments for Ice (e.g. "--Ice.ThreadPool.Server.SizeMax=10")
         */
        ArmarXManager(std::string applicationName, int port = 4061, std::string host = "localhost", std::string locatorName = "IceGrid/Locator", Ice::StringSeq args = Ice::StringSeq());

        /**
         * Constructs an ArmarXManager by passing a communicator. This can be used if communicator has already
         * been constructed e.g in Ice::Application.
         *
         * @param applicationName name of the application used for registering the manager to Ice and for logging
         * @param communicator configured Ice::Communicator
         */
        ArmarXManager(std::string applicationName, Ice::CommunicatorPtr communicator);

        virtual ~ArmarXManager();

        /**
         * Enable or disable logging
         *
         * @param enable whether to enable logging
         */
        void enableLogging(bool enable);

        /**
          * Enable or disable profiling of CPU Usage
          */
        void enableProfiling(bool enable);

        /**
         * Set minimum logging level to output in log stream.
         *
         * @param applicationName name of the application used for registering the manager to Ice and for logging
         * @param communicator configured Ice::Communicator
         */
        void setGlobalMinimumLoggingLevel(MessageType minimumLoggingLevel);

        /**
         * Set data paths used to search for datafiles
         *
         * @param dataPaths semicolon seperated list of paths
         */
        void setDataPaths(std::string dataPaths);

        /**
         * Add a ManagedIceObject to the manager. Takes care of the IceManagedObject
         * lifcycle using and ArmarXObjectScheduler. addObject is
         * thread-safe and can be called from any method in order to dynamically
         * add ManagedIceObjects (as e.g. required for GUI).
         *
         * @param object      object to add
         */
        virtual void addObject(ManagedIceObjectPtr object, bool addWithOwnAdapter = true);
        virtual void addObject(ManagedIceObjectPtr object, Ice::ObjectAdapterPtr objectAdapterToAddTo);

        /**
         * Removes an object from the manager.
         *
         * This version waits until all calls to this component are finished and
         * the has been completely removed.
         *
         * @param object object to remove
         */
        virtual void removeObjectBlocking(ManagedIceObjectPtr object);

        /**
         * Removes an object from the manager.
         *
         * This version waits until all calls to this component are finished and
         * the has been completely removed.
         *
         * @param objectName name of the object to remove
         */
        virtual void removeObjectBlocking(const std::string& objectName);

        /**
         * Removes an object from the manager.
         *
         * This version returns immediatly and does <b>not</b> wait for all call to this
         * to finish.
         *
         * @param object object to remove
         */
        virtual void removeObjectNonBlocking(ManagedIceObjectPtr object);

        /**
         * Removes an object from the manager.
         *
         * This version returns immediatly and does <b>not</b> wait for all call to this
         * to finish.
         *
         * @param objectName name of the object to remove
         */
        virtual void removeObjectNonBlocking(const std::string& objectName);

        /**
         * Wait for shutdown. Shutdown is initialized once shutdown() is called on ArmarXManager e.g. from a signal handler.
         */
        void waitForShutdown();

        /**
         * Shuts down the ArmarXManager. If a thread is waiting in waitForShutdown, the thread is activated after shutdown has been completed.
         */
        void shutdown();

        /**
         * @brief Calls shutdown() after a timeout.
         * @param timeoutMs The timeout to wait. (unit ms)
         */
        void asyncShutdown(std::size_t timeoutMs = 0);

        /**
         * Whether ArmarXManager shutdown has been finished.
         *
         * @return shutdown finished
         */
        bool isShutdown();

        /**
         * Retrieve pointers to all ManagedIceObject.
         *
         * @return vector of pointers
         */
        std::vector<ManagedIceObjectPtr> getManagedObjects();

        /**
         * Retrieve state of a ManagedIceObject. Part of the Ice interface.
         *
         * @param objectName name of the object
         * @param c Ice context
         *
         * @return state of the ManagedIceObject
         */
        ManagedIceObjectState getObjectState(const std::string& objectName, const Ice::Current& c = ::Ice::Current());

        /**
         * Retrieve connectivity of a ManagedIceObject. Part of the Ice interface.
         *
         * @param objectName name of the object
         * @param c Ice context
         *
         * @return connectivity of the ManagedIceObject
         */
        ManagedIceObjectConnectivity getObjectConnectivity(const std::string& objectName, const Ice::Current& c = ::Ice::Current());

        /**
         * @brief getObjectProperties is used to retrieve the properties of an object
         * @return
         */
        virtual ::armarx::StringStringDictionary getObjectProperties(const ::std::string& objectName, const ::Ice::Current& = ::Ice::Current());

        /**
         * Retrieve the names of all ManagedIceObject. Part of the Ice interface.
         *
         * @param c Ice context
         *
         * @return vector of names
         */
        StringSequence getManagedObjectNames(const Ice::Current& c = ::Ice::Current());

        /**
         * @brief Retrieve the instance of the icemanager
         * @return Pointer to the iceManager
         * @see ManagedIceObject::getIceManager()
         */
        IceManagerPtr getIceManager() const;


        /**
         * @brief Registers all object factories that are known with Ice.
         *
         * Object factories are automatically preregistered when their library
         * is linked. Function is idempotent.
         * This function is called in init() of ArmarXManager and needs only be called, if
         * no ArmarXManager is used or libraries are loaded at runtime.
         * @see FactoryCollectionBase
         */
        static void RegisterKnownObjectFactoriesWithIce(Ice::CommunicatorPtr ic);

        /**
         * @brief non static convenience version of ArmarXManager::RegisterKnownObjectFactoriesWithIce()
         * @see RegisterKnownObjectFactoriesWithIce()
         */
        void registerKnownObjectFactoriesWithIce();

        Ice::ObjectAdapterPtr getAdapter() const
        {
            return armarxManagerAdapter;
        }

    private:
        // typedefs
        typedef std::map<std::string, ArmarXObjectSchedulerPtr> ObjectSchedulerMap;
        typedef std::vector<ArmarXObjectSchedulerPtr> ObjectSchedulerList;

        // internal init used by both constructors
        void init(std::string applicationName, Ice::CommunicatorPtr communicator);

        // thread method to cleanup terminated schedulers
        void cleanupSchedulers();

        /**
         * @brief Disconnects all objects that dependent on the given object.
         * @param object name of the object of which all dependees should be removed
         */
        void disconnectDependees(const std::string& object);
        void disconnectAllObjects();
        std::vector<std::string> getDependendees(const std::string& removedObject);

        /**
         * @brief Schedulers which are waiting for dependencies are checking the
         * dependencies in a fix interval. Call this function to wake them up and
         * perform the check immediatly.
         */
        void wakeupWaitingSchedulers();

        // remove all managed objects
        void removeAllObjects(bool blocking);

        // remove specific managed objects
        bool removeObject(const ObjectSchedulerMap::iterator& iter, bool blocking);
        bool removeObject(const ArmarXObjectSchedulerPtr& objectScheduler, bool blocking);

        void checkDependencies();

        // synchronization
        bool acquireManagedObjectsMutex();
        void releaseManagedObjectsMutex();

        // name of the application
        std::string applicationName;

        // state of the manager
        ArmarXManagerState managerState;
        boost::mutex managerStateMutex;

        // ice manager
        IceManagerPtr iceManager;

        ArmarXObjectObserverPtr objObserver;

        // object scheduler registry
        ObjectSchedulerMap managedObjects;
        ObjectSchedulerList terminatingObjects;
        boost::mutex managedObjectsMutex;
        boost::mutex terminatingObjectsMutex;

        // shutdown handling
        boost::mutex shutdownMutex;
        boost::condition_variable shutdownCondition;

        // cleanup task
        IceUtil::Handle<PeriodicTask<ArmarXManager> > cleanupSchedulersTask;

        IceUtil::Handle<PeriodicTask<ArmarXManager> > checkDependencyStatusTask;
        Ice::ObjectAdapterPtr armarxManagerAdapter;
    };
}

#endif
