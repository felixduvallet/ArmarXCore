#ifndef RAPIDXMLWRITER_H
#define RAPIDXMLWRITER_H

#include <ArmarXCore/core/rapidxml/rapidxml.hpp>
#include <ArmarXCore/core/rapidxml/rapidxml_print.hpp>
#include <string>
#include <fstream>

namespace armarx
{

    class RapidXmlWriterNode
    {
        friend class RapidXmlWriter;

    private:
        rapidxml::xml_document<>* document;
        rapidxml::xml_node<>* node;

        RapidXmlWriterNode(rapidxml::xml_document<>* document, rapidxml::node_type node_type)
        {
            this->document = document;
            node = document->allocate_node(node_type);
        }
        RapidXmlWriterNode(rapidxml::xml_document<>* document, rapidxml::node_type node_type, const std::string& name)
        {
            this->document = document;
            node = document->allocate_node(node_type, cloneString(name));
        }

    public:
        void append_attribute(const std::string& name, const std::string& value)
        {
            node->append_attribute(document->allocate_attribute(cloneString(name), cloneString(value)));
        }
        void append_bool_attribute(const std::string& name, const std::string& trueValue, const std::string& falseValue, bool value)
        {
            append_attribute(name, value ? trueValue : falseValue);
        }
        void append_optional_bool_attribute(const std::string& name, const std::string& trueValue, const std::string& falseValue, bool value, bool defaultValue)
        {
            if (value != defaultValue)
            {
                append_attribute(name, value ? trueValue : falseValue);
            }
        }

        RapidXmlWriterNode append_node(const std::string& name)
        {
            RapidXmlWriterNode node(document, rapidxml::node_element, name);
            this->node->append_node(node.node);
            return node;
        }
        void append_data_node(const std::string& value)
        {
            this->node->append_node(document->allocate_node(rapidxml::node_data, 0, cloneString(value)));
        }

        void append_string_node(const std::string& name, const std::string& value)
        {
            this->node->append_node(document->allocate_node(rapidxml::node_element, cloneString(name), cloneString(value)));
        }

    private:
        const char* cloneString(const std::string& str)
        {
            return document->allocate_string(str.c_str());
        }

    };

    class RapidXmlWriter
    {
    private:
        rapidxml::xml_document<> document;

    public:
        RapidXmlWriter()
        {
            RapidXmlWriterNode declaration(&document, rapidxml::node_declaration);
            declaration.append_attribute("version", "1.0");
            declaration.append_attribute("encoding", "utf-8");
            document.append_node(declaration.node);
        }

        RapidXmlWriterNode createRootNode(const std::string& name)
        {
            RapidXmlWriterNode rootNode(&document, rapidxml::node_element, name);
            document.append_node(rootNode.node);
            return rootNode;
        }

        std::string print(bool indent)
        {
            std::string s;
            rapidxml::print(std::back_inserter(s), document, indent ? 0 : rapidxml::print_no_indenting);
            return s;
        }
        void saveToFile(const std::string& path, bool indent)
        {
            std::ofstream file;
            file.open(path.c_str());
            file << print(indent);
            file.close();
        }


    };
}

#endif // RAPIDXMLWRITER_H
