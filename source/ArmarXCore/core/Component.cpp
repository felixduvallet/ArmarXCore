/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core
* @author     Kai Welke (welke at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "Component.h"
#include "application/properties/PropertyDefinitionContainer.h"

using namespace armarx;

std::string Component::getConfigDomain()
{
    return configDomain;
}

std::string Component::getConfigName()
{
    return configName;
}

std::string Component::getConfigIdentifier()
{
    return configDomain + "." + configName;
}

void Component::initializeProperties(const std::string& configName, Ice::PropertiesPtr properties, const std::string& configDomain)
{
    this->configDomain = configDomain;
    this->configName = configName;

    // set default object name to configName
    setName(this->configName);

    // set properties object
    setIceProperties(properties);

    // set the logging behavior for this component
    if (getProperty<MessageType>("MinimumLoggingLevel").isSet())
    {
        setLocalMinimumLoggingLevel(
            getProperty<MessageType>("MinimumLoggingLevel").getValue());
    }

    // get well-known name of this component instance
    // (default is the ManagedIceObject name itself)
    if (getProperty<std::string>("ObjectName").isSet())
    {
        setName(getProperty<std::string>("ObjectName").getValue());
    }

    // set logging tag, may be overwritten by implementer
    setTag(getName());

    // enable Profiling
    enableProfiler(getProperty<bool>("EnableProfiling").getValue());
}

PropertyDefinitionsPtr Component::createPropertyDefinitions()
{
    return PropertyDefinitionsPtr(new ComponentPropertyDefinitions(getConfigDomain() + "." + getConfigName()));
}
