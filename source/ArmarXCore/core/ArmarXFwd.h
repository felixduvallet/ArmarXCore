/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef _ARMARXCORE_FWD_H
#define _ARMARXCORE_FWD_H

//#include <boost/shared_ptr.hpp>
#include <IceUtil/Handle.h>
#include <Ice/Handle.h>

namespace armarx
{
    class ArmarXObjectScheduler;
    typedef IceUtil::Handle<ArmarXObjectScheduler> ArmarXObjectSchedulerPtr;

    class ArmarXManager;
    typedef IceUtil::Handle<ArmarXManager> ArmarXManagerPtr;

    class IceManager;
    typedef IceUtil::Handle<IceManager> IceManagerPtr;

    class ManagedIceObject;
    typedef IceInternal::Handle<ManagedIceObject> ManagedIceObjectPtr;

    class Component;
    typedef IceInternal::Handle<Component> ComponentPtr;

    class IceGridAdmin;
    typedef IceUtil::Handle<IceGridAdmin> IceGridAdminPtr;

}

#endif
