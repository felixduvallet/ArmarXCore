/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Kai Welke (kai dot welke at kit dot edu)
 * @date       2012
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_CORE_ARMARXOBJECTSCHEDULER_H
#define _ARMARX_CORE_ARMARXOBJECTSCHEDULER_H
#include <ArmarXCore/core/logging/Logging.h>

#include <ArmarXCore/interface/core/ManagedIceObjectDefinitions.h>

namespace armarx
{
    /**
     * forward declarations
     */
    class ArmarXManager;
    typedef IceUtil::Handle<ArmarXManager> ArmarXManagerPtr;
    class IceManager;
    typedef IceUtil::Handle<IceManager> IceManagerPtr;
    class ManagedIceObject;
    typedef IceInternal::Handle<ManagedIceObject> ManagedIceObjectPtr;

    template <class T>
    class RunningTask;

    template <class T>
    class PeriodicTask;

    /**
     * ArmarXObjectScheduler shared pointer for convenience
     */
    class ArmarXObjectScheduler;
    typedef IceUtil::Handle<ArmarXObjectScheduler> ArmarXObjectSchedulerPtr;

    /**
     * @class ArmarXObjectScheduler
     * @brief Takes care of the lifecycle management of ManagedIceObjects.
     * @ingroup DistributedProcessingSub
     *
     * The ArmarXObjectScheduler schedules the lifecycle of a ManagedIceObject in a single thread.
     * According to the current state, the ManagedIceObject framework hooks are called.
     * It provides all necessary functionality for a clean initialisation of ManagedIceObjects
     * as well as dependency-resolution, dependency-monitoring, and exception handling.
     * ArmarXObjectSchedulers are created by the ArmarXManager.

     * @image html Lifecycle-UML.svg Lifecyle of a ManagedIceObject managed by the ArmarXObjectScheduler
     */
    class ArmarXObjectScheduler :
        public IceUtil::Shared,
        virtual public Logging
    {
    public:
        /**
         * Constructs an ArmarXObjectScheduler
         *
         * @param armarXManager pointer to the armarXManager
         * @param iceManager pointer to the iceManager
         * @param object object to schedule
         */
        ArmarXObjectScheduler(ArmarXManagerPtr armarXManager, IceManagerPtr iceManager, ManagedIceObjectPtr object, Ice::ObjectAdapterPtr objectAdapterToAddTo);
        ~ArmarXObjectScheduler();


        /**
         * waits until all depenencies are resolved.
         * @param timeoutMs If set to -1, it waits indefinitely. Otherwise throws
         * exception after waiting the specifed time (given in milliseconds).
         */
        void waitForDependencies(int timeoutMs = -1);
        void wakeupDependencyCheck();
        bool dependsOn(const std::string& objectName);
        void disconnected(bool reconnect);

        /**
         * Waits until scheduler has been terminated. After termination the lifecycle of the ManagedIceObject
         * has ceased and the thread has joined.
         */
        void waitForTermination();



        /**
         * @brief waitForObjectStart waits (thread sleeps) until the object
         * reached a specific state.
         * @param stateToWaitFor State for which the calling thread should wait
         * for.
         * @param timeoutMs Timeout in miliseconds until this function stops
         * waiting and returns false. Set to -1 for infinite waiting.
         * @return Whether the object reached the state before timeout or not.
         * @see waitForObjectStateMinimum()
         */
        bool waitForObjectState(ManagedIceObjectState stateToWaitFor, const long timeoutMs = -1);

        /**
         * @brief waitForObjectStart waits (thread sleeps) until the object
         * reached a specific state (or higher/later).
         * @param minimumStateToWaitFor Minimum State for which the calling thread should wait
         * for.
         * @param timeoutMs Timeout in miliseconds until this function stops
         * waiting and returns false. Set to -1 for infinite waiting.
         * @return Whether the object reached the minimum state before timeout
         * or not.
         * @see waitForObjectState()
         */
        bool waitForObjectStateMinimum(ManagedIceObjectState minimumStateToWaitFor, const long timeoutMs = -1);

        /**
         * Terminates the ManagedIceObject.
         */
        void terminate();

        /**
         * Check whether the Scheduler is terminated
         *
         * @return terminated state
         */
        bool isTerminated() const;

        bool isTerminationRequested() const;

        /**
         * Retrieve pointer to scheduled ManagedIceObject
         *
         * @return ManagedIceObjectPtr
         */
        ManagedIceObjectPtr getObject() const;


        bool checkDependenciesStatus() const;
    private:

        // main scheduling thread functions
        void scheduleObject();

        void waitForInterrupt();




        // scheduling thread
        IceUtil::Handle<RunningTask<ArmarXObjectScheduler> > scheduleObjectTask;



        // phases of the object lifecycle
        void initObject();
        void startObject();
        void disconnectObject();
        void exitObject();

        // private fields
        ArmarXManagerPtr armarXManager;
        IceManagerPtr iceManager;
        ManagedIceObjectPtr managedObject;

        // termination handling
        //        boost::mutex terminateMutex;
        //        boost::condition_variable terminateCondition;
        //        bool terminateConditionVariable;
        Mutex interruptMutex;
        boost::condition_variable interruptCondition;
        bool interruptConditionVariable;
        Mutex dependencyWaitMutex;
        boost::condition_variable dependencyWaitCondition;
        bool dependencyWaitConditionVariable;
        bool terminateRequested;
        bool objectedInitialized;
        bool tryReconnect;
        Ice::ObjectAdapterPtr objectAdapterToAddTo;

    };
}

#endif
