/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "ThreadList.h"
#include "PeriodicTask.h"
#include <ArmarXCore/core/system/CPULoadWatcher.h>


using namespace armarx;

ThreadList::ThreadList()
{
    cpuLoadWatcher.reset(new CPULoadWatcher(getProfiler()));
}

void ThreadList::onInitComponent()
{
}

void ThreadList::onConnectComponent()
{
    cpuLoadWatcher->start();
}

void ThreadList::updateCPUUsage()
{
    //    std::set<RunningTaskIceBase*>::const_iterator it = activeRunningTaskList.begin();
    //    for(; it != activeRunningTaskList.end(); it++)
    //    {
    //        RunningTaskIceBase* ptr = *it;
    //        double jiffies = GetThreadJiffies(ptr->threadId);
    //        if()
    //        ptr->workloadList.push_back(jiffies/GetHertz());
    //        if(ptr->workloadList.size() > 150)
    //            ptr->workloadList.resize(100, 0.0);
    //    }
}







StringList ThreadList::getRunningTaskNames(const Ice::Current& c)
{
    ScopedLock lock(runListMutex);
    StringList result;

    for (std::set<RunningTaskIceBase*> ::const_iterator it = activeRunningTaskList.begin(); it != activeRunningTaskList.end(); ++it)
    {
        result.push_back((*it)->name);
    }

    return result;
}

StringList ThreadList::getPeriodicTaskNames(const Ice::Current& c)
{
    ScopedLock lock(periodicListMutex);

    StringList result;

    for (std::set<PeriodicTaskIceBase*>::const_iterator it = activePeriodicTaskList.begin(); it != activePeriodicTaskList.end(); ++it)
    {
        result.push_back((*it)->name);
    }

    return result;
}

double ThreadList::getCpuUsage(const Ice::Current& c)
{
    ScopedLock lock(procTotalTimeMutex);
    return cpuLoadWatcher->getProcessCpuUsage().proc_total_time;
}

RunningTaskList ThreadList::getRunningTasks(const Ice::Current& c)
{
    ScopedLock lock(runListMutex);
    RunningTaskList resultList;
    std::set<RunningTaskIceBase*>::const_iterator it =  activeRunningTaskList.begin();

    for (; it != activeRunningTaskList.end(); it++)
    {
        resultList.push_back(**it);
        resultList.rbegin()->workload = cpuLoadWatcher->getThreadLoad((*it)->threadId);
    }

    return resultList;
}

PeriodicTaskList ThreadList::getPeriodicTasks(const Ice::Current& c)
{
    ScopedLock lock(periodicListMutex);
    PeriodicTaskList resultList;
    std::set<PeriodicTaskIceBase*>::const_iterator it =  activePeriodicTaskList.begin();

    for (; it != activePeriodicTaskList.end(); it++)
    {
        resultList.push_back(**it);
    }

    return resultList;
}


void ThreadList::addRunningTask(RunningTaskIceBase* threadPtr)
{
    ScopedLock lock(runListMutex);
    cpuLoadWatcher->addThread(threadPtr->threadId);

    if (activeRunningTaskList.find(threadPtr) != activeRunningTaskList.end())
    {
        return;
    }

    activeRunningTaskList.insert(threadPtr);
}

bool ThreadList::removeRunningTask(RunningTaskIceBase* threadPtr)
{
    ScopedLock lock(runListMutex);
    cpuLoadWatcher->removeThread(threadPtr->threadId);

    if (activeRunningTaskList.find(threadPtr) == activeRunningTaskList.end())
    {
        return false;
    }

    activeRunningTaskList.erase(threadPtr);
    return true;
}

void ThreadList::addPeriodicTask(PeriodicTaskIceBase* threadPtr)
{
    ScopedLock lock(periodicListMutex);

    if (activePeriodicTaskList.find(threadPtr) != activePeriodicTaskList.end())
    {
        return;
    }

    activePeriodicTaskList.insert(threadPtr);
}

bool ThreadList::removePeriodicTask(PeriodicTaskIceBase* threadPtr)
{
    ScopedLock lock(periodicListMutex);

    if (activePeriodicTaskList.find(threadPtr) == activePeriodicTaskList.end())
    {
        return false;
    }

    activePeriodicTaskList.erase(threadPtr);
    return true;
}

ThreadListPtr ThreadList::getApplicationThreadList()
{
    static ThreadListPtr applicationThreadList = new ThreadList();
    return applicationThreadList;
}

void ThreadList::setApplicationThreadListName(const std::string& threadListName)
{
    getApplicationThreadList()->setName(threadListName);
}


std::ostream& std::operator<<(std::ostream& stream, const armarx::RunningTaskIceBase& task)
{
    stream << "Threadname: " << task.name << " \n ";
    stream << "\tWorldload: " << task.workload << " \n ";
    stream << "\tRunning: " << task.running << " \n ";
    stream << "\tStartTime: " << IceUtil::Time::microSeconds(task.startTime).toDateTime() << " \n ";
    stream << "\tLastFeedbackTime: " << IceUtil::Time::microSeconds(task.lastFeedbackTime).toDateTime() << " \n ";
    stream << "\tStopped: " << task.stopped << " \n ";
    stream << "\tFinished: " << task.finished << "";

    return stream;
}
