#ifndef ARMARX_PROFILER_ICE_LOGGING_STRATEGY_H
#define ARMARX_PROFILER_ICE_LOGGING_STRATEGY_H

/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core::services::profiler
 * @author     Manfred Kroehnert ( manfred dot kroehnert at dot kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "LoggingStrategy.h"

#include "../../services/tasks/PeriodicTask.h"

#include <ArmarXCore/interface/core/Profiler.h>

#include <boost/thread/mutex.hpp>

namespace armarx
{
    namespace Profiler
    {
        class IceLoggingStrategy;
        typedef boost::shared_ptr<IceLoggingStrategy> IceLoggingStrategyPtr;

        /**
         * @class IceLoggingStrategy
         * @ingroup Profiling
         * @brief IceLoggingStrategy publishes incoming log method calls directly on IceLoggingStrategy::profilerListenerPrx.
         *
         * Instances of this strategy object is used by armarx::Profiler.
         */
        class IceLoggingStrategy :
            virtual public LoggingStrategy
        {
        public:
            IceLoggingStrategy(ProfilerListenerPrx profilerTopic);

            virtual ~IceLoggingStrategy();

            virtual void logEvent(pid_t processId, const std::string& executableName, uint64_t timestamp, const std::string& timestampUnit, const std::string& eventName, const std::string& parentName, const std::string& functionName);
            void logStatechartTransition(const std::string& parentStateName, const std::string& sourceStateName, const std::string& targetStateName);
            virtual void logProcessCpuUsage(pid_t processId, uint64_t timestamp, float cpuUsage);
        protected:
            ProfilerListenerPrx profilerListenerPrx;
        };


        /**
         * @class IceBufferdLoggingStrategy
         * @ingroup Profiling
         * @brief IceBufferdLoggingStrategy buffers incoming log method calls and publishes them as collections on IceLoggingStrategy::profilerListenerPrx.
         *
         * Instances of this strategy object is used by armarx::Profiler.
         */
        class IceBufferedLoggingStrategy :
            virtual public LoggingStrategy
        {
        public:
            IceBufferedLoggingStrategy(ProfilerListenerPrx profilerTopic);

            virtual ~IceBufferedLoggingStrategy();

            virtual void logEvent(pid_t processId, const std::string& executableName, uint64_t timestamp, const std::string& timestampUnit, const std::string& eventName, const std::string& parentName, const std::string& functionName);
            void logStatechartTransition(const std::string& parentStateName, const std::string& sourceStateName, const std::string& targetStateName);
            virtual void logProcessCpuUsage(pid_t processId, uint64_t timestamp, float cpuUsage);
        protected:
            void publishData();

            boost::mutex profilerEventsMutex;
            ProfilerEventList profilerEvents;

            boost::mutex profilerTransitionsMutex;
            ProfilerTransitionList profilerTransitions;

            boost::mutex profilerCpuUsagesMutex;
            ProfilerProcessCpuUsageList profilerProcessCpuUsages;

            ProfilerListenerPrx profilerListenerPrx;

            PeriodicTask<IceBufferedLoggingStrategy>::pointer_type publisherTask;
        };
    }
}

#endif
