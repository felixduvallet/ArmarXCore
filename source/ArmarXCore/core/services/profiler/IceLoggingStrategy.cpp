/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core::services::profiler
 * @author     Manfred Kroehnert ( manfred dot kroehnert at dot kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "IceLoggingStrategy.h"

#include <ArmarXCore/core/application/Application.h>


armarx::Profiler::IceLoggingStrategy::IceLoggingStrategy(armarx::ProfilerListenerPrx profilerTopic) :
    profilerListenerPrx(profilerTopic)
{
}

armarx::Profiler::IceLoggingStrategy::~IceLoggingStrategy()
{
}

void armarx::Profiler::IceLoggingStrategy::logEvent(pid_t processId, const std::string& executableName, uint64_t timestamp, const std::string& timestampUnit, const std::string& eventName, const std::string& parentName, const std::string& functionName)
{
    ProfilerEvent event;
    event.processId = processId;
    event.executableName = executableName;
    event.timestamp = timestamp;
    event.timestampUnit = timestampUnit;
    event.eventName = eventName;
    event.parentName = parentName;
    event.functionName = functionName;
    profilerListenerPrx->reportEvent(event);
}

void armarx::Profiler::IceLoggingStrategy::logStatechartTransition(const std::string& parentStateName, const std::string& sourceStateName, const std::string& targetStateName)
{
    profilerListenerPrx->reportStatechartTransition(parentStateName, sourceStateName, targetStateName);
}

void armarx::Profiler::IceLoggingStrategy::logProcessCpuUsage(pid_t processId, uint64_t timestamp, float cpuUsage)
{
    ProfilerProcessCpuUsage process;
    process.processId = processId;
    process.processName = ::armarx::Application::getInstance()->getName();
    process.timestamp = timestamp;
    process.cpuUsage = cpuUsage;
    profilerListenerPrx->reportProcessCpuUsage(process);
}



armarx::Profiler::IceBufferedLoggingStrategy::IceBufferedLoggingStrategy(armarx::ProfilerListenerPrx profilerTopic) :
    profilerListenerPrx(profilerTopic)
{
    profilerEvents.reserve(500);
    profilerTransitions.reserve(500);
    publisherTask = new PeriodicTask<IceBufferedLoggingStrategy>(this, &IceBufferedLoggingStrategy::publishData, 500);
    publisherTask->start();
}

armarx::Profiler::IceBufferedLoggingStrategy::~IceBufferedLoggingStrategy()
{
    publisherTask->stop();
}

void armarx::Profiler::IceBufferedLoggingStrategy::logEvent(pid_t processId, const std::string& executableName, uint64_t timestamp, const std::string& timestampUnit, const std::string& eventName, const std::string& parentName, const std::string& functionName)
{
    ProfilerEvent event;
    event.processId = processId;
    event.executableName = executableName;
    event.timestamp = timestamp;
    event.timestampUnit = timestampUnit;
    event.eventName = eventName;
    event.parentName = parentName;
    event.functionName = functionName;
    {
        boost::mutex::scoped_lock lock(profilerEventsMutex);
        profilerEvents.push_back(event);
    }
}

void armarx::Profiler::IceBufferedLoggingStrategy::logStatechartTransition(const std::string& parentStateName, const std::string& sourceStateName, const std::string& targetStateName)
{
    ProfilerTransition transition;
    transition.parentStateName = parentStateName;
    transition.sourceStateName = sourceStateName;
    transition.targetStateName = targetStateName;
    {
        boost::mutex::scoped_lock lock(profilerTransitionsMutex);
        profilerTransitions.push_back(transition);
    }
}

void armarx::Profiler::IceBufferedLoggingStrategy::logProcessCpuUsage(pid_t processId, uint64_t timestamp, float cpuUsage)
{
    ProfilerProcessCpuUsage process;
    process.processId = processId;
    process.processName = ::armarx::Application::getInstance()->getName();
    process.timestamp = timestamp;
    process.cpuUsage = cpuUsage;
    {
        boost::mutex::scoped_lock lock(profilerCpuUsagesMutex);
        profilerProcessCpuUsages.push_back(process);
    }
}

void armarx::Profiler::IceBufferedLoggingStrategy::publishData()
{
    if (!profilerEvents.empty())
    {
        ProfilerEventList eventsCopy;
        {
            boost::mutex::scoped_lock lock(profilerEventsMutex);
            profilerEvents.swap(eventsCopy);
        }
        profilerListenerPrx->reportEventList(eventsCopy);
    }

    if (!profilerTransitions.empty())
    {
        ProfilerTransitionList transitionsCopy;
        {
            boost::mutex::scoped_lock lock(profilerTransitionsMutex);
            profilerTransitions.swap(transitionsCopy);
        }
        profilerListenerPrx->reportStatechartTransitionList(transitionsCopy);
    }

    if (!profilerProcessCpuUsages.empty())
    {
        ProfilerProcessCpuUsageList cpuUsagesCopy;
        {
            boost::mutex::scoped_lock lock(profilerCpuUsagesMutex);
            profilerProcessCpuUsages.swap(cpuUsagesCopy);
        }
        profilerListenerPrx->reportProcessCpuUsageList(cpuUsagesCopy);
    }
}
