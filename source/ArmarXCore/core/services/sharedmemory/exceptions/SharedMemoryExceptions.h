/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core
* @author     Kai Welke (kai dot welke at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef _ARMARX_SHARED_MEMORY_EXCEPTIONS_H_
#define _ARMARX_SHARED_MEMORY_EXCEPTIONS_H_

#include "ArmarXCore/core/exceptions/Exception.h"

#include <string>

namespace armarx
{
    namespace exceptions
    {
        namespace local
        {
            class SharedMemoryException : public armarx::LocalException
            {
            public:
                SharedMemoryException(std::string memoryName, std::string reason) : armarx::LocalException("")
                {
                    setReason("Error in shared memory '" + memoryName + "':  " + reason);
                }

                ~SharedMemoryException() throw() { }

                virtual std::string name() const
                {
                    return "armarx::exceptions::local::SharedMemoryException";
                }
            };

            class SharedMemoryConnectionFailure : public armarx::LocalException
            {
            public:
                SharedMemoryConnectionFailure(std::string providerName, std::string reason) : armarx::LocalException("")
                {
                    setReason("Error connecting to shared memory provider '" + providerName + "': " + reason);
                }

                ~SharedMemoryConnectionFailure() throw() { }


                virtual std::string name() const
                {
                    return "armarx::exceptions::local::SharedMemoryConnectionFailure";
                }
            };
        };
    };
};

#endif // ARMARX_SHARED_MEMORY_EXCEPTIONS_H_
