/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "ArmarXObjectObserver.h"

#include "ArmarXManager.h"

#include "logging/Logging.h"

using namespace armarx;

ArmarXObjectObserver::ArmarXObjectObserver(ArmarXManagerPtr armarxManager):
    armarxManager(armarxManager)
{
}

void ArmarXObjectObserver::objectInit(const IceGrid::ObjectInfoSeq& objSeq, const Ice::Current&)
{
}

void ArmarXObjectObserver::objectAdded(const IceGrid::ObjectInfo& objInfo, const Ice::Current&)
{
    //    ARMARX_INFO_S << "Object added: " << objInfo.proxy->ice_getIdentity().name;
    armarxManager->wakeupWaitingSchedulers();
}

void ArmarXObjectObserver::objectUpdated(const IceGrid::ObjectInfo& objInfo, const Ice::Current&)
{
    //    ARMARX_INFO_S << "Object updated: " << objInfo.proxy->ice_getIdentity().name;
    armarxManager->wakeupWaitingSchedulers();
}

void ArmarXObjectObserver::objectRemoved(const Ice::Identity& objIdentity, const Ice::Current&)
{
    //    ARMARX_INFO_S << "Object removed: " << objIdentity.name;
    armarxManager->disconnectDependees(objIdentity.name);

}
