/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core
* @author     Jan Issac (jan dot issac at gmx dot de)
* @author     Kai Welke (welke at kit dot edu)
* @date       2011
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "Exception.h"
#include <execinfo.h>
#include <cxxabi.h>
#include <dlfcn.h>

#include <cstdio>
#include <cstdlib>
#include <sstream>

#include <IceUtil/Exception.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <Ice/LocalException.h>
#include <ArmarXCore/core/system/FactoryCollectionBase.h>


using namespace armarx;

LocalException::LocalException() :
    backtrace("")
{
    backtrace = generateBacktrace();
    setReason("");
}

LocalException::LocalException(const LocalException& e)
{
    reason << e.reason.str();
    backtrace = e.backtrace;
    resultingMessage = e.resultingMessage;
}

void LocalException::setReason(const std::string& reason)
{
    this->reason.str("");
    this->reason.clear();

    this->reason << reason;

}

const char* LocalException::what() const throw()
{
    generateOutputString();
    return resultingMessage.c_str();
}

std::string LocalException::generateBacktrace()
{
    int skip = 3;
    void* callstack[128];
    const int nMaxFrames = sizeof(callstack) / sizeof(callstack[0]);
    char buf[1024];

    int nFrames = ::backtrace(callstack, nMaxFrames);
    char** symbols = backtrace_symbols(callstack, nFrames);

    std::ostringstream trace_buf;

    for (int i = skip; i < nFrames; i++)
    {
        Dl_info info;

        if (dladdr(callstack[i], &info) && info.dli_sname)
        {
            char* demangled = NULL;
            int status = -1;

            if (info.dli_sname[0] == '_')
            {
                demangled = abi::__cxa_demangle(info.dli_sname, NULL, 0, &status);
            }

            snprintf(buf, sizeof(buf), "%-3d %*p %s + %zd\n",
                     i - skip + 1, int(2 + sizeof(void*) * 2), callstack[i],
                     status == 0 ? demangled :
                     info.dli_sname == 0 ? symbols[i] : info.dli_sname,
                     (char*)callstack[i] - (char*)info.dli_saddr);
            free(demangled);
        }
        else
        {
            snprintf(buf, sizeof(buf), "%-3d %*p %s\n",
                     i - skip + 1, int(2 + sizeof(void*) * 2), callstack[i], symbols[i]);
        }

        trace_buf << buf;
    }

    free(symbols);

    if (nFrames == nMaxFrames)
    {
        trace_buf << "[truncated]\n";
    }

    return trace_buf.str();
}

void LocalException::generateOutputString() const
{
    std::stringstream what_buf;

    what_buf << std::endl;
    what_buf << "Reason: " << ((reason.str().empty()) ? "undefined" : reason.str()) << std::endl;
    what_buf << "Backtrace: " << std::endl;
    what_buf << backtrace << std::endl;

    resultingMessage = what_buf.str();
}

void armarx::handleExceptions()
{
    try
    {
        throw;
    }
    catch (const armarx::UserException& userException)
    {
        // Handle marshallable user exceptions
        *((ARMARX_LOG_S)  .setBacktrace(false))
                << armarx::eERROR
                << "Caught userException: " << userException.what() << "\n"
                << "\tReason: " << userException.reason << "\n"
                << "\tStacktrace: \n" << userException.ice_stackTrace()
                << flush;
    }



    catch (const LocalException& exception)
    {
        // Handle local (run-time) exceptions
        *((ARMARX_LOG_S)  .setBacktrace(false))
                << armarx::eERROR
                << "Caught " << exception.name() << ":\n" << exception.what()
                << flush;
    }

    catch (const Ice::NoObjectFactoryException& e)
    {
        std::stringstream availableFactories;

        for (const auto & elem : FactoryCollectionBase::GetPreregistratedFactories())
        {
            ObjectFactoryMap objFacMap = elem->getFactories();
            ObjectFactoryMap::iterator it = objFacMap.begin();

            for (; it != objFacMap.end(); it++)
            {
                availableFactories << "\t" << it->first;
                bool last_iteration = it == (--objFacMap.end());

                if (!last_iteration)
                {
                    availableFactories << "\n";
                }
            }
        }

        *((ARMARX_LOG_S)  .setBacktrace(false))
                << armarx::eERROR
                << "Caught NoObjectFactoryException:\n " << e.what()
                << "\n\nPossible reasons:\n"
                << "\t- You did not link the library where the object factory is declared\n"
                << "\t- You did not include the header of the object factory\n\n"
                << "\tThese object factories are usually located in header files called XYZObjectFactories.h. This header file needs to be included into your library."
                << "Additionally, also the library needs to be included. To find these file you can try the following command (linux):\n"
                << "\tgrep -r \"" << e.type << "\" ~/armarx/ --include \"*Factor*\"\n"
                << "\t Include that header file and find out to which libray this header belongs (usually the CMakeLists.txt next to it) and link this lib to your library."
                << "\n\nAvailable factories: \n" << availableFactories.str()
                //                   << "\nBacktrace: " << exception.ice_stackTrace()
                << flush;
    }

    catch (const IceUtil::Exception& exception)
    {

        *((ARMARX_LOG_S)  .setBacktrace(false))
                << armarx::eERROR
                << "Caught IceUtil::Exception:\n " << exception.what()
                << "\nBacktrace: " << exception.ice_stackTrace()
                << flush;
    }
    catch (const std::exception& exception)
    {

        *((ARMARX_LOG_S)  .setBacktrace(true))
                << armarx::eERROR
                << "Caught std::exception:\n" << exception.what()
                << flush;
    }
    catch (...)
    {
        ARMARX_LOG_S
                << armarx::eERROR
                << "Caught unknown exception!"
                << flush;
    }
}
