/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core
* @author     Jan Issac (jan dot issac at gmx dot de)
* @date       2011
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef _ARMARX_VALUERANGE_EXCEEDED_EXCEPTION_H_
#define _ARMARX_VALUERANGE_EXCEEDED_EXCEPTION_H_

#include "ArmarXCore/core/exceptions/Exception.h"

#include <string>
#include <iostream>

namespace armarx
{
    namespace exceptions
    {
        namespace local
        {
            /**
              \class ValueRangeExceededException
              \brief This exception is thrown if a value is out of a specified or allowed range.
              \ingroup Exceptions
             */
            class ValueRangeExceededException: public armarx::LocalException
            {
            public:

                ValueRangeExceededException(std::string propertyName, double min, double max, double value) :
                    armarx::LocalException()
                {
                    std::stringstream sstream;
                    sstream << "<" << propertyName << "> property value '" << value << "' is out of bounds [" << min << ", " << max << "]";
                    setReason(sstream.str());
                }

                ~ValueRangeExceededException() throw()
                { }

                virtual std::string name() const
                {
                    return "armarx::exceptions::local::ValueRangeExceededException";
                }
            };
        }
    }
}

#endif
