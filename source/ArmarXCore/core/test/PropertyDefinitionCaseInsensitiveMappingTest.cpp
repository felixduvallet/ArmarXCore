/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core::Test
* @author     Jan Issac (jan dot issac at gmx dot de)
* @date       2011
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#define BOOST_TEST_MODULE ArmarX::Core::PropertyDefinitionCaseInSensitiveMapping
#define ARMARX_BOOST_TEST
#include <ArmarXCore/Test.h>

#include "PropertyFixtures.h"

// ArmarXCore
#include <ArmarXCore/core/application/properties/Properties.h>
#include <ArmarXCore/core/Component.h>

// Ice
#include <Ice/Ice.h>

#include <string>
#include <iostream>
#include <limits>

using namespace armarx;

class MyPropertyDefinitions: public ComponentPropertyDefinitions
{
public:
    MyPropertyDefinitions(std::string identifier):
        ComponentPropertyDefinitions(identifier)
    {
        defineRequiredProperty<std::string>("prop:mapped:string", "Desc: ...")
        .setCaseInsensitive(true)
        .map("one", "1")
        .map("TwO", "2")
        .map("THREE", "3");

        defineRequiredProperty<int>("prop:mapped:int", "Desc: ...")
        .map("One", 1)
        .setCaseInsensitive(true)
        .map("TwO", 2)
        .map("THREE", 3);

        defineRequiredProperty<float>("prop:mapped:float", "Desc: ...")
        .map("One", 1.f)
        .setCaseInsensitive(true)
        .map("TwO", 2.f)
        .setCaseInsensitive(true)
        .map("THREE", 3.f);

        defineRequiredProperty<TestTypes::EnumType>("prop:mapped:EnumType", "Desc: ...")
        .map("one", TestTypes::One)
        .map("TwO", TestTypes::Two)
        .map("THREE", TestTypes::Three)
        .setCaseInsensitive(true);

        defineRequiredProperty<TestTypes::StructType>("prop:mapped:StructType", "Desc: ...")
        .setCaseInsensitive(true)
        .map("One", TestTypes::StructType(1, 1, "1"))
        .setCaseInsensitive(false)
        .map("TwO", TestTypes::StructType(2, 2, "2"))
        .setCaseInsensitive(true)
        .map("THREE", TestTypes::StructType(3, 3, "3"))
        .setCaseInsensitive(true)
        .setCaseInsensitive(false)
        .setCaseInsensitive(true);

        defineRequiredProperty<TestTypes::ClassType>("prop:mapped:ClassType", "Desc: ...")
        .setCaseInsensitive(true)
        .setCaseInsensitive(true)
        .map("one", TestTypes::ClassType(1, 1, "1"))
        .setCaseInsensitive(true)
        .map("TwO", TestTypes::ClassType(2, 2, "2"))
        .setCaseInsensitive(true)
        .map("THREE", TestTypes::ClassType(3, 3, "3"));
    }
};


PropertyDefinitionsPtr ComponentPrototype::createPropertyDefinition()
{
    return PropertyDefinitionsPtr(new MyPropertyDefinitions("ArmarX"));
}


struct DefinitionFixture
{
    DefinitionFixture()
    {
        defs = component.getPropertyDefinitions();

        keyValues = new std::string[3];
        keyValues[0] = "one";
        keyValues[1] = "two";
        keyValues[2] = "three";
    }

    ~DefinitionFixture()
    {
        delete [] keyValues;
    }

    ComponentPrototype component;
    PropertyDefinitionsPtr defs;
    std::string* keyValues;
};


BOOST_AUTO_TEST_SUITE(PropertyDefinitions)


BOOST_FIXTURE_TEST_CASE(DefinitionMappingSearchTest, DefinitionFixture)
{
    for (int i = 0; i < 3; i++)
    {
        {
            PropertyDefinition<std::string>::PropertyValuesMap valueMap
                = defs->getDefintion<std::string>(
                      "prop:mapped:string").getValueMap();
            BOOST_CHECK(valueMap.find(keyValues[i]) != valueMap.end());
            BOOST_CHECK(valueMap.find("four") == valueMap.end());
        }

        {
            PropertyDefinition<int>::PropertyValuesMap valueMap
                = defs->getDefintion<int>(
                      "prop:mapped:int").getValueMap();
            BOOST_CHECK(valueMap.find(keyValues[i]) != valueMap.end());
            BOOST_CHECK(valueMap.find("four") == valueMap.end());
        }

        {
            PropertyDefinition<float>::PropertyValuesMap valueMap
                = defs->getDefintion<float>(
                      "prop:mapped:float").getValueMap();
            BOOST_CHECK(valueMap.find(keyValues[i]) != valueMap.end());
            BOOST_CHECK(valueMap.find("four") == valueMap.end());
        }

        {
            PropertyDefinition<TestTypes::EnumType>::PropertyValuesMap valueMap
                = defs->getDefintion<TestTypes::EnumType>(
                      "prop:mapped:EnumType").getValueMap();
            BOOST_CHECK(valueMap.find(keyValues[i]) != valueMap.end());
            BOOST_CHECK(valueMap.find("four") == valueMap.end());
        }

        {
            PropertyDefinition<TestTypes::ClassType>::PropertyValuesMap valueMap
                = defs->getDefintion<TestTypes::ClassType>(
                      "prop:mapped:ClassType").getValueMap();
            BOOST_CHECK(valueMap.find(keyValues[i]) != valueMap.end());
            BOOST_CHECK(valueMap.find("four") == valueMap.end());
        }
    }
}

BOOST_AUTO_TEST_SUITE_END(/* PropertyDefinitions */)



