
/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core::Test
* @author     Jan Issac (jan dot issac at gmx dot de)
* @date       2011
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#define BOOST_TEST_MODULE ArmarX::Core::PropertyDefinitionDefaults
#define ARMARX_BOOST_TEST
#include <ArmarXCore/Test.h>

#include "PropertyFixtures.h"

// ArmarXCore
#include <ArmarXCore/core/application/properties/Properties.h>
#include <ArmarXCore/core/Component.h>

// Ice
#include <Ice/Ice.h>

#include <string>
#include <iostream>

using namespace armarx;

class MyPropertyDefinitions: public ComponentPropertyDefinitions
{
public:
    MyPropertyDefinitions(std::string identifier):
        ComponentPropertyDefinitions(identifier)
    {
        defineOptionalProperty<std::string>("i-am-optional:string", "default text", "Desc: ...");
        defineOptionalProperty<int>("i-am-optional:int", 42, "Desc: ...");
        defineOptionalProperty<float>("i-am-optional:float", 42, "Desc: ...");
        defineOptionalProperty<TestTypes::EnumType>("i-am-optional:EnumType", TestTypes::One, "Desc: ...");
        defineOptionalProperty<TestTypes::StructType>("i-am-optional:StructType", TestTypes::StructType(), "Desc: ...");
        defineOptionalProperty<TestTypes::ClassType>("i-am-optional:ClassType", TestTypes::ClassType(), "Desc: ...");

        defineRequiredProperty<std::string>("i-am-required:string", "Desc: ...");
        defineRequiredProperty<int>("i-am-required:int", "Desc: ...");
        defineRequiredProperty<float>("i-am-required:float", "Desc: ...");
        defineRequiredProperty<TestTypes::EnumType>("i-am-required:EnumType", "Desc: ...");
        defineRequiredProperty<TestTypes::StructType>("i-am-required:StructType", "Desc: ...");
        defineRequiredProperty<TestTypes::ClassType>("i-am-required:ClassType", "Desc: ...");
    }
};


PropertyDefinitionsPtr ComponentPrototype::createPropertyDefinition()
{
    return PropertyDefinitionsPtr(new MyPropertyDefinitions("ArmarX"));
}


struct DefinitionFixture
{
    DefinitionFixture()
    {
        component.properties->setProperty("i-am-optional:string", "custom text");
        component.properties->setProperty("i-am-optional:int", "12");
        component.properties->setProperty("i-am-optional:float", "12");
        component.properties->setProperty("i-am-optional:EnumType", "Two");
        component.properties->setProperty("i-am-optional:StructType", "EmptyStruct");
        component.properties->setProperty("i-am-optional:ClassType", "EmptyClass");

        component.properties->setProperty("i-am-required:string", "custom text");
        component.properties->setProperty("i-am-required:int", "12");
        component.properties->setProperty("i-am-required:float", "12");
        component.properties->setProperty("i-am-required:EnumType", "Two");
        component.properties->setProperty("i-am-required:StructType", "EmptyStruct");
        component.properties->setProperty("i-am-required:ClassType", "EmptyClass");

        defs = component.getPropertyDefinitions();
    }

    ComponentPrototype component;
    PropertyDefinitionsPtr defs;
};


BOOST_AUTO_TEST_SUITE(PropertyDefinitions)

BOOST_FIXTURE_TEST_CASE(DefinitionDefaultTest,  DefinitionFixture)
{
    BOOST_CHECK_EQUAL(defs->getDefintion<std::string>("i-am-optional:string").getDefaultValue(), "default text");
    BOOST_CHECK_EQUAL(defs->getDefintion<int>("i-am-optional:int").getDefaultValue(), 42);
    BOOST_CHECK_EQUAL(defs->getDefintion<float>("i-am-optional:float").getDefaultValue(), 42);
    BOOST_CHECK_EQUAL(defs->getDefintion<TestTypes::EnumType>("i-am-optional:EnumType").getDefaultValue(), TestTypes::One);
    BOOST_CHECK(defs->getDefintion<TestTypes::StructType>("i-am-optional:StructType").getDefaultValue() == TestTypes::StructType());
    BOOST_CHECK(defs->getDefintion<TestTypes::ClassType>("i-am-optional:ClassType").getDefaultValue() == TestTypes::ClassType());
}

BOOST_AUTO_TEST_SUITE_END(/* PropertyDefinitions */)
