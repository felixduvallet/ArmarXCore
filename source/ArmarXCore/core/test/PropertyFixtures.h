/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core::Test
* @author     Jan Issac (jan dot issac at gmx dot de)
* @date       2011
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

// ArmarXCore
#include <ArmarXCore/core/application/properties/Properties.h>
#include <ArmarXCore/core/Component.h>

// Ice
#include <Ice/Properties.h>

#include <string>
#include <iostream>

#ifndef ARMARXCORE_TEST_PROPERTYFIXTURES_H
#define ARMARXCORE_TEST_PROPERTYFIXTURES_H

namespace TestTypes
{
    enum EnumType
    {
        One,
        Two,
        Three,
        Four,
        Five
    };

    struct StructType
    {
        StructType():
            one(0),
            two(0),
            three("0")
        {

        }

        StructType(int one, float two, std::string three):
            one(one),
            two(two),
            three(three)
        {

        }

        int one;
        float two;
        std::string three;

        bool operator==(const StructType& obj) const
        {
            return (obj.one == one && obj.two == two && obj.three == three);
        }
    };

    class ClassType
    {
    public:
        ClassType():
            one(0),
            two(0),
            three("0"),
            four(StructType()),
            five(Five)
        {

        }

        ClassType(int one, float two, std::string three):
            one(one),
            two(two),
            three(three),
            four(StructType(one, two, three)),
            five(Five)
        {

        }

        bool operator==(const ClassType& obj) const
        {
            return (obj.one == one
                    && obj.two == two
                    && obj.three == three
                    && obj.four == four
                    && obj.five == five);
        }

        int one;
        float two;
        std::string three;
        StructType four;
        EnumType five;
    };
}

namespace armarx
{
    class ComponentPrototype
    {
    public:
        ComponentPrototype():
            definitions(createPropertyDefinition()),
            properties(Ice::createProperties())
        {

        }

        /**
         * Custom definition instantiation
         */
        PropertyDefinitionsPtr createPropertyDefinition();

        /**
         * Property creation and retrieval
         */
        template <typename PropertyType>
        Property<PropertyType> getProperty(std::string name)
        {
            return Property<PropertyType>(
                       getPropertyDefinitions()
                       ->getDefintion<PropertyType>(name), properties);
        }

        PropertyDefinitionsPtr getPropertyDefinitions()
        {
            return definitions;
        }

        PropertyDefinitionsPtr definitions;
        Ice::PropertiesPtr properties;
    };
}


#endif
