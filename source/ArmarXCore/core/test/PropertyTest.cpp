/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core::Test
* @author     Jan Issac (jan dot issac at gmx dot de)
* @date       2011
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#define BOOST_TEST_MODULE ArmarX::Core::Property
#define ARMARX_BOOST_TEST
#include <ArmarXCore/Test.h>

// ArmarXCore

#include <ArmarXCore/core/Property.h>
#include <ArmarXCore/core/PropertyDefinition.h>
#include <ArmarXCore/core/PropertyDefinitionContainer.h>
#include <ArmarXCore/core/Component.h>

// Ice
#include <Ice/Ice.h>

#include <string>
#include <iostream>

using namespace armarx;

class MyPropertyDefinitions: public ComponentPropertyDefinitions
{
public:
    MyPropertyDefinitions(std::string identifier):
        ComponentPropertyDefinitions(identifier)
    {
        defineOptionalProperty<std::string>("i-am-optional::string", "default text");
        defineOptionalProperty<int>("i-am-optional::int", 42);

        defineRequiredProperty<std::string>("i-am-required::string");
        defineRequiredProperty<int>("i-am-required::int");

        defineOptionalProperty<std::string>("mapped-optional::string", "default text")
        .map("one", "1")
        .map("two", "1");

        defineOptionalProperty<int>("mapped-optional::int", 42)
        .map("one", 1)
        .map("two", 2);

        defineRequiredProperty<std::string>("mapped-required::string")
        .map("one", "1")
        .map("two", "1");

        defineRequiredProperty<int>("mapped-required::int")
        .map("one", 1)
        .map("two", 2);
    }
};

class ComponentPrototype
{
public:
    ComponentPrototype():
        definitions(createPropertyDefinition())
    {
        //definitions = createPropertyDefinition();
        properties = Ice::createProperties();

        properties->setProperty("mapped-optional::string", "three");
        properties->setProperty("mapped-optional::int", "three");
        properties->setProperty("mapped-required::string", "three");
        properties->setProperty("mapped-required::int", "three");
    }

    /**
     * Custom definition instantiation
     */
    PropertyDefinitionsPtr createPropertyDefinition()
    {
        return PropertyDefinitionsPtr(new MyPropertyDefinitions("ArmarX"));
    }


    /**
     * Property creation and retrieval
     */
    template <typename PropertyType>
    PropertyX<PropertyType> getProperty(std::string name)
    {
        return PropertyX<PropertyType>(
                   definitions->getDefintion<PropertyType>(name), properties);
    }

    PropertyDefinitionsPtr definitions;
    Ice::PropertiesPtr properties;
};


BOOST_AUTO_TEST_SUITE(PropertyDefinitions)

BOOST_AUTO_TEST_CASE(X)
{
    ComponentPrototype component;

    BOOST_CHECK_EQUAL(component.getProperty<std::string>("i-am-optional::string").getValue(), "default text");
    BOOST_CHECK_EQUAL(component.getProperty<int>("i-am-optional::int").getValue(), 42);

    BOOST_CHECK_THROW
    (
        component.getProperty<std::string>("i-am-required::string").getValue(),
        armarx::exceptions::local::MissingRequiredPropertyException
    );

    BOOST_CHECK_THROW
    (
        component.getProperty<int>("i-am-required::int").getValue(),
        armarx::exceptions::local::MissingRequiredPropertyException
    );

    BOOST_CHECK_THROW
    (
        component.getProperty<std::string>("mapped-optional::string").getValue(),
        armarx::exceptions::local::UnmappedValueException
    );

    BOOST_CHECK_THROW
    (
        component.getProperty<int>("mapped-optional::int").getValue(),
        armarx::exceptions::local::UnmappedValueException
    );

    BOOST_CHECK_THROW
    (
        component.getProperty<std::string>("mapped-required::string").getValue(),
        armarx::exceptions::local::UnmappedValueException
    );

    BOOST_CHECK_THROW
    (
        component.getProperty<int>("mapped-required::int").getValue(),
        armarx::exceptions::local::UnmappedValueException
    );
}

BOOST_AUTO_TEST_SUITE_END(/* PropertyDefinitions */)
