/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core::Test
* @author     Jan Issac (jan dot issac at gmx dot de)
* @date       2011
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#define BOOST_TEST_MODULE ArmarX::Core::PropertyDefinitionRegex
#define ARMARX_BOOST_TEST
#include <ArmarXCore/Test.h>

#include "PropertyFixtures.h"

// ArmarXCore
#include <ArmarXCore/core/application/properties/Properties.h>
#include <ArmarXCore/core/Component.h>

// Ice
#include <Ice/Ice.h>

#include <string>
#include <iostream>
#include <limits>

using namespace armarx;

class MyPropertyDefinitions: public ComponentPropertyDefinitions
{
public:
    MyPropertyDefinitions(std::string identifier):
        ComponentPropertyDefinitions(identifier)
    {
        defineRequiredProperty<std::string>("test-prop", "Desc: ...").setMatchRegex("\\d+(.\\d*)?");
    }
};


PropertyDefinitionsPtr ComponentPrototype::createPropertyDefinition()
{
    return PropertyDefinitionsPtr(new MyPropertyDefinitions("ArmarX"));
}


struct DefinitionFixture
{
    DefinitionFixture()
    {
        defs = component.getPropertyDefinitions();
    }

    ComponentPrototype component;
    PropertyDefinitionsPtr defs;
};


BOOST_AUTO_TEST_SUITE(PropertyDefinitions)


BOOST_FIXTURE_TEST_CASE(DefinitionRegexTest,  DefinitionFixture)
{
    BOOST_CHECK_EQUAL(defs->getDefintion<std::string>("test-prop").getMatchRegex(), "\\d+(.\\d*)?");
}


BOOST_AUTO_TEST_SUITE_END(/* PropertyDefinitions */)



