/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core::Test
* @author     Nils Adermann (naderman at naderman dot de)
* @date       2010
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#ifndef _ARMARX_CORE_TEST_ICETESTHELPER_H
#define _ARMARX_CORE_TEST_ICETESTHELPER_H

#include <Ice/Ice.h>
#include <boost/shared_ptr.hpp>
#include <boost/lexical_cast.hpp>
#include <map>
#include <stdexcept>
#include <cstdlib>

#include <ArmarXCore/core/test/TestArmarXManager.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <boost/filesystem.hpp>


/**
* Helper class IceTestHelper using Ice in tests
*/
class IceTestHelper :
    public IceUtil::Shared
{
public:
    IceTestHelper(int registryPort = 11220, int port = 11221) :
        registryPort(registryPort),
        port(port)
    {
        Ice::StringSeq args;
        std::stringstream defaultLocator;
        defaultLocator << "--Ice.Default.Locator=IceGrid/Locator:tcp -p " << registryPort << " -h localhost";
        args.push_back(defaultLocator.str());
        args.push_back("--Ice.ThreadPool.Client.SizeMax=20");
        args.push_back("--Ice.ThreadPool.Server.SizeMax=20");
        args.push_back("--Ice.Default.EncodingVersion=1.0 ");

        ic = Ice::initialize(args);
        init();
    }

    ~IceTestHelper()
    {
        armarx::LogSender::SetSendLoggingActivated(false);

        if (grid)
        {
            stopIceGrid();
        }
    }

    void startEnvironment()
    {
        startIceGrid();
        //            admin = new armarx::IceGridAdmin(ic, "TestIceGrid"); <-- is already in armarxManager, so not needed here? (Mirko W)
        startIceStorm();
    }

    void startIceStorm()
    {
        deployApplication(armarx::test::getCmakeValue("ArmarXCore_CONFIG_DIR") + "/IceStorm.icegrid.xml");
    }

    void startIceGrid()
    {



        configDir = (boost::filesystem::temp_directory_path() / boost::filesystem::unique_path()).string();
        configDir += "/icegridtestnode";
        ARMARX_INFO_S << "IceGrid Config dir: " << configDir;
        exec(std::string("mkdir -p ") + configDir + "/log");
        exec(std::string("mkdir -p ") + configDir + "/registry");
        exec(std::string("mkdir -p ") + configDir + "/data");

        std::string registryPortStr = boost::lexical_cast<std::string>(registryPort);

        try
        {
            IceGrid::RegistryPrx prx = IceGrid::RegistryPrx::checkedCast(ic->stringToProxy("IceGrid/Registry"));

            if (!prx->createAdminSession("user", "password")->getAdmin()->pingNode("NodeMain"))
            {
                throw std::exception();
            }
        }
        catch (std::exception& e)
        {
            std::cout << "exception: " << e.what() << std::endl;
            IceUtil::ThreadControl::sleep(IceUtil::Time::milliSeconds(300));

            std::string command =
                std::string("cd ") + configDir
                + ";icegridnode "
                + "--daemon "
                + "--Ice.Config=" + armarx::test::getCmakeValue("ARMARX_USER_CONFIG_DIR_ABSOLUT") + "/default.cfg,"
                + armarx::test::getCmakeValue("ArmarXCore_CONFIG_DIR") + "/icegrid_registry.cfg,"
                + armarx::test::getCmakeValue("ArmarXCore_CONFIG_DIR") + "/armarx-icegridnode.cfg "
                + "--Ice.Default.Locator=\"IceGrid/Locator:tcp -p " + registryPortStr + " -h localhost\" "
                + "--IceGrid.Registry.Client.Endpoints=\"tcp -p " + registryPortStr + "\" "
                + "--nochdir "
                + "--Ice.ProgramName=\"NodeMain\" "
                + "--IceGrid.Node.Name=\"NodeMain\" ";

            exec(command);
        }

        grid = true;
    }

    void stopIceGrid()
    {
        std::string registryPortStr = boost::lexical_cast<std::string>(registryPort);

        std::string gridCommand = std::string("icegridadmin ")
                                  + "--Ice.Default.Locator=\"IceGrid/Locator:tcp -p " + registryPortStr + " -h localhost\" "
                                  + "-u x -p y "
                                  + "-e \"node shutdown NodeMain\"";
        exec(gridCommand);

        IceUtil::ThreadControl::sleep(IceUtil::Time::milliSeconds(300));

        //            exec("killall icegridnode -9");
        //            exec("killall icebox -9");
        //            exec("killall icegridadmin -9");
    }

    void deployApplication(const std::string& xmlPath)
    {
        if (!std::ifstream(xmlPath.c_str()))
        {
            throw std::runtime_error(std::string("File not found: ") + xmlPath);
        }

        std::string registryPortStr = boost::lexical_cast<std::string>(registryPort);

        std::string command = std::string("icegridadmin ")
                              + "--Ice.Default.Locator=\"IceGrid/Locator:tcp -p " + registryPortStr + " -h localhost\" "
                              + "--Ice.PrintStackTraces=1 "
                              + "-u x -p y "
                              + "-e \"application add " + xmlPath + "\" ";
        exec(command);
    }

    void registerAdapter(const std::string& name, Ice::ObjectPtr object)
    {
        std::string endpoint("tcp -p ");
        endpoint += boost::lexical_cast<std::string>(port++);

        endpoints[name] = endpoint;

        Ice::ObjectAdapterPtr adapter =
            ic->createObjectAdapterWithEndpoints(name, endpoint);
        adapter->add(object, ic->stringToIdentity(name));
        adapter->activate();
    }

    Ice::CommunicatorPtr getCommunicator()
    {
        return ic;
    }

    /**
        * Retrieves a proxy object.
        *
        * @param  objectIdentity The object's identity, e.g. Log
        *
        * @return A proxy of the remote instance.
        */
    template <class ProxyType>
    ProxyType getProxy(const std::string& name)
    {
        Ice::ObjectPrx base = ic->stringToProxy(
                                  name + std::string(":") + endpoints[name]);
        ProxyType proxy = ProxyType::checkedCast(base);

        if (!proxy)
        {
            throw std::runtime_error("Invalid proxy");
        }

        return proxy;
    }

protected:
    void init()
    {
        grid = false;
    }

    void exec(const std::string& command)
    {
        ARMARX_INFO_S << "Executing command: " << command << std::endl;
        system(command.c_str());
    }

private:
    bool grid;
    std::string configDir;
    Ice::CommunicatorPtr ic;
    //        armarx::IceGridAdminPtr admin;
    std::map<std::string, std::string> endpoints;

    int registryPort;
    int port;
};
typedef IceUtil::Handle<IceTestHelper> IceTestHelperPtr;
//int IceTestHelper::registryPort = 11220;
//int IceTestHelper::port = 11221;

#endif
