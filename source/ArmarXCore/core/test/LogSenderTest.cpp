/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core::Test
* @author     Nils Adermann (naderman at naderman dot de)
* @date       2010
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#define BOOST_TEST_MODULE ArmarX::Core::LogSender
#define ARMARX_BOOST_TEST
#include <ArmarXCore/Test.h>
#include <ArmarXCore/core/logging/LogSender.h>
#include <ArmarXCore/core/test/IceTestHelper.h>

#include <iostream>

class LogMock : public armarx::Log
{
public:
    virtual void write(
        const std::string& who,
        Ice::Long time,
        const std::string& category,
        armarx::MessageType t,
        const std::string& what,
        const std::string& file,
        Ice::Int line,
        const std::string& function,
        const Ice::Current& c)
    {
        BOOST_CHECK_EQUAL(who, std::string("ComponentName"));
        BOOST_CHECK_EQUAL(category, std::string("CategoryName"));
        BOOST_CHECK_EQUAL(t, armarx::eWARN);
        BOOST_CHECK_EQUAL(what, std::string("message 23"));
        BOOST_CHECK_EQUAL(file, std::string("file"));
        BOOST_CHECK_EQUAL(line, 42);
        BOOST_CHECK_EQUAL(function, std::string("function"));

        counter++;
    }
    virtual void writeLog(
        const armarx::LogMessage& logMsg,
        const Ice::Current& c)
    {
        BOOST_CHECK_EQUAL(logMsg.who, std::string("ComponentName"));
        BOOST_CHECK_EQUAL(logMsg.tag, std::string("CategoryName"));
        BOOST_CHECK_EQUAL(logMsg.type, armarx::eWARN);
        BOOST_CHECK_EQUAL(logMsg.what, std::string("message 23"));
        BOOST_CHECK_EQUAL(logMsg.file, std::string("file"));
        BOOST_CHECK_EQUAL(logMsg.line, 42);
        BOOST_CHECK_EQUAL(logMsg.function, std::string("function"));
        counter++;
    }

    static int counter;
};
int LogMock::counter;

BOOST_AUTO_TEST_CASE(testLogSenderLogStream)
{
    IceTestHelper iceTestHelper;
    Ice::ObjectPtr logMock(new LogMock);
    iceTestHelper.registerAdapter("Log", logMock);
    armarx::LogPrx logProxy = iceTestHelper.getProxy<armarx::LogPrx>("Log");

    armarx::LogSenderPtr logSender(
        new armarx::LogSender("ComponentName", logProxy)
    );

    LogMock::counter = 0;

    (*(logSender->setTag(std::string("CategoryName"))->setFile("file")->setLine(42)->setFunction("function")))
            << armarx::eWARN << "message " << 23;

    *logSender << armarx::flush;

    logSender->setTag(std::string("CategoryName"))->setFile("file")->setLine(42)->setFunction("function");
    *logSender << std::string("mess") << "age "
               << 23 << armarx::flush;

    logSender->setTag(std::string("CategoryName"))->setFile("file")->setLine(42)->setFunction("function");
    *logSender << std::string("message 23");
    logSender->flush();

    ARMARX_DEBUG_S << "Debug Level" << std::endl;
    ARMARX_VERBOSE_S << "Verbose Level" << std::endl;
    ARMARX_INFO_S  << "Info Level" << std::endl;
    ARMARX_IMPORTANT_S  << "Important Level" << std::endl;
    ARMARX_WARNING_S << "Warning Level" << std::endl;
    ARMARX_ERROR_S << "Error Level" << std::endl;
    ARMARX_FATAL_S << "Fatal Level" << std::endl;

    //    sleep(2); // sleep a bit to wait for async call of logProxy in logSender::log()
    //    BOOST_CHECK_EQUAL(3, LogMock::counter);// <-- this check doesnot work with begin_writeLog(), maybe max_threads is set to 1?
}

BOOST_AUTO_TEST_CASE(testStringOperator)
{
    std::string str;
    str << 6;
    int x = 6;
    BOOST_CHECK_EQUAL("6", str);
    std::string str2 = VAROUT(x);
    BOOST_CHECK_EQUAL("x: 6 ", str2);
    std::map<std::string, double> mymap;
    mymap["keystring"] = 4.5;
    str << mymap;
    std::cout << str << std::endl;
    std::cout << VAROUT(x) << std::endl;

    double d = 5.3;
    BOOST_CHECK_EQUAL(armarx::GetTypeString(d), "double");
    ARMARX_INFO_S << VAROUT(d);
}


class SpamTest : armarx::Logging
{
public:
    void testSpam()
    {
        for (int j = 0; j < 2; ++j)
        {
            for (int i = 0; i < 10; ++i)
            {
                ARMARX_INFO << deactivateSpam(1, armarx::ValueToString(i)) << "Identified " << i;
                ARMARX_INFO << deactivateSpam(1) << "Unidentified spam" << i;
            }
        }
    }
};

BOOST_AUTO_TEST_CASE(testSpamProtection)
{
    SpamTest c;
    c.testSpam();
}
