/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core
* @author     Kai Welke (welke at kit dot edu)
* @author     Jan Issac (jan dot issac at gmx dot de)
* @date       2011
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "ManagedIceObject.h"
#include "ManagedIceObjectDependency.h"
#include "ArmarXObjectScheduler.h"
#include "IceManager.h"
#include "ArmarXManager.h"

#include "CoreObjectFactories.h"

#include "services/profiler/LoggingStrategy.h"
#include "services/profiler/IceLoggingStrategy.h"

#include <ArmarXCore/interface/core/Profiler.h>

#include <Ice/ObjectAdapter.h>


using namespace armarx;

// *******************************************************
// construction
// *******************************************************
ManagedIceObject::ManagedIceObject() :
    name(""),
    objectState(eManagedIceObjectCreated),
    profiler(new Profiler::Profiler())
{
    enableProfilerFunction = &ManagedIceObject::Noop;
}

ManagedIceObject::ManagedIceObject(ManagedIceObject const& other) :
    IceUtil::Shared(),
    name(other.name),
    armarXManager(other.armarXManager),
    iceManager(NULL),
    objectScheduler(NULL),
    objectState(eManagedIceObjectCreated),
    proxy(NULL),
    objectAdapter(NULL),
    connectivity(other.connectivity),
    profiler(other.profiler)
{
    enableProfilerFunction = &ManagedIceObject::Noop;
}


ManagedIceObject::~ManagedIceObject()
{
}



// *******************************************************
// establish dependencies
// *******************************************************

bool  ManagedIceObject::usingProxy(const std::string& name, const std::string& endpoints)
{
    if (name.empty())
    {
        throw LocalException("proxyName must not be empty.");
    }

    boost::mutex::scoped_lock lock(connectivityMutex);

    // check if proxy already exists
    if (connectivity.dependencies.find(name) != connectivity.dependencies.end())
    {
        return false;
    }

    // add dependency
    ManagedIceObjectDependencyPtr proxyDependency = new ProxyDependency(getIceManager(), name);
    connectivity.dependencies.insert(std::make_pair(name, proxyDependency));

    return true;
}

std::vector<std::string> ManagedIceObject::getUnresolvedDependencies() const
{
    ManagedIceObjectConnectivity con = getConnectivity();
    std::vector<std::string> dependencies;

    for (DependencyMap::iterator i = con.dependencies.begin(); i != con.dependencies.end(); i++)
    {
        ManagedIceObjectDependencyBasePtr& dep = i->second;

        if (!dep->getResolved())
        {
            dependencies.push_back(dep->getName());
        }
    }

    return dependencies;
}

std::string ManagedIceObject::GetObjectStateAsString(ManagedIceObjectState state)
{
    switch (state)
    {
        case eManagedIceObjectCreated:
            return "Created";

        case eManagedIceObjectInitializing:
            return "Initializing";

        case eManagedIceObjectInitialized:
            return "Initialized";

        case eManagedIceObjectStarting:
            return "Starting";

        case eManagedIceObjectStarted:
            return "Started";

        case eManagedIceObjectExiting:
            return "Exiting";

        case eManagedIceObjectExited:
            return "Exited";
    }

    return "Unknown";
}


void ManagedIceObject::usingTopic(const std::string& name)
{
    boost::mutex::scoped_lock lock(connectivityMutex);

    // check if topic is already used
    if (std::find(connectivity.usedTopics.begin(), connectivity.usedTopics.end(), name) != connectivity.usedTopics.end())
    {
        return;
    }

    // add dependency
    connectivity.usedTopics.push_back(name);
    ARMARX_VERBOSE << "Using topic with name: '" << name << "'";

    // only subscribe direcly if component has been initialized. Otherwise the dependency resolution will take care
    if (getState() >= eManagedIceObjectStarting)
    {
        iceManager->subscribeTopic(proxy, name);
    }
}


void ManagedIceObject::offeringTopic(const std::string& name)
{
    boost::mutex::scoped_lock lock(connectivityMutex);

    // check if topic is already used
    if (std::find(connectivity.offeredTopics.begin(), connectivity.offeredTopics.end(), name) != connectivity.offeredTopics.end())
    {
        return;
    }

    // add dependency
    connectivity.offeredTopics.push_back(name);
    ARMARX_INFO << "Offering topic with name: '" << name << "'";

    // only retrieve topic direcly if component has been initialized. Otherwise the dependency resolution will take care
    if (getState() >= eManagedIceObjectStarting)
    {
        iceManager->getTopic<IceStorm::TopicPrx>(name);
    }
}




bool ManagedIceObject::removeProxyDependency(const std::string& name)
{
    boost::mutex::scoped_lock lock(connectivityMutex);

    // check if proxy already exists
    DependencyMap::iterator it = connectivity.dependencies.find(name);

    if (it != connectivity.dependencies.end())
    {
        return false;
    }

    ARMARX_VERBOSE << "Removing proxy '" << name << "' from depedency list.";
    connectivity.dependencies.erase(it);
    getObjectScheduler()->wakeupDependencyCheck();
    return true;
}

// *******************************************************
// getters
// *******************************************************
ArmarXManagerPtr ManagedIceObject::getArmarXManager() const
{
    return armarXManager;
}

IceManagerPtr ManagedIceObject::getIceManager() const
{
    return iceManager;
}


Profiler::ProfilerPtr ManagedIceObject::getProfiler() const
{
    return profiler;
}

void ManagedIceObject::enableProfiler(bool enable)
{
    Profiler::LoggingStrategyPtr strategy(new Profiler::LoggingStrategy);
    // set to empty log strategy if enable == false
    enableProfilerFunction = &ManagedIceObject::Noop;

    if (!enable)
    {
        profiler->setLoggingStrategy(strategy);
        return;
    }

    // enable the IceLoggingStrategy if IceManager is available
    // otherwise defer the creation until ManagedIceObject::init() is called
    if (getIceManager())
    {
        // create the IceLoggingStrategy immediately if Ice is available
        //enableProfilerFunction(this);
        ManagedIceObject::EnableProfilerOn(this);
    }
    else
    {
        // if Ice is not yet available, the creation of IceLoggingStrategy is delayed via the enableProfilerFunction
        enableProfilerFunction = &ManagedIceObject::EnableProfilerOn;
    }
}


// *******************************************************
// termination
// *******************************************************
void ManagedIceObject::terminate()
{
    stateCondition.notify_all();
    objectScheduler->terminate();
}

// *******************************************************
// phase handling
// *******************************************************
void ManagedIceObject::init(IceManagerPtr iceManager)
{
    // set state
    setObjectState(eManagedIceObjectInitializing);

    // set ice manager
    this->iceManager = iceManager;

    // evaluate function created by enableProfiling();
    enableProfilerFunction(this);

    // call framwork hook
    onInitComponent();

    // set state
    setObjectState(eManagedIceObjectInitialized);
}

void ManagedIceObject::start(Ice::ObjectPrx& proxy, const Ice::ObjectAdapterPtr& objectAdapter)
{
    // set state
    setObjectState(eManagedIceObjectStarting);

    // set members
    this->proxy = proxy;
    this->objectAdapter = objectAdapter;

    // call framwork hook
    onConnectComponent();

    // set state
    setObjectState(eManagedIceObjectStarted);
}

void ManagedIceObject::disconnect()
{
    // set state

    // call framwork hook
    onDisconnectComponent();

    // set state
    setObjectState(eManagedIceObjectInitialized);
}

void ManagedIceObject::exit()
{
    // set state
    setObjectState(eManagedIceObjectExiting);

    Ice::Identity id;
    id.name = getName();

    // remove self so no new calls are coming in
    try
    {
        if (getObjectAdapter())
        {
            getObjectAdapter()->remove(id);
        }
    }
    catch (Ice::ObjectAdapterDeactivatedException& e)
    {
    }
    catch (Ice::NotRegisteredException& e)
    {
    }

    // call framwork hook
    onExitComponent();

    // set state
    setObjectState(eManagedIceObjectExited);
    armarXManager = NULL;
    iceManager = NULL;
    objectScheduler = NULL;
    objectAdapter = NULL;
    proxy = NULL;
}

void ManagedIceObject::setObjectState(ManagedIceObjectState newState)
{
    ScopedLock lock(objectStateMutex);

    if (newState == objectState)
    {
        return;
    }

    objectState = newState;
    stateCondition.notify_all();
}

void ManagedIceObject::Noop(ManagedIceObject*)
{
    // NOOP
}

void ManagedIceObject::EnableProfilerOn(ManagedIceObject* object)
{
    object->offeringTopic(armarx::Profiler::PROFILER_TOPIC_NAME);
    ProfilerListenerPrx profilerTopic = object->getIceManager()->getTopic<ProfilerListenerPrx>(armarx::Profiler::PROFILER_TOPIC_NAME);
    Profiler::LoggingStrategyPtr strategy(new Profiler::IceBufferedLoggingStrategy(profilerTopic));
    object->profiler->setLoggingStrategy(strategy);
    ARMARX_DEBUG_S << "Profiler enabled";
}

ManagedIceObjectState ManagedIceObject::getState() const
{
    ScopedLock lock(objectStateMutex);
    return objectState;
}

ArmarXObjectSchedulerPtr ManagedIceObject::getObjectScheduler()
{
    return objectScheduler;
}


ManagedIceObjectConnectivity ManagedIceObject::getConnectivity() const
{
    boost::mutex::scoped_lock lock(connectivityMutex);
    return connectivity;
}


void armarx::ManagedIceObject::waitForObjectScheduler()
{
    getObjectScheduler()->waitForDependencies();
}
