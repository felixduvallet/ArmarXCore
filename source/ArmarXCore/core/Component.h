/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core
* @author     Kai Welke (welke at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef _ARMARX_CORE_COMPONENT_H
#define _ARMARX_CORE_COMPONENT_H

// ArmarXCore
#include <ArmarXCore/core/system/ImportExport.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/application/properties/PropertyUser.h>
#include <ArmarXCore/core/ManagedIceObject.h>
//#include <ArmarXCore/core/ArmarXManager.h>


#include <Ice/Initialize.h>

namespace armarx
{
    /**
    \defgroup Component ArmarX Component
    \ingroup DistributedProcessingGrp
    \brief Subclass of ManagedIceObjects which contains additional properties.
    */

    class Component;

    /**
    * Component smart pointer type
    */
    typedef IceInternal::Handle<Component> ComponentPtr;

    /**
    \class ComponentPropertyDefinitions
    \brief Default component property definition container.
    \ingroup Component
    \see properties

    Inherit from this class to extend your property definitions for components!
    */
    class ComponentPropertyDefinitions:
        public PropertyDefinitionContainer
    {
    public:
        ComponentPropertyDefinitions(std::string prefix):
            PropertyDefinitionContainer(prefix)
        {
            defineOptionalProperty<std::string>("ObjectName", "", "Name of IceGrid well-known object");

            defineOptionalProperty<MessageType>("MinimumLoggingLevel", eUNDEFINED, "Local logging level only for this component")
            .setCaseInsensitive(true)
            .map("Debug", eDEBUG)
            .map("Verbose", eVERBOSE)
            .map("Info", eINFO)
            .map("Important", eIMPORTANT)
            .map("Warning", eWARN)
            .map("Error", eERROR)
            .map("Fatal", eFATAL)
            .map("Undefined", eUNDEFINED);
            defineOptionalProperty<bool>("EnableProfiling", false, "enable profiler which is used for logging performance events")
            .setCaseInsensitive(true)
            .map("false", false)
            .map("no", false)
            .map("0", false)
            .map("true", true)
            .map("yes", true)
            .map("1", true);
        }
    };

    /**
    \class Component
    \brief Baseclass for all ArmarX ManagedIceObjects requiring properties.
    \ingroup Component

    Components are the equivalents to ManagedIceObjects with additional properties.
    Usually components are the static elements of an ArmarX distributed application,
    which are configured through properties from a config file on startup.
    */
    class ARMARXCORE_IMPORT_EXPORT Component :
        virtual public ManagedIceObject,
        virtual public PropertyUser
    {
    public:
        /**
         * Factory method for a component. Sets the specified properties.
         *
         * The properties are identified using a config domain, a config name and a property name.
         * The identifier of properties follows the rule:
         *
         * configDomain.configName.propertyName
         *
         * The configName is used as name of the well-known object in Ice.
         * @see ManagedIceObject::ManagedIceObject(std::string name)
         * This name can be changes by setting the property:
         *
         * configDomain.configName.ObjectName=<desiredname>
         *
         * \param configName name of the properties identifier
         * \param properties properties for the object
         * \param configDomain domain of the properties identifier
         */
        template <class T>
        static IceInternal::Handle<T> create(Ice::PropertiesPtr properties = Ice::createProperties(), const std::string& configName = "", const std::string& configDomain = "ArmarX")
        {
            IceInternal::Handle<T> ptr = new T();
            ComponentPtr compPtr = ptr;

            if (configName == "")
            {
                ptr->initializeProperties(compPtr->getDefaultName(), properties, configDomain);
            }
            else
            {
                ptr->initializeProperties(configName, properties, configDomain);
            }

            return IceInternal::Handle<T>::dynamicCast(ptr);
        }

        /**
         * \see PropertyUser::getProperty()
         */
        using PropertyUser::getProperty;

        /**
         * @brief initializes the properties of this component.
         * Must not be called after the component was added to the ArmarXManager.
         * Use with caution!
         * @param configName
         * @param properties
         * @param configDomain
         */
        void initializeProperties(const std::string& configName, Ice::PropertiesPtr properties, const std::string& configDomain);

    protected:
        /**
         * Protected default constructor. Used for virtual inheritance. Use createManagedIceObject() instead.
         */
        Component()
        {
            properties = Ice::createProperties(),
            configName = "";
            configDomain = "ArmarX";
        }

        /**
         * \see PropertyUser::createPropertyDefinitions()
         */
        virtual PropertyDefinitionsPtr createPropertyDefinitions();

        /**
         * Retrieve config domain for this component as set in constructor. The configdomain
         * defines which properties are used from the properties read from commandline or config file.
         * The name of the used properties follows the rule:
         *
         * configDomain.configName.propertyName
         */
        std::string getConfigDomain();

        /**
         * Retrieve config name for this component as set in constructor. The configname
         * defines which properties are used from the properties read from commandline or config file.
         * The name of the properties follows the rule:
         *
         * configDomain.configName.propertyName
         */
        std::string getConfigName();

        /**
         * Retrieve config identifier for this component as set in constructor. The config identifier
         * defines which properties are used from the properties read from commandline or config file.
         * The name of the properties follows the rule:
         *
         * configDomain.configName.propertyName
         *
         * where the configIdentifier corresponds to
         *
         * configDomain.configName
         */
        std::string getConfigIdentifier();

    private:

        // idenfitication of properties
        std::string configDomain;
        std::string configName;
    };
}

#endif
