/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core::application
* @author     Manfred Kroehnert (manfred dot kroehnert at kit dot edu)
* @date       2015
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef _ARMARX_CORE_APPLICATIONNETWORKSTATS_H
#define _ARMARX_CORE_APPLICATIONNETWORKSTATS_H

#include "../services/tasks/PeriodicTask.h"

#include <ArmarXCore/interface/core/Profiler.h>

#include <Ice/Stats.h>

#include <boost/thread/mutex.hpp>

#include <map>

namespace armarx
{
    /**
     * \brief The ApplicationNetworkStats class implements the Ice::Stats interface to meassure network traffic.
     *
     * It accumulates the amount of bytes received and sent over network and periodically publishes them
     * over the IceStorm topic set via ApplicationNetworkStats::setProfilerTopic().
     *
     * The Stats object must be added to the Ice::InitializationData object used during Ice initialization.
     * This is done in armarx::Application::doMain()
     *
     * Detailed instructions can be found here:
     * https://doc.zeroc.com/display/Ice34/Stats+Facility
     */
    class ApplicationNetworkStats :
        virtual public Ice::Stats
    {
    public:
        /**
         * @brief ~ApplicationNetworkStats initializes and starts the periodic reporting task
         */
        ApplicationNetworkStats();

        /**
         * @brief ~ApplicationNetworkStats stops the periodic reporting task
         */
        virtual ~ApplicationNetworkStats();

        virtual void bytesSent(const std::string& protocol, Ice::Int numberBytes);
        virtual void bytesReceived(const std::string& protocol, Ice::Int numberBytes);

        void setProfilerTopic(ProfilerListenerPrx profiler);
        void setApplicationName(const std::string& appName);
    private:
        void reportNetworkTraffic();

        PeriodicTask<ApplicationNetworkStats>::pointer_type reportNetworkTrafficTask;

        struct NetworkTraffic
        {
            Ice::Int inBytes;
            Ice::Int outBytes;
        };

        std::map<std::string, NetworkTraffic> protocolTrafficMap;
        boost::mutex protocolTrafficMapMutex;

        Ice::Int networkBytesIn;
        Ice::Int networkBytesOut;
        std::string applicationName;
        const int REPORT_TIME_MS;

        ProfilerListenerPrx profiler;
    };
    typedef ::IceUtil::Handle<ApplicationNetworkStats> ApplicationNetworkStatsPtr;
}

#endif
