/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Jan Issac (jan dot issac at gmx dot de)
 * @date       2011
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARXCORE_PPROPERTYDEFINITION_H
#define _ARMARXCORE_PPROPERTYDEFINITION_H

#include <boost/unordered/unordered_map.hpp>

#include <string>
#include <functional>
#include <vector>
#include <typeinfo>

#include "PropertyDefinitionInterface.h"
#include "PropertyDefinitionFormatter.h"

#include "ArmarXCore/core/exceptions/Exception.h"
#include "ArmarXCore/core/exceptions/local/InvalidPropertyValueException.h"
#include "ArmarXCore/core/exceptions/local/MissingRequiredPropertyException.h"
#include "ArmarXCore/core/exceptions/local/UnmappedValueException.h"
#include "ArmarXCore/core/exceptions/local/ValueRangeExceededException.h"

#include <Ice/Properties.h>

//#include <boost/regex.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/numeric/conversion/cast.hpp>

// Workaround for QTBUG-22829
#ifndef Q_MOC_RUN
#include <boost/lexical_cast.hpp>
#endif


namespace armarx
{
    /* ====================================================================== */
    /* === Property Definition Declaration ================================== */
    /* ====================================================================== */

    /**
     * @ingroup properties
     *
     * @class PropertyDefinition
     * @brief PropertyDefinition defines a property that will be available
     *        within the PropertyUser
     *
     * PropertyDefinition provides the definition and description of a
     * property that may be definined in a config file or passed via command
     * line as an option. Depending which constructor is used a property
     * is either required or optional. In the latter case a default value has
     * to be specified.
     *
     * The property specific value type (@em PropertyType) can be arbitrary, yet
     * it is devided into these three groups which are handled internally
     * differently:
     *  - String values
     *  - Arithmetic values (integral & floating point types)
     *  - Custom objects (Classes, Structs, Enums)
     *
     * @section fluentinterface Fluent Interface
     *
     * All setters are self referential and therefor can be chained to
     * simplify the implementation and increase the readability.
     *
     * @subsection fluentinterface_example Example
     * @code
     * PropertyDefinition<float> frameRateDef("MyFrameRate", "Capture frame rate");
     *
     * frameRateDef
     *     .setMatchRegex("\\d+(.\\d*)?")
     *     .setMin(0.0f)
     *     .setMax(60.0f);
     * @endcode
     */
    template<typename PropertyType>
    class PropertyDefinition: public PropertyDefinitionBase
    {
    public:
        template <int> struct Qualifier
        {
            Qualifier(int) {}
        };

        typedef boost::shared_ptr<PropertyType> PropertyTypePtr;
        typedef std::pair<std::string, PropertyType> ValueEntry;
        typedef std::map<std::string, ValueEntry> PropertyValuesMap;
        typedef PropertyType(*PropertyFactoryFunction)(std::string);

        /**
         * Constructs a property definition of a required property
         *
         * @param propertyName  Name of the property used in config files
         * @param description   Mandatory property description
         */
        PropertyDefinition(const std::string& propertyName, const std::string& description);

        /**
         * Constructs a property definition of an optional property
         *
         * @param propertyName  Name of the property used in config files
         * @param defaultValue  Property default value of the type
         *                      @em PropertyType
         * @param description   Mandatory property description
         */
        PropertyDefinition(const std::string& propertyName,
                           PropertyType defaultValue,
                           const std::string& description);

        /**
         * Maps a string value onto a value of the specified template type
         *
         * @param valueString   Property string value key
         * @param value         Property value
         *
         * @return self reference
         */
        PropertyDefinition<PropertyType>& map(const std::string& valueString,
                                              PropertyType value);


        /**
         * Sets the factory function that creates the specified template type from the actual string value
         *
         * @param func         the factory function
         *
         * @return self reference
         */
        PropertyDefinition<PropertyType>& setFactory(PropertyFactoryFunction& func);

        /**
         * Sets whether the property value matching is case insensitive.
         *
         * @param caseInsensitive   Case sensitivity state
         *
         * @return self reference
         */
        PropertyDefinition<PropertyType>& setCaseInsensitive(bool caseInsensitive);

        /**
         * Sets whether for string values environment varbiale expanding should be considered.
         *
         * @param expand   Expand entries like '${ENV_VAR}' to the according values of the environment.
         *
         * @return self reference
         */
        PropertyDefinition<PropertyType>& setExpandEnvironmentVariables(bool expand);

        /**
         * Sets whether for string values leading and trailing quotes should be removed.
         *
         * @param removeQuotes The indicator.
         *
         * @return self reference
         */
        PropertyDefinition<PropertyType>& setRemoveQuotes(bool removeQuotes);

        /**
        * Sets the regular expression which the value has to be matched with.
        *
        * @param expr              Value regular expression
        *
        * @return self reference
        */
        PropertyDefinition<PropertyType>& setMatchRegex(const std::string& expr);

        /**
         * Sets the min allowed numeric value
         *
         * @param min   Min. allowed value
         *
         * @return self reference
         */
        PropertyDefinition<PropertyType>& setMin(double min);

        /**
         * Sets the max allowed numeric value
         *
         * @param max   Max. allowed value
         *
         * @return self reference
         */
        PropertyDefinition<PropertyType>& setMax(double max);

        bool isCaseInsensitive() const;
        bool expandEnvironmentVariables() const;
        bool removeQuotes() const;
        bool isRequired() const;
        std::string getMatchRegex() const;
        double getMin() const;
        double getMax() const;
        std::string getPropertyName() const;
        PropertyValuesMap& getValueMap();
        PropertyType getDefaultValue();
        PropertyFactoryFunction getFactory() const;
        std::string getDefaultAsString();

        /**
         * @see PropertyDefinitionBase::toString()
         */
        std::string toString(PropertyDefinitionFormatter& formatter, const std::string& value);

        std::string getDescription() const;

    protected:
        /**
         * Main property map
         */
        PropertyValuesMap propertyValuesMap;

        /**
         * Property name
         */
        std::string propertyName;

        /**
         * Property description
         */
        std::string description;

        /**
         * Fallback/Default property value
         */
        PropertyType defaultValue;


        /**
          * Builder function
          */
        PropertyFactoryFunction factory;

        /**
         * Regular expression to approve a required value pattern
         */
        std::string regex;

        /**
         * Requirement indicator
         */
        bool required;

        /**
         * Case sensitivity indicator
         */
        bool caseInsensitive;

        /**
         * Exand environments variables indicator (standard: true)
         */
        bool expandEnvVars;

        /**
         * Remove leading and trailing quotes indicator (standard: true)
         * First and last character of a string property value are checked for quotes.
         * E.g.
         *     "test" -> test
         *     'test' -> test
         */
        bool stringRemoveQuotes;

        /**
         * Upper bound of numeric values (used for numeric value retrieval
         * without mapping)
         */
        double max;

        /**
         * Lower bound of numeric values (used for numeric value retrieval
         * without mapping)
         */
        double min;

    private:
        template <typename T>
        typename boost::enable_if_c<boost::is_arithmetic<T>::value, std::string>::type
        getDefaultAsStringImpl(Qualifier<0> = 0);

        template <typename T>
        typename boost::enable_if_c<boost::is_same<T, std::string>::value, std::string>::type
        getDefaultAsStringImpl(Qualifier<1> = 0);

        template <typename T>
        typename boost::disable_if_c < boost::is_same<T, std::string>::value || boost::is_arithmetic<T>::value, std::string >::type
        getDefaultAsStringImpl(Qualifier<2> = 0);

        /*
        template <typename T>
        typename boost::enable_if_c<boost::is_floating_point<T>::value, std::string>::type
        getMinImpl(Qualifier<0> = 0) const;

        template <typename T>
        typename boost::enable_if_c<boost::is_integral<T>::value, std::string>::type
        getMinImpl(Qualifier<1> = 0) const;

        template <typename T>
        typename boost::disable_if_c<boost::is_same<T, std::string>::value || boost::is_arithmetic<T>::value, std::string>::type
        getMinImpl(Qualifier<2> = 0);

        template <typename T>
        typename boost::enable_if_c<boost::is_floating_point<T>::value, std::string>::type
        getMaxImpl(Qualifier<0> = 0) const;

        template <typename T>
        typename boost::enable_if_c<boost::is_integral<T>::value, std::string>::type
        getMaxImpl(Qualifier<1> = 0) const;

        template <typename T>
        typename boost::disable_if_c<boost::is_same<T, std::string>::value || boost::is_arithmetic<T>::value, std::string>::type
        getMaxImpl(Qualifier<2> = 0);
        */
    };


    /* ====================================================================== */
    /* === Property Definition Implementation =============================== */
    /* ====================================================================== */

    template<typename PropertyType>
    PropertyDefinition<PropertyType>::PropertyDefinition(
        const std::string& propertyName,
        const std::string& description):
        propertyName(propertyName),
        description(description),
        factory(NULL),
        regex(""),
        required(true),
        caseInsensitive(true),
        expandEnvVars(true),
        stringRemoveQuotes(true),
        max(std::numeric_limits<double>::max()),
        min(-std::numeric_limits<double>::max())
    {
    }


    template<typename PropertyType>
    PropertyDefinition<PropertyType>::PropertyDefinition(
        const std::string& propertyName,
        PropertyType defaultValue,
        const std::string& description):
        propertyName(propertyName),
        description(description),
        defaultValue(defaultValue),
        factory(NULL),
        regex(""),
        required(false),
        caseInsensitive(true),
        expandEnvVars(true),
        stringRemoveQuotes(true),
        max(std::numeric_limits<double>::max()),
        min(-std::numeric_limits<double>::max())
    {
    }


    template <typename PropertyType>
    PropertyDefinition<PropertyType>&
    PropertyDefinition<PropertyType>::map(
        const std::string& valueString,
        PropertyType value)
    {
        if (caseInsensitive)
        {
            std::string lowerCaseValueString =
                boost::algorithm::to_lower_copy(valueString);

            propertyValuesMap[lowerCaseValueString] = ValueEntry(valueString,
                    value);
        }
        else
        {
            propertyValuesMap[valueString] = ValueEntry(valueString, value);
        }

        return *this;
    }


    template <typename PropertyType>
    PropertyDefinition<PropertyType>&
    PropertyDefinition<PropertyType>::setFactory(PropertyFactoryFunction& func)
    {
        this->factory = func;

        return *this;
    }


    template <typename PropertyType>
    PropertyDefinition<PropertyType>&
    PropertyDefinition<PropertyType>::setCaseInsensitive(bool caseInsensitive)
    {
        if (this->caseInsensitive != caseInsensitive)
        {
            // rebuild value map
            PropertyValuesMap newPropertyValuesMap;

            typename PropertyValuesMap::iterator valueIter =
                propertyValuesMap.begin();

            std::string key;

            while (valueIter != propertyValuesMap.end())
            {
                key = valueIter->second.first;

                if (caseInsensitive)
                {
                    key = boost::algorithm::to_lower_copy(key);
                }

                newPropertyValuesMap[key] = valueIter->second;
                valueIter++;
            }

            // set the new map
            propertyValuesMap = newPropertyValuesMap;
            this->caseInsensitive = caseInsensitive;
        }

        return *this;
    }

    template <typename PropertyType>
    PropertyDefinition<PropertyType>&
    PropertyDefinition<PropertyType>::setExpandEnvironmentVariables(bool expand)
    {
        this->expandEnvVars = expand;
        return *this;
    }

    template <typename PropertyType>
    PropertyDefinition<PropertyType>&
    PropertyDefinition<PropertyType>::setRemoveQuotes(bool removeQuotes)
    {
        this->stringRemoveQuotes = removeQuotes;
        return *this;
    }


    template <typename PropertyType>
    PropertyDefinition<PropertyType>&
    PropertyDefinition<PropertyType>::setMatchRegex(
        const std::string& regex)
    {
        this->regex = regex;

        return *this;
    }


    template <typename PropertyType>
    PropertyDefinition<PropertyType>&
    PropertyDefinition<PropertyType>::setMin(double min)
    {
        // positive & negative overflow check
        boost::numeric_cast<PropertyType, double>(min);

        //this->min = boost::numeric_cast<double, PropertyType>(min);
        this->min = min;

        return *this;
    }


    template <typename PropertyType>
    PropertyDefinition<PropertyType>&
    PropertyDefinition<PropertyType>::setMax(double max)
    {
        // positive & negative overflow check
        boost::numeric_cast<PropertyType, double>(max);

        //this->max = boost::numeric_cast<double, PropertyType>(max);
        this->max = max;

        return *this;
    }

    template <typename PropertyType>
    bool
    PropertyDefinition<PropertyType>::isRequired() const
    {
        return required; //defaultValue == 0;
    }


    template <typename PropertyType>
    bool
    PropertyDefinition<PropertyType>::isCaseInsensitive() const
    {
        return caseInsensitive;
    }

    template <typename PropertyType>
    bool
    PropertyDefinition<PropertyType>::expandEnvironmentVariables() const
    {
        return expandEnvVars;
    }

    template <typename PropertyType>
    bool
    PropertyDefinition<PropertyType>::removeQuotes() const
    {
        return stringRemoveQuotes;
    }


    template <typename PropertyType>
    std::string PropertyDefinition<PropertyType>::getMatchRegex() const
    {
        return regex;
    }


    template <typename PropertyType>
    double
    PropertyDefinition<PropertyType>::getMin() const
    {
        return min;
    }


    template <typename PropertyType>
    double
    PropertyDefinition<PropertyType>::getMax() const
    {
        return max;
    }


    template <typename PropertyType>
    std::string
    PropertyDefinition<PropertyType>::getPropertyName() const
    {
        return propertyName;
    }

    template <typename PropertyType>
    std::string
    PropertyDefinition<PropertyType>::getDescription() const
    {
        return description;
    }

    template <typename PropertyType>
    typename PropertyDefinition<PropertyType>::PropertyValuesMap&
    PropertyDefinition<PropertyType>::getValueMap()
    {
        return propertyValuesMap;
    }


    template <typename PropertyType>
    PropertyType
    PropertyDefinition<PropertyType>::getDefaultValue()
    {
        // throw exception if no default exists

        if (isRequired())
        {
            throw armarx::LocalException("Required property '" + getPropertyName() + "': default doesn't exist.");
        }

        return defaultValue;
    }


    template <typename PropertyType>
    typename PropertyDefinition<PropertyType>::PropertyFactoryFunction PropertyDefinition<PropertyType>::getFactory() const
    {
        return factory;
    }

    template <typename PropertyType>
    std::string
    PropertyDefinition<PropertyType>::toString(PropertyDefinitionFormatter& formatter, const std::string& value)
    {
        std::vector<std::string> mapValues;

        typename PropertyValuesMap::iterator it = propertyValuesMap.begin();

        while (it != propertyValuesMap.end())
        {
            mapValues.push_back(it->second.first);

            ++it;
        }

        return formatter.formatDefinition(
                   propertyName,
                   description,
                   (min != -std::numeric_limits<double>::max() ? boost::lexical_cast<std::string>(min) : ""),
                   (max != std::numeric_limits<double>::max() ? boost::lexical_cast<std::string>(max) : ""),
                   getDefaultAsString(),
                   (!isCaseInsensitive() ? "yes" : "no"),
                   (isRequired() ? "yes" : "no"),
                   regex,
                   mapValues,
                   value);
    }


    template <typename PropertyType>
    std::string PropertyDefinition<PropertyType>::getDefaultAsString()
    {
        if (!isRequired())
        {
            return getDefaultAsStringImpl<PropertyType>();
        }

        // no default available
        return std::string();
    }


    template <typename PropertyType>
    template <typename T>
    typename boost::enable_if_c<boost::is_arithmetic<T>::value, std::string>::type
    PropertyDefinition<PropertyType>::getDefaultAsStringImpl(Qualifier<0>)
    {
        return boost::lexical_cast<std::string>(getDefaultValue());
    }


    template <typename PropertyType>
    template <typename T>
    typename boost::enable_if_c<boost::is_same<T, std::string>::value, std::string>::type
    PropertyDefinition<PropertyType>::getDefaultAsStringImpl(Qualifier<1>)
    {
        if (getDefaultValue().empty())
        {
            return "\"\"";
        }

        return getDefaultValue();
    }


    template <typename PropertyType>
    template <typename T>
    typename boost::disable_if_c <
    boost::is_same<T, std::string>::value || boost::is_arithmetic<T>::value, std::string >::type
    PropertyDefinition<PropertyType>::getDefaultAsStringImpl(Qualifier<2>)
    {
        typename PropertyValuesMap::iterator it = propertyValuesMap.begin();

        while (it != propertyValuesMap.end())
        {
            if (it->second.second == getDefaultValue())
            {
                return it->second.first;
            }

            ++it;
        }

        return "Default value not mapped.";
    }
}

#endif
