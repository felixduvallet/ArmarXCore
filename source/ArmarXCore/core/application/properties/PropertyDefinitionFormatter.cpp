/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Jan Issac (jan dot issac at gmx dot de)
 * @date       2012
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "PropertyDefinitionFormatter.h"

#include <boost/algorithm/string.hpp>

using namespace armarx;

std::string PropertyDefinitionFormatter::formatDefinition(std::string name,
        std::string description,
        std::string min,
        std::string max,
        std::string default_,
        std::string casesensitivity,
        std::string requirement,
        std::string regex,
        std::vector<std::string> mapValues,
        std::string value)
{
    std::string output = getFormat();

    boost::replace_first(output, "%name%",          formatName(name));
    boost::replace_first(output, "%description%",   formatDescription(description));
    boost::replace_first(output, "%bounds%",        formatBounds(min, max));
    boost::replace_first(output, "%default%",       formatDefault(getValue(name, default_)));
    boost::replace_first(output, "%casesensitive%", formatCaseSennsitivity(casesensitivity));
    boost::replace_first(output, "%required%",      formatRequirement(requirement));
    boost::replace_first(output, "%regex%",         formatRegex(regex));
    boost::replace_first(output, "%values%",        formatValues(mapValues));

    return output;
}

std::string PropertyDefinitionFormatter::getFormat()
{
    return std::string("%name%:")
           + "\t%description%\n"
           + "\tAttributes:\n"
           + "%default%"
           + "%bounds%"
           + "%casesensitive%"
           + "%required%"
           + "%regex%"
           + "%values%";
}

std::string PropertyDefinitionFormatter::formatName(std::string name)
{
    return name;
}

std::string PropertyDefinitionFormatter::formatDescription(std::string description)
{
    return description;
}

std::string PropertyDefinitionFormatter::formatBounds(std::string min, std::string max)
{
    std::string bounds;

    if (!min.empty() && max.empty())
    {
        bounds = formatEntry(min, "Min: ");
    }
    else if (min.empty() && !max.empty())
    {
        bounds = formatEntry(max, "Max: ");
    }
    else if (!min.empty() && !max.empty())
    {
        bounds = formatEntry("Bounds: [" + min + "; " + max + "]");
    }

    return bounds;
}

std::string PropertyDefinitionFormatter::formatDefault(std::string default_)
{
    return formatEntry(default_, "Default: ");
}

std::string PropertyDefinitionFormatter::formatCaseSennsitivity(std::string caseSensitivity)
{
    return formatEntry(caseSensitivity, "Case sensitivity: ");
}

std::string PropertyDefinitionFormatter::formatRequirement(std::string requirement)
{
    return formatEntry(requirement, "Required: ");
}

std::string PropertyDefinitionFormatter::formatRegex(std::string regex)
{
    return formatEntry(regex, "Format: ");
}

std::string PropertyDefinitionFormatter::formatValues(std::vector<std::string> mapValues)
{
    std::string valueStrings;

    if (mapValues.size() > 0)
    {
        valueStrings += formatEntry("Possible values:");

        std::vector<std::string>::iterator it = mapValues.begin();

        while (it != mapValues.end())
        {
            if (!it->empty())
            {
                valueStrings += formatValue(*it);
            }

            ++it;
        }
    }

    return valueStrings;
}

std::string PropertyDefinitionFormatter::formatValue(std::string value)
{
    return "\t\t - " + value + "\n";
}

std::string PropertyDefinitionFormatter::formatEntry(std::string entry, std::string prefix, std::string suffix)
{
    if (!entry.empty())
    {
        return "\t - " + prefix + entry + suffix + "\n";
    }

    return std::string();
}


