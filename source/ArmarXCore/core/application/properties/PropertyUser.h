/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Jan Issac (jan dot issac at gmx dot de)
 * @date       2012
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARXCORE_PROPERTYUSER_H_
#define _ARMARXCORE_PROPERTYUSER_H_

#include "Property.h"

#include "PropertyDefinitionContainer.h"

#include <Ice/Properties.h>

namespace armarx
{
    class PropertyDefinitionContainer;
    typedef IceUtil::Handle<PropertyDefinitionContainer> PropertyDefinitionsPtr;
    /* ====================================================================== */
    /* === PropertyUser Declaration ========================================= */
    /* ====================================================================== */

    /**
     * @class PropertyUser
     * @brief Abstract PropertyUser class
     *
     * PropertyUser provides the interface to create a PropertyDefinition as
     * well as the access to each property which has been defined.
     */
    class PropertyUser:
        public virtual IceUtil::Shared
    {
    public:

        /**
         * Property creation and retrieval
         *
         * @param name  Requested property name (note: without prefix)
         *
         * @return Property instance
         */
        template <typename PropertyType>
        Property<PropertyType> getProperty(const std::string& name);

        /**
         * Creates the property definition container.
         * @note Implement this factory function to create your own definitions.
         *
         * @return Property definition container pointer
         */
        virtual PropertyDefinitionsPtr createPropertyDefinitions() = 0;

        /* === Implementation details ======================================= */

        /**
         * Returns the component's property definition container
         *
         * @return Property definition container
         */
        PropertyDefinitionsPtr getPropertyDefinitions();

        /**
         * Returns the set of Ice properties.
         *
         * @return Pointer to the Ice::Properties set.
         */
        Ice::PropertiesPtr getIceProperties() const;

        /**
         * Sets the Ice properties.
         *
         * @param properties Ice Properties.
         */
        void setIceProperties(Ice::PropertiesPtr properties);

    protected:
        /**
         * Component property definitions pointer
         */
        PropertyDefinitionsPtr propertyDefinitions;

        /**
         * Component related properties
         */
        Ice::PropertiesPtr properties;
    };


    /* ====================================================================== */
    /* === PropertyUser implementation ====================================== */
    /* ====================================================================== */

    template <typename PropertyType>
    Property<PropertyType> PropertyUser::getProperty(const std::string& name)
    {
        return Property<PropertyType>(
                   getPropertyDefinitions()
                   ->getDefintion<PropertyType>(name),
                   getPropertyDefinitions()
                   ->getPrefix(),
                   properties);
    }

    /**
     * PropertyUser smart pointer type
     */
    typedef IceUtil::Handle<PropertyUser> PropertyUserPtr;

    /**
     * UserProperty list type
     */
    typedef std::list<PropertyUserPtr> PropertyUserList;
}

#endif
