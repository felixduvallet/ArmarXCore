/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Jan Issac (jan dot issac at gmx dot de)
 * @date       2012
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "PropertyDefinitionXmlFormatter.h"

#include <sstream>
#include <iomanip>

#include <boost/algorithm/string.hpp>

using namespace armarx;

std::string PropertyDefinitionXmlFormatter::formatDefinition(
    std::string name,
    std::string description,
    std::string min,
    std::string max,
    std::string default_,
    std::string casesensitivity,
    std::string requirement,
    std::string regex,
    std::vector<std::string> values,
    std::string value)
{
    std::string output = getFormat();

    boost::replace_first(output, "%name%",          formatName(getPrefix() + name));
    boost::replace_first(output, "%description%",   formatDescription(description));
    boost::replace_first(output, "%bounds%",        formatBounds(min, max));
    boost::replace_first(output, "%default%",       formatDefault(default_));
    boost::replace_first(output, "%casesensitive%", formatCaseSensitivity(casesensitivity));
    boost::replace_first(output, "%required%",      formatRequirement(requirement));
    boost::replace_first(output, "%regex%",         formatRegex(regex));
    boost::replace_first(output, "%values%",        formatValues(values));
    boost::replace_first(output, "%value%",        formatValue(value));

    return output + "\n";
}


std::string PropertyDefinitionXmlFormatter::getFormat()
{
    return std::string("<property name=\"%name%\">\n")
           + "  %description%"
           + "  <attributes>\n"
           + "%default%"
           + "%bounds%"
           + "%casesensitive%"
           + "%required%"
           + "%regex%"
           + "%values%"
           + "%value%"
           + "  </attributes>\n"
           + "</property>\n";
}


std::string PropertyDefinitionXmlFormatter::formatName(std::string name)
{
    return name;
}


std::string PropertyDefinitionXmlFormatter::formatDescription(std::string description)
{
    return "<description>" + description + "</description>\n";
}


std::string PropertyDefinitionXmlFormatter::formatBounds(std::string min, std::string max)
{
    std::string bounds;

    bounds  = formatAttribute("min", min);
    bounds += formatAttribute("max", max);

    return bounds;
}


std::string PropertyDefinitionXmlFormatter::formatDefault(std::string default_)
{
    return formatAttribute("Default", default_);
}


std::string PropertyDefinitionXmlFormatter::formatCaseSensitivity(std::string caseSensitivity)
{
    return formatAttribute("CaseSensitivity", caseSensitivity);
}


std::string PropertyDefinitionXmlFormatter::formatRequirement(std::string requirement)
{
    return formatAttribute("Required", requirement);
}


std::string PropertyDefinitionXmlFormatter::formatRegex(std::string regex)
{
    return formatAttribute("Format", regex);
}


std::string PropertyDefinitionXmlFormatter::formatValues(std::vector<std::string> mapValues)
{
    std::string valueStrings;

    if (mapValues.size() > 0)
    {
        valueStrings += "    <values>\n";

        std::vector<std::string>::iterator it = mapValues.begin();

        while (it != mapValues.end())
        {
            if (!it->empty())
            {
                valueStrings += formatValue(*it);
            }

            ++it;
        }

        valueStrings += "    </values>\n";
    }

    return valueStrings;
}


std::string PropertyDefinitionXmlFormatter::formatValue(std::string value)
{
    if (value.empty())
    {
        return "";
    }

    return "      <value>" + value + "</value>\n";
}


std::string PropertyDefinitionXmlFormatter::formatAttribute(std::string name, std::string details)
{
    if (!details.empty())
    {
        return "    <attribute name=\"" + getPrefix() + name + "\">" + details + "</attribute>\n";
    }

    return std::string();
}


std::string PropertyDefinitionXmlFormatter::formatHeader(std::string headerText)
{
    return   "<!-- " +  headerText + " -->\n\n";
}
