/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Jan Issac (jan dot issac at gmx dot de)
 * @date       2012
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "PropertyDefinitionContainerBriefHelpFormatter.h"

#include <sstream>
#include <iomanip>

#include <boost/algorithm/string.hpp>

using namespace armarx;

namespace po = boost::program_options;

PropertyDefinitionContainerBriefHelpFormatter::PropertyDefinitionContainerBriefHelpFormatter(
    PropertyDefinitionFormatter& defFormatter,
    po::options_description* optionsDescription):
    PropertyDefinitionContainerFormatter(defFormatter)
{
    this->optionsDescription = optionsDescription;
}

std::string PropertyDefinitionContainerBriefHelpFormatter::formatPropertyUsers(
    const PropertyUserList& propertyUserList)
{
    std::stringstream ss;

    for (PropertyUserList::const_iterator it = propertyUserList.begin();
         it != propertyUserList.end();
         ++it)
    {
        ss << (*it)->getPropertyDefinitions()->toString(defFormatter);
    }

    ss << *optionsDescription << std::endl;

    return ss.str();
}
