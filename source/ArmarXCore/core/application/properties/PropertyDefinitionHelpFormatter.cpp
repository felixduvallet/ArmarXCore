/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Jan Issac (jan dot issac at gmx dot de)
 * @date       2012
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "PropertyDefinitionHelpFormatter.h"

#include <sstream>
#include <iomanip>

#include <boost/algorithm/string.hpp>

using namespace armarx;

std::string PropertyDefinitionHelpFormatter::formatDefinition(std::string name,
        std::string description,
        std::string min,
        std::string max,
        std::string default_,
        std::string casesensitivity,
        std::string requirement,
        std::string regex,
        std::vector<std::string> values, std::string value)
{
    std::string output = getFormat();

    boost::replace_first(output, "%name%",          formatName(getPrefix() + name));
    boost::replace_first(output, "%description%",   formatDescription(description));
    boost::replace_first(output, "%bounds%",        formatBounds(min, max));
    boost::replace_first(output, "%default%",       formatDefault(default_));
    boost::replace_first(output, "%casesensitive%", formatCaseSensitivity(casesensitivity));
    boost::replace_first(output, "%required%",      formatRequirement(requirement));
    boost::replace_first(output, "%regex%",         formatRegex(regex));
    boost::replace_first(output, "%values%",        formatValues(values));

    return output + "\n";
}

std::string PropertyDefinitionHelpFormatter::getFormat()
{
    return std::string("%name%:")
           + "  %description%\n"
           + "  Attributes:\n"
           + "%default%"
           + "%bounds%"
           + "%casesensitive%"
           + "%required%"
           + "%regex%"
           + "%values%";
}

std::string PropertyDefinitionHelpFormatter::formatName(std::string name)
{
    return name;
}

std::string PropertyDefinitionHelpFormatter::formatDescription(std::string description)
{
    return description;
}

std::string PropertyDefinitionHelpFormatter::formatBounds(std::string min, std::string max)
{
    std::string bounds;

    if (!min.empty() && max.empty())
    {
        bounds = formatAttribute("Min:", min);
    }
    else if (min.empty() && !max.empty())
    {
        bounds = formatAttribute("Max:", max);
    }
    else if (!min.empty() && !max.empty())
    {
        bounds = formatAttribute("Bounds:", "[" + min + "; " + max + "]");
    }

    return bounds;
}

std::string PropertyDefinitionHelpFormatter::formatDefault(std::string default_)
{
    return formatAttribute("Default:", default_);
}

std::string PropertyDefinitionHelpFormatter::formatCaseSensitivity(std::string caseSensitivity)
{
    return formatAttribute("Case sensitivity:", caseSensitivity);
}

std::string PropertyDefinitionHelpFormatter::formatRequirement(std::string requirement)
{
    return formatAttribute("Required:", requirement);
}

std::string PropertyDefinitionHelpFormatter::formatRegex(std::string regex)
{
    return formatAttribute("Format:", regex);
}

std::string PropertyDefinitionHelpFormatter::formatValues(std::vector<std::string> mapValues)
{
    std::string valueStrings;

    if (mapValues.size() > 0)
    {
        valueStrings += "  - Possible values: ";

        std::vector<std::string>::iterator it = mapValues.begin();

        while (it != mapValues.end())
        {
            if (!it->empty())
            {
                valueStrings += formatValue(*it);
            }

            ++it;

            valueStrings += (it != mapValues.end() ? ", " : "\n");
        }
    }

    return valueStrings;
}

std::string PropertyDefinitionHelpFormatter::formatValue(std::string value)
{
    return value;
}

std::string PropertyDefinitionHelpFormatter::formatAttribute(std::string name, std::string details)
{
    if (!details.empty())
    {
        std::stringstream strStream;
        strStream << std::setfill(' ') << std::left << std::setw(20) << name;
        strStream << details;

        return "  - " + strStream.str() + "\n";
    }

    return std::string();
}

std::string PropertyDefinitionHelpFormatter::formatHeader(std::string headerText)
{
    return   "==================================================================\n"
             +  headerText + "\n"
             + "==================================================================\n\n";
}
