/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef _ARMARX_STRINGHELPERS_H
#define _ARMARX_STRINGHELPERS_H

#include <string>
#include <sstream>
#include <vector>
#include <map>
#include <ArmarXCore/core/system/Synchronization.h>
#include <cxxabi.h>
#include <boost/algorithm/string/split.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string/classification.hpp>

namespace armarx
{
    template <typename T>
    std::string ValueToString(const T& value)
    {
        std::stringstream str;
        str << value;
        return str.str();
    }

    template <typename T>
    std::string GetTypeString()
    {
        static Mutex classNameMutex;
        ScopedLock lock(classNameMutex);
        static std::string className;

        if (className.empty())
        {
            // only get the name once for each state class (each template version creates it own version of this function and so it's own static variable)
            T* type;
            char* demangled = NULL;
            int status = -1;
            demangled = abi::__cxa_demangle(typeid(type).name(), NULL, 0, &status);
            className = demangled;

            if (className.size() > 1)
            {
                className = className.substr(0, className.size() - 1);
            }

            free(demangled);
        }

        return className;
    }

    template <typename T>
    std::string GetTypeString(const T& var)
    {
        return GetTypeString<T>();
    }

    inline std::vector<std::string> Split(const std::string& source, const std::string& splitBy)
    {
        std::vector<std::string> result;

        if (source.empty())
        {
            return result;
        }

        boost::split(result, source, boost::is_any_of(splitBy));
        return result;
    }

    template<typename T>
    inline std::vector<T> Split(const std::string& source, const std::string& splitBy)
    {
        if (source.empty())
        {
            return std::vector<T>();
        }

        std::vector<std::string> components;
        boost::split(components, source, boost::is_any_of(splitBy));

        std::vector<T> result(components.size());
        std::transform(components.begin(), components.end(), result.begin(), boost::lexical_cast<T, std::string>);
        return result;
    }
}
namespace std
{


    template <typename T>
    std::string& operator <<(std::string& str, const T& value)
    {
        str += armarx::ValueToString(value);
        return str;
    }

    template<typename T>
    ostream&
    operator<<(ostream& str, const std::vector<T>& vector)
    {
        str << "Vector(" << vector.size() << "):\n";

        for (unsigned int i = 0; i < vector.size(); ++i)
        {
            str << "\t(" << i << "):" << vector.at(i) << "\n";
        }

        return str;
    }

    template<typename T1, typename T2>
    ostream&
    operator<<(ostream& str, const std::map<T1, T2>& map)
    {
        typedef std::map<T1, T2> MapType;

        str << "Map<" << armarx::GetTypeString<T1>() << ", " <<  armarx::GetTypeString<T2>() << ">(" << map.size() << "):\n";

        for (typename MapType::const_iterator i = map.begin(); i != map.end(); ++i)
        {
            str << "\t" << i->first << ": " << i->second << "\n";
        }

        return str;
    }
}
#define VAROUT(x) std::string(std::string(#x) +": " + armarx::ValueToString(x)) + " "

#endif
