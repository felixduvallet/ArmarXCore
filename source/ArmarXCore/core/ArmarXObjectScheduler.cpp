/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author    Kai Welke (kai dot welke at kit dot edu)
 * @date      2012
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ArmarXObjectScheduler.h"
#include <ArmarXCore/core/ManagedIceObject.h>
#include <ArmarXCore/core/ManagedIceObjectDependency.h>
#include <ArmarXCore/core/ArmarXManager.h>
#include "IceGridAdmin.h"
#include <ArmarXCore/core/IceManager.h>
#include <ArmarXCore/core/exceptions/Exception.h>
#include <ArmarXCore/core/services/tasks/RunningTask.h>
#include <ArmarXCore/core/system/Synchronization.h>
#include <ArmarXCore/core/services/tasks/PeriodicTask.h>

#include <boost/thread/thread_time.hpp>

#include <IceUtil/Time.h>

#define WAITMESSAGEINTERVAL (long)5000


using namespace armarx;

// *******************************************************
// construction
// *******************************************************
ArmarXObjectScheduler::ArmarXObjectScheduler(ArmarXManagerPtr armarXManager, IceManagerPtr iceManager, ManagedIceObjectPtr object, Ice::ObjectAdapterPtr objectAdapterToAddTo)
    : armarXManager(armarXManager),
      iceManager(iceManager),
      managedObject(object),
      terminateRequested(false),
      objectedInitialized(false),
      tryReconnect(true),
      objectAdapterToAddTo(objectAdapterToAddTo)
{
    setTag("ObjectScheduler");
    // set object scheduler and armarxManager in managedIceObject
    object->objectScheduler = this;
    object->armarXManager = armarXManager;


    // start management thread
    scheduleObjectTask = new RunningTask<ArmarXObjectScheduler>(this, &ArmarXObjectScheduler::scheduleObject, managedObject->getName() + "ArmarXObjectScheduler");
    scheduleObjectTask->start();

}

armarx::ArmarXObjectScheduler::~ArmarXObjectScheduler()
{
    wakeupDependencyCheck();

    if (scheduleObjectTask)
    {
        scheduleObjectTask->stop(true);
    }

    //    ARMARX_VERBOSE << "~ArmarXObjectScheduler()" ;
}



// *******************************************************
// termination handling
// *******************************************************
void ArmarXObjectScheduler::terminate()
{
    terminateRequested = true;

    if (managedObject)
    {
        managedObject->stateCondition.notify_all();
    }

    wakeupDependencyCheck();
    {
        ScopedLock lock(interruptMutex);
        interruptConditionVariable = true;
    }
    interruptCondition.notify_all();

    if (scheduleObjectTask)
    {
        scheduleObjectTask->stop(false);
    }
}

void ArmarXObjectScheduler::waitForTermination()
{
    scheduleObjectTask->waitForFinished();
}

void ArmarXObjectScheduler::waitForInterrupt()
{
    ScopedLock lock(interruptMutex);

    if (terminateRequested || !scheduleObjectTask->isRunning())
    {
        return;
    }

    interruptConditionVariable = false;

    while (!interruptConditionVariable)
    {
        interruptCondition.wait(lock);
    }
}


bool ArmarXObjectScheduler::waitForObjectState(ManagedIceObjectState stateToWaitFor, const long timeoutMs)
{
    if (!managedObject)
    {
        return false;
    }

    if (timeoutMs == -1)
    {
        while (!terminateRequested)
        {
            ScopedLock lock(managedObject->objectStateMutex);

            if (managedObject->objectState != stateToWaitFor)
            {
                managedObject->stateCondition.timed_wait(lock, boost::posix_time::milliseconds(WAITMESSAGEINTERVAL));
            }
            else
            {
                return true;
            }

            ARMARX_VERBOSE << deactivateSpam(4) << "Waiting for '" << managedObject->getName()  << "' to reach state " << ManagedIceObject::GetObjectStateAsString(stateToWaitFor);
        }
    }
    else
    {
        IceUtil::Time startTime = IceUtil::Time::now();
        IceUtil::Time waitTime = startTime;
        long waitTimeLeft = timeoutMs;

        while (waitTimeLeft > 0 && !terminateRequested)
        {
            waitTime = IceUtil::Time::now() - startTime;
            waitTimeLeft = timeoutMs - waitTime.toMilliSeconds();

            if (waitTime.toMilliSeconds() > 2000)
            {
                ARMARX_VERBOSE << "Waiting for " << waitTimeLeft << "ms for '" << managedObject->getName()  << "' to reach state " << ManagedIceObject::GetObjectStateAsString(stateToWaitFor);
            }

            ScopedLock lock(managedObject->objectStateMutex);

            if (managedObject->objectState != stateToWaitFor)
            {
                managedObject->stateCondition.timed_wait(lock, boost::posix_time::milliseconds(std::min(WAITMESSAGEINTERVAL, waitTimeLeft)));
            }
            else
            {
                return true;
            }
        }
    }

    return false;
}

bool ArmarXObjectScheduler::waitForObjectStateMinimum(ManagedIceObjectState minimumStateToWaitFor, const long timeoutMs)
{
    if (!managedObject)
    {
        return false;
    }

    if (timeoutMs == -1)
    {

        while (!terminateRequested)
        {
            ScopedLock lock(managedObject->objectStateMutex);

            if (managedObject->objectState < minimumStateToWaitFor)
            {
                if (!managedObject->stateCondition.timed_wait(lock, boost::posix_time::milliseconds(WAITMESSAGEINTERVAL)))
                {
                    ARMARX_IMPORTANT << deactivateSpam(4) << "Waiting for '" << managedObject->getName()  << "' to reach minimum state " << ManagedIceObject::GetObjectStateAsString(minimumStateToWaitFor);
                }
            }
            else
            {
                return true;
            }
        }
    }
    else
    {

        IceUtil::Time startTime = IceUtil::Time::now();
        IceUtil::Time waitTime = startTime;
        long waitTimeLeft = timeoutMs;

        while (waitTimeLeft > 0 && !terminateRequested)
        {
            waitTime = IceUtil::Time::now() - startTime;
            waitTimeLeft = timeoutMs - waitTime.toMilliSeconds();

            if (waitTime.toMilliSeconds() > 2000)
            {
                ARMARX_IMPORTANT << "Waiting for " << waitTimeLeft << "ms for '" << managedObject->getName()  << "' to reach minimum state " << ManagedIceObject::GetObjectStateAsString(minimumStateToWaitFor);
            }

            ScopedLock lock(managedObject->objectStateMutex);

            if (managedObject->objectState < minimumStateToWaitFor)
            {
                managedObject->stateCondition.timed_wait(lock, boost::posix_time::milliseconds(std::min(WAITMESSAGEINTERVAL, waitTimeLeft)));
            }
            else
            {
                return true;
            }
        }
    }

    return false;
}

bool ArmarXObjectScheduler::isTerminated() const
{
    return scheduleObjectTask->isFinished();
}

bool ArmarXObjectScheduler::isTerminationRequested() const
{
    return terminateRequested;
}

ManagedIceObjectPtr ArmarXObjectScheduler::getObject() const
{
    return managedObject;
}



// *******************************************************
// dependency resolution
// *******************************************************
void ArmarXObjectScheduler::waitForDependencies(int timeoutMs)
{
    IceUtil::Time startTime = IceUtil::Time::now();

    bool dependenciesResolved = false;

    while (!dependenciesResolved && !terminateRequested)
    {
        dependenciesResolved = true;
        bool stateChanged = false;
        std::string unresolvedNames;

        // retrieve dependencies
        DependencyMap dependencies = managedObject->getConnectivity().dependencies;
        DependencyMap::iterator iter = dependencies.begin();

        while (iter != dependencies.end())
        {
            ManagedIceObjectDependencyPtr dependency = ManagedIceObjectDependencyPtr::dynamicCast(iter->second);

            // check dependency
            dependency->check();

            // check whether dependency has been resolves
            if (!dependency->getResolved())
            {
                dependenciesResolved = false;
                unresolvedNames += "\t" + dependency->getName() + "\n";
            }

            // check if dependecy state has changed
            if (dependency->getStateChanged())
            {
                stateChanged = true;
            }

            iter++;
        }

        // output list of objects we still need
        if (stateChanged && (unresolvedNames.length() > 0))
        {
            ARMARX_INFO << "ManagedIceObject '" << managedObject->getName() << "' still waiting for: \n " << unresolvedNames;
        }

        if (timeoutMs != -1 && (IceUtil::Time::now() - startTime).toMilliSeconds()  >= timeoutMs)
        {
            throw LocalException("Could not resolve dependencies in ") << timeoutMs << " ms";
        }

        if (!dependenciesResolved) // only wait when dependencies are not resolved yet
        {
            ScopedLock lock(dependencyWaitMutex);
            dependencyWaitConditionVariable = false;
            bool timeout = false;

            while (! dependencyWaitConditionVariable && !timeout)
            {
                timeout = !dependencyWaitCondition.timed_wait(lock, boost::posix_time::milliseconds(1000));
            }
        }


    }

    if (!terminateRequested)
    {
        ARMARX_VERBOSE << "All "
                       << managedObject->getName()
                       << " dependencies resolved";
    }
}

void ArmarXObjectScheduler::wakeupDependencyCheck()
{
    //    ARMARX_DEBUG << managedObject->getName() << " scheduler was woken up";
    {
        ScopedLock lock(dependencyWaitMutex);
        dependencyWaitConditionVariable = true;
    }
    dependencyWaitCondition.notify_all();
}

bool ArmarXObjectScheduler::checkDependenciesStatus() const
{
    bool dependencyLost = false;
    // retrieve dependencies
    DependencyMap dependencies = managedObject->getConnectivity().dependencies;
    DependencyMap::iterator iter = dependencies.begin();

    while (iter != dependencies.end())
    {
        ManagedIceObjectDependencyPtr dependency = ManagedIceObjectDependencyPtr::dynamicCast(iter->second);

        // check dependency
        dependency->check();

        // check whether dependency has been resolves
        if (!dependency->getResolved())
        {
            dependencyLost = true;
        }

        iter++;
    }

    return !dependencyLost;

}




bool ArmarXObjectScheduler::dependsOn(const std::string& objectName)
{
    DependencyMap dependencies = managedObject->getConnectivity().dependencies;
    DependencyMap::iterator iter = dependencies.begin();

    while (iter != dependencies.end())
    {
        ManagedIceObjectDependencyPtr dependency = ManagedIceObjectDependencyPtr::dynamicCast(iter->second);

        if (dependency->getName() == objectName)
        {
            return true;
        }

        iter++;
    }

    return false;
}

void ArmarXObjectScheduler::disconnected(bool reconnect)
{
    tryReconnect = reconnect;
    {
        ScopedLock lock(interruptMutex);
        interruptConditionVariable = true;
    }

    interruptCondition.notify_all();
}

// *******************************************************
// main scheduling thread
// *******************************************************
void ArmarXObjectScheduler::scheduleObject()
{
    // first let Managed ice objects initialize
    if (!objectedInitialized)
    {
        initObject();
    }

    while (!terminateRequested && !scheduleObjectTask->isStopped())
    {
        // try to resolve dependencies
        waitForDependencies();


        // register component with ice
        if (!terminateRequested)
        {
            startObject();
        }

        // wait for disconnect or shutdown
        waitForInterrupt();

        //        checkDependencyStatusTask->stop();

        disconnectObject();

        if (!tryReconnect)
        {
            break;
        }
    }

    scheduleObjectTask->waitForStop();

    // exit managed object
    if (terminateRequested)
    {
        exitObject();
    }
}

// *******************************************************
// ManagedIceObject phases
// *******************************************************
void ArmarXObjectScheduler::initObject()
{
    try
    {
        objectedInitialized = true;
        managedObject->init(iceManager);
    }
    catch (...) // dispatch and handle exception
    {
        handleExceptions();
        terminate();
    }
}

void ArmarXObjectScheduler::startObject()
{
    // register to iceManager
    ObjectHandles objectHandles = iceManager->registerObject(
                                      managedObject, managedObject->getName(),
                                      objectAdapterToAddTo);

    // call hook
    try
    {
        managedObject->start(objectHandles.first, objectAdapterToAddTo ? objectAdapterToAddTo : objectHandles.second);
    }
    catch (...) // dispatch and handle exception
    {
        handleExceptions();
    }


    // offer topics
    StringSequence offeredTopics = managedObject->getConnectivity().offeredTopics;
    StringSequence::iterator iterOT = offeredTopics.begin();

    while (iterOT != offeredTopics.end())
    {
        iceManager->getTopic<IceStorm::TopicPrx>(*iterOT);
        iterOT++;
    }

    // subscribe to topics
    StringSequence usedTopics = managedObject->getConnectivity().usedTopics;
    StringSequence::iterator iterUT = usedTopics.begin();

    while (iterUT != usedTopics.end())
    {
        iceManager->subscribeTopic(objectHandles.first, *iterUT);
        iterUT++;
    }

    // retrieve (precache) proxies
    DependencyMap dependencies = managedObject->getConnectivity().dependencies;
    // TODO: precaching cannot work with current iceManager since it only provides
    // a template getProxy which uses the typeid for caching


    // register to admin
    // from now on object is pingable throuhg registry and can be found by
    // waiting ManagedIceObjects
    IceGrid::AdminPrx admin = iceManager->getIceGridSession()->getAdmin();

    try
    {
        admin->addObject(objectHandles.first);
    }
    catch (const IceGrid::ObjectExistsException& e)
    {
        admin->updateObject(objectHandles.first);
    }
    catch (const IceGrid::DeploymentException& e)
    {
        ARMARX_ERROR << "*** IceGrid::Admin >> adding "
                     << managedObject->getName()
                     << " raised a DeploymentException("
                     <<  e.reason
                     << ")"
                     << flush;
    }

    ARMARX_VERBOSE << "Object '" << managedObject->getName() << "'  started";
}

void ArmarXObjectScheduler::disconnectObject()
{
    ARMARX_INFO << "disconnecting object " << managedObject->getName();

    try
    {
        managedObject->disconnect();
    }

    catch (...) // dispatch and handle exception
    {
        handleExceptions();
    }

    try
    {
        if (iceManager && managedObject)
        {
            iceManager->removeObject(managedObject->getName());
        }

        //        IceGrid::AdminPrx admin = iceManager->getIceGridSession()->getAdmin();
        //        if(getObject()->getObjectAdapter())
        //            getObject()->getObjectAdapter()->deactivate();
        //        if(managedObject->getProxy())
        //            admin->removeObject(managedObject->getProxy()->ice_getIdentity());
    }
    catch (IceGrid::ObjectNotRegisteredException& notRegisteredException)
    {
        //        // removing an unregistered object
        //         //!!!
        //        ARMARX_WARNING << "removing "
        //                  << getObject()->getName()
        //                  << " object failed due to ObjectNotRegisteredException"
        //                  << flush;

    }
}


void ArmarXObjectScheduler::exitObject()
{
    //    ARMARX_INFO << "Exiting object " << managedObject->getName();
    try
    {
        managedObject->exit();
    }
    catch (...) // dispatch and handle exception
    {
        handleExceptions();
    }

    iceManager = NULL;
    armarXManager = NULL;
}
