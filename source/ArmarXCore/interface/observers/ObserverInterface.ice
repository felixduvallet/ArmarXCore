/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Kai Welke <welke@kit.edu>
* @copyright  2011 Humanoids Group, HIS, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef _ARMARX_CORE_OBSERVER_SLICE_
#define _ARMARX_CORE_OBSERVER_SLICE_

#include <ArmarXCore/interface/observers/VariantBase.ice>
#include <ArmarXCore/interface/observers/Event.ice>
#include <ArmarXCore/interface/observers/ConditionCheckBase.ice>
#include <ArmarXCore/interface/observers/ChannelRefBase.ice>

module armarx
{

    exception InvalidChannelException extends UserException
    {

    };

    exception InvalidDatafieldException extends UserException
    {

    };

    interface ObserverInterface;

    class ChannelRefBase extends VariantDataClass
    {
        bool initialized;

        string observerName;
        string channelName;
        StringSequence datafieldNames;

        ObserverInterface*	observerProxy;
    };



    ["cpp:virtual"]
    class DatafieldRefBase extends VariantDataClass
    {
        string datafieldName;
        ChannelRefBase channelRef;
    };


    dictionary<long, VariantBase> TimeVariantBaseMap;


    class DatafieldFilterBase
    {
        TimeVariantBaseMap dataHistory;
        int windowFilterSize = 11;
        VariantBase filteredValue;
        void update(long timestamp, VariantBase value);
        ["cpp:const"]
        VariantBase getValue();
        ["cpp:const"]
        VariantBase calculate();
        ["cpp:const"]
        ParameterTypeList getSupportedTypes();
        ["cpp:const"]
        bool checkTypeSupport(int variantType);


    };

    struct DataFieldRegistryEntry
    {
        DatafieldRefBase identifier;
        string description;
        VariantBase value;
    };
    
    dictionary<string, DataFieldRegistryEntry> DataFieldRegistry;
    
    struct ChannelRegistryEntry
    {
        string name;
        string description;
        DataFieldRegistry dataFields;
        ConditionCheckRegistry conditionChecks;
        bool initialized;
    };
    
    dictionary<string, ChannelRegistryEntry> ChannelRegistry;
    
    sequence<DataFieldIdentifierBase> DataFieldIdentifierBaseList;

    interface ObserverInterface
    {
        CheckIdentifier installCheck(CheckConfiguration config)  throws InvalidConditionException;
        void removeCheck(CheckIdentifier id);
        
        VariantBase getDataField(DataFieldIdentifierBase identifier);
        VariantBaseList getDataFields(DataFieldIdentifierBaseList identifiers);
        
        ChannelRegistry getAvailableChannels(bool includeMetaChannels);
        StringConditionCheckMap getAvailableChecks();

        ["cpp:const"]
        bool existsChannel(string channelName);
        ["cpp:const"]
        bool existsDataField(string channelName, string datafieldName);

        DatafieldRefBase createFilteredDatafield(DatafieldFilterBase filter, DatafieldRefBase datafielRef);
        void removeFilteredDatafield(DatafieldRefBase datafielRef);
    };

    interface DebugObserverInterface extends ObserverInterface
    {
        void setDebugDatafield(string channelName, string datafieldName, VariantBase value);
        void setDebugChannel(string channelName, StringVariantBaseMap valueMap);
        void removeDebugChannel(string channelname);
        void removeDebugDatafield(string channelName, string datafieldName);
        void removeAllChannels();
    };
};

#endif 
