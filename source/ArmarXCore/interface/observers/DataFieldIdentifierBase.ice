/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Kai Welke <welke@kit.edu>
* @copyright  2011 Humanoids Group, HIS, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef _ARMARX_OBSERVERS_DATAFIELDIDENTIFIER_BASE_SLICE_
#define _ARMARX_OBSERVERS_DATAFIELDIDENTIFIER_BASE_SLICE_

module armarx
{
    /**
     * Interfaces for conditions
     */
    class DataFieldIdentifierBase
    {
        string observerName;
        string channelName;
        string datafieldName;
    };    
};

#endif
