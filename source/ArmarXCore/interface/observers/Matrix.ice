/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Peter Kaiser <peter dot kaiser at kit dot edu>
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef _ARMARX_CORE_MATRIX_SLICE_
#define _ARMARX_CORE_MATRIX_SLICE_

#include <ArmarXCore/interface/observers/VariantBase.ice>

module armarx
{
    class MatrixFloatBase extends VariantDataClass
    {
        int rows;
        int cols;
        
        FloatSequence data;
    };
    
    class MatrixDoubleBase extends VariantDataClass
    {
        int rows;
        int cols;
        
        DoubleSequence data;
    };
};


#endif 
