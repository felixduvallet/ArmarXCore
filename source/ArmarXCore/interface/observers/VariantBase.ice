/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Kai Welke <welke@kit.edu>
* @copyright  2011 Humanoids Group, HIS, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef _ARMARX_OBSERVERS_VARIANT_SLICE_
#define _ARMARX_OBSERVERS_VARIANT_SLICE_

#include <ArmarXCore/interface/core/BasicTypes.ice>
#include <ArmarXCore/interface/core/UserException.ice>
#include <ArmarXCore/interface/observers/Serialization.ice>
#include <ArmarXCore/interface/observers/VariantContainers.ice>

module armarx
{

  class VariantData
  {
  };


  
  ["cpp:virtual"]
  class VariantBase//extends VariantContainerBase
  {
    void setType(int type);
    void setInt(int i) throws InvalidTypeException;
    void setFloat(float f) throws InvalidTypeException;
    void setDouble(double f) throws InvalidTypeException;
    void setString(string s) throws InvalidTypeException;
    void setBool(bool b) throws InvalidTypeException;
    
    ["cpp:const"]    
    idempotent int getInt() throws InvalidTypeException;  
    ["cpp:const"]
    idempotent float getFloat() throws InvalidTypeException;
    ["cpp:const"]
    idempotent double getDouble() throws InvalidTypeException;
    ["cpp:const"]
    idempotent string getString() throws InvalidTypeException;
    ["cpp:const"]    
    idempotent bool getBool() throws InvalidTypeException;
    
    ["cpp:const"]
    bool validate();

    ["cpp:const"]
    idempotent int getType();
    ["cpp:const"]
    idempotent string getTypeName();
    ["cpp:const"]    
	idempotent bool getInitialized();

    VariantData data;
    ["protected"]
    int typeId = -1;
  };
  
  dictionary<string, VariantBase> StringVariantBaseMap;  
  sequence<VariantBase> VariantBaseList;
  
  class InvalidVariantData extends VariantData
  {
  };

  class IntVariantData extends VariantData
  {
    int n = 0;
  };

  const int zeroInt = 0; // needed to init float values due to bug in SLICE for initialization of floats
  class FloatVariantData extends VariantData
  {
    float f = zeroInt;
  };

  class DoubleVariantData extends VariantData
  {
    double d = zeroInt;
  };

  class StringVariantData extends VariantData
  {
  	string s;
  };

  class BoolVariantData extends VariantData
  {
    bool b = false;
  };

  class VariantDataClass extends VariantData implements Serializable
  {
    ["cpp:const"]
   	VariantDataClass clone();
   	["cpp:const"]
   	string output();
   	["cpp:const"]
   	int getType();
    /**
     * @brief Function to read a paramater from a XML-string
     * @param xmlData string with XML--Data, <b>not</b>  a path
     * @return ErrorCode, 1 on success
     */
    int readFromXML(string xmlData);

    /**
     * @brief writeAsXML returns the xml-representation of this Variant in
     * a string.
     *
     */
    string writeAsXML();

    /**
     * @brief Function to validate the sanity of the data.
     *
     * If your variant does not this, just return true;
     * @return true, if valid.
     */
    bool validate();

  };
  

  class SingleVariantBase extends VariantContainerBase
  {
      ["cpp:const"]
      VariantBase getElementBase();
      ["protected"]
      VariantBase element;
  };

};

#endif
