/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Kai Welke <welke@kit.edu>
* @copyright  2011 Humanoids Group, HIS, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef _ARMARX_CORE_CONDITIONHANDLER_SLICE_
#define _ARMARX_CORE_CONDITIONHANDLER_SLICE_

#include <ArmarXCore/interface/observers/VariantBase.ice>
#include <ArmarXCore/interface/observers/Event.ice>
#include <ArmarXCore/interface/observers/TermImplBase.ice>
#include <ArmarXCore/interface/observers/ConditionCheckBase.ice>
#include <ArmarXCore/interface/observers/ObserverInterface.ice>

module armarx
{  

    dictionary<int, ConditionRootBase> ConditionRegistry;
    
    interface ConditionHandlerInterface 
    {
        // condition installation and removal
        ConditionIdentifier installCondition(EventListenerInterface* listener, TermImplBase term, EventBase e)  throws InvalidConditionException;
        ConditionIdentifier installConditionWithDescription(EventListenerInterface* listener, TermImplBase term, EventBase e, string desc)  throws InvalidConditionException;
        
        void removeCondition(ConditionIdentifier id);
        
        StringList getObserverNames();
        ConditionRegistry getActiveConditions();
        ConditionRegistry getPastConditions();
        ConditionRootBase getCondition(int id);
    };
};

#endif 
