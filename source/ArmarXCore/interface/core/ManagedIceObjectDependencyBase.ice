#ifndef ARMARX_MANAGEDICEOBJECTDEPENDENCYBASE_SLICE
#define ARMARX_MANAGEDICEOBJECTDEPENDENCYBASE_SLICE

module armarx
{
    class ManagedIceObjectDependencyBase
    {
        string getName();
        string getType();
    
        bool getResolved();

        string name;
        string type;
        bool resolved;
    };

    // todo: these should be proxies not pointers
    dictionary<string, ManagedIceObjectDependencyBase> DependencyMap;
};

#endif
