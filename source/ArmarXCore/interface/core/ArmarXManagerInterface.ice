#ifndef ARMARX_ARMARXMANAGER_SLICE
#define ARMARX_ARMARXMANAGER_SLICE

#include <ArmarXCore/interface/core/ManagedIceObjectDefinitions.ice>
#include <ArmarXCore/interface/core/ManagedIceObjectDependencyBase.ice>

module armarx
{
    /**
     * The ArmarXManagerInterface provides means to query the state, connectivity
     * and properties of remote Ice objects.
     */
    interface ArmarXManagerInterface
    {
        ManagedIceObjectState getObjectState(string objectName);
        ManagedIceObjectConnectivity getObjectConnectivity(string objectName);
        /**
         * getObjectProperties returns a map of propertynames and values
         */
        StringStringDictionary getObjectProperties(string objectName);

        /**
         * returns a list of object names managed by the current ArmarXManager.
         */
        StringSequence getManagedObjectNames();
    };

};

#endif
