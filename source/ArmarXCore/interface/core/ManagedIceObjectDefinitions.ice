#ifndef ARMARX_MANAGEDICEOBJECT_SLICE
#define ARMARX_MANAGEDICEOBJECT_SLICE

#include <ArmarXCore/interface/core/ManagedIceObjectDependencyBase.ice>
#include <ArmarXCore/interface/core/BasicTypes.ice>

module armarx
{
    enum ManagedIceObjectState
    {
        eManagedIceObjectCreated,			// component has been constructed
        eManagedIceObjectInitializing,		// initialization started
        eManagedIceObjectInitialized,		// initialization done and onInitComponent call finished
        eManagedIceObjectStarting,		// dependencies resolved calling onStartComponent
        eManagedIceObjectStarted,		// call to onStartComponent done
        eManagedIceObjectExiting,		// component interrupted (interrupt())
        eManagedIceObjectExited
    };

    struct ManagedIceObjectConnectivity
    {
        DependencyMap dependencies;
        StringSequence usedTopics;
        StringSequence offeredTopics;
    };
};

#endif
