/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Jan Issac <jan dot issac at gmx dot de>
* @copyright  2010 Humanoids Group, HIS, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef _ARMARX_CORE_USEREXCEPTION_SLICE_
#define _ARMARX_CORE_USEREXCEPTION_SLICE_

module armarx
{
    /*!
     * \brief Base class for all ice exceptions in armarx.
     */
	exception UserException 
	{
		string reason;
	};

    /*!
     * \brief Exception to indicate missing functionality.
     */
    exception NotImplementedYetException extends UserException
    {
    };

    /*!
     * \brief Exception to indicate an attempt to access
     * elements out of a defined range.
     */
    exception IndexOutOfBoundsException extends UserException
    {
    };

    /*!
     * \brief Exception to indicate a specific type is invalid in
     * a context.
     */
    exception InvalidTypeException extends UserException
    {
    };

    /*!
     * \brief Exception to indicate an argument value has not been
     * accepted.
     */
    exception InvalidArgumentException extends UserException
    {
    };

    /*!
     * \brief Exception to indicate a missing initialization.
     */
    exception NotInitializedException extends UserException
    {
        string fieldName;
    };
};

#endif
